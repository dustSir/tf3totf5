/**
	* 제    목  : RPP_RScheduleChoGwaSuRyeonDeungRok.js
	* 설    명  : 전공의 초과수련 등록
	* 설 계 자 : 김세진
	* 작 성 자 : 김세진
	* 작 성 일 : 2018.07.18
	* 수정이력 :
		- 2018.07.18 / 최초작성 / 김세진
	* 기    타 :
*/
var g_grid1; //grid1에 대한 포인터
var g_gridId = "grid1";
var g_nodeSet = "/root/MainData/main/ovtrh";
var g_con2nodeSet = "/root/MainData/condition2";

var g_today;
var deptcd = "";
var deptnm = "";
var emplno = "";
var empknm = "";

var g_submitInfo1 = "reqGetRScheduleChoGwaSuRyeonDeungRok";
var g_submitInfo2 = "reqSetRScheduleChoGwaSuRyeonDeungRok";

var g_msgNode = "/root/HideData/resultMsg";

var m_File = "/erp/insaweb/reb/RPP_RScheduleChoGwaSuRyeonDeungRok_Prn.reb";
var m_Path = "/root";

TFRexpert(m_File, m_Path, "local");

//=============================================================================================
// 화면 Open 시 초기화 작업
//=============================================================================================
function fOpen() {
	ERP_FOpen(); //기초코드 Load, 서버날짜 조회
    g_today = model.getValue("/root/InitData/today");
    
	//그리드 속성 초기화
	g_grid1 = model.vsGrid(g_gridId);
	g_grid1.WordWrap = true; //Data 길이에 따라 그리드 Size 변경되지 않도록..
	
	fReset();
	
	fClickQueryBtn();
	
	model.refresh();
}

//=============================================================================================
//출력물 초기화 작업
//=============================================================================================
function fLoad(){
	f_CreateReport("TF3");
	
	var m_File = "/erp/insaweb/reb/RPP_RScheduleChoGwaSuRyeonDeungRok_Prn.reb";
	var m_Path = "/root";
	
	TFRexpert(m_File, m_Path, "local");
}

//=============================================================================================
// 화면 Clear (/root/MainData)
//=============================================================================================
function fReset() {
	model.resetInstanceNode("/root/MainData");
	fResetForMain(); //그리드 빈 Row 삭제
	model.refresh();
	
	deptcd = TFGetSessionInfo("userdeptcd");
	deptnm = TFGetSessionInfo("userdeptname");
	emplno = TFGetSessionInfo("userid");
	empknm = TFGetSessionInfo("username");
	
	if (deptcd == "000033" || deptcd == "000010" || deptcd == "000796") {
	} else {
		model.enable("input5", false);
		model.enable("input6", false);
		model.enable("input23", false);
		model.enable("input27", false);
		model.enable("button1", false);
		model.enable("button2", false);
	}

	model.setValue("/root/MainData/condition/deptcd", deptcd);
	model.setValue("/root/MainData/condition/deptnm", deptnm);
	model.setValue("/root/MainData/condition/emplno", emplno);
	model.setValue("/root/MainData/condition/empknm", empknm);
	
	model.makeValue(""+g_con2nodeSet+"/deptcd", deptcd);
	model.setValue(""+g_con2nodeSet+"/deptnm", deptnm);
	model.setValue(""+g_con2nodeSet+"/emplno", emplno);
	model.setValue(""+g_con2nodeSet+"/empknm", empknm);
	
	model.setValue("/root/MainData/condition/trndd", g_today.substr(0, 6));
	model.setValue("/root/MainData/condition/appryn", "%");
	model.refresh();
	
	model.setFocus("input16");
}

//=============================================================================================
//	화면 Clear (/root/MainData/main)
//=============================================================================================
function fResetForMain() {
	model.removeNodeset(g_nodeSet); //그리드 빈 Row 삭제
	model.gridRebuild(g_gridId); //그리드의 경우 Rebuild 필요!
}

//=============================================================================================
// 정리 버튼 클릭 시 처리
// pNum : A -> 전체화면 정리, S -> 입력옵션 정리
//=============================================================================================
function fClickClearBtn(pNum) {
	if(pNum == "S"){
		model.resetInstanceNode(g_con2nodeSet);
		
		model.makeValue(""+g_con2nodeSet+"/deptcd", deptcd);
		model.setValue(""+g_con2nodeSet+"/deptnm", deptnm);
		model.setValue(""+g_con2nodeSet+"/emplno", emplno);
		model.setValue(""+g_con2nodeSet+"/empknm", empknm);
		
		model.refresh();
		fLoad();
	}else{
		fReset();
		fLoad();
	 	//fClearInitData(); //초기화 자료 Clear
	}
}

//=============================================================================================
// 출력물 필요정보 조회
//=============================================================================================
function fPrintUserInfo() {
	//검색조건 Validation 체크
	if (!fIsValid()) return;

	//이전 자료 Clear!
	model.resetInstanceNode("/root/MainData/userh/userInfo");
	
	//서버로 전송할 Data(root/SendData) Set
	model.resetInstanceNode("/root/SendData");
	model.setValue("/root/SendData/Mode", "reqGetPrintUserInfo");
	model.copyNode("/root/MainData/condition", "/root/SendData/Data1");

	//조회 서비스 Call
	model.submitInstance("reqGetPrintUserInfo");

	//조회 후 처리
    if (TFGetGridCount(g_gridId) > 0) {
		TFSetMessage("/root/MainData/userh/userInfo");
    } else {
		model.setValue(g_msgNode + "/message", ERP_FGetMsg(ERP_V_MSG_SEARCH, new Array('0')));
		TFSetMessage("/root/MainData/userh/userInfo");
    }
}

//=============================================================================================
// 입력 항목의 유효성 검사 (검색조건의 유효성 검사)
//=============================================================================================
function fIsValid() {
	//검색조건 Validation Check
	if (model.getValue("/root/MainData/condition/trndd") == "" || model.getValue("/root/MainData/condition/trndd").length == 0) {
		TFGetMsgBox(-1, ERP_FGetMsg(ERP_V_MSG_ITEM, new Array("수련월")) + ERP_V_MSG_REQUIRED_ERR, "입력오류");
		model.setFocus("input16");
		return false;
	}
	
	return true;
}

//=============================================================================================
//그리드 클릭 시 처리
//=============================================================================================
function fClickGrid() {
	var iRow = TFGetGridPos("grid1");
	if (iRow < 0) return;
	
	model.copyNode(""+g_nodeSet+"["+iRow+"]", g_con2nodeSet);
	
	fLoad();
	
	model.refresh();
}

//=============================================================================================
// 조회 버튼 클릭 시 처리
//=============================================================================================
function fClickQueryBtn() {
	//검색조건 Validation 체크
	if (!fIsValid()) return;

	//이전 자료 Clear!
	fResetForMain();
	fClickClearBtn("S");
	
	//서버로 전송할 Data(root/SendData) Set
	model.resetInstanceNode("/root/SendData");
	model.setValue("/root/SendData/Mode", g_submitInfo1);
	model.copyNode("/root/MainData/condition", "/root/SendData/Data1");

	//조회 서비스 Call
	model.submitInstance(g_submitInfo1);

	//조회 후 처리
    if (TFGetGridCount(g_gridId) > 0) {
		TFSetMessage(g_nodeSet);
    } else {
		model.setValue(g_msgNode + "/message", ERP_FGetMsg(ERP_V_MSG_SEARCH, new Array('0')));
		TFSetMessage(g_msgNode);
    }
	
	fPrintUserInfo();
}

//=============================================================================================
// 	저장 전 처리 (저장할 항목의 유효성 검사)
//=============================================================================================
function fIsValidForSave() {
	
	if (model.getValue(""+g_con2nodeSet+"/trndd") == "" || model.getValue(""+g_con2nodeSet+"/trndd") == null) {
		TFGetMsgBox(-1, ERP_FGetMsg(ERP_V_MSG_ITEM, new Array("수련일자")) + ERP_V_MSG_REQUIRED_ERR, "입력오류");
		model.setFocus("inp_trndd");
		return false;
	}
	
	if (model.getValue(""+g_con2nodeSet+"/ovtrgb") == ""  || model.getValue(""+g_con2nodeSet+"/ovtrgb") == null) {
		TFGetMsgBox(-1, ERP_FGetMsg(ERP_V_MSG_ITEM, new Array("구분")) + ERP_V_MSG_REQUIRED_ERR, "입력오류");
		model.setFocus("inp_ovtrgb");
		return false;
	}
	
	if (model.getValue(""+g_con2nodeSet+"/ovtrsttm") == ""  || model.getValue(""+g_con2nodeSet+"/ovtrsttm") == null) {
		TFGetMsgBox(-1, ERP_FGetMsg(ERP_V_MSG_ITEM, new Array("시작시간")) + ERP_V_MSG_REQUIRED_ERR, "입력오류");
		model.setFocus("inp_ovtrsttm");
		return false;
	}
	
	if (model.getValue(""+g_con2nodeSet+"/ovtrentm") == ""  || model.getValue(""+g_con2nodeSet+"/ovtrentm") == null) {
		TFGetMsgBox(-1, ERP_FGetMsg(ERP_V_MSG_ITEM, new Array("종료시간")) + ERP_V_MSG_REQUIRED_ERR, "입력오류");
		model.setFocus("inp_ovtrentm");
		return false;
	}
	/*
	if (model.getValue(""+g_con2nodeSet+"/ovtradm") == "" || model.getValue(""+g_con2nodeSet+"/ovtradm") == null) {
		TFGetMsgBox(-1, ERP_FGetMsg(ERP_V_MSG_ITEM, new Array("총시간")) + ERP_V_MSG_REQUIRED_ERR, "입력오류");
		model.setFocus("inp_ovtradm");
		return false;
	}
	*/
	if (model.getValue(""+g_con2nodeSet+"/ovtrflag") == "" || model.getValue(""+g_con2nodeSet+"/ovtrflag") == null) {
		TFGetMsgBox(-1, ERP_FGetMsg(ERP_V_MSG_ITEM, new Array("초과목적")) + ERP_V_MSG_REQUIRED_ERR, "입력오류");
		model.setFocus("inp_ovtrflag");
		return false;
	}
	
	if (model.getValue(""+g_con2nodeSet+"/ovtrresn") == "" || model.getValue(""+g_con2nodeSet+"/ovtrresn") == null) {
		TFGetMsgBox(-1, ERP_FGetMsg(ERP_V_MSG_ITEM, new Array("초과사유")) + ERP_V_MSG_REQUIRED_ERR, "입력오류");
		model.setFocus("inp_ovtrresn");
		return false;
	}
	
	if (model.getValue(""+g_con2nodeSet+"/ovtrflag") == "A" && (model.getValue(""+g_con2nodeSet+"/pid") == "" || model.getValue(""+g_con2nodeSet+"/pid") == null)) {
		TFGetMsgBox(-1, ERP_FGetMsg(ERP_V_MSG_ITEM, new Array("환자번호")) + ERP_V_MSG_REQUIRED_ERR, "입력오류");
		model.setFocus("inp_pid");
		return false;
	}
	
    return true;
}

//=============================================================================================
//시간입력 유효성 검사
//=============================================================================================
function fTimeCheck(gbn){
	var ovtrsttm = model.getValue(g_con2nodeSet + "/ovtrsttm");
	var ovtrentm = model.getValue(g_con2nodeSet + "/ovtrentm");
	var ovtrgb = model.getValue(g_con2nodeSet + "/ovtrgb");
	var totadm = "";
	
	if (gbn == "sttm") {
		if (ovtrsttm.substr(3,1) != "0") {
			model.alert("10분 단위로 입력해야 합니다.");
			model.setValue(g_con2nodeSet + "/ovtrsttm", "");
			model.refresh();
			return;
		}
	} else if (gbn == "entm") {
		if (ovtrentm.substr(3,1) != "0") {
			model.alert("10분 단위로 입력해야 합니다.");
			model.setValue(g_con2nodeSet + "/ovtrentm", "");
			model.refresh();
			return;
		}
	}
		
	if (ovtrsttm != "" && ovtrentm != "") {
		var sthh = ovtrsttm.substr(0,2);
		var stmi = ovtrsttm.substr(2,2);
		var enhh = ovtrentm.substr(0,2);
		var enmi = ovtrentm.substr(2,2);
		var tothh = "";
		var totmi = "";
			
		// 당일
		if (ovtrgb == "D") {
			tothh = Number(enhh) - Number(sthh);
			totmi = Number(enmi) - Number(stmi);
			
			if (totmi < 0) {
				tothh = tothh - 1;
				totmi = totmi * -1;
			}
			
			if (tothh < 10) {
				tothh = "0" + String(tothh);
			}
			
			if (totmi == 0) {
				totmi = "0" + String(totmi);
			}
			
			totadm = String(tothh) + String(totmi);
			
		// 익일
		} else {
			tothh = Number(enhh) + (24 - Number(sthh));
			totmi = Number(enmi) + (60 + Number(stmi));
			
			if (totmi >= 60) {
				tothh = tothh + 1;
				totmi = totmi - 60;
			}
			
			if (tothh < 10) {
				tothh = "0" + String(tothh);
			}
			
			if (totmi == 0) {
				totmi = "0" + String(totmi);
			}
			
			totadm = String(tothh) + String(totmi);
		}
		
		model.setValue(g_con2nodeSet + "/ovtradm", totadm);
		model.refresh();
	}
}
//=============================================================================================
// 저장 버튼 클릭 시 처리
//=============================================================================================
function fClickSaveBtn(pMode) {
	
	//저장할 항목의 Validation 체크
    if (!fIsValidForSave()) return;
   
	//SendData Set!
	model.resetInstanceNode("/root/SendData");
	model.setValue("/root/SendData/Mode", g_submitInfo2);
	 
    //저장, 수정 체크
    var check = model.getValue(""+g_con2nodeSet+"/seqno");
    if(pMode == "D"){
    	model.makeValue(""+g_con2nodeSet+"/status", "D");
    }else{
    	if(!check || check == null){
    		model.makeValue(""+g_con2nodeSet+"/status", "I");
    	}else{
    		model.makeValue(""+g_con2nodeSet+"/status", "U");
    	}
    }
	
	model.copyNode(g_con2nodeSet, "/root/SendData/Data1");
	
	//저장
    if (model.submitInstance( g_submitInfo2 )) {
    	model.gridRebuild("grid1");
    	model.resetInstanceNode("/root/SendData");
    	TFSetMessage("/root/HideData/resultMsg"); //정상적인 메시지 처리
		
		if (!fIsValid()) return; //검색조건 Validation 체크
	
		fClickQueryBtn(); // 저장 후 재조회
		model.refresh();
    }
    
    TFSetMessage(g_msgNode);
}

//=============================================================================================
// Excel 버튼 클릭 시 처리
//=============================================================================================
function fClickExcelBtn() {
	//model.saveExcel(g_gridId);
	model.excelExport(g_gridId);
}