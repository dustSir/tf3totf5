#-*- coding: utf-8 -*-
import re

def strInBracket(targetStr):
    '''
    괄호 안의 문자열 반환
    '''
    openBracket = '('
    closeBracket = ')'

    openBracketIdx = targetStr.find(openBracket)
    closeBracketIdx = targetStr.find(closeBracket)

    return targetStr[openBracketIdx+1:closeBracketIdx]

def isFindString(targetStr, findStr):
    '''
    해당 문자열에서 문자 찾기
    '''
    m = re.search(findStr, targetStr)
    return m

def removeQuotes(targetStr):
    '''
    따옴표 삭제
    '''
    return targetStr.replace("'",'').replace('"', '')

def isString(targetStr):
    '''
    문자열 여부 판단 (따옴표로 쌓여있는지)
    '''
    if (targetStr[0] == '"' or targetStr[0] == "'") and (targetStr[-1] == '"' or targetStr[-1] == "'"):
        # 따옴표로 쌓여있음 == 문자열 형식
        return True
    else:
        # 따옴표로 쌓여있지않음 == 변수 형식
        return False

def getArgumentList(codeLine, funtionName):
    '''
    함수의 인자를 갖는 리스트 반환
    '''
    reg = funtionName+'.*\)'
    # regex = re.compile(reg)
    functionHaed = re.findall(reg, codeLine)
    return strInBracket(functionHaed[0]).split(',')