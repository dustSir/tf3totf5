import sys
import main
import re
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QApplication, QFileSystemModel, QLineEdit, QTreeView, QWidget, QVBoxLayout,QDesktopWidget, QHBoxLayout, QPushButton, QMessageBox
from PyQt5.QtGui import QIcon

class lineEdit_dragFile_injector():
    '''
    해당 lineEdit에 파일 드레그 & 드롭 이벤트 오버라이딩 클래스
    '''
    def __init__(self, lineEdit, auto_inject = True):
        self.lineEdit = lineEdit
        if auto_inject:
            self.inject_dragFile()

    def inject_dragFile( self ):
        self.lineEdit.setDragEnabled(True)
        self.lineEdit.dragEnterEvent = self._dragEnterEvent
        self.lineEdit.dragMoveEvent = self._dragMoveEvent
        self.lineEdit.dropEvent = self._dropEvent

    def _dragEnterEvent( self, event ):
        data = event.mimeData()
        urls = data.urls()
        if ( urls and urls[0].scheme() == 'file' ):
            event.acceptProposedAction()

    def _dragMoveEvent( self, event ):
        data = event.mimeData()
        urls = data.urls()
        if ( urls and urls[0].scheme() == 'file' ):
            event.acceptProposedAction()

    def _dropEvent( self, event ):
        data = event.mimeData()
        urls = data.urls()
        if ( urls and urls[0].scheme() == 'file' ):
            # for some reason, this doubles up the intro slash
            filepath = str(urls[0].path())[1:]
            self.lineEdit.setText(filepath)    


class FileTreeSelectorModel(QtWidgets.QFileSystemModel):
    '''
    파일 탐색기 동작 Model
    '''
    def __init__(self, parent=None, rootpath='/'):
        QtWidgets.QFileSystemModel.__init__(self, None)
        self.root_path      = rootpath
        self.checks         = {}
        self.nodestack      = []
        self.parent_index   = self.setRootPath(self.root_path)
        self.root_index     = self.index(self.root_path)

        self.setFilter(QtCore.QDir.AllEntries | QtCore.QDir.Hidden | QtCore.QDir.NoDot)
        self.directoryLoaded.connect(self._loaded)

    def _loaded(self, path):
        print('_loaded', self.root_path, self.rowCount(self.parent_index))

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if role != QtCore.Qt.CheckStateRole:
            return QtWidgets.QFileSystemModel.data(self, index, role)
        else:
            if index.column() == 0:
                return self.checkState(index)

    def flags(self, index):
        return QtWidgets.QFileSystemModel.flags(self, index) | QtCore.Qt.ItemIsUserCheckable

    def checkState(self, index):
        if index in self.checks:
            return self.checks[index]
        else:
            return QtCore.Qt.Checked

    def setData(self, index, value, role):
        if (role == QtCore.Qt.CheckStateRole and index.column() == 0):
            self.checks[index] = value
            print('setData(): {}'.format(value))
            return True
        return QtWidgets.QFileSystemModel.setData(self, index, value, role)

    def traverseDirectory(self, parentindex, callback=None):
        print('traverseDirectory():')
        callback(parentindex)
        if self.hasChildren(parentindex):
            print('|children|: {}'.format(self.rowCount(parentindex)))
            for childRow in range(self.rowCount(parentindex)):
                childIndex = parentindex.child(childRow, 0)
                print('child[{}]: recursing'.format(childRow))
                self.traverseDirectory(childIndex, callback=callback)
        else:
            print('no children')

    def printIndex(self, index):
        print('model printIndex(): {}'.format(self.filePath(index)))


class FileTreeSelectorDialog(QtWidgets.QWidget):
    '''
    파일 탐색기 다이얼로그
    '''
    def __init__(self):
        super().__init__()

        self.root_path      = ''

        # Widget
        # self.title          = "Application Window"
        # self.left           = 10
        # self.top            = 10
        # self.width          = 1080
        # self.height         = 640

        # self.setWindowTitle(self.title)         #TODO:  Whilch title?
        # self.setGeometry(self.left, self.top, self.width, self.height)
        

        # Model
        self.model          = FileTreeSelectorModel(rootpath=self.root_path)
        # self.model          = QtWidgets.QFileSystemModel()

        # View
        self.view           = QtWidgets.QTreeView()

        self.view.setObjectName('treeView_fileTreeSelector')
        # self.view.setWindowTitle("Dir View")    #TODO:  Which title?
        self.view.setAnimated(False)
        self.view.setIndentation(20)
        self.view.setSortingEnabled(True)

        self.title = '변환하면 해당 경로에 해당 파일이름_TF5 스크립트 파일이 생깁니다.'
        self.setWindowTitle(self.title)
        self.resize(740, 430)
        self.center()
        self.setAcceptDrops(True)

        
        self.qle = QLineEdit(self)

        self.lineEdit = lineEdit_dragFile_injector(self.qle)
        self.qle.setReadOnly(True)

        self.convertButton = QPushButton('변환')
        self.convertButton.clicked.connect(self.convertButtonClicked)

        self.hbox = QHBoxLayout()
        self.hbox.addWidget(self.qle)
        self.hbox.addWidget(self.convertButton)

        # Attach Model to View
        self.view.setModel(self.model)
        self.view.setRootIndex(self.model.parent_index)
        self.view.setColumnWidth(0,380)
        self.view.setColumnWidth(1,80)
        self.view.setColumnWidth(2,100)

        # Misc
        self.node_stack     = []

        # GUI
        windowlayout = QtWidgets.QVBoxLayout()
        windowlayout.addLayout(self.hbox)
        windowlayout.addWidget(self.view)
        self.setLayout(windowlayout)
        QtCore.QMetaObject.connectSlotsByName(self)

        self.show()

    def convertButtonClicked(self):
        # print(self.qle.text())
        filePath = self.qle.text()
        if self.validJSFile(filePath):
            isConv = main.mainRun(filePath)
            if isConv == True:
                QMessageBox.question(self, 'Ok', '변환 완료!', QMessageBox.Yes)
            else:
                errMsg = str(isConv)
                QMessageBox.question(self, 'ERROR', f'에러메세지 : {errMsg}', QMessageBox.Yes)
        else:
            QMessageBox.question(self, 'Error', '자바스크립트 파일만 변환이 가능합니다!', QMessageBox.Yes)
    
    def validJSFile(self, filePath):
        fileName = filePath
        return re.match('^[A-Z]:.*(\.js)$', fileName)

    @QtCore.pyqtSlot(QtCore.QModelIndex)
    def on_treeView_fileTreeSelector_doubleClicked(self, index):
        # print('tree clicked: {}'.format(self.model.filePath(index)))
        self.model.traverseDirectory(index, callback=self.model.printIndex)
        self.qle.setText(self.model.filePath(index))

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = FileTreeSelectorDialog()
    sys.exit(app.exec_())