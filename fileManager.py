#-*- coding: utf-8 -*-
class FileManager():
    '''
    - 필요 기능
        1. 파일 읽기
        2. 파일 쓰기
        3. 파일 조작
            1) 문자열로 반환
            2) line 별 문자열 리스트로 반환
            3) ... 추가로 더 생각해보자
    '''

    readFileInfo = {
        'path' : '',
        'name' : '',
        'extension' : '',
        'fullPath' : ''
    }

    writeFileInfo = {
        'path' : '',
        'name' : '',
        'extension' : '',
        'fullPath' : ''
    }

    fileByLineList = []

    def __init__(self, readFileInfo):
        self.readFileInfo['path'] = readFileInfo['path']
        self.readFileInfo['name'] = readFileInfo['name']
        self.readFileInfo['extension'] = readFileInfo['extension']
        self.readFileInfo['fullPath'] = self.readFileInfo['path'] + '/' + self.readFileInfo['name'] + '.' + self.readFileInfo['extension']

        self.writeFileInfo['path'] = self.readFileInfo['path']
        self.writeFileInfo['name'] = self.readFileInfo['name'] + "_TF5"
        self.writeFileInfo['extension'] = self.readFileInfo['extension']
        self.writeFileInfo['fullPath'] = self.writeFileInfo['path'] + '/' + self.writeFileInfo['name'] + '.' + self.writeFileInfo['extension']

    def fileOpen(self, fullPath, option):
        '''
        경로의 파일 open() 후 파일객체 반환
        default Option : r (읽기)
        '''
        try:
            f = open(fullPath, option)
        except FileNotFoundError as e:
            print('['+ fullPath + '] 파일이 해당 경로에 존재하지 않습니다.')
            print(e)
            return False

        return f

    def fileRead(self, option='r'):
        '''
        해당 경로의 파일 읽어옴
        '''
        file = self.fileOpen(self.readFileInfo['fullPath'], option)
        fileByLineList = file.readlines()
        file.close()
        return fileByLineList

    
    def fileWrite(self, scriptCodeListByLine, option='w'):
        '''
        해당 경로에 파일 작성
        default Option : w (덮어쓰기)
        '''
        file = self.fileOpen(self.writeFileInfo['fullPath'], option)

        if len(scriptCodeListByLine) > 0:
            for line in scriptCodeListByLine:
                file.write(line)

        file.close()



