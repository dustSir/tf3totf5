#-*- coding: utf-8 -*-
import re

class Analysis():
    '''
    스크립트 읽어 분석하는 클래스
    스크립트를 문자열로 받는 시점부터 시작
    - 초기모델
        1. 변수 Dict
        2. 함수 Dict
    - 정규 표현식을 이용한 변수 함수 구분
    '''

    fullDocument = ''
    def __init__(self, fullDocument):
        self.fullDocument = fullDocument

    # 읽어온 스크립트 변수 모음 DICT
    variableDict = dict()

    def initVariableDict(self, codeList):
        '''
        읽어온 스크립트의 변수들 Dict에 초기화
        '''
        variableDict = {}
        
        
        return variableDict
    


statusByLine = list()
def initStatusByLine(statusByLine) :
    '''
    코드 각 라인의 상태를 담고있는 리스트 초기화해주는 함수
    '''
    return True