#-*- coding: utf-8 -*-
import re
import conversion as conv
import fileManager as fm
import stringManager as sm

# readFileInfo 는 시스템변수로 
# readFileInfo = {
#     'path' : 'C://Users/KUH/Desktop/python/tf3totf5/testingScript',
#     'name' : 'MRR_GRGeomSaek',
#     'extension' : 'js'
# }

def mainRun(filePath):
    try:
        filePathSplitSlash = filePath.split('/')
        length = len(filePathSplitSlash)
        path = '/'.join(filePathSplitSlash[0:length-1])
        path = re.sub('/{1}', '//', path)
        name = filePathSplitSlash[-1]
        nameList = name.split('.')

        readFileInfo = {
            'path' : path,
            'name' : nameList[0],
            'extension' : nameList[1]
        }
        
        scriptFile = fm.FileManager(readFileInfo)
        scriptCodeListByLine = scriptFile.fileRead()
        # variableDict = conv.initVariableDict(scriptCodeListByLine)

        for scriptLine in scriptCodeListByLine:
            '''
            실제로 코드를 한바퀴 돌면서 각 조건에 맞는 문법들을 고쳐주는 프로그램의 시작
            '''
            if not re.match('^[//]', scriptLine.strip()):
                idx = scriptCodeListByLine.index(scriptLine)
                curCodeLine = scriptCodeListByLine[idx]
                
                if sm.isFindString(scriptLine, 'model.gridRebuild'):
                    # model.gridRebuild -> gridId.refresh()
                    scriptCodeListByLine[idx] = conv.gridRebuild2Refresh(scriptLine)
                if sm.isFindString(scriptLine, 'model.gridrebuild'):
                    # model.gridRebuild -> gridId.refresh()
                    scriptCodeListByLine[idx] = conv.gridRebuild2Refresh(scriptLine)
                if sm.isFindString(scriptLine, 'model.vsGrid'):
                    # model.vsGrid() -> document.controls()
                    scriptCodeListByLine[idx] = conv.vsGrid2Controls(scriptLine)
                if sm.isFindString(scriptLine, 'model.control'):
                    # model.contorl("<controlID>") -> document.contorls("<controlID>")
                    scriptCodeListByLine[idx] = conv.control2Controls(scriptLine)
                
                if sm.isFindString(scriptLine, 'model.enable\\('):
                    # model.enable(<"contrilId">, [true|false]) -> <controlId>.disabled = [true|false]
                    scriptCodeListByLine[idx] = conv.enable2Disabled(scriptLine)

                if sm.isFindString(scriptLine, 'model.visible\\('):
                    # model.enable(<"contrilId">, [true|false]) -> <controlId>.disabled = [true|false]
                    scriptCodeListByLine[idx] = conv.visible2Visible(scriptLine)
                
                if sm.isFindString(scriptLine, 'model.submitInstance'):
                    # model.submitInstance("서브밋아이디") -> model.send("서브밋아이디")
                    scriptCodeListByLine[idx] = conv.submitInstance2Send(scriptLine)
                
                if sm.isFindString(scriptLine, 'model.copyNode'):
                    # model.copyNode(param1, param2) -> model.copyNode(param2 -> param1)
                    scriptCodeListByLine[idx] = conv.copyNode2copyNode(scriptLine)
                
                
                
                if sm.isFindString(scriptLine, '.lvTrim()'):
                    # .lvTrim() -> .replace(/\s/gi, "") 로 변경
                    scriptCodeListByLine[idx] = conv.lvTrim2replace(scriptLine)
                
                if sm.isFindString(scriptLine, '.style.width'):
                    # .style.width -> .attribute("width")
                    scriptCodeListByLine[idx] = conv.ERP_style2AttributeWidth(scriptLine)
                
                if sm.isFindString(scriptLine, '.style.height'):
                    # .style.height -> .attribute("height")
                    scriptCodeListByLine[idx] = conv.ERP_style2AttributeHeight(scriptLine)
                
                if sm.isFindString(scriptLine, '.style.left'):
                    # .style.left -> .attribute("left")
                    scriptCodeListByLine[idx] = conv.ERP_style2AttributeLeft(scriptLine)
                
                if sm.isFindString(scriptLine, '.style.top'):
                    # .style.top -> .attribute("top")
                    scriptCodeListByLine[idx] = conv.ERP_style2AttributeTop(scriptLine)
                
                if sm.isFindString(scriptLine, '.style.backgroundColor'):
                    # .style.backgroundColor -> .attribute("background-color")
                    scriptCodeListByLine[idx] = conv.ERP_style2AttributeBackgroundColor(scriptLine)
                
                if sm.isFindString(scriptLine, 'new Date\\('):
                    # new Date() -> moment()
                    scriptCodeListByLine[idx] = conv.date2Moment(scriptLine)
                if sm.isFindString(scriptLine, '.getYear()'):
                    # .getYear() -> moment().get('year')
                    scriptCodeListByLine[idx] = conv.getYear2get(scriptLine)
                if sm.isFindString(scriptLine, '.getMonth()'):
                    # .getMonth() -> moment().get('month')
                    scriptCodeListByLine[idx] = conv.getMonth2get(scriptLine)
                if sm.isFindString(scriptLine, '.getDate()'):
                    # .getDate() -> moment().get('date')
                    scriptCodeListByLine[idx] = conv.getDate2get(scriptLine)
                


                #LiveTF.js 함수 대체
                if sm.isFindString(scriptLine, 'TFGetMsgBox\\('):
                    # TFGetMsgBox() -> TF.window.getMsgBox()
                    scriptCodeListByLine[idx] = conv.msgBox2TF5msgBox(scriptLine)
                if sm.isFindString(scriptLine, 'TFGetSessionInfo\\('):
                    # TFGetSessionInfo() -> TF.com.session.getSessionInfo()
                    scriptCodeListByLine[idx] = conv.getSessionInfo2TF5getSessionInfo(scriptLine)
                if sm.isFindString(scriptLine, 'findNode\\('):
                    # findNode() -> TF.xml.selectSingleNode(<wid>, <xpath>)
                    scriptCodeListByLine[idx] = conv.findNode2SelectSingleNode(scriptLine)
                if sm.isFindString(scriptLine, 'TFGetGridCount\\('):
                    # TFGetGridCount() -> TF.grid.dataRows()
                    scriptCodeListByLine[idx] = conv.getGridCount2TF5DataRows(scriptLine)
                if sm.isFindString(scriptLine, 'TFSetMessage'):
                    # TFSetMessage()함수 주석처리
                    scriptCodeListByLine[idx] = conv.setMessage2Comment(scriptLine)
                if sm.isFindString(scriptLine, 'TFGetGridPos'):
                    # TFGetGridPos(<girdID>) -> <gridID>.row
                    scriptCodeListByLine[idx] = conv.getGridPos2Row(scriptLine)
                if sm.isFindString(scriptLine, 'TFGetGridCol'):
                    # TFGetGridCol(<girdID>) -> <gridID>.col
                    scriptCodeListByLine[idx] = conv.getGridCol2Col(scriptLine)



                if sm.isFindString(scriptLine, 'makeNode\\('):
                    # makeNode() -> model.makeNode()
                    scriptCodeListByLine[idx] = conv.makeNode2ModelMakeNode(scriptLine)

                if sm.isFindString(scriptLine, 'model.trace\\('):
                    # model.trace() -> TF.log4js.log()
                    scriptCodeListByLine[idx] = conv.trace2log(scriptLine)

                
                # datagrid 관련 함수 변환
                if sm.isFindString(scriptLine, '.Row') or sm.isFindString(scriptLine, '.Col') or sm.isFindString(scriptLine, '.Rows') or sm.isFindString(scriptLine, '.Cols') or sm.isFindString(scriptLine, '.FixedRows'):
                    # <datagrid>.Row -> <datagrid>.row
                    scriptCodeListByLine[idx] = conv.row2Lower(scriptLine)
                if sm.isFindString(scriptLine, '.textMatrix\\(') or sm.isFindString(scriptLine, '.TextMatrix\\('):
                    # <gridId>.textMatrix(r,c) -> <gridId>.valueMatrix(r,c)
                    scriptCodeListByLine[idx] = conv.textMatrix2valueMatrix(scriptLine)

        scriptFile.fileWrite(scriptCodeListByLine)

        return True
    except Exception as e:
        return e