/*
  - ▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩
    JavaScript  : HIT Web Project ERP 공통 JS Common Module
      - Author  : SunYoung Kim
      - Version : 2004.11.09 / 최초작성 / 김선영
                  2005.01.06 / 통합,정리 / 김선영
  - ▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩▩
  ※ Version 관리를 위해 History Check를 당부드립니다.

  -------------------------------   일반함수 리스트 [시작]   -------------------------------
  ※ 프로젝트 일반
  - ERP_FOpen        : 화면 Open시 공통코드 테이블의 코드 목록을 조회 / ksy
  - ERP_FReset       : MainData Reset / ksy
  - ERP_FReset2      : 지정한 XML String으로 MainData Reset / ksy
  - ERP_FValidate    : 코드로 명칭 조회 또는 명칭으로 코드 조회 / ksy
  - ERP_FSetToDay    : 해당컨트롤에 오늘날자셋팅 / ckj
  - ERP_FSetCtlStyle : 해당컨트롤의 크기및 위치셋팅 / ckj
  - ERP_FGetXPath      : 컨트롤명으로 해당 컨트롤에 바인드된 XPath 구하기 / ckj
  - ERP_FChangeInputMode : Input Control의 inputMode 속성 변경 / ksy
  - ERP_FGetIP       : IP 얻음 / ksy

  - ERP_FValidateZipCode : 그리드가 아닌 일반 Control에서 우편번호 또는 읍면동의 Validation Check / ksy
  - ERP_FValidateZipCodeOfGrid : 그리드에서 우편번호 또는 읍면동의 Validation Check / ksy
  - ERP_FOpenZipCodeHelp : 우편번호 Help Open / ksy

  - ERP_FValidateBuSeo : 그리드가 아닌 일반 Control에서 부서코드 또는 부서명의 Validation Check / ksy
  - ERP_FValidateBuSeoOfGrid : 그리드에서 부서코드 또는 부서명의 Validation Check / ksy
  - ERP_FOpenBuSeoHelp : 부서코드 Help Open / ksy

  - ERP_FValidateSaWeon  : 그리드가 아닌 일반 Control에서 사원번호 또는 사원명의 Validation Check / ksy
  - ERP_FValidateSaWeonOfGrid : 그리드에서 사원번호 또는 사원명의 Validation Check / ksy
  - ERP_FOpenSaWeonHelp  : 사원검색 Help Open / ksy
  - ERP_FOpenSaWeonHelp2 : 사원검색 Help Open / ksy
  - ERP_FOpenSaWeonGyoYukHelp : [KMS] 교육용 - 사원검색 Help Open / 양두진. 
  
  - ERP_FValidateHwanJa : 그리드가 아닌 일반 Control에서 등록번호 또는 환자명의 Validation Check / ksy
  - ERP_FValidateHwanJaOfGrid : 그리드에서 등록번호 또는 환자명의 Validation Check / ksy
  - ERP_FOpenHwanJaHelp : 등록번호 Help Open / ksy

  - ERP_FValidateGyeJeong : 그리드가 아닌 일반 Control에서 계정코드 또는 계정명의 Validation Check / ckj
  - ERP_FValidateGyeJeongOfGrid : 그리드에서 계정코드 또는 계정명의 Validation Check / ckj
  - ERP_FOpenGyeJeongHelp : 계정코드 Help Open / ckj

  - ERP_FValidateGeoRaeCheo : 그리드가 아닌 일반 Control에서 거래처코드 또는 거래처명의 Validation Check / ckj
  - ERP_FValidateGeoRaeCheoOfGrid : 그리드에서 거래처코드 또는 거래처명의 Validation Check / ckj
  - ERP_FOpenGeoRaeCheoHelp : 거래처코드 Help Open / ckj

  - ERP_FValidateGongTongCode : 그리드가 아닌 일반 Control에서 공통코드 또는 코드명의 Validation Check / ckj
  - ERP_FValidateGongTongCodeOfGrid : 그리드에서 공통코드 또는 코드명 Validation Check / ckj
  - ERP_FOpenGongTongCodeHelp : 공통코드 Help Open / ckj

  - ERP_FOpenUserHelp : 사용자관리(comcuserm) Help Open / leejp
  - ERP_FValidateUser : 그리드가 아닌 일반 Control에서 사용자ID 또는 사용자명의 Validation Check / leejp

  - ERP_FOpenUnitHelp : 장비코드관리 Help Open / leejp
  - ERP_FValidateUnit : 그리드가 아닌 일반 Control에서 장비코드 또는 장비명의 Validation Check / leejp

  - ERP_FValidateHangule : KSC5601에 없고 MS949 확장완성형에만 있는 한글을 Validation Check (TFCheckValidHangule 변형함수) / msh

  ※ 그리드 관련
  - ERP_FGridColHidden : 그리드의 Hidden Column 숨기기/보이기 / ksy
  - ERP_FGridSetFocus  : 그리드의 특정 Cell로 Focus 위치 / ksy
  - ERP_FGridRowAdd    : 그리드 줄추가 / ksy
  - ERP_FGridRowInsert : 그리드 줄삽입 / ksy
  - ERP_FGridRowDelete : 그리드 줄삭제 / ksy
  - ERP_FGridRowCopy   : 그리드 줄복사 (구현 안됨)
  - ERP_FGridValueChanged : 그리드 Data가 변경된 경우
  - ERP_FGridEditMode  : 그리드 클릭시 바로 에디트모드로 설정 / ckj
  - ERP_FGridDelRow    : 그리드 무효행 삭제  / ckj
  - ERP_FGridCheckFlag : 그리드 변경된 행수를 리턴한다  / ckj

  ※ XML 관련
  - ERP_FResetForNode  : Node Reset (해당 Node를 주어진 xml Data로 교체) / ksy

  ※ 일반 Java Script 관련
  - ERP_FGetTokenCount  : 문자열에 특정 문자열이 포함된 개수를 리턴 / ksy
  - ERP_FSetPAD         : 문자열에 앞뒤로 정해진 자리수만큼 특정 문자 채우기 / sjh
  - ERP_FGetToDay       : 오늘날짜 리턴 (서버날짜 또는 로컬스시템날짜) / ckj
  - ERP_FGetStrCount    : 문자열에 포함된 숫자를 증감하여 리턴한다 / ckj
  - ERP_FGetRelativeDay : 특정일자에 몇일(몇달 또는 몇년)이 더해진 일자를 리턴한다. / ksy
  - ERP_FRemoveChar     : 문자열에 앞의 특정 문자를 지운 문자열을 리턴한다. / sjh
  -------------------------------   일반함수 리스트 [종료]   -------------------------------
*/
//Message
var ERP_V_MSG_SEARCH           = "@건이 조회되었습니다.";
var EPR_V_MSG_NO_CHANGED       = "변경된 정보가 없습니다.";
var ERP_V_MSG_ITEM             = "'@' 은(는) ";
var ERP_V_MSG_LENGTH_ERR       = "@자리이어야 합니다.";
var ERP_V_MSG_MIN_LENGTH_ERR   = "@자리 이상이어야 합니다.";
var ERP_V_MSG_MIN_NUM_ERR      = "@ 이상이어야 합니다.";
var ERP_V_MSG_MAX_NUM_ERR      = "@ 이하이어야 합니다.";
var ERP_V_MSG_REQUIRED_ERR     = "필수항목입니다.";
var ERP_V_MSG_NUMBER_ERR       = "숫자이어야 합니다.";
var ERP_V_MSG_FORMAT_ERR       = "'@' 형식이어야 합니다.\n" +
                                 "  - # : 문자 혹은 숫자\n" +
                                 "  - A, Z : 문자(Z는 공백포함)\n" +
                                 "  - 0, 9 : 숫자(9는 공백포함)";
var ERP_V_MSG_DATE_ERR         = "유효한 날짜가 아닙니다.";
var ERP_V_MSG_YEAR_ERR         = "년도가 잘못되었습니다.";
var ERP_V_MSG_MONTH_ERR        = "월이 잘못되었습니다.";
var ERP_V_MSG_DAY_ERR          = "일이 잘못되었습니다.";
var ERP_V_MSG_HOUR_ERR         = "시간이 잘못되었습니다.";
var ERP_V_MSG_MINUTE_ERR       = "분이 잘못되었습니다.";
var ERP_V_MSG_SECOND_ERR       = "초가 잘못되었습니다.";
var ERP_V_MSG_MIN_DATE_ERR     = "@년 @월 @일 이후이어야 합니다.";
var ERP_V_MSG_MAX_DATE_ERR     = "@년 @월 @일 이전이어야 합니다.";

/**
 *  공통메세지에 정의된 메세지를 치환하여 리턴
 *  @param message : ErpCommon.js의 공통 메세지 영역에 선언된 메세지 ID
 *  @param paramArray : optional. 메세지에서 '@' 문자와 치환될 스트링 Array. (Array의 index와 메세지 내의 '@' 문자의 순서가 일치한다.)
 *  사 용 예) var message = ERP_FGetMsg(ERP_V_MSG_SEARCH, new Array("0"));
 *  변경내역) 2005.01.19 / 최초작성 / ksy
 */
function ERP_FGetMsg(message, paramArray) {
  var index = -1;
  var re = /@/g;
  var count = 0;

  if (typeof(paramArray) == "undefined") {
    return message;
  }

  while ( (index = message.indexOf("@")) != -1) {
    message = message.substr(0, index + 1).replace(re, paramArray[count++]) +
              message.substring(index + 1);
  }

  return message;
}



/**
 *  화면 Open시 공통코드 테이블의 코드 목록을 조회
 *
 *  주의사항) 1. 화면에 SubitInfo로 reqGetInitData가 정의되어 있어야 함.
 *              reqGetInitData
 *                  - Action    : /com/commonweb/GCCodeLoader.live
 *                  - Ref       : root/SendData
 *                  - ResultRef : root/InitData
 *           2. 초기에 Load 하고자 하는 공통코드(comccodem)테이블의 코드구분(codeflag) 목록을 정의
 *              <root>
 *                  <HideData>
 *                      <codeFlag>
 *                          <codeflag>2001,2002</codeflag>
 *                      </codeFlag>
 *                  </HideData>
 *              </root>
 *  변경내역) 2004.12.24 / 최초작성 / ksy
 */
function ERP_FOpen() {
    model.resetInstanceNode("/root/SendData");
    model.setValue("/root/SendData/Mode", "reqGetGCCodeList");      //Action Method
    model.copyNode("/root/SendData/Data1", "/root/HideData/codeFlag"); //필요한 공통코드의 구분자 리스트 예)2001,2002,2003
    model.send("reqGetInitData");
    model.resetInstanceNode("/root/SendData");

    //====================================== 주의사항!! ===========================================//
    // 아직 Message XFM이 초기화 되지 않은 상태에서 TFSetMessage() Call하는 경우 브라우저 죽는 현상 발생!
    // Main Xfm, Message Xfm의 화면이 초기화되는 순서가 일정하지 않으므로...
    // 초기화 로직에 대한 메시지는 아래와 같이 Call한다.
    //model.iEScriptCall("","TFSetMessage","/root/InitData");
//     TFSetMessage("/root/InitData"); //param : ResultRef : 하위 노드 중 message의 value를 출력
    //====================================== 주의사항!! ===========================================//
}

/**
 *  MainData Reset : 초기의 MainData로 Reset!
 *  변경내역) 2004.12.24 / 최초작성 / ksy
 */
var ERP_V_InitMainData = TFGetNodeStr("/root/MainData");
function ERP_FReset() {
    var initNode = model.makeNode(ERP_V_InitMainData);
    var curNode = TF.xml.selectSingleNode("/root/MainData");
    replaceNode(initNode, curNode);
    model.refresh();
}

/**
 *  지정한 XML String으로 MainData Reset
 *  @param pInitMainData : /root/MainData의 XML String
 *  변경내역) 2005.01.07 / 최초작성 / ksy
 */
function ERP_FReset2(pInitMainData) {
    var initNode = model.makeNode(pInitMainData);
    var curNode = TF.xml.selectSingleNode("/root/MainData");
    replaceNode(initNode, curNode);
    model.refresh();
}

/**
 *  코드로 명칭 조회 또는 명칭으로 코드 조회
 *  @param pStatId : Statement ID (ErmCommonMap.xml에 정의되어 있는 Statement ID)
 *  @param pParam  : jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5
 *  @param pAction : reqGetCodeName SubmitInfo의 Action.
 *  주의사항) 1. 화면에 SubitInfo로 reqGetCodeName가 정의되어 있어야 함.
 *              reqGetCodeName
 *                  - Action    : /erp/erpcommonweb/ErpCommon.live
 *                  - Ref       : root/SendData
 *                  - ResultRef : root/HideData/codeName
 *           2. 조회 결과를 담을 Data 구조를 정의
 *              <root>
 *                  <HideData>
 *                      <codeName>
 *                          <count/>
 *                          <code/>
 *                          <name1/>
 *                          <name2/>
 *                          <name3/>
 *                          <name4/>
 *                          <name5/>
 *                          <name6/>
 *                          <name7/>
 *                          <name8/>
 *                          <name9/>
 *                          <name10/>
 *                      </codeName>
 *                  </HideData>
 *              </root>
 *  사 용 예) ERP_FValidate("comcdeptm", "2▦2▦");
 *  변경내역) 2004.12.24 / 최초작성 / ksy
 */
function ERP_FValidate(pStatId, pParam, pAction) {

    var paramArr = pParam.split("▦");

    model.model.makeNode("root/SendData/Data1/statid");
    model.model.makeNode("root/SendData/Data1/jobgbn");
    model.model.makeNode("root/SendData/Data1/srtgbn");
    model.model.makeNode("root/SendData/Data1/srhtxt");
    model.model.makeNode("root/SendData/Data1/cond1");
    model.model.makeNode("root/SendData/Data1/cond2");
    model.model.makeNode("root/SendData/Data1/cond3");
    model.model.makeNode("root/SendData/Data1/cond4");
    model.model.makeNode("root/SendData/Data1/cond5");
    model.model.makeNode("root/SendData/Data1/cond6");
    model.model.makeNode("root/SendData/Data1/cond7");
    model.model.makeNode("root/SendData/Data1/cond8");
    model.model.makeNode("root/SendData/Data1/cond9");
    model.model.makeNode("root/SendData/Data1/cond10");
	model.model.makeNode("root/SendData/Data1/cond11");
	model.model.makeNode("root/SendData/Data1/cond12");

    model.setValue("/root/SendData/Mode", "reqGetCodeName");
    model.setValue("root/SendData/Data1/statid", pStatId);
    model.setValue("root/SendData/Data1/jobgbn", paramArr[0]);
    model.setValue("root/SendData/Data1/srtgbn", paramArr[1]);
    model.setValue("root/SendData/Data1/srhtxt", paramArr[2]);
    model.setValue("root/SendData/Data1/cond1",  paramArr[3]);
    model.setValue("root/SendData/Data1/cond2",  paramArr[4]);
    model.setValue("root/SendData/Data1/cond3",  paramArr[5]);
    model.setValue("root/SendData/Data1/cond4",  paramArr[6]);
    model.setValue("root/SendData/Data1/cond5",  paramArr[7]);
    model.setValue("root/SendData/Data1/cond6",  paramArr[8]);
    model.setValue("root/SendData/Data1/cond7",  paramArr[9]);
    model.setValue("root/SendData/Data1/cond8",  paramArr[10]);
    model.setValue("root/SendData/Data1/cond9",  paramArr[11]);
    model.setValue("root/SendData/Data1/cond10",  paramArr[12]);
	model.setValue("root/SendData/Data1/cond11",  paramArr[13]);
	model.setValue("root/SendData/Data1/cond12",  paramArr[14]);

    //SubmitInfo의 Action정보를 동적으로 변경.
    var p_submitInfo = model.submitInfo("reqGetCodeName");
    if (pAction == null) {
        p_submitInfo.action = "/erp/erpcommonweb/ErpCommon.live";
    } else {
        p_submitInfo.action = pAction;
    }

    return model.send("reqGetCodeName");
}

/**
 *  해당컨트롤에 오늘날짜셋팅
 *  @param pCtl : Control ID
 *  @param pDate : 셋팅할 날짜(yyyymmdd)
 *  변경내역     :  2004.12.24 / 최초작성 / ckj
 *                  2005.01.31 / pDate 추가 / ckj
 */
function ERP_FSetToDay(pCtl, pDate)
{
    var toDay;
    if(typeof(pDate) == "undefined") {
        toDay = ERP_FGetToDay();
    } else {
        toDay = pDate;
    }

    var xPath = ERP_FGetXPath(pCtl);

    model.setvalue(xPath, toDay);
    model.refresh();
}

/**
 *  Input Control의 inputMode 속성 변경
 *  @param pCtrlId : Input Control ID
 *  @param pJobGbn : 1/2 (1:영문대문자로 변경, 2:한글로 변경)
 */
function ERP_FChangeInputMode(pCtrlId, pJobGbn) {
    var inp_txt = document.controls(pCtrlId);
    if (pJobGbn == "2") {
        inp_txt.inputMode = "hangul"; //hangul, hangulOnly
    } else if (pJobGbn == "1") {
        inp_txt.inputMode = "upperCase";
    } else if (pJobGbn == "3") {
        inp_txt.inputMode = "upperCase";
    }
    inp_txt.refresh();
}


/**
 *  IP 얻음. - 브라우저객체 사용 시 필요!
 *  변경내역) 2005.08.06 / 최초작성 / ksy
 */
function ERP_FGetIP() {
    var uri = model.getURI ();
    var pos = uri.indexOf("http://");
    var pos2 = uri.indexOf("/", pos + 1 + 7);
    var ip = uri.substring(pos, pos2);
    return ip;
}

/**
 *  Browser객체에 지정한 xfm display
 *  변경내역) 2005.08.06 / 최초작성 / ksy
 */
function ERP_FSetBrowser(pSource) {
    var browserObj = document.controls("browser1");
    var sSource = ERP_FGetIP() + "/erp/erpcommonweb/jsp/ErpCommon_Browser.jsp?src=" + ERP_FGetIP() + pSource;

    browserObj.src = sSource;
    browserObj.refresh();
}

/**
 *  해당컨트롤에 스타일적용
 *  @param pCtl   : Control ID
 *  @param pStyle : 속성(w,h,l,t,c) width, heigth, left, top, bgcolor
 *  @param pNum   : 속성값
 *  변경내역) 2004.12.30 / 최초작성 / ckj
 */
function ERP_FSetCtlStyle(pCtl, pStyle, pNum) {
    var vCtl = document.controls(pCtl);
    switch (pStyle.toUpperCase()){
        case "W" :  // width
            vCtl.attribute("width") = pNum;
            break;
        case "H" :  // heigth
            vCtl.attribute("height") = pNum;
            break;
        case "L" :  // left
            vCtl.attribute("left") = pNum;
            break;
        case "T" :  // top
            vCtl.attribute("top") = pNum;
            break;
        case "C" :  // bgColor
            vCtl.attribute("background-color") = pNum;
            break;
        default :
            alert("구분자 오류!!");
            return;
    }
    model.refresh();
}

/**
 *  컨트롤명으로 바인드된 인스턴스경로 구하기
 *  @param pCtl : 컨트롤명
 *  @return     : 인스턴스경로
 *  변경내역) 2004.12.24 / 최초작성 / ckj
 */
function ERP_FGetXPath(pCtl) {
    var bd = model.bind( document.controls(pCtl).bind );
    return bd.ref;
}

/**
 *  그리드가 아닌 일반 Control에서 우편번호 또는 읍면동의 Validation Check
 *  @param pJobGbn : 1/2 (1:코드, 2:명칭)
 *  @param pCond   : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pCtrlCd : 코드 Control ID
 *  @param pCtrlNm : 명칭 Control ID (▦으로 구분됨.)
 *  @param pCtrlNext : 다음으로 이동할 Control ID
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 */
function ERP_FValidateZipCode(pJobGbn, pCond, pCtrlCd, pCtrlNm, pCtrlNext) { //(★)
    var sCodePath = ERP_FGetXPath(pCtrlCd); //code XPath
    var aCtrlNm = pCtrlNm.split('▦');      //name Control List (Array)
    var iArrSize = aCtrlNm.length;          //array size
    var aNamePath = new Array(iArrSize);    //name XPath List (Array)
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = ERP_FGetXPath(aCtrlNm[i]);
    }

    var sStatId = "comczipcm"; //교육코드 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sControlId = ""; //입력 Control의 ID
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수

    if (pJobGbn == "1") {
        sName = "우편번호"; //(★)
        sControlId = pCtrlCd;
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "읍면동"; //(★)
        sControlId = aCtrlNm[0];
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }
    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;

    if (ERP_FValidate(sStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            model.refresh();
            if (pCtrlNext != null) {
                model.setFocus(pCtrlNext); //다음 입력할 위치로 이동.
            }
        } else if (iCnt > 1) {
            ERP_FOpenZipCodeHelp("", sParam, sCodePath, aNamePath.join("▦")); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.refresh();
            model.setFocus(sControlId);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        model.setFocus(sControlId);
    }
}

/**
 *  그리드에서 우편번호 또는 읍면동의 Validation Check
 *  @param pJobGbn  : 1/2 (1:코드, 2:명칭)
 *  @param pCond    : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pColCd   : 코드 Column
 *  @param pColNm   : 명칭 Column
 *  @param pGridId  : Grid ID
 *  @param pNodeset : Grid Nodeset (XPath)
 *  @param pNextCol : 다음으로 이동할 위치(상대적위치). Optional.
 *  참고사항) (★)표시 : 수정할 부분!
 *  사 용 예) ERP_FValidateGyoYukCodeOfGrid("1", "", "edcode", "edname▦frdate▦todate", "grid1", "/root/MainData/xxList");
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 */
function ERP_FValidateZipCodeOfGrid(pJobGbn, pCond, pColCd, pColNm, pGridId, pNodeset, pNextCol) { //(★)
    var sCodePath = ""; //code XPath
    var aColNm = pColNm.split("▦");
    var iArrSize = aColNm.length;
    var aNamePath = new Array(iArrSize); //name XPath
    var sStatusPath = ""; //status XPath

    var sStatId = "comczipcm"; //우편번호 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수
    var iCurRow, iCurCol, iNextCol;
    var fg = document.controls(pGridId);

    iCurRow = fg.Row - fg.FixedRows + 1; //현재 Row Num.
    iCurCol = fg.Col - fg.FixedCols + 1; //현재 Col Num.
    sCodePath = pNodeset + "[" + iCurRow + "]/" + pColCd;
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = pNodeset + "[" + iCurRow + "]/" + aColNm[i];
    }
    sStatusPath = pNodeset + "[" + iCurRow + "]/status";

    if (pJobGbn == "1") {
        sName = "우편번호"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 2;
        }
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "읍면동"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 1;
        }
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }
    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;

    if (ERP_FValidate(sStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            if (model.getValue(sStatusPath) == "-") {
                model.setValue(sStatusPath, "U");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iNextCol);
        } else if (iCnt > 1) {
            ERP_FOpenZipCodeHelp(pGridId, sParam, sCodePath, aNamePath.join("▦"), sStatusPath); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.gridRefresh(pGridId);
        ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
    }
}

/**
 *  우편번호 Help Window를 Open
 *  @param pGridId   : Grid ID (그리드가 아닌 경우는 "")
 *  @param pParam    : 검색조건 (jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5)
 *  @param pCodePath : 코드 XPath
 *  @param pNamePath : 명칭 XPath
 *  @param pStatusPath : status XPath
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 */
function ERP_FOpenZipCodeHelp(pGridId, pParam, pCodePath, pNamePath, pStatusPath) { //(★)
    var sOldCd = model.getValue(pCodePath);
    var aNamePath = pNamePath.split("▦"); //Array
    var iArrSize = aNamePath.length;
    var g_param = "";
    if (pParam != "") {
        g_param = pParam;
    } else {
        g_param = "2▦2▦"; //jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5
    }
    model.setAttribute("g_param", g_param);
    model.dialog( "text/xml", "/erp/erpcommonweb/xfm/RCM_ZipCodeHelp.xfm", 300, 300, 610, 550 );

    var g_result = model.getAttribute("g_result");

    model.setAttribute("g_result", ""); //전역변수 Clear

    if (g_result != "") {
        var resultArr = g_result.split("▦");
        model.setValue(pCodePath, resultArr[0].replace(/-/,'')); //(★★★)
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], resultArr[i+1]);
        }
        if (pGridId == "") {
            model.refresh();
        } else {
            if (sOldCd != resultArr[0] && model.getValue(pStatusPath) == "-") {
                model.setValue(pStatusPath, "U");
            }
            model.gridRefresh(pGridId);
        }
    }
}


/**
 *  그리드가 아닌 일반 Control에서 부서코드 또는 부서명의 Validation Check
 *  @param pJobGbn : 1/2 (1:코드, 2:명칭)
 *  @param pCond   : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pCtrlCd : 코드 Control ID
 *  @param pCtrlNm : 명칭 Control ID
 *  @param pCtrlNext : 다음으로 이동할 Control ID
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateBuSeo(pJobGbn, pCond, pCtrlCd, pCtrlNm, pCtrlNext) { //(★)
    var sCodePath = ERP_FGetXPath(pCtrlCd); //code XPath
    var aCtrlNm = pCtrlNm.split('▦');      //name Control List (Array)
    var iArrSize = aCtrlNm.length;          //array size
    var aNamePath = new Array(iArrSize);    //name XPath List (Array)
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = ERP_FGetXPath(aCtrlNm[i]);
    }

    var sStatId = "comcdeptm"; //부서코드 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sControlId = ""; //입력 Control의 ID
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수

    if (pJobGbn == "1") {
        sName = "부서코드"; //(★)
        sControlId = pCtrlCd;
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "부서명"; //(★)
        sControlId = aCtrlNm[0];
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }

    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;

    if (ERP_FValidate(sStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            model.refresh();
            if (pCtrlNext != null) {
                model.setFocus(pCtrlNext); //다음 입력할 위치로 이동.
            }
        } else if (iCnt > 1) {
            ERP_FOpenBuSeoHelp("", sParam, sCodePath, aNamePath.join("▦")); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.refresh();
            model.setFocus(sControlId);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        model.setFocus(sControlId);
    }
}

/**
 *  그리드에서 부서코드 또는 부서명의 Validation Check
 *  @param pJobGbn  : 1/2 (1:코드, 2:명칭)
 *  @param pCond    : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pColCd   : 코드 Column
 *  @param pColNm   : 명칭 Column
 *  @param pGridId  : Grid ID
 *  @param pNodeset : Grid Nodeset (XPath)
 *  @param pNextCol : 다음으로 이동할 위치(상대적위치). Optional.
 *  참고사항) (★)표시 : 수정할 부분!
 *  사 용 예) ERP_FValidateBuSeoOfGrid("1", "", "deptcd", "deptnm", "grid1", "/root/MainData/xxList", 1);
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateBuSeoOfGrid(pJobGbn, pCond, pColCd, pColNm, pGridId, pNodeset, pNextCol) { //(★)
    var sStatusPath = ""; //status XPath
    var sCodePath = ""; //code XPath
    var aColNm = pColNm.split("▦");
    var iArrSize = aColNm.length;
    var aNamePath = new Array(iArrSize); //name XPath

    var sStatId = "comcdeptm"; //부서코드 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수
    var iCurRow, iCurCol, iNextCol;
    var fg = document.controls(pGridId);

    iCurRow = fg.Row - fg.FixedRows + 1; //현재 Row Num.
    iCurCol = fg.Col - fg.FixedCols + 1; //현재 Col Num.
    sCodePath = pNodeset + "[" + iCurRow + "]/" + pColCd;
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = pNodeset + "[" + iCurRow + "]/" + aColNm[i];
    }
    sStatusPath = pNodeset + "[" + iCurRow + "]/status";

    if (pJobGbn == "1") {
        sName = "부서코드"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 2;
        }
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "부서명"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 1;
        }
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }
    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;

    if (ERP_FValidate(sStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            if (model.getValue(sStatusPath) == "-") {
                model.setValue(sStatusPath, "U");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iNextCol);
        } else if (iCnt > 1) {
            ERP_FOpenBuSeoHelp(pGridId, sParam, sCodePath, aNamePath.join("▦"), sStatusPath); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.gridRefresh(pGridId);
        ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
    }
}

/**
 *  부서검색 Help Window를 Open
 *  @param pGridId   : Grid ID (그리드가 아닌 경우는 "")
 *  @param pParam    : 검색조건 (jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5)
 *  @param pCodePath : 코드 XPath
 *  @param pNamePath : 명칭 XPath
 *  @param pStatusPath : status XPath
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FOpenBuSeoHelp(pGridId, pParam, pCodePath, pNamePath, pStatusPath) { //(★)
    var sOldCd = model.getValue(pCodePath);
    var aNamePath = pNamePath.split("▦"); //Array
    var iArrSize = aNamePath.length;
    var g_param = "";
    if (pParam != "") {
        g_param = pParam;
    } else {
        g_param = "2▦2▦"; //jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5
    }
    model.setAttribute("g_param", g_param);
    model.dialog( "text/xml", "/erp/erpcommonweb/xfm/RCM_BuSeoHelp.xfm", 300, 300, 610, 580 ); //(★)

    var g_result = model.getAttribute("g_result");
    model.setAttribute("g_result", ""); //전역변수 Clear

    if (g_result != "") {
        var resultArr = g_result.split("▦");
        model.setValue(pCodePath, resultArr[0]);
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], resultArr[i+1]);
        }
        if (pGridId == "") {
            model.refresh();
        } else {
            if (sOldCd != resultArr[0] && model.getValue(pStatusPath) == "-") {
                model.setValue(pStatusPath, "U");
            }
            model.gridRefresh(pGridId);
        }
    }
}

/**
 *  그리드가 아닌 일반 Control에서 사원번호 또는 사원명의 Validation Check
 *  @param pJobGbn : 1/2 (1:코드, 2:명칭)
 *  @param pCond   : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pCtrlCd : 코드 Control ID
 *  @param pCtrlNm : 명칭 Control ID
 *  @param pCtrlNext : 다음으로 이동할 Control ID
 *  @param pHelpGbn  : 사원검색 Help중 어떤 것을 띄울지에 대한 정보. Optional
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateSaWeon(pJobGbn, pCond, pCtrlCd, pCtrlNm, pCtrlNext, pHelpGbn) { //(★)
    var sCodePath = ERP_FGetXPath(pCtrlCd); //code XPath
    var aCtrlNm = pCtrlNm.split('▦');      //name Control List (Array)
    var iArrSize = aCtrlNm.length;          //array size
    var aNamePath = new Array(iArrSize);    //name XPath List (Array)
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = ERP_FGetXPath(aCtrlNm[i]);
    }

    var sStatId = "rppbemplm"; //인사기본정보 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sControlId = ""; //입력 Control의 ID
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수

    if (pJobGbn == "1") {
        sName = "사원번호"; //(★)
        sControlId = pCtrlCd;
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "사원명"; //(★)
        sControlId = aCtrlNm[0];
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }
    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;

    if (ERP_FValidate(sStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
				//alert("i : " + i +  " aNamePath : " + aNamePath[i] + "  resultArr : " + model.getValue("/root/HideData/codeName/name" + (i + 1)) );
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            model.refresh();
            if (pCtrlNext != null) {
                model.setFocus(pCtrlNext); //다음 입력할 위치로 이동.
            }
        } else if (iCnt > 1) {
            if (pHelpGbn == null) {
                ERP_FOpenSaWeonHelp("", sParam, sCodePath, aNamePath.join("▦")); //(★)
            } else {
                ERP_FOpenSaWeonHelp2("", sParam, sCodePath, aNamePath.join("▦")); //(★)
            }
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.refresh();
            model.setFocus(sControlId);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        model.setFocus(sControlId);
    }
}

/**
 *  그리드에서 사원번호 또는 사원명의 Validation Check
 *  @param pJobGbn  : 1/2 (1:코드, 2:명칭)
 *  @param pCond    : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pColCd   : 코드 Column
 *  @param pColNm   : 명칭 Column
 *  @param pGridId  : Grid ID
 *  @param pNodeset : Grid Nodeset (XPath)
 *  @param pNextCol : 다음으로 이동할 위치(상대적위치). Optional.
 *  @param pHelpGbn  : 사원검색 Help중 어떤 것을 띄울지에 대한 정보. Optional
 *  참고사항) (★)표시 : 수정할 부분!
 *  사 용 예) ERP_FValidateBuSeoOfGrid("1", "", "deptcd", "deptnm", "grid1", "/root/MainData/xxList");
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateSaWeonOfGrid(pJobGbn, pCond, pColCd, pColNm, pGridId, pNodeset, pNextCol, pHelpGbn) { //(★)
    var sStatusPath = ""; //status XPath
    var sCodePath = ""; //code XPath
    var aColNm = pColNm.split("▦");
    var iArrSize = aColNm.length;
    var aNamePath = new Array(iArrSize); //name XPath

    var sStatId = "rppbemplm"; //부서코드 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수
    var iCurRow, iCurCol, iNextCol;
    var fg = document.controls(pGridId);

    iCurRow = fg.Row - fg.FixedRows + 1; //현재 Row Num.
    iCurCol = fg.Col - fg.FixedCols + 1; //현재 Col Num.
    sCodePath = pNodeset + "[" + iCurRow + "]/" + pColCd;
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = pNodeset + "[" + iCurRow + "]/" + aColNm[i];
    }
    sStatusPath = pNodeset + "[" + iCurRow + "]/status";

    if (pJobGbn == "1") {
        sName = "사원번호"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 2;
        }
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "사원명"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 1;
        }
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }
    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;

    if (ERP_FValidate(sStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            if (model.getValue(sStatusPath) == "-") {
                model.setValue(sStatusPath, "U");
            }
            model.gridRefresh(pGridId);
             ERP_FGridSetFocus(pGridId, iCurRow, iNextCol);
        } else if (iCnt > 1) {
            if (pHelpGbn == null) {
                ERP_FOpenSaWeonHelp(pGridId, sParam, sCodePath, aNamePath.join("▦"), sStatusPath); //(★)
            } else {
                ERP_FOpenSaWeonHelp2(pGridId, sParam, sCodePath, aNamePath.join("▦"), sStatusPath); //(★)
            }
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.gridRefresh(pGridId);
        ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
    }
}

/**
 *  사원검색 Help Window를 Open
 *  @param pGridId   : Grid ID (그리드가 아닌 경우는 "")
 *  @param pParam    : 검색조건 (jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5)
 *  @param pCodePath : 코드 XPath
 *  @param pNamePath : 명칭 XPath
 *  @param pStatusPath : status XPath
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FOpenSaWeonHelp(pGridId, pParam, pCodePath, pNamePath, pStatusPath) { //(★)
    var sOldCd = model.getValue(pCodePath);
    var aNamePath = pNamePath.split("▦"); //Array
    var iArrSize = aNamePath.length;
    var g_param = "";
    if (pParam != "") {
        g_param = pParam;
    } else {
        g_param = "2▦2▦"; //jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5
    }
    model.setAttribute("g_param", g_param);
    model.dialog( "text/xml", "/erp/erpcommonweb/xfm/RCM_SaWeonHelp.xfm", 300, 300, 610, 580 ); //(★)
    //model.dialog( "text/xml", "./RPP_SaWeonHelp.xfm", 100, 100, 750, 580 );

    var g_result = model.getAttribute("g_result");
    model.setAttribute("g_result", ""); //전역변수 Clear

    if (g_result != "") {
        var resultArr = g_result.split("▦");
        model.setValue(pCodePath, resultArr[0]);
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], resultArr[i+1]);
        }
        if (pGridId == "") {
            model.refresh();
        } else {
            if (sOldCd != resultArr[0] && model.getValue(pStatusPath) == "-") {
                model.setValue(pStatusPath, "U");
            }
            model.gridRefresh(pGridId);
        }
    }
}

/**
 *  사원검색 Help Window를 Open
 *  @param pGridId   : Grid ID (그리드가 아닌 경우는 "")
 *  @param pParam    : 검색조건 (jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5)
 *  @param pCodePath : 코드 XPath
 *  @param pNamePath : 명칭 XPath
 *  @param pStatusPath : status XPath
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FOpenSaWeonHelp2(pGridId, pParam, pCodePath, pNamePath, pStatusPath) { //(★)
    var sOldCd = model.getValue(pCodePath);
    var aNamePath = pNamePath.split("▦"); //Array
    var iArrSize = aNamePath.length;
    var g_param = "";
    if (pParam != "") {
        g_param = pParam;
    } else {
        g_param = "2▦2▦"; //jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5
    }
    model.setAttribute("g_param", g_param);
    model.dialog( "text/xml", "/erp/insaweb/xfm/RPP_SaWeonHelp.xfm", 300, 300, 749, 560 );

    var g_result = model.getAttribute("g_result");
    model.setAttribute("g_result", ""); //전역변수 Clear

    if (g_result != "") {
        g_result = g_result.replace(/▩/g, "");
        var resultArr = g_result.split("▦");
        model.setValue(pCodePath, resultArr[0]);
        for (i=0; i<iArrSize; i++) {
			//alert("i : " + i +  " aNamePath : " + aNamePath[i] + "  resultArr : " + resultArr[i+1] );
            model.setValue(aNamePath[i], resultArr[i+1]);
        }
        if (pGridId == "") {
            model.refresh();
        } else {
            if (sOldCd != resultArr[0] && model.getValue(pStatusPath) == "-") {
                model.setValue(pStatusPath, "U");
            }
            model.gridRefresh(pGridId);
        }
    }
}

/**
 *  [KMS] 교육용 -사원검색 Help Window를 Open
 *  @param pGridId   : Grid ID (그리드가 아닌 경우는 "")
 *  @param pParam    : 검색조건 (jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5)
 *  @param pCodePath : 코드 XPath
 *  @param pNamePath : 명칭 XPath
 *  @param pStatusPath : status XPath
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2010.07.22 / 최초작성 / 양두진
 *          
 */
function ERP_FOpenSaWeonGyoYukHelp(pGridId, pParam, pCodePath, pNamePath, pStatusPath) { //(★)
    var sOldCd = model.getValue(pCodePath);
    var aNamePath = pNamePath.split("▦"); //Array
    var iArrSize = aNamePath.length;
    var g_param = "";
    
    if (pParam != "") {
        g_param = pParam;
    } else {
        g_param = "2▦2▦"; //jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5
    }
    
    model.setAttribute("g_param", g_param);
    model.dialog( "text/xml", "/erp/erpcommonweb/xfm/RCM_SaWeonGyoYukHelp.xfm", 300, 300, 700, 530); //(★)
    
    var g_result = model.getAttribute("g_result");
    model.setAttribute("g_result", ""); //전역변수 Clear

    if (g_result != "") {
        var resultArr = g_result.split("▦");
        model.setValue(pCodePath, resultArr[0]);
        
        model.setValue(aNamePath[0], resultArr[1]);
        model.setValue(aNamePath[1], resultArr[2]);
        model.setValue(aNamePath[2], resultArr[4]);
        model.setValue(aNamePath[3], resultArr[5]);
        model.setValue(aNamePath[4], resultArr[6]);
        model.setValue(aNamePath[5], resultArr[7]);
        model.setValue(aNamePath[6], resultArr[8]);
        model.setValue(aNamePath[7], resultArr[9]);
        model.setValue(aNamePath[8], resultArr[10]);
		model.setValue(aNamePath[9], resultArr[11]);
        
        if (pGridId == "") {
            model.refresh();
        } else {
            if (sOldCd != resultArr[0] && model.getValue(pStatusPath) == "-") {
                model.setValue(pStatusPath, "U");
            }
            model.gridRefresh(pGridId);
        }
    }
}

/**
 *  그리드가 아닌 일반 Control에서 등록번호 또는 환자명의 Validation Check
 *  @param pJobGbn : 1/2 (1:코드, 2:명칭)
 *  @param pCond   : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pCtrlCd : 코드 Control ID
 *  @param pCtrlNm : 명칭 Control ID
 *  @param pCtrlNext : 다음으로 이동할 Control ID
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateHwanJa(pJobGbn, pCond, pCtrlCd, pCtrlNm, pCtrlNext) { //(★)
    var sCodePath = ERP_FGetXPath(pCtrlCd); //code XPath
    var aCtrlNm = pCtrlNm.split('▦');      //name Control List (Array)
    var iArrSize = aCtrlNm.length;          //array size
    var aNamePath = new Array(iArrSize);    //name XPath List (Array)
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = ERP_FGetXPath(aCtrlNm[i]);
    }

    var sStatId = "opmcptbsm"; //부서코드 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sControlId = ""; //입력 Control의 ID
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수

    if (pJobGbn == "1") {
        sName = "등록번호"; //(★)
        sControlId = pCtrlCd;
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "환자명"; //(★)
        sControlId = aCtrlNm[0];
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }
    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;

    if (ERP_FValidate(sStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            model.refresh();
            if (pCtrlNext != null) {
                model.setFocus(pCtrlNext); //다음 입력할 위치로 이동.
            }
        } else if (iCnt > 1) {
            ERP_FOpenHwanJaHelp("", sParam, sCodePath, aNamePath.join("▦")); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.refresh();
            model.setFocus(sControlId);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        model.setFocus(sControlId);
    }
}

/**
 *  그리드에서 등록번호 또는 환자명의 Validation Check
 *  @param pJobGbn  : 1/2 (1:코드, 2:명칭)
 *  @param pCond    : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pColCd   : 코드 Column
 *  @param pColNm   : 명칭 Column
 *  @param pGridId  : Grid ID
 *  @param pNodeset : Grid Nodeset (XPath)
 *  @param pNextCol : 다음으로 이동할 위치(상대적위치). Optional.
 *  참고사항) (★)표시 : 수정할 부분!
 *  사 용 예) ERP_FValidateHwanJaOfGrid("1", "", "pid", "kornm", "grid1", "/root/MainData/xxList", 1);
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateHwanJaOfGrid(pJobGbn, pCond, pColCd, pColNm, pGridId, pNodeset, pNextCol) { //(★)
    var sStatusPath = ""; //status XPath
    var sCodePath = ""; //code XPath
    var aColNm = pColNm.split("▦");
    var iArrSize = aColNm.length;
    var aNamePath = new Array(iArrSize); //name XPath

    var sStatId = "opmcptbsm"; //부서코드 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수
    var iCurRow, iCurCol, iNextCol;
    var fg = document.controls(pGridId);

    iCurRow = fg.Row - fg.FixedRows + 1; //현재 Row Num.
    iCurCol = fg.Col - fg.FixedCols + 1; //현재 Col Num.
    sCodePath = pNodeset + "[" + iCurRow + "]/" + pColCd;
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = pNodeset + "[" + iCurRow + "]/" + aColNm[i];
    }
    sStatusPath = pNodeset + "[" + iCurRow + "]/status";

    if (pJobGbn == "1") {
        sName = "등록번호"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 2;
        }
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "환자명"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 1;
        }
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }
    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;

    if (ERP_FValidate(sStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            if (model.getValue(sStatusPath) == "-") {
                model.setValue(sStatusPath, "U");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iNextCol);
        } else if (iCnt > 1) {
            ERP_FOpenHwanJaHelp(pGridId, sParam, sCodePath, aNamePath.join("▦"), sStatusPath); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.gridRefresh(pGridId);
        ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
    }
}

/**
 *  환자검색 Help Window를 Open
 *  @param pGridId   : Grid ID (그리드가 아닌 경우는 "")
 *  @param pParam    : 검색조건 (jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5)
 *  @param pCodePath : 코드 XPath
 *  @param pNamePath : 명칭 XPath
 *  @param pStatusPath : status XPath
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FOpenHwanJaHelp(pGridId, pParam, pCodePath, pNamePath, pStatusPath) { //(★)
    var sOldCd = model.getValue(pCodePath);
    var aNamePath = pNamePath.split("▦"); //Array
    var iArrSize = aNamePath.length;
    var g_param = "";
    if (pParam != "") {
        g_param = pParam;
    } else {
        g_param = "2▦2▦"; //jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5
    }
    model.setAttribute("g_param", g_param);
    model.dialog( "text/xml", "/erp/erpcommonweb/xfm/RCM_HwanJaHelp.xfm", 300, 300, 610, 580 ); //(★)

    var g_result = model.getAttribute("g_result");
    model.setAttribute("g_result", ""); //전역변수 Clear

    if (g_result != "") {
        var resultArr = g_result.split("▦");
        model.setValue(pCodePath, resultArr[0]);
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], resultArr[i+1]);
        }
        if (pGridId == "") {
            model.refresh();
        } else {
            if (sOldCd != resultArr[0] && model.getValue(pStatusPath) == "-") {
                model.setValue(pStatusPath, "U");
            }
            model.gridRefresh(pGridId);
        }
    }
}

/**
 *  그리드가 아닌 일반 Control에서 계정코드 또는 계정명의 Validation Check
 *  @param pJobGbn : 1/2 (1:코드, 2:명칭)
 *  @param pCond   : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pCtrlCd : 코드 Control ID
 *  @param pCtrlNm : 명칭 Control ID
 *  @param pCtrlNext : 다음으로 이동할 Control ID
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ckj
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateGyeJeong(pJobGbn, pCond, pCtrlCd, pCtrlNm, pCtrlNext) { //(★)
    var sCodePath = ERP_FGetXPath(pCtrlCd); //code XPath
    var aCtrlNm = pCtrlNm.split('▦');      //name Control List (Array)
    var iArrSize = aCtrlNm.length;          //array size
    var aNamePath = new Array(iArrSize);    //name XPath List (Array)
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = ERP_FGetXPath(aCtrlNm[i]);
    }

    var sStatId = "rfacacntm"; //계정코드 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sControlId = ""; //입력 Control의 ID
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수

    if (pJobGbn == "1") {
        sName = "계정코드"; //(★)
        sControlId = pCtrlCd;
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "계정명"; //(★)
        sControlId = aCtrlNm[0];
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }

    if (pCond == "") {
        pCond =  "%" + "▦" + "%" + "▦" + "0";  // 계정구분, 예산사용여부, 스크롤여부 0
    }

    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;
    if (ERP_FValidate(sStatId, sParam)) {

        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));

        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            model.refresh();
            if (pCtrlNext != null) {
                model.setFocus(pCtrlNext); //다음 입력할 위치로 이동.
            }
        } else if (iCnt > 1) {
            ERP_FOpenGyeJeongHelp("", sParam, sCodePath, aNamePath.join("▦")); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.refresh();
            model.setFocus(sControlId);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        model.setFocus(sControlId);
    }
}

/**
 *  그리드에서 계정코드 또는 계정명의 Validation Check
 *  @param pJobGbn  : 1/2 (1:코드, 2:명칭)
 *  @param pCond    : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pColCd   : 코드 Column
 *  @param pColNm   : 명칭 Column
 *  @param pGridId  : Grid ID
 *  @param pNodeset : Grid Nodeset (XPath)
 *  @param pNextCol : 다음으로 이동할 위치(상대적위치). Optional.
 *  참고사항) (★)표시 : 수정할 부분!
 *  사 용 예) ERP_FValidateGyeJeongOfGrid("1", "", "deptcd", "deptnm", "grid1", "/root/MainData/xxList");
 *  변경내역) 2005.01.06 / 최초작성 / ckj
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateGyeJeongOfGrid(pJobGbn, pCond, pColCd, pColNm, pGridId, pNodeset, pNextCol) { //(★)
    var sStatusPath = ""; //status XPath
    var sCodePath = ""; //code XPath
    var aColNm = pColNm.split("▦");
    var iArrSize = aColNm.length;
    var aNamePath = new Array(iArrSize); //name XPath

    var sStatId = "rfacacntm"; // 계정코드 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수
    var iCurRow, iCurCol, iNextCol;
    var fg = document.controls(pGridId);

    iCurRow = fg.Row - fg.FixedRows + 1; //현재 Row Num.
    iCurCol = fg.Col - fg.FixedCols + 1; //현재 Col Num.
    sCodePath = pNodeset + "[" + iCurRow + "]/" + pColCd;
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = pNodeset + "[" + iCurRow + "]/" + aColNm[i];
    }
    sStatusPath = pNodeset + "[" + iCurRow + "]/status";

    if (pJobGbn == "1") {
        sName = "계정코드"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 2;
        }
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "계정명"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 1;
        }
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }

    if (pCond == "") {
        pCond =  "%" + "▦" + "%" + "▦" + "0";  // 계정구분, 예산사용여부, 스크롤여부 0
    }

    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;

    if (ERP_FValidate(sStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            if (model.getValue(sStatusPath) == "-") {
                model.setValue(sStatusPath, "U");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iNextCol);
        } else if (iCnt > 1) {
            ERP_FOpenGyeJeongHelp(pGridId, sParam, sCodePath, aNamePath.join("▦"), sStatusPath); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.gridRefresh(pGridId);
        ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
    }
}

/**
 *  계정코드검색 Help Window를 Open
 *  @param pGridId   : Grid ID (그리드가 아닌 경우는 "")
 *  @param pParam    : 검색조건 (jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5)
 *                     cond1 : 계정구분 ( % 전체, 1 자산, 2 부체, 3 자본, 4 수익, 5 비용 )
 *                     cond2 : 예산사용여부 ( % 전체, Y 사용, N 미사용 )
 *                     rowcnt: 스크롤여부로써 0이면 기본으로 50건 조회한다.
 *  @param pCodePath : 코드 XPath
 *  @param pNamePath : 명칭 XPath
 *  @param pStatusPath : status XPath
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ckj
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FOpenGyeJeongHelp(pGridId, pParam, pCodePath, pNamePath, pStatusPath) { //(★)
    var sOldCd = model.getValue(pCodePath);
    var aNamePath = pNamePath.split("▦"); //Array
    var iArrSize = aNamePath.length;
    var g_param = "";
    if (pParam != "") {
        g_param = pParam;
    } else {
        g_param = "2▦2▦"; //jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5
    }

    model.setAttribute("g_param", g_param);
    model.dialog( "text/xml", "/erp/erpcommonweb/xfm/RCM_GyeJeongKDHelp.xfm", 400, 300, 650, 600); //(★)

    var g_result = model.getAttribute("g_result");
    model.setAttribute("g_result", ""); //전역변수 Clear

    if (g_result != "") {
        var resultArr = g_result.split("▦");
        model.setValue(pCodePath, resultArr[0]);
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], resultArr[i+1]);
        }
        if (pGridId == "") {
            model.refresh();
        } else {
            if (sOldCd != resultArr[0] && model.getValue(pStatusPath) == "-") {
                model.setValue(pStatusPath, "U");
            }
            model.gridRefresh(pGridId);
        }
    }
}

/**
 *  그리드가 아닌 일반 Control에서 사업자등록번호 또는 거래처명의 Validation Check
 *  @param pJobGbn : 1/2 (1:코드, 2:명칭)
 *  @param pCond   : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pCtrlCd : 코드 Control ID
 *  @param pCtrlNm : 명칭 Control ID
 *  @param pCtrlNext : 다음으로 이동할 Control ID
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ksy
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateGeoRaeCheo(pJobGbn, pCond, pCtrlCd, pCtrlNm, pCtrlNext) { //(★)
    var sCodePath = ERP_FGetXPath(pCtrlCd); //code XPath
    var aCtrlNm = pCtrlNm.split('▦');      //name Control List (Array)
    var iArrSize = aCtrlNm.length;          //array size
    var aNamePath = new Array(iArrSize);    //name XPath List (Array)
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = ERP_FGetXPath(aCtrlNm[i]);
    }

    var sStatId = "rfaccustm"; //사업자등록번호 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sControlId = ""; //입력 Control의 ID
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수

    if (pJobGbn == "1") {
        sName = "사업자등록번호"; //(★)
        sControlId = pCtrlCd;
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "거래처명"; //(★)
        sControlId = aCtrlNm[0];
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }

    if (pCond == "") {
        pCond = "N";  // 거래종료된것은 제외
    }

    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;
    if (ERP_FValidate(sStatId, sParam)) {

        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));

        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            model.refresh();
            if (pCtrlNext != null) {
                model.setFocus(pCtrlNext); //다음 입력할 위치로 이동.
            }
        } else if (iCnt > 1) {
            ERP_FOpenGeoRaeCheoHelp("", sParam, sCodePath, aNamePath.join("▦")); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.refresh();
            model.setFocus(sControlId);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        model.setFocus(sControlId);
    }
}

/**
 *  그리드에서 사업자등록번호 또는 거래처명의 Validation Check
 *  @param pJobGbn  : 1/2 (1:코드, 2:명칭)
 *  @param pCond    : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pColCd   : 코드 Column
 *  @param pColNm   : 명칭 Column
 *  @param pGridId  : Grid ID
 *  @param pNodeset : Grid Nodeset (XPath)
 *  @param pNextCol : 다음으로 이동할 위치(상대적위치). Optional.
 *  참고사항) (★)표시 : 수정할 부분!
 *  사 용 예) ERP_FValidateBuSeoOfGrid("1", "", "deptcd", "deptnm", "grid1", "/root/MainData/xxList");
 *  변경내역) 2005.01.06 / 최초작성 / ckj
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateGeoRaeCheoOfGrid(pJobGbn, pCond, pColCd, pColNm, pGridId, pNodeset, pNextCol) { //(★)
    var sStatusPath = ""; //status XPath
    var sCodePath = ""; //code XPath
    var aColNm = pColNm.split("▦");
    var iArrSize = aColNm.length;
    var aNamePath = new Array(iArrSize); //name XPath

    var sStatId = "rfaccustm"; // 거래처 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수
    var iCurRow, iCurCol, iNextCol;
    var fg = document.controls(pGridId);

    iCurRow = fg.Row - fg.FixedRows + 1; //현재 Row Num.
    iCurCol = fg.Col - fg.FixedCols + 1; //현재 Col Num.
    sCodePath = pNodeset + "[" + iCurRow + "]/" + pColCd;
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = pNodeset + "[" + iCurRow + "]/" + aColNm[i];
    }
    sStatusPath = pNodeset + "[" + iCurRow + "]/status";

    if (pJobGbn == "1") {
        sName = "사업자등록번호"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 2;
        }
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "거래처명"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 1;
        }
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }
    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;

    if (ERP_FValidate(sStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            if (model.getValue(sStatusPath) == "-") {
                model.setValue(sStatusPath, "U");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iNextCol);
        } else if (iCnt > 1) {
            ERP_FOpenGeoRaeCheoHelp(pGridId, sParam, sCodePath, aNamePath.join("▦"), sStatusPath); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.gridRefresh(pGridId);
        ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
    }
}

/**
 *  거래처검색 Help Window를 Open
 *  @param pGridId   : Grid ID (그리드가 아닌 경우는 "")
 *  @param pParam    : 검색조건 (jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5)
 *  @param pCodePath : 코드 XPath
 *  @param pNamePath : 명칭 XPath
 *  @param pStatusPath : status XPath
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ckj
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FOpenGeoRaeCheoHelp(pGridId, pParam, pCodePath, pNamePath, pStatusPath) { //(★)
    var sOldCd = model.getValue(pCodePath);
    var aNamePath = pNamePath.split("▦"); //Array
    var iArrSize = aNamePath.length;
    var g_param = "";
    if (pParam != "") {
        g_param = pParam;
    } else {
        g_param = "2▦2▦"; //jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5
    }

    model.setAttribute("g_param", g_param);
    model.dialog( "text/xml", "/erp/erpcommonweb/xfm/RCM_GeoRaeCheoHelp.xfm", 400, 300, 610, 600); //(★)

    var g_result = model.getAttribute("g_result");
    model.setAttribute("g_result", ""); //전역변수 Clear

    if (g_result != "") {
        var resultArr = g_result.split("▦");
        model.setValue(pCodePath, resultArr[0]);
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], resultArr[i+1]);
        }
        if (pGridId == "") {
            model.refresh();
        } else {
            if (sOldCd != resultArr[0] && model.getValue(pStatusPath) == "-") {
                model.setValue(pStatusPath, "U");
            }
            model.gridRefresh(pGridId);
        }
    }
}

/**
 *  그리드가 아닌 일반 Control에서 공통코드 또는 코드명의 Validation Check
 *  @param pJobGbn : 1/2 (1:코드, 2:명칭)
 *  @param pCond   : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pCtrlCd : 코드 Control ID
 *  @param pCtrlNm : 명칭 Control ID
 *  @param pCtrlNext : 다음으로 이동할 Control ID
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ckj
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateGongTongCode(pJobGbn, pCond, pCtrlCd, pCtrlNm, pCtrlNext) { //(★)
    var sCodePath = ERP_FGetXPath(pCtrlCd); //code XPath
    var aCtrlNm = pCtrlNm.split('▦');      //name Control List (Array)
    var iArrSize = aCtrlNm.length;          //array size
    var aNamePath = new Array(iArrSize);    //name XPath List (Array)
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = ERP_FGetXPath(aCtrlNm[i]);
    }

    var sStatId = "comccodem"; //공통코드 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sControlId = ""; //입력 Control의 ID
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수

    if (pJobGbn == "1") {
        sName = "공통코드"; //(★)
        sControlId = pCtrlCd;
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "공통코드명"; //(★)
        sControlId = aCtrlNm[0];
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }

    if (pCond == "") {
        alert("CODEFLAG를 지정하시오.");
        return;
    }

    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;
    if (ERP_FValidate(sStatId, sParam)) {

        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));

        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            model.refresh();
            if (pCtrlNext != null) {
                model.setFocus(pCtrlNext); //다음 입력할 위치로 이동.
            }
        } else if (iCnt > 1) {
            ERP_FOpenGongTongCodeHelp("", sParam, sCodePath, aNamePath.join("▦")); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.refresh();
            model.setFocus(sControlId);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        model.setFocus(sControlId);
    }
}

/**
 *  그리드에서 공통코드 또는 공통코드명의 Validation Check
 *  @param pJobGbn  : 1/2 (1:코드, 2:명칭)
 *  @param pCond    : 추가 검색조건 (cond1▦cond2▦...▦cond5)
 *  @param pColCd   : 코드 Column
 *  @param pColNm   : 명칭 Column
 *  @param pGridId  : Grid ID
 *  @param pNodeset : Grid Nodeset (XPath)
 *  @param pNextCol : 다음으로 이동할 위치(상대적위치). Optional.
 *  참고사항) (★)표시 : 수정할 부분!
 *  사 용 예) ERP_FValidateGongTongCodeOfGrid("1", "", "deptcd", "deptnm", "grid1", "/root/MainData/xxList");
 *  변경내역) 2005.01.06 / 최초작성 / ckj
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FValidateGongTongCodeOfGrid(pJobGbn, pCond, pColCd, pColNm, pGridId, pNodeset, pNextCol) { //(★)
    var sStatusPath = ""; //status XPath
    var sCodePath = ""; //code XPath
    var aColNm = pColNm.split("▦");
    var iArrSize = aColNm.length;
    var aNamePath = new Array(iArrSize); //name XPath

    var sStatId = "comccodem"; // 공통코드 Statement ID (★)
    var sName = "";      //입력 항목 Name
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수
    var iCurRow, iCurCol, iNextCol;
    var fg = document.controls(pGridId);

    iCurRow = fg.Row - fg.FixedRows + 1; //현재 Row Num.
    iCurCol = fg.Col - fg.FixedCols + 1; //현재 Col Num.
    sCodePath = pNodeset + "[" + iCurRow + "]/" + pColCd;
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = pNodeset + "[" + iCurRow + "]/" + aColNm[i];
    }
    sStatusPath = pNodeset + "[" + iCurRow + "]/status";

    if (pJobGbn == "1") {
        sName = "공통코드"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 2;
        }
        sSrhTxt = model.getValue(sCodePath);
    } else {
        sName = "공통코드명"; //(★)
        if (pNextCol != null) {
            iNextCol = iCurCol + pNextCol;
        } else {
            iNextCol = iCurCol + 1;
        }
        sSrhTxt = model.getValue(aNamePath[0]);
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }
    sParam = pJobGbn + "▦" + pJobGbn + "▦" + sSrhTxt + "▦" + pCond;

    if (ERP_FValidate(sStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            model.setValue(sCodePath, model.getValue("/root/HideData/codeName/code"));
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], model.getValue("/root/HideData/codeName/name" + (i + 1)));
            }
            if (model.getValue(sStatusPath) == "-") {
                model.setValue(sStatusPath, "U");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iNextCol);
        } else if (iCnt > 1) {
            ERP_FOpenGongTongCodeHelp(pGridId, sParam, sCodePath, aNamePath.join("▦"), sStatusPath); //(★)
        } else {
            TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "입력오류", "E", "OK");
            model.setValue(sCodePath, "");
            for (i=0; i<iArrSize; i++) {
                model.setValue(aNamePath[i], "");
            }
            model.gridRefresh(pGridId);
            ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
        }
    } else {
        TF.window.getMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 " + sName + " 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.gridRefresh(pGridId);
        ERP_FGridSetFocus(pGridId, iCurRow, iCurCol);
    }
}

/**
 *  공통코드검색 Help Window를 Open
 *  @param pGridId   : Grid ID (그리드가 아닌 경우는 "")
 *  @param pParam    : 검색조건 (jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5)
 *  @param pCodePath : 코드 XPath
 *  @param pNamePath : 명칭 XPath
 *  @param pStatusPath : status XPath
 *  참고사항) (★)표시 : 수정할 부분!
 *  변경내역) 2005.01.06 / 최초작성 / ckj
 *           2005.01.07 / code, name 외 추가 정보도 받을 수 있도록 수정 / ksy
 */
function ERP_FOpenGongTongCodeHelp(pGridId, pParam, pCodePath, pNamePath, pStatusPath) { //(★)
    var sOldCd = model.getValue(pCodePath);
    var aNamePath = pNamePath.split("▦"); //Array
    var iArrSize = aNamePath.length;
    var g_param = "";
    if (pParam != "") {
        g_param = pParam;
    } else {
        g_param = "2▦2▦"; //jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5
    }

    model.setAttribute("g_param", g_param);
    model.dialog( "text/xml", "/erp/erpcommonweb/xfm/RCM_GongTongCodeHelp.xfm", 400, 300, 610, 600); //(★)

    var g_result = model.getAttribute("g_result");
    model.setAttribute("g_result", ""); //전역변수 Clear

    if (g_result != "") {
        var resultArr = g_result.split("▦");
        model.setValue(pCodePath, resultArr[0]);
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], resultArr[i+1]);
        }
        if (pGridId == "") {
            model.refresh();
        } else {
            if (sOldCd != resultArr[0] && model.getValue(pStatusPath) == "-") {
                model.setValue(pStatusPath, "U");
            }
            model.gridRefresh(pGridId);
        }
    }
}


/**
 *  사용자검색(comcuserm) Help Window를 Open
 *  @param pGridId   : Grid ID (그리드가 아닌 경우는 "")
 *  @param pParam    : 검색조건 (jobgbn▦srtgbn▦srhtxt▦cond1▦...▦cond5)
 *  @param pCodePath : 코드 XPath
 *  @param pNamePath : 명칭 XPath
 *  @param pStatusPath : status XPath
 *  변경내역) 2011.06.02 / 최초작성 / leejp
 */
function ERP_FOpenUserHelp(pGridId, pParam, pCodePath, pNamePath, pStatusPath) {
    var sOldCd = model.getValue(pCodePath);
    var aNamePath = pNamePath.split("▦");
    var iArrSize = aNamePath.length;
    var g_param = "";

    if (pParam != "") {
        g_param = pParam;
    } else {
        g_param = "02▦▦";
    }

    model.setAttribute("COM_UserCodeHelp_parm", g_param);
    model.dialog( "text/xml", "/com/gibonjeongboweb/xfm/COM_UserCodeHelp.xfm", 300, 300, 485, 408 );

	var sCOM_UserCodeHelp_rtnflag = model.getAttribute("COM_UserCodeHelp_rtnflag");	// ID
	var sCOM_UserCodeHelp_rtnparm = model.getAttribute("COM_UserCodeHelp_rtnparm");	// Name

	if (sCOM_UserCodeHelp_rtnflag == "Y") {
		model.setValue(pCodePath, TFGetMatrixData(sCOM_UserCodeHelp_rtnparm, 0, 0));
		model.setValue(pNamePath, TFGetMatrixData(sCOM_UserCodeHelp_rtnparm, 0, 1));

        if (pGridId == "") {
            model.refresh();
        } else {
            model.gridRefresh(pGridId);
        }

	}
}

/**
 *  그리드가 아닌 일반 Control에서 사원번호 또는 사원명의 Validation Check (comcuserm)
 *  @param pJobGbn : 1/2 (1:코드, 2:명칭)
 *  @param pCond   : 추가 검색조건
 *  @param pCtrlCd : 코드 Control ID
 *  @param pCtrlNm : 명칭 Control ID
 *  @param pCtrlNext : 다음으로 이동할 Control ID
 *  @param pHelpGbn  : 사원검색 Help중 어떤 것을 띄울지에 대한 정보. Optional
 *  변경내역) 2011.06.02 / 최초작성 / leejp
 */
function ERP_FValidateUser(pJobGbn, pCond, pCtrlCd, pCtrlNm, pCtrlNext, pHelpGbn) { //(★)
    var sCodePath = ERP_FGetXPath(pCtrlCd); //code XPath
	var sNamePath = ERP_FGetXPath(pCtrlNm); //code XPath

    var aCtrlNm = pCtrlNm.split('▦');      //name Control List (Array)
    var iArrSize = aCtrlNm.length;          //array size
    var aNamePath = new Array(iArrSize);    //name XPath List (Array)
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = ERP_FGetXPath(aCtrlNm[i]);
    }

	var setFlg = "";
    var sControlId = "";
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";

    if (pJobGbn == "1") {
        sControlId = pCtrlCd;
        sSrhTxt = model.getValue(sCodePath);
		setFlg = "02";
    } else {
        sControlId = aCtrlNm[0];
        sSrhTxt = model.getValue(aNamePath[0]);
		setFlg = "03";
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }

	sParam = setFlg + "▦" + sSrhTxt + "▦▦";

	model.setAttribute("COM_UserCodeHelp_parm", sParam);
    model.dialog( "text/xml", "/com/gibonjeongboweb/xfm/COM_UserCodeHelp.xfm", 300, 300, 485, 408 );

	var sCOM_UserCodeHelp_rtnflag = model.getAttribute("COM_UserCodeHelp_rtnflag");	// ID
	var sCOM_UserCodeHelp_rtnparm = model.getAttribute("COM_UserCodeHelp_rtnparm");	// Name

	if (sCOM_UserCodeHelp_rtnflag == "Y") {
		model.setValue(sCodePath, TFGetMatrixData(sCOM_UserCodeHelp_rtnparm, 0, 0));
		model.setValue(sNamePath, TFGetMatrixData(sCOM_UserCodeHelp_rtnparm, 0, 1));

		model.setFocus(pCtrlNext);

        model.refresh();
	}
}


/**
 *  장비검색 Help Window를 Open
 *  @param pGridId   : Grid ID (그리드가 아닌 경우는 "")
 *  @param pParam    : 검색조건
 *  @param pCodePath : 코드 XPath
 *  @param pNamePath : 명칭 XPath
 *  @param pStatusPath : status XPath
 *  변경내역) 2011.06.02 / 최초작성 / leejp
 */
function ERP_FOpenUnitHelp(pGridId, pParam, pCodePath, pNamePath, pGubnPath, pStatusPath) { //(★)
    var sOldCd = model.getValue(pCodePath);
    var aNamePath = pNamePath.split("▦"); //Array
    var iArrSize = aNamePath.length;
    var g_param = "";

    if (pParam != "") {
        g_param = pParam;
    } else {
        g_param = "▦▦";
    }

    model.setAttribute("ERP_UnitCodeHelp_parm", g_param);
	model.dialog( "text/xml", "/erp/gojeongjasanweb/xfm/RGF_JangBiGiChoGRHelp.xfm", 300, 300, 667, 565);

	var sERP_UnitCodeHelp_Code = model.getAttribute("ERP_UnitCodeHelp_Code");	// ID
	var sERP_UnitCodeHelp_Name = model.getAttribute("ERP_UnitCodeHelp_Name");	// Name
	var sERP_UnitCodeHelp_Gubn = model.getAttribute("ERP_UnitCodeHelp_Gubn");	// Name

	model.setValue(pCodePath, sERP_UnitCodeHelp_Code);
	model.setValue(pNamePath, sERP_UnitCodeHelp_Name);
	model.setValue(pGubnPath, sERP_UnitCodeHelp_Gubn);

	if (pGridId == "") {
		model.refresh();
	} else {
		model.gridRefresh(pGridId);
	}
}

/**
 *  그리드가 아닌 일반 Control에서 장비코드 또는 장비명의 Validation Check
 *  @param pJobGbn : 1/2 (1:코드, 2:명칭)
 *  @param pCond   : 추가 검색조건 (장비구분▦장비명▦)
 *  @param pCtrlCd : 코드 Control ID
 *  @param pCtrlNm : 명칭 Control ID
 *  @param pCtrlNext : 다음으로 이동할 Control ID
 *  @param pHelpGbn  :
 *  변경내역) 2011.06.02 / 최초작성 / leejp
 */
function ERP_FValidateUnit(pJobGbn, pCond, pCtrlCd, pCtrlNm, pCtrlGb, pCtrlNext, pHelpGbn) { //(★)
    var sCodePath = ERP_FGetXPath(pCtrlCd); //code XPath
	var sNamePath = ERP_FGetXPath(pCtrlNm); //name XPath
	var sGubnPath = ERP_FGetXPath(pCtrlGb); //gubn XPath

    var aCtrlNm = pCtrlNm.split('▦');      //name Control List (Array)
    var iArrSize = aCtrlNm.length;          //array size
    var aNamePath = new Array(iArrSize);    //name XPath List (Array)
    for (i=0; i<iArrSize; i++) {
        aNamePath[i] = ERP_FGetXPath(aCtrlNm[i]);
    }

	var setFlg = "";

    var sName = "";      //입력 항목 Name
    var sControlId = ""; //입력 Control의 ID
    var sSrhTxt = "";    //Validation Check하고자 하는 정보(코드 또는 명칭)
    var sParam = "";     //검색조건(jobgbn▦srtgbn▦srhtxt▦cond1▦cond2▦cond3▦cond4▦cond5)
    var iCnt = 0;        //해당되는 자료의 건수

    if (pJobGbn == "1") {
        sControlId = pCtrlCd;
        sSrhTxt = model.getValue(sCodePath);
		setFlg = "02";
    } else {
        sControlId = aCtrlNm[0];
        sSrhTxt = model.getValue(aNamePath[0]);
		setFlg = "03";
    }

    if (sSrhTxt.replace(/\s/gi, "") == "") {
        model.setValue(sCodePath, "");
        for (i=0; i<iArrSize; i++) {
            model.setValue(aNamePath[i], "");
        }
        model.refresh();
        return;
    }

	sParam = "▦" + sSrhTxt + "▦▦";

	model.setAttribute("ERP_UnitCodeHelp_parm", sParam);
	model.dialog( "text/xml", "/erp/gojeongjasanweb/xfm/RGF_JangBiGiChoGRHelp.xfm", 300, 300, 667, 565);

	var sERP_UnitCodeHelp_Code = model.getAttribute("ERP_UnitCodeHelp_Code");	// ID
	var sERP_UnitCodeHelp_Name = model.getAttribute("ERP_UnitCodeHelp_Name");	// Name
	var sERP_UnitCodeHelp_Gubn = model.getAttribute("ERP_UnitCodeHelp_Gubn");	// Name

	model.setValue(sCodePath, sERP_UnitCodeHelp_Code);
	model.setValue(sNamePath, sERP_UnitCodeHelp_Name);
	model.setValue(sGubnPath, sERP_UnitCodeHelp_Gubn);

	model.setFocus(pCtrlNext);

	model.refresh();

}


//========================================= Grid 관련 =========================================//
/**
 *  그리드의 Hidden Column 숨기기/보이기
 *  @param pGridId  : Grid ID
 *  @param pFlag    : true/false
 *  @param pColList : ','로 구분된 colnum 리스트
 *  사 용 예) RPFGridColHidden("grid1", true, "3,4"); //grid1의 3,4번째 Column 숨기기
 *  변경내역) 2004.12.24 / 최초작성 / ksy
 */
function ERP_FGridColHidden(pGridId, pFlag, pColList) {
    var fg = document.controls(pGridId);
    var colArr = pColList.split(',');
    for(i=0; i<colArr.length; i++) {
        fg.ColHidden(parseInt(colArr[i]) - 1) = pFlag;
    }
}

/**
 *  그리드의 특정 Cell로 Focus 위치
 *  @param pGridId : Grid ID
 *  @param pRow    : 이동할 Row
 *  @param pCol    : 이동할 Column
 *  사 용 예) RPFGridSetFocus("grid1", 1, 1); //grid1의 (1,1)Cell로 Focus 이동.
 *  변경내역) 2004.12.24 / 최초작성 / ksy
 *           2005.01.06 / Range 선택 => Cell 선택 / ksy
 */
function ERP_FGridSetFocus(pGridId, pRow, pCol) {

    model.setFocus(pGridId);
    var fg = document.controls(pGridId);
    var iRow = fg.FixedRows + pRow -1;
    var iCol = fg.FixedCols + pCol - 1;
    fg.Select(iRow, iCol);
    //fg.EditCell();
    //fg.Select(iRow, iCol);
}

/**
 *  그리드 줄추가
 *  @param pGridId  : 그리드 ID
 *  @param pNodeset : 그리드의 Nodeset (XPath)
 *  @param pIsExist : true/false
 *  @param pRebuild : Optional. (false일 경우 gridRebuild 하지 않음.)
 *  주의사항) pIsExist == true 인 경우 반드시 fInsertAfter()가 정의되어 있어야 한다.
 *          function fInsertAfter(pGridId, pNodeset, pCurRow) {
 *              //줄추가 후 처리 작업 기술 (Default값 Set 등)
 *          }
 *  사 용 예) ERP_FGridRowAdd("grid1", "/root/MainData/main/depthList", true);
 *  변경내역) 2004.12.24 / 최초작성 / ksy
 */
function ERP_FGridRowAdd(pGridId, pNodeset, pIsExist, pRebuild) {
    var iCurRow = TFGridInsertLastA(pGridId, pNodeset, pRebuild); //줄추가
    model.setValue(pNodeset + "[" + iCurRow + "]/status", "I"); //그리드 Row 상태 Flag Set

    if (pIsExist) { //Insert후 처리할 작업이 있는가?
        fInsertAfter(pGridId, pNodeset, iCurRow);
    }
    if (pRebuild == null || pRebuild == true) {
        pGridId.refresh(); //변경된 Data를 그리드에 반영
    }
    if (pRebuild == false) {
        return iCurRow;
    }

    if (TF.grid.dataRows(document.controls(pGridId)) > 0) {
        var fg = model.vsgrid(pGridId);
        iCurRow = fg.Rows - 1;
        fg.Row = iCurRow;
        fg.Select(iCurRow, fg.FixedCols);
        fg.TopRow = fg.Row;
        return iCurRow;
    }
    return 0;
}

/**
 *  그리드 줄삽입
 *  @param pGridId  : 그리드 ID
 *  @param pNodeset : 그리드의 Nodeset (XPath)
 *  @param pIsExist : true/false
 *  @param pRebuild : Optional. (false일 경우 gridRebuild 하지 않음.)
 *  주의사항) pIsExist == true 인 경우 반드시 fInsertAfter()가 정의되어 있어야 한다.
 *          function fInsertAfter(pGridId, pNodeset, pCurRow) {
 *              //줄추가 후 처리 작업 기술 (Default값 Set 등)
 *          }
 *  사 용 예) ERP_FGridRowInsert("grid1", "/root/MainData/main/depthList", true);
 *  변경내역) 2004.12.24 / 최초작성 / ksy
 */
function ERP_FGridRowInsert(pGridId, pNodeset, pIsExist, pRebuild) {
    var iCurRow = TFGridInsertCurrent(pGridId, "before", pRebuild); //줄삽입
    if (iCurRow < 1) { //삽입할 위치가 선택되지 않은 경우
        model.alert("삽입할 위치를 선택하세요!");
        return;
    }
    model.setValue(pNodeset + "[" + iCurRow + "]/status", "I"); //그리드 Row 상태 Flag Set

    if (pIsExist) { //Insert후 처리할 작업이 있는가?
        fInsertAfter(pGridId, pNodeset, iCurRow);
    }
    if (pRebuild == null || pRebuild == true) {
        pGridId.refresh(); //변경된 Data를 그리드에 반영
    }
    if (pRebuild == false) {
        return iCurRow;
    }

    if (TF.grid.dataRows(document.controls(pGridId)) > 0) {
        var fg = document.controls(pGridId);
        iCurRow = iCurRow + fg.FixedRows - 1;
        fg.Select(iCurRow, fg.FixedCols); //추가된 Row로 Focus 이동
        fg.TopRow = iCurRow;
        return iCurRow;
    }
    return 0;
}

/**
 *  그리드 줄삭제
 *  @param pGridId  : 그리드 ID
 *  @param pNodeset : 그리드의 Nodeset (XPath)
 *  @param pRebuild : false (false : Rebuild X), optional
 *  사 용 예) ERP_FGridRowDelete("grid1", "/root/MainData/main/depthList");
 *  변경내역) 2004.12.24 / 최초작성 / ksy
 *           2005.04.13 / 헤더가 두 줄인 경우 에러 수정 / 김선영
 */
function ERP_FGridRowDelete(pGridId, pNodeset, pRebuild) {
    var fg = document.controls(pGridId);
    var iCurRow = document.controls(pGridId).row; //선택된 Row ID

    if (iCurRow < 1) { //삭제할 위치가 선택되지 않은 경우
        model.alert("삭제할 정보를 선택하세요!");
        return;
    }

    var sStatus = model.getValue(pNodeset + "[" + iCurRow + "]/status");
    if (sStatus == "I") {
        //fg.Select(iCurRow + fg.FixedRows - 1, fg.FixedCols); //이미 선택 후 삭제하므로 필요 X (2005.01.12 ksy)
        model.griddeleteSelect(pGridId); //현재 Row 상태가 "I"인 경우 Row 삭제
        if (pRebuild == null || pRebuild == true) {
            pGridId.refresh(); //변경된 Data를 그리드에 반영
        }
        return iCurRow;
    } else {
        model.setValue(pNodeset + "[" + iCurRow + "]/status", "D"); //그리드 Row 상태 Flag Set
    }
    model.gridRefresh(pGridId); //변경된 Data를 그리드에 반영

    if (iCurRow > 1) {
        //===== 2005.04.13 / 아래 4문장 주석처리(헤더가 두 줄인 경우 문제) / 김선영
        //iCurRow = iCurRow + fg.FixedRows - 1;
        //fg.Select(iCurRow, fg.FixedCols);
        //fg.EditCell();
        //fg.Select(iCurRow, fg.FixedCols);
        return iCurRow;
    }
    return 0;
}

/**
 *  그리드 Row 상태를 '수정'으로. 그리드 값이 변경된 경우 호출.
 *  @param pGridId  : 그리드 ID
 *  @param pNodeset : 그리드의 Nodeset (XPath)
 *  @param pRow     : Row Number
 *  @param pCol     : Col Number
 *  @param pIsExist : true/false
 *  주의사항) pIsExist == true 인 경우 반드시 fValidationCheck()가 정의되어 있어야 한다.
 *          function fValidationCheck(pGridId, pNodeset, pRow, pCol) {
 *              if (pGridId == "grid1") {
 *                  if (pCol == 5) {
 *                      //Validation Check 작업 기술
 *                  }
 *              }
 *              return true;
 *          }
 *  사 용 예) function grid1::OnCellChanged( Row, Col ) { //Grid Global Event
 *               ERP_FGridValueChanged("grid1", "/root/MainData/main/depthList", Row, Col, true);
 *           }
 *  변경내역) 2004.12.24 / 최초작성 / ksy
 */
function ERP_FGridValueChanged(pGridId, pNodeset, pRow, pCol, pIsExist) {
    if (pIsExist) { //Validation Check할 항목이 있는가?
        fValidationCheck(pGridId, pNodeset, pRow, pCol);
    }

    var sStatus = model.getValue(pNodeset + "[" + pRow + "]/status");
    if (sStatus == '-') { //현재 Row 상태가 Default인 경우
        model.setValue(pNodeset + "[" + pRow + "]/status", "U"); //그리드 Row 상태 Flag Set
    }
    model.gridRefresh(pGridId); //변경된 Data를 그리드에 반영 (주의! gridRebuild X)
}

/**
 *  그리드 클릭시 바로 에디트모드로 설정
 *  변경내역) 2004.12.24 / 최초작성 / ckj
 */
function ERP_FGridEditMode(pGrid) {
    var fg = document.controls(pGrid);

    // bool 컨트롤은 오류가 있어서 리턴한다.
    if(fg.ColDataType(fg.MouseCol) == 11) {
        return;
    }

    fg.Select (fg.MouseRow, fg.MouseCol);
    fg.EditCell();
}

/**
 *  그리드 생성시 무효행 삭제
 *  @param pGridId  : Grid ID
 *  @param pNodeset : Grid Nodeset (XPath)
 *  변경내역) 2004.12.24 / 최초작성 / ckj
 */
function ERP_FGridDelRow(pGridId, pNodeset) {
    model.removeNodeset(pNodeset);
    pGridId.refresh();
}

/**
 *  그리드 변경된 행수 리턴
 *  @param pGrid : Grid ID
 *  변경내역) 2005.01.05 / 최초작성 / ckj
 */
function ERP_FGridCheckFlag(pGrid) {
    var rowCnt = TF.grid.dataRows(document.controls(pGrid)); //Grid Row Count
    var flag; // 행의 상태
    var cnt = 0;  // 변경된 행수

    if (rowCnt <= 0) {
        alert("입력된 행이 없습니다.");
        return 0;
    }

    for (i = 1; i <= rowCnt; i++) {
        flag = model.getGridFlag(pGrid, i);
        if (flag == "i"  || flag == "u" || flag == "d") {
            cnt++;
        }
    }

    return cnt;
}

/**
 *  그리드 틀고정
 *  @param pGrid : Grid ID
 *  @param pNum  : 고정할 컬럼 또는 행의 수 (1부터 시작한다)
 *  @param pGubn : 컬럼고정(C), 행고정(R)
 *  변경내역) 2005.02.02 / 최초작성 / ckj
 */
function ERP_FGridFrozen(pGrid, pNum, pGubn) {
    var vg = document.controls(pGrid);

    if (pNum <= 0) {
        alert("컬럼 또는 행의 수는 1이상이 어야 합니다.");
        return false;
    }

    pNum = pNum - 1;
    if (pGubn.lvToUpper() == "C") {
        vg.frozenCols = pNum;
    } else {
        vg.frozenRows = pNum;
    }

    return true;
}

//====================================== MSXML(XML 처리 관련) ======================================//
/**
 *   Node Reset : 해당 Node를 주어진 xml Data로 교체
 *  @param pNodePath : 변경할 Node의 XPath
 *  @param pXmlData  : 교체할 Xml String
 *  주의사항) model.makeNode(), fineNode(), replaceNode()는 LiveTF.js에 정의되어 있음.
 *  변경내역) 2004.12.01 / 최초작성 / ksy
 */
function ERP_FResetForNode(pNodePath, pXmlData) {
    var initNode = model.makeNode(pXmlData);
    var curNode = TF.xml.selectSingleNode(pNodePath);
    replaceNode(initNode, curNode);
    model.refresh();
}



//====================================== 일반 Java Script 관련 ======================================//
/**
 *  String Data에 Token의 개수를 리턴
 *  @param pData   : String Data
 *  @param pToken  : Token
 */
function ERP_FGetTokenCount(pData, pToken) {
    var arr = pData.split(pToken);
    if (arr < 1) {
        return 0;
    } else {
        return (arr.length - 1);
    }
}

/**
 *  오늘날짜반환 (서버날짜 또는 로컬시스템날짜)
 *  @return : yyyymmdd
 *  변경내역) 2004.12.24 / 최초작성 / ckj
 *           2005.01.03 / 변수 sMonth, sDay 추가 / ckj
 */
function ERP_FGetToDay() {
    var toDay = model.getvalue("/root/InitData/today");

    if (toDay == "") {
        var date  = moment();
        var year  = date.get('year');
        var month = date.get('month') + 1;
        var day   = date.get('date');
        var sMonth;
        var sDay;

        if (month > 9) {
            sMonth = month.toString();
        } else {
            sMonth = "0" + month.toString();
        }

        if (day > 9) {
            sDay = day.toString();
        } else {
            sDay = "0" + day.toString();
        }
        toDay = year.toString() + sMonth + sDay;
    } else {
        toDay;
    }
    return toDay;
}

/**
 *  문자열에 앞뒤로 정해진 자리수만큼 특정 문자 채우기
 *  @param pData   : 문자열
 *  @param pLen    : 자리수
 *  @param pFill   : 채울 문자
 *  @param pLR     : 채울 방향 'L' 왼쪽, 'R' 오른쪽 (default : "L")
 *  변경내역) 2004.12.24 / 최초작성 / sjh
 */
function ERP_FSetPAD(pData, pLen, pFill, pLR) {
    var sPad = "";
    for(var i=pData.length; i<=(pLen - 1); i++) {
        sPad = sPad.concat(pFill);
    }

    if (pLR == "R") {
        return pData.concat(sPad);
    } else {
        return sPad.concat(pData);
    }
}

/**
 *  문자열의 숫자값을 증감하여 일정한 값을 리턴한다
 *  @param pStr : 숫자가 포함된 문자열
 *  @param pNum : 증감할 정수값
 *  @param pLen : 반환할 문자열의 자리수
 *  @return     : ex) "001"
 *  변경내역) 2005.01.04 / 최초작성 / ckj
 *           2005.01.05 / 오류수정 -> parseInt(pStr, 10) 부분 수정 / ckj
 */
function ERP_FGetStrCount(pStr, pNum, pLen) {
    var vNum = parseInt(pStr, 10);

    if (isNaN(vNum)) {
        vNum = 0;
    } else {
        vNum += pNum;
    }
    return ERP_FSetPAD(vNum.toString(), pLen, "0", "L");
}

/**
 *  특정일자에 몇일(몇달 또는 몇년)이 더해진 일자를 리턴한다.
 *  @param pDate : 기준 일자
 *  @param pGubn : Y/M/D (※ D : 일자에 pNum을 더한일자)
 *  @param pNum : 증감할 정수값
 *  @return     : ex) "20041231"
 *  변경내역) 2005.01.11 / 최초작성 / ksy
 */
function ERP_FGetRelativeDay(pDate, pGubn, pNum) {
    //pDate가 날짜 형식이 맞는지 체크

    //Date Type으로 변환
    var dGiJunDate = pDate.lvToDate("YYYYMMDD");

    //pGubn에 따라 증감
    if (pGubn == "Y") {
        dGiJunDate.setFullYear(dGiJunDate.getFullYear() + pNum);
    } else if (pGubn == "M") {
        dGiJunDate.setMonth(dGiJunDate.get('month') + pNum);
    } else if (pGubn == "D") {
        dGiJunDate.setDate(dGiJunDate.get('date') + pNum);
    }

    return dGiJunDate.lvFormat("YYYYMMDD");
}

/**
 *  문자열에 앞의 특정 문자를 지운 문자열 return
 *  @param pData   : 문자열
 *  @param pRemove : 지울문자 Optional. (default : 0(zero))
 *  변경내역) 2005.1.12 / 최초작성 / sjh
 */
function ERP_FRemoveChar(pData, pRemove) {
    var sReturn = "";
    var sChar = "";
    var iLen = pData.length;
    var sFlag = "";

    if (pRemove == null) pRemove = "0";

    for(var i=0; i<=(iLen - 1); i++) {
        sChar = pData.charAt(i);
        if (sChar != pRemove || sFlag == "T") {
            sReturn = sReturn.concat(sChar);
            sFlag = "T";
        }
    }
    return sReturn;
}

/**
 * @group  :
 * @ver    : 2005.09.09
 * @by     : 김기호
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : KSC5601에 없고 MS949 확장완성형에만 있는 한글을 체크한다.
 * @param1 : 체크하고자 하는 문자열
 * @return : 입력할수없는 글자를      글자▦글자위치▩글자▦글자위치▩글자▦글자위치▩... 으로 반환 예) 밷▦1▩숔▦8▩...
 * @---------------------------------------------------
 */
function ERP_FValidateHangule(part, str) {
    if (str == "" || str == null) {
        return -1;
    }

    var strLength = str.length;
    var code = "";
    var invalidChars = "";  // 입력불가능한 문자의 집합

    for (var idx = 0; idx < strLength; idx++ ) {    // start for..
        code = str.charCodeAt(idx);

        // 영문(소), 영문(대), 숫자 인 경우...
/*
        if ((0x41 <= char && char <= 0x5A) || (0x61 <= char && char <= 0x7A) || (0x30 <= char && char <= 0x39)) {
            continue;
        }
*/
        // 한글이 아닌경우..그냥 통과! 가->힣
        if (!(0xAC00 <= code && code <= 0xD7A3)) {
            continue;
        }

        {   // if 조건 숨기기 start.. --> ms949 코드 비교하는 로직.. 2005.09.09 김기호
            // 한글인 경우..
            if ( code == 44034 || code == 44035 || code == 44037 || code == 44038 || code == 44043 || code == 44044 || code == 44045 || code == 44046 || code == 44047 || code == 44056 ||
                 code == 44062 || code == 44063 || code == 44065 || code == 44066 || code == 44067 || code == 44069 || code == 44070 || code == 44071 || code == 44072 || code == 44073 ||
                 code == 44074 || code == 44075 || code == 44078 || code == 44082 || code == 44083 || code == 44084 || code == 44085 || code == 44086 || code == 44087 || code == 44090 ||
                 code == 44091 || code == 44093 || code == 44094 || code == 44095 || code == 44097 || code == 44098 || code == 44099 || code == 44100 || code == 44101 || code == 44102 ||
                 code == 44103 || code == 44104 || code == 44105 || code == 44106 || code == 44108 || code == 44110 || code == 44111 || code == 44112 || code == 44113 || code == 44114 ||
                 code == 44115 || code == 44117 || code == 44118 || code == 44119 || code == 44121 || code == 44122 || code == 44123 || code == 44125 || code == 44126 || code == 44127 ||
                 code == 44128 || code == 44129 || code == 44130 || code == 44131 || code == 44132 || code == 44133 || code == 44134 || code == 44135 || code == 44136 || code == 44137 ||
                 code == 44138 || code == 44139 || code == 44140 || code == 44141 || code == 44142 || code == 44143 || code == 44146 || code == 44147 || code == 44149 || code == 44150 ||
                 code == 44153 || code == 44155 || code == 44156 || code == 44157 || code == 44158 || code == 44159 || code == 44162 || code == 44167 || code == 44168 || code == 44173 ||
                 code == 44174 || code == 44175 || code == 44177 || code == 44178 || code == 44179 || code == 44181 || code == 44182 || code == 44183 || code == 44184 || code == 44185 ||
                 code == 44186 || code == 44187 || code == 44190 || code == 44194 || code == 44195 || code == 44196 || code == 44197 || code == 44198 || code == 44199 || code == 44203 ||
                 code == 44205 || code == 44206 || code == 44209 || code == 44210 || code == 44211 || code == 44212 || code == 44213 || code == 44214 || code == 44215 || code == 44218 ||
                 code == 44222 || code == 44223 || code == 44224 || code == 44226 || code == 44227 || code == 44229 || code == 44230 || code == 44231 || code == 44233 || code == 44234 ||
                 code == 44235 || code == 44237 || code == 44238 || code == 44239 || code == 44240 || code == 44241 || code == 44242 || code == 44243 || code == 44244 || code == 44246 ||
                 code == 44248 || code == 44249 || code == 44250 || code == 44251 || code == 44252 || code == 44253 || code == 44254 || code == 44255 || code == 44258 || code == 44259 ||
                 code == 44261 || code == 44262 || code == 44265 || code == 44267 || code == 44269 || code == 44270 || code == 44274 || code == 44276 || code == 44279 || code == 44280 ||
                 code == 44281 || code == 44282 || code == 44283 || code == 44286 || code == 44287 || code == 44289 || code == 44290 || code == 44291 || code == 44293 || code == 44295 ||
                 code == 44296 || code == 44297 || code == 44298 || code == 44299 || code == 44302 || code == 44304 || code == 44306 || code == 44307 || code == 44308 || code == 44309 ||
                 code == 44310 || code == 44311 || code == 44313 || code == 44314 || code == 44315 || code == 44317 || code == 44318 || code == 44319 || code == 44321 || code == 44322 ||
                 code == 44323 || code == 44324 || code == 44325 || code == 44326 || code == 44327 || code == 44328 || code == 44330 || code == 44331 || code == 44334 || code == 44335 ||
                 code == 44336 || code == 44337 || code == 44338 || code == 44339 || code == 44342 || code == 44343 || code == 44345 || code == 44346 || code == 44347 || code == 44349 ||
                 code == 44350 || code == 44351 || code == 44352 || code == 44353 || code == 44354 || code == 44355 || code == 44358 || code == 44360 || code == 44362 || code == 44363 ||
                 code == 44364 || code == 44365 || code == 44366 || code == 44367 || code == 44369 || code == 44370 || code == 44371 || code == 44373 || code == 44374 || code == 44375 ||
                 code == 44377 || code == 44378 || code == 44379 || code == 44380 || code == 44381 || code == 44382 || code == 44383 || code == 44384 || code == 44386 || code == 44388 ||
                 code == 44389 || code == 44390 || code == 44391 || code == 44392 || code == 44393 || code == 44394 || code == 44395 || code == 44398 || code == 44399 || code == 44401 ||
                 code == 44402 || code == 44407 || code == 44408 || code == 44409 || code == 44410 || code == 44414 || code == 44416 || code == 44419 || code == 44420 || code == 44421 ||
                 code == 44422 || code == 44423 || code == 44426 || code == 44427 || code == 44429 || code == 44430 || code == 44431 || code == 44433 || code == 44434 || code == 44435 ||
                 code == 44436 || code == 44437 || code == 44438 || code == 44439 || code == 44440 || code == 44441 || code == 44442 || code == 44443 || code == 44446 || code == 44447 ||
                 code == 44448 || code == 44449 || code == 44450 || code == 44451 || code == 44453 || code == 44454 || code == 44455 || code == 44456 || code == 44457 || code == 44458 ||
                 code == 44459 || code == 44460 || code == 44461 || code == 44462 || code == 44463 || code == 44464 || code == 44465 || code == 44466 || code == 44467 || code == 44468 ||
                 code == 44469 || code == 44470 || code == 44472 || code == 44473 || code == 44474 || code == 44475 || code == 44476 || code == 44477 || code == 44478 || code == 44479 ||
                 code == 44482 || code == 44483 || code == 44485 || code == 44486 || code == 44487 || code == 44489 || code == 44490 || code == 44491 || code == 44492 || code == 44493 ||
                 code == 44494 || code == 44495 || code == 44498 || code == 44500 || code == 44501 || code == 44502 || code == 44503 || code == 44504 || code == 44505 || code == 44506 ||
                 code == 44507 || code == 44509 || code == 44510 || code == 44511 || code == 44513 || code == 44514 || code == 44515 || code == 44517 || code == 44518 || code == 44519 ||
                 code == 44520 || code == 44521 || code == 44522 || code == 44523 || code == 44524 || code == 44525 || code == 44526 || code == 44527 || code == 44528 || code == 44529 ||
                 code == 44530 || code == 44531 || code == 44532 || code == 44533 || code == 44534 || code == 44535 || code == 44538 || code == 44539 || code == 44541 || code == 44542 ||
                 code == 44546 || code == 44547 || code == 44548 || code == 44549 || code == 44550 || code == 44551 || code == 44554 || code == 44556 || code == 44558 || code == 44559 ||
                 code == 44560 || code == 44561 || code == 44562 || code == 44563 || code == 44565 || code == 44566 || code == 44567 || code == 44568 || code == 44569 || code == 44570 ||
                 code == 44571 || code == 44572 || code == 44573 || code == 44574 || code == 44575 || code == 44576 || code == 44577 || code == 44578 || code == 44579 || code == 44580 ||
                 code == 44581 || code == 44582 || code == 44583 || code == 44584 || code == 44585 || code == 44586 || code == 44587 || code == 44588 || code == 44589 || code == 44590 ||
                 code == 44591 || code == 44594 || code == 44595 || code == 44597 || code == 44598 || code == 44601 || code == 44603 || code == 44604 || code == 44605 || code == 44606 ||
                 code == 44607 || code == 44610 || code == 44612 || code == 44615 || code == 44616 || code == 44617 || code == 44619 || code == 44623 || code == 44625 || code == 44626 ||
                 code == 44627 || code == 44629 || code == 44631 || code == 44632 || code == 44633 || code == 44634 || code == 44635 || code == 44638 || code == 44642 || code == 44643 ||
                 code == 44644 || code == 44646 || code == 44647 || code == 44650 || code == 44651 || code == 44653 || code == 44654 || code == 44655 || code == 44657 || code == 44658 ||
                 code == 44659 || code == 44660 || code == 44661 || code == 44662 || code == 44663 || code == 44666 || code == 44670 || code == 44671 || code == 44672 || code == 44673 ||
                 code == 44674 || code == 44675 || code == 44678 || code == 44679 || code == 44680 || code == 44681 || code == 44682 || code == 44683 || code == 44685 || code == 44686 ||
                 code == 44687 || code == 44688 || code == 44689 || code == 44690 || code == 44691 || code == 44692 || code == 44693 || code == 44694 || code == 44695 || code == 44696 ||
                 code == 44697 || code == 44698 || code == 44699 || code == 44700 || code == 44701 || code == 44702 || code == 44703 || code == 44704 || code == 44705 || code == 44706 ||
                 code == 44707 || code == 44708 || code == 44709 || code == 44710 || code == 44711 || code == 44712 || code == 44713 || code == 44714 || code == 44715 || code == 44716 ||
                 code == 44717 || code == 44718 || code == 44719 || code == 44720 || code == 44721 || code == 44722 || code == 44723 || code == 44724 || code == 44725 || code == 44726 ||
                 code == 44727 || code == 44728 || code == 44729 || code == 44730 || code == 44731 || code == 44735 || code == 44737 || code == 44738 || code == 44739 || code == 44741 ||
                 code == 44742 || code == 44743 || code == 44744 || code == 44745 || code == 44746 || code == 44747 || code == 44750 || code == 44754 || code == 44755 || code == 44756 ||
                 code == 44757 || code == 44758 || code == 44759 || code == 44762 || code == 44763 || code == 44765 || code == 44766 || code == 44767 || code == 44768 || code == 44769 ||
                 code == 44770 || code == 44771 || code == 44772 || code == 44773 || code == 44774 || code == 44775 || code == 44777 || code == 44778 || code == 44780 || code == 44782 ||
                 code == 44783 || code == 44784 || code == 44785 || code == 44786 || code == 44787 || code == 44789 || code == 44790 || code == 44791 || code == 44793 || code == 44794 ||
                 code == 44795 || code == 44797 || code == 44798 || code == 44799 || code == 44800 || code == 44801 || code == 44802 || code == 44803 || code == 44804 || code == 44805 ||
                 code == 44806 || code == 44809 || code == 44810 || code == 44811 || code == 44812 || code == 44814 || code == 44815 || code == 44817 || code == 44818 || code == 44819 ||
                 code == 44820 || code == 44821 || code == 44822 || code == 44823 || code == 44824 || code == 44825 || code == 44826 || code == 44827 || code == 44828 || code == 44829 ||
                 code == 44830 || code == 44831 || code == 44832 || code == 44833 || code == 44834 || code == 44835 || code == 44836 || code == 44837 || code == 44838 || code == 44839 ||
                 code == 44840 || code == 44841 || code == 44842 || code == 44843 || code == 44846 || code == 44847 || code == 44849 || code == 44851 || code == 44853 || code == 44854 ||
                 code == 44855 || code == 44856 || code == 44857 || code == 44858 || code == 44859 || code == 44862 || code == 44864 || code == 44868 || code == 44869 || code == 44870 ||
                 code == 44871 || code == 44874 || code == 44875 || code == 44876 || code == 44877 || code == 44878 || code == 44879 || code == 44881 || code == 44882 || code == 44883 ||
                 code == 44884 || code == 44885 || code == 44886 || code == 44887 || code == 44888 || code == 44889 || code == 44890 || code == 44891 || code == 44894 || code == 44895 ||
                 code == 44896 || code == 44897 || code == 44898 || code == 44899 || code == 44902 || code == 44903 || code == 44904 || code == 44905 || code == 44906 || code == 44907 ||
                 code == 44908 || code == 44909 || code == 44910 || code == 44911 || code == 44912 || code == 44913 || code == 44914 || code == 44915 || code == 44916 || code == 44917 ||
                 code == 44918 || code == 44919 || code == 44920 || code == 44922 || code == 44923 || code == 44924 || code == 44925 || code == 44926 || code == 44927 || code == 44929 ||
                 code == 44930 || code == 44931 || code == 44933 || code == 44934 || code == 44935 || code == 44937 || code == 44938 || code == 44939 || code == 44940 || code == 44941 ||
                 code == 44942 || code == 44943 || code == 44946 || code == 44947 || code == 44948 || code == 44950 || code == 44951 || code == 44952 || code == 44953 || code == 44954 ||
                 code == 44955 || code == 44957 || code == 44958 || code == 44959 || code == 44960 || code == 44961 || code == 44962 || code == 44963 || code == 44964 || code == 44965 ||
                 code == 44966 || code == 44967 || code == 44968 || code == 44969 || code == 44970 || code == 44971 || code == 44972 || code == 44973 || code == 44974 || code == 44975 ||
                 code == 44976 || code == 44977 || code == 44978 || code == 44979 || code == 44980 || code == 44981 || code == 44982 || code == 44983 || code == 44986 || code == 44987 ||
                 code == 44989 || code == 44990 || code == 44991 || code == 44993 || code == 44994 || code == 44995 || code == 44996 || code == 44997 || code == 44998 || code == 45002 ||
                 code == 45004 || code == 45007 || code == 45008 || code == 45009 || code == 45010 || code == 45011 || code == 45013 || code == 45014 || code == 45015 || code == 45016 ||
                 code == 45017 || code == 45018 || code == 45019 || code == 45021 || code == 45022 || code == 45023 || code == 45024 || code == 45025 || code == 45026 || code == 45027 ||
                 code == 45028 || code == 45029 || code == 45030 || code == 45031 || code == 45034 || code == 45035 || code == 45036 || code == 45037 || code == 45038 || code == 45039 ||
                 code == 45042 || code == 45043 || code == 45045 || code == 45046 || code == 45047 || code == 45049 || code == 45050 || code == 45051 || code == 45052 || code == 45053 ||
                 code == 45054 || code == 45055 || code == 45058 || code == 45059 || code == 45061 || code == 45062 || code == 45063 || code == 45064 || code == 45065 || code == 45066 ||
                 code == 45067 || code == 45069 || code == 45070 || code == 45071 || code == 45073 || code == 45074 || code == 45075 || code == 45077 || code == 45078 || code == 45079 ||
                 code == 45080 || code == 45081 || code == 45082 || code == 45083 || code == 45086 || code == 45087 || code == 45088 || code == 45089 || code == 45090 || code == 45091 ||
                 code == 45092 || code == 45093 || code == 45094 || code == 45095 || code == 45097 || code == 45098 || code == 45099 || code == 45100 || code == 45101 || code == 45102 ||
                 code == 45103 || code == 45104 || code == 45105 || code == 45106 || code == 45107 || code == 45108 || code == 45109 || code == 45110 || code == 45111 || code == 45112 ||
                 code == 45113 || code == 45114 || code == 45115 || code == 45116 || code == 45117 || code == 45118 || code == 45119 || code == 45120 || code == 45121 || code == 45122 ||
                 code == 45123 || code == 45126 || code == 45127 || code == 45129 || code == 45131 || code == 45133 || code == 45135 || code == 45136 || code == 45137 || code == 45138 ||
                 code == 45142 || code == 45144 || code == 45146 || code == 45147 || code == 45148 || code == 45150 || code == 45151 || code == 45152 || code == 45153 || code == 45154 ||
                 code == 45155 || code == 45156 || code == 45157 || code == 45158 || code == 45159 || code == 45160 || code == 45161 || code == 45162 || code == 45163 || code == 45164 ||
                 code == 45165 || code == 45166 || code == 45167 || code == 45168 || code == 45169 || code == 45170 || code == 45171 || code == 45172 || code == 45173 || code == 45174 ||
                 code == 45175 || code == 45176 || code == 45177 || code == 45178 || code == 45179 || code == 45182 || code == 45183 || code == 45185 || code == 45186 || code == 45187 ||
                 code == 45189 || code == 45190 || code == 45191 || code == 45192 || code == 45193 || code == 45194 || code == 45195 || code == 45198 || code == 45200 || code == 45202 ||
                 code == 45203 || code == 45204 || code == 45205 || code == 45206 || code == 45207 || code == 45211 || code == 45213 || code == 45214 || code == 45219 || code == 45220 ||
                 code == 45221 || code == 45222 || code == 45223 || code == 45226 || code == 45232 || code == 45234 || code == 45238 || code == 45239 || code == 45241 || code == 45242 ||
                 code == 45243 || code == 45245 || code == 45246 || code == 45247 || code == 45248 || code == 45249 || code == 45250 || code == 45251 || code == 45254 || code == 45258 ||
                 code == 45259 || code == 45260 || code == 45261 || code == 45262 || code == 45263 || code == 45266 || code == 45267 || code == 45269 || code == 45270 || code == 45271 ||
                 code == 45273 || code == 45274 || code == 45275 || code == 45276 || code == 45277 || code == 45278 || code == 45279 || code == 45281 || code == 45282 || code == 45283 ||
                 code == 45284 || code == 45286 || code == 45287 || code == 45288 || code == 45289 || code == 45290 || code == 45291 || code == 45292 || code == 45293 || code == 45294 ||
                 code == 45295 || code == 45296 || code == 45297 || code == 45298 || code == 45299 || code == 45300 || code == 45301 || code == 45302 || code == 45303 || code == 45304 ||
                 code == 45305 || code == 45306 || code == 45307 || code == 45308 || code == 45309 || code == 45310 || code == 45311 || code == 45312 || code == 45313 || code == 45314 ||
                 code == 45315 || code == 45316 || code == 45317 || code == 45318 || code == 45319 || code == 45322 || code == 45325 || code == 45326 || code == 45327 || code == 45329 ||
                 code == 45332 || code == 45333 || code == 45334 || code == 45335 || code == 45338 || code == 45342 || code == 45343 || code == 45344 || code == 45345 || code == 45346 ||
                 code == 45350 || code == 45351 || code == 45353 || code == 45354 || code == 45355 || code == 45357 || code == 45358 || code == 45359 || code == 45360 || code == 45361 ||
                 code == 45362 || code == 45363 || code == 45366 || code == 45370 || code == 45371 || code == 45372 || code == 45373 || code == 45374 || code == 45375 || code == 45378 ||
                 code == 45379 || code == 45381 || code == 45382 || code == 45383 || code == 45385 || code == 45386 || code == 45387 || code == 45388 || code == 45389 || code == 45390 ||
                 code == 45391 || code == 45394 || code == 45395 || code == 45398 || code == 45399 || code == 45401 || code == 45402 || code == 45403 || code == 45405 || code == 45406 ||
                 code == 45407 || code == 45409 || code == 45410 || code == 45411 || code == 45412 || code == 45413 || code == 45414 || code == 45415 || code == 45416 || code == 45417 ||
                 code == 45418 || code == 45419 || code == 45420 || code == 45421 || code == 45422 || code == 45423 || code == 45424 || code == 45425 || code == 45426 || code == 45427 ||
                 code == 45428 || code == 45429 || code == 45430 || code == 45431 || code == 45434 || code == 45435 || code == 45437 || code == 45438 || code == 45439 || code == 45441 ||
                 code == 45443 || code == 45444 || code == 45445 || code == 45446 || code == 45447 || code == 45450 || code == 45452 || code == 45454 || code == 45455 || code == 45456 ||
                 code == 45457 || code == 45461 || code == 45462 || code == 45463 || code == 45465 || code == 45466 || code == 45467 || code == 45469 || code == 45470 || code == 45471 ||
                 code == 45472 || code == 45473 || code == 45474 || code == 45475 || code == 45476 || code == 45477 || code == 45478 || code == 45479 || code == 45481 || code == 45482 ||
                 code == 45483 || code == 45484 || code == 45485 || code == 45486 || code == 45487 || code == 45488 || code == 45489 || code == 45490 || code == 45491 || code == 45492 ||
                 code == 45493 || code == 45494 || code == 45495 || code == 45496 || code == 45497 || code == 45498 || code == 45499 || code == 45500 || code == 45501 || code == 45502 ||
                 code == 45503 || code == 45504 || code == 45505 || code == 45506 || code == 45507 || code == 45508 || code == 45509 || code == 45510 || code == 45511 || code == 45512 ||
                 code == 45513 || code == 45514 || code == 45515 || code == 45517 || code == 45518 || code == 45519 || code == 45521 || code == 45522 || code == 45523 || code == 45525 ||
                 code == 45526 || code == 45527 || code == 45528 || code == 45529 || code == 45530 || code == 45531 || code == 45534 || code == 45536 || code == 45537 || code == 45538 ||
                 code == 45539 || code == 45540 || code == 45541 || code == 45542 || code == 45543 || code == 45546 || code == 45547 || code == 45549 || code == 45550 || code == 45551 ||
                 code == 45553 || code == 45554 || code == 45555 || code == 45556 || code == 45557 || code == 45558 || code == 45559 || code == 45560 || code == 45562 || code == 45564 ||
                 code == 45566 || code == 45567 || code == 45568 || code == 45569 || code == 45570 || code == 45571 || code == 45574 || code == 45575 || code == 45577 || code == 45578 ||
                 code == 45581 || code == 45582 || code == 45583 || code == 45584 || code == 45585 || code == 45586 || code == 45587 || code == 45590 || code == 45592 || code == 45594 ||
                 code == 45595 || code == 45596 || code == 45597 || code == 45598 || code == 45599 || code == 45601 || code == 45602 || code == 45603 || code == 45604 || code == 45605 ||
                 code == 45606 || code == 45607 || code == 45608 || code == 45609 || code == 45610 || code == 45611 || code == 45612 || code == 45613 || code == 45614 || code == 45615 ||
                 code == 45616 || code == 45617 || code == 45618 || code == 45619 || code == 45621 || code == 45622 || code == 45623 || code == 45624 || code == 45625 || code == 45626 ||
                 code == 45627 || code == 45629 || code == 45630 || code == 45631 || code == 45632 || code == 45633 || code == 45634 || code == 45635 || code == 45636 || code == 45637 ||
                 code == 45638 || code == 45639 || code == 45640 || code == 45641 || code == 45642 || code == 45643 || code == 45644 || code == 45645 || code == 45646 || code == 45647 ||
                 code == 45648 || code == 45649 || code == 45650 || code == 45651 || code == 45652 || code == 45653 || code == 45654 || code == 45655 || code == 45657 || code == 45658 ||
                 code == 45659 || code == 45661 || code == 45662 || code == 45663 || code == 45665 || code == 45666 || code == 45667 || code == 45668 || code == 45669 || code == 45670 ||
                 code == 45671 || code == 45674 || code == 45675 || code == 45676 || code == 45677 || code == 45678 || code == 45679 || code == 45680 || code == 45681 || code == 45682 ||
                 code == 45683 || code == 45686 || code == 45687 || code == 45688 || code == 45689 || code == 45690 || code == 45691 || code == 45693 || code == 45694 || code == 45695 ||
                 code == 45696 || code == 45697 || code == 45698 || code == 45699 || code == 45702 || code == 45703 || code == 45704 || code == 45706 || code == 45707 || code == 45708 ||
                 code == 45709 || code == 45710 || code == 45711 || code == 45714 || code == 45715 || code == 45717 || code == 45718 || code == 45719 || code == 45723 || code == 45724 ||
                 code == 45725 || code == 45726 || code == 45727 || code == 45730 || code == 45732 || code == 45735 || code == 45736 || code == 45737 || code == 45739 || code == 45741 ||
                 code == 45742 || code == 45743 || code == 45745 || code == 45746 || code == 45747 || code == 45749 || code == 45750 || code == 45751 || code == 45752 || code == 45753 ||
                 code == 45754 || code == 45755 || code == 45756 || code == 45757 || code == 45758 || code == 45759 || code == 45760 || code == 45761 || code == 45762 || code == 45763 ||
                 code == 45764 || code == 45765 || code == 45766 || code == 45767 || code == 45770 || code == 45771 || code == 45773 || code == 45774 || code == 45775 || code == 45777 ||
                 code == 45779 || code == 45780 || code == 45781 || code == 45782 || code == 45783 || code == 45786 || code == 45788 || code == 45790 || code == 45791 || code == 45792 ||
                 code == 45793 || code == 45795 || code == 45799 || code == 45801 || code == 45802 || code == 45808 || code == 45809 || code == 45810 || code == 45814 || code == 45820 ||
                 code == 45821 || code == 45822 || code == 45826 || code == 45827 || code == 45829 || code == 45830 || code == 45831 || code == 45833 || code == 45834 || code == 45835 ||
                 code == 45836 || code == 45837 || code == 45838 || code == 45839 || code == 45842 || code == 45846 || code == 45847 || code == 45848 || code == 45849 || code == 45850 ||
                 code == 45851 || code == 45853 || code == 45854 || code == 45855 || code == 45856 || code == 45857 || code == 45858 || code == 45859 || code == 45860 || code == 45861 ||
                 code == 45862 || code == 45863 || code == 45864 || code == 45865 || code == 45866 || code == 45867 || code == 45868 || code == 45869 || code == 45870 || code == 45871 ||
                 code == 45872 || code == 45873 || code == 45874 || code == 45875 || code == 45876 || code == 45877 || code == 45878 || code == 45879 || code == 45880 || code == 45881 ||
                 code == 45882 || code == 45883 || code == 45884 || code == 45885 || code == 45886 || code == 45887 || code == 45888 || code == 45889 || code == 45890 || code == 45891 ||
                 code == 45892 || code == 45893 || code == 45894 || code == 45895 || code == 45896 || code == 45897 || code == 45898 || code == 45899 || code == 45900 || code == 45901 ||
                 code == 45902 || code == 45903 || code == 45904 || code == 45905 || code == 45906 || code == 45907 || code == 45911 || code == 45913 || code == 45914 || code == 45917 ||
                 code == 45920 || code == 45921 || code == 45922 || code == 45923 || code == 45926 || code == 45928 || code == 45930 || code == 45932 || code == 45933 || code == 45935 ||
                 code == 45938 || code == 45939 || code == 45941 || code == 45942 || code == 45943 || code == 45945 || code == 45946 || code == 45947 || code == 45948 || code == 45949 ||
                 code == 45950 || code == 45951 || code == 45954 || code == 45958 || code == 45959 || code == 45960 || code == 45961 || code == 45962 || code == 45963 || code == 45965 ||
                 code == 45966 || code == 45967 || code == 45969 || code == 45970 || code == 45971 || code == 45973 || code == 45974 || code == 45975 || code == 45976 || code == 45977 ||
                 code == 45978 || code == 45979 || code == 45980 || code == 45981 || code == 45982 || code == 45983 || code == 45986 || code == 45987 || code == 45988 || code == 45989 ||
                 code == 45990 || code == 45991 || code == 45993 || code == 45994 || code == 45995 || code == 45997 || code == 45998 || code == 45999 || code == 46000 || code == 46001 ||
                 code == 46002 || code == 46003 || code == 46004 || code == 46005 || code == 46006 || code == 46007 || code == 46008 || code == 46009 || code == 46010 || code == 46011 ||
                 code == 46012 || code == 46013 || code == 46014 || code == 46015 || code == 46016 || code == 46017 || code == 46018 || code == 46019 || code == 46022 || code == 46023 ||
                 code == 46025 || code == 46026 || code == 46029 || code == 46031 || code == 46033 || code == 46034 || code == 46035 || code == 46038 || code == 46040 || code == 46042 ||
                 code == 46044 || code == 46046 || code == 46047 || code == 46049 || code == 46050 || code == 46051 || code == 46053 || code == 46054 || code == 46055 || code == 46057 ||
                 code == 46058 || code == 46059 || code == 46060 || code == 46061 || code == 46062 || code == 46063 || code == 46064 || code == 46065 || code == 46066 || code == 46067 ||
                 code == 46068 || code == 46069 || code == 46070 || code == 46071 || code == 46072 || code == 46073 || code == 46074 || code == 46075 || code == 46077 || code == 46078 ||
                 code == 46079 || code == 46080 || code == 46081 || code == 46082 || code == 46083 || code == 46084 || code == 46085 || code == 46086 || code == 46087 || code == 46088 ||
                 code == 46089 || code == 46090 || code == 46091 || code == 46092 || code == 46093 || code == 46094 || code == 46095 || code == 46097 || code == 46098 || code == 46099 ||
                 code == 46100 || code == 46101 || code == 46102 || code == 46103 || code == 46105 || code == 46106 || code == 46107 || code == 46109 || code == 46110 || code == 46111 ||
                 code == 46113 || code == 46114 || code == 46115 || code == 46116 || code == 46117 || code == 46118 || code == 46119 || code == 46122 || code == 46124 || code == 46125 ||
                 code == 46126 || code == 46127 || code == 46128 || code == 46129 || code == 46130 || code == 46131 || code == 46133 || code == 46134 || code == 46135 || code == 46136 ||
                 code == 46137 || code == 46138 || code == 46139 || code == 46140 || code == 46141 || code == 46142 || code == 46143 || code == 46144 || code == 46145 || code == 46146 ||
                 code == 46147 || code == 46148 || code == 46149 || code == 46150 || code == 46151 || code == 46152 || code == 46153 || code == 46154 || code == 46155 || code == 46156 ||
                 code == 46157 || code == 46158 || code == 46159 || code == 46162 || code == 46163 || code == 46165 || code == 46166 || code == 46167 || code == 46169 || code == 46170 ||
                 code == 46171 || code == 46172 || code == 46173 || code == 46174 || code == 46175 || code == 46178 || code == 46180 || code == 46182 || code == 46183 || code == 46184 ||
                 code == 46185 || code == 46186 || code == 46187 || code == 46189 || code == 46190 || code == 46191 || code == 46192 || code == 46193 || code == 46194 || code == 46195 ||
                 code == 46196 || code == 46197 || code == 46198 || code == 46199 || code == 46200 || code == 46201 || code == 46202 || code == 46203 || code == 46204 || code == 46205 ||
                 code == 46206 || code == 46207 || code == 46209 || code == 46210 || code == 46211 || code == 46212 || code == 46213 || code == 46214 || code == 46215 || code == 46217 ||
                 code == 46218 || code == 46219 || code == 46220 || code == 46221 || code == 46222 || code == 46223 || code == 46224 || code == 46225 || code == 46226 || code == 46227 ||
                 code == 46228 || code == 46229 || code == 46230 || code == 46231 || code == 46232 || code == 46233 || code == 46234 || code == 46235 || code == 46236 || code == 46238 ||
                 code == 46239 || code == 46240 || code == 46241 || code == 46242 || code == 46243 || code == 46245 || code == 46246 || code == 46247 || code == 46249 || code == 46250 ||
                 code == 46251 || code == 46253 || code == 46254 || code == 46255 || code == 46256 || code == 46257 || code == 46258 || code == 46259 || code == 46260 || code == 46262 ||
                 code == 46264 || code == 46266 || code == 46267 || code == 46268 || code == 46269 || code == 46270 || code == 46271 || code == 46273 || code == 46274 || code == 46275 ||
                 code == 46277 || code == 46278 || code == 46279 || code == 46281 || code == 46282 || code == 46283 || code == 46284 || code == 46285 || code == 46286 || code == 46287 ||
                 code == 46289 || code == 46290 || code == 46291 || code == 46292 || code == 46294 || code == 46295 || code == 46296 || code == 46297 || code == 46298 || code == 46299 ||
                 code == 46302 || code == 46303 || code == 46305 || code == 46306 || code == 46309 || code == 46311 || code == 46312 || code == 46313 || code == 46314 || code == 46315 ||
                 code == 46318 || code == 46320 || code == 46322 || code == 46323 || code == 46324 || code == 46325 || code == 46326 || code == 46327 || code == 46329 || code == 46330 ||
                 code == 46331 || code == 46332 || code == 46333 || code == 46334 || code == 46335 || code == 46336 || code == 46337 || code == 46338 || code == 46339 || code == 46340 ||
                 code == 46341 || code == 46342 || code == 46343 || code == 46344 || code == 46345 || code == 46346 || code == 46347 || code == 46348 || code == 46349 || code == 46350 ||
                 code == 46351 || code == 46352 || code == 46353 || code == 46354 || code == 46355 || code == 46358 || code == 46359 || code == 46361 || code == 46362 || code == 46365 ||
                 code == 46366 || code == 46367 || code == 46368 || code == 46369 || code == 46370 || code == 46371 || code == 46374 || code == 46379 || code == 46380 || code == 46381 ||
                 code == 46382 || code == 46383 || code == 46386 || code == 46387 || code == 46389 || code == 46390 || code == 46391 || code == 46393 || code == 46394 || code == 46395 ||
                 code == 46396 || code == 46397 || code == 46398 || code == 46399 || code == 46402 || code == 46406 || code == 46407 || code == 46408 || code == 46409 || code == 46410 ||
                 code == 46414 || code == 46415 || code == 46417 || code == 46418 || code == 46419 || code == 46421 || code == 46422 || code == 46423 || code == 46424 || code == 46425 ||
                 code == 46426 || code == 46427 || code == 46430 || code == 46434 || code == 46435 || code == 46436 || code == 46437 || code == 46438 || code == 46439 || code == 46440 ||
                 code == 46441 || code == 46442 || code == 46443 || code == 46444 || code == 46445 || code == 46446 || code == 46447 || code == 46448 || code == 46449 || code == 46450 ||
                 code == 46451 || code == 46452 || code == 46453 || code == 46454 || code == 46455 || code == 46456 || code == 46457 || code == 46458 || code == 46459 || code == 46460 ||
                 code == 46461 || code == 46462 || code == 46463 || code == 46464 || code == 46465 || code == 46466 || code == 46467 || code == 46468 || code == 46469 || code == 46470 ||
                 code == 46471 || code == 46472 || code == 46473 || code == 46474 || code == 46475 || code == 46476 || code == 46477 || code == 46478 || code == 46479 || code == 46480 ||
                 code == 46481 || code == 46482 || code == 46483 || code == 46484 || code == 46485 || code == 46486 || code == 46487 || code == 46488 || code == 46489 || code == 46490 ||
                 code == 46491 || code == 46492 || code == 46493 || code == 46494 || code == 46495 || code == 46498 || code == 46499 || code == 46501 || code == 46502 || code == 46503 ||
                 code == 46505 || code == 46508 || code == 46509 || code == 46510 || code == 46511 || code == 46514 || code == 46518 || code == 46519 || code == 46520 || code == 46521 ||
                 code == 46522 || code == 46526 || code == 46527 || code == 46529 || code == 46530 || code == 46531 || code == 46533 || code == 46534 || code == 46535 || code == 46536 ||
                 code == 46537 || code == 46538 || code == 46539 || code == 46542 || code == 46546 || code == 46547 || code == 46548 || code == 46549 || code == 46550 || code == 46551 ||
                 code == 46553 || code == 46554 || code == 46555 || code == 46556 || code == 46557 || code == 46558 || code == 46559 || code == 46560 || code == 46561 || code == 46562 ||
                 code == 46563 || code == 46564 || code == 46565 || code == 46566 || code == 46567 || code == 46568 || code == 46569 || code == 46570 || code == 46571 || code == 46573 ||
                 code == 46574 || code == 46575 || code == 46576 || code == 46577 || code == 46578 || code == 46579 || code == 46580 || code == 46581 || code == 46582 || code == 46583 ||
                 code == 46584 || code == 46585 || code == 46586 || code == 46587 || code == 46588 || code == 46589 || code == 46590 || code == 46591 || code == 46592 || code == 46593 ||
                 code == 46594 || code == 46595 || code == 46596 || code == 46597 || code == 46598 || code == 46599 || code == 46600 || code == 46601 || code == 46602 || code == 46603 ||
                 code == 46604 || code == 46605 || code == 46606 || code == 46607 || code == 46610 || code == 46611 || code == 46613 || code == 46614 || code == 46615 || code == 46617 ||
                 code == 46618 || code == 46619 || code == 46620 || code == 46621 || code == 46622 || code == 46623 || code == 46624 || code == 46625 || code == 46626 || code == 46627 ||
                 code == 46628 || code == 46630 || code == 46631 || code == 46632 || code == 46633 || code == 46634 || code == 46635 || code == 46637 || code == 46638 || code == 46639 ||
                 code == 46640 || code == 46641 || code == 46642 || code == 46643 || code == 46645 || code == 46646 || code == 46647 || code == 46648 || code == 46649 || code == 46650 ||
                 code == 46651 || code == 46652 || code == 46653 || code == 46654 || code == 46655 || code == 46656 || code == 46657 || code == 46658 || code == 46659 || code == 46660 ||
                 code == 46661 || code == 46662 || code == 46663 || code == 46665 || code == 46666 || code == 46667 || code == 46668 || code == 46669 || code == 46670 || code == 46671 ||
                 code == 46672 || code == 46673 || code == 46674 || code == 46675 || code == 46676 || code == 46677 || code == 46678 || code == 46679 || code == 46680 || code == 46681 ||
                 code == 46682 || code == 46683 || code == 46684 || code == 46685 || code == 46686 || code == 46687 || code == 46688 || code == 46689 || code == 46690 || code == 46691 ||
                 code == 46693 || code == 46694 || code == 46695 || code == 46697 || code == 46698 || code == 46699 || code == 46700 || code == 46701 || code == 46702 || code == 46703 ||
                 code == 46704 || code == 46705 || code == 46706 || code == 46707 || code == 46708 || code == 46709 || code == 46710 || code == 46711 || code == 46712 || code == 46713 ||
                 code == 46714 || code == 46715 || code == 46716 || code == 46717 || code == 46718 || code == 46719 || code == 46720 || code == 46721 || code == 46722 || code == 46723 ||
                 code == 46724 || code == 46725 || code == 46726 || code == 46727 || code == 46728 || code == 46729 || code == 46730 || code == 46731 || code == 46732 || code == 46733 ||
                 code == 46734 || code == 46735 || code == 46736 || code == 46737 || code == 46738 || code == 46739 || code == 46740 || code == 46741 || code == 46742 || code == 46743 ||
                 code == 46744 || code == 46745 || code == 46746 || code == 46747 || code == 46750 || code == 46751 || code == 46753 || code == 46754 || code == 46755 || code == 46757 ||
                 code == 46758 || code == 46759 || code == 46760 || code == 46761 || code == 46762 || code == 46765 || code == 46766 || code == 46767 || code == 46768 || code == 46770 ||
                 code == 46771 || code == 46772 || code == 46773 || code == 46774 || code == 46775 || code == 46776 || code == 46777 || code == 46778 || code == 46779 || code == 46780 ||
                 code == 46781 || code == 46782 || code == 46783 || code == 46784 || code == 46785 || code == 46786 || code == 46787 || code == 46788 || code == 46789 || code == 46790 ||
                 code == 46791 || code == 46792 || code == 46793 || code == 46794 || code == 46795 || code == 46796 || code == 46797 || code == 46798 || code == 46799 || code == 46800 ||
                 code == 46801 || code == 46802 || code == 46803 || code == 46805 || code == 46806 || code == 46807 || code == 46808 || code == 46809 || code == 46810 || code == 46811 ||
                 code == 46812 || code == 46813 || code == 46814 || code == 46815 || code == 46816 || code == 46817 || code == 46818 || code == 46819 || code == 46820 || code == 46821 ||
                 code == 46822 || code == 46823 || code == 46824 || code == 46825 || code == 46826 || code == 46827 || code == 46828 || code == 46829 || code == 46830 || code == 46831 ||
                 code == 46833 || code == 46834 || code == 46835 || code == 46837 || code == 46838 || code == 46839 || code == 46841 || code == 46842 || code == 46843 || code == 46844 ||
                 code == 46845 || code == 46846 || code == 46847 || code == 46850 || code == 46851 || code == 46852 || code == 46854 || code == 46855 || code == 46856 || code == 46857 ||
                 code == 46858 || code == 46859 || code == 46860 || code == 46861 || code == 46862 || code == 46863 || code == 46864 || code == 46865 || code == 46866 || code == 46867 ||
                 code == 46868 || code == 46869 || code == 46870 || code == 46871 || code == 46872 || code == 46873 || code == 46874 || code == 46875 || code == 46876 || code == 46877 ||
                 code == 46878 || code == 46879 || code == 46880 || code == 46881 || code == 46882 || code == 46883 || code == 46884 || code == 46885 || code == 46886 || code == 46887 ||
                 code == 46890 || code == 46891 || code == 46893 || code == 46894 || code == 46897 || code == 46898 || code == 46899 || code == 46900 || code == 46901 || code == 46902 ||
                 code == 46903 || code == 46906 || code == 46908 || code == 46909 || code == 46910 || code == 46911 || code == 46912 || code == 46913 || code == 46914 || code == 46915 ||
                 code == 46917 || code == 46918 || code == 46919 || code == 46921 || code == 46922 || code == 46923 || code == 46925 || code == 46926 || code == 46927 || code == 46928 ||
                 code == 46929 || code == 46930 || code == 46931 || code == 46934 || code == 46935 || code == 46936 || code == 46937 || code == 46938 || code == 46939 || code == 46940 ||
                 code == 46941 || code == 46942 || code == 46943 || code == 46945 || code == 46946 || code == 46947 || code == 46949 || code == 46950 || code == 46951 || code == 46953 ||
                 code == 46954 || code == 46955 || code == 46956 || code == 46957 || code == 46958 || code == 46959 || code == 46962 || code == 46964 || code == 46966 || code == 46967 ||
                 code == 46968 || code == 46969 || code == 46970 || code == 46971 || code == 46974 || code == 46975 || code == 46977 || code == 46978 || code == 46979 || code == 46981 ||
                 code == 46982 || code == 46983 || code == 46984 || code == 46985 || code == 46986 || code == 46987 || code == 46990 || code == 46995 || code == 46996 || code == 46997 ||
                 code == 47002 || code == 47003 || code == 47005 || code == 47006 || code == 47007 || code == 47009 || code == 47010 || code == 47011 || code == 47012 || code == 47013 ||
                 code == 47014 || code == 47015 || code == 47018 || code == 47022 || code == 47023 || code == 47024 || code == 47025 || code == 47026 || code == 47027 || code == 47030 ||
                 code == 47031 || code == 47033 || code == 47034 || code == 47035 || code == 47036 || code == 47037 || code == 47038 || code == 47039 || code == 47040 || code == 47041 ||
                 code == 47042 || code == 47043 || code == 47044 || code == 47045 || code == 47046 || code == 47048 || code == 47050 || code == 47051 || code == 47052 || code == 47053 ||
                 code == 47054 || code == 47055 || code == 47056 || code == 47057 || code == 47058 || code == 47059 || code == 47060 || code == 47061 || code == 47062 || code == 47063 ||
                 code == 47064 || code == 47065 || code == 47066 || code == 47067 || code == 47068 || code == 47069 || code == 47070 || code == 47071 || code == 47072 || code == 47073 ||
                 code == 47074 || code == 47075 || code == 47076 || code == 47077 || code == 47078 || code == 47079 || code == 47080 || code == 47081 || code == 47082 || code == 47083 ||
                 code == 47086 || code == 47087 || code == 47089 || code == 47090 || code == 47091 || code == 47093 || code == 47094 || code == 47095 || code == 47096 || code == 47097 ||
                 code == 47098 || code == 47099 || code == 47102 || code == 47106 || code == 47107 || code == 47108 || code == 47109 || code == 47110 || code == 47114 || code == 47115 ||
                 code == 47117 || code == 47118 || code == 47119 || code == 47121 || code == 47122 || code == 47123 || code == 47124 || code == 47125 || code == 47126 || code == 47127 ||
                 code == 47130 || code == 47132 || code == 47134 || code == 47135 || code == 47136 || code == 47137 || code == 47138 || code == 47139 || code == 47142 || code == 47143 ||
                 code == 47145 || code == 47146 || code == 47147 || code == 47149 || code == 47150 || code == 47151 || code == 47152 || code == 47153 || code == 47154 || code == 47155 ||
                 code == 47158 || code == 47162 || code == 47163 || code == 47164 || code == 47165 || code == 47166 || code == 47167 || code == 47169 || code == 47170 || code == 47171 ||
                 code == 47173 || code == 47174 || code == 47175 || code == 47176 || code == 47177 || code == 47178 || code == 47179 || code == 47180 || code == 47181 || code == 47182 ||
                 code == 47183 || code == 47184 || code == 47186 || code == 47188 || code == 47189 || code == 47190 || code == 47191 || code == 47192 || code == 47193 || code == 47194 ||
                 code == 47195 || code == 47198 || code == 47199 || code == 47201 || code == 47202 || code == 47203 || code == 47205 || code == 47206 || code == 47207 || code == 47208 ||
                 code == 47209 || code == 47210 || code == 47211 || code == 47214 || code == 47216 || code == 47218 || code == 47219 || code == 47220 || code == 47221 || code == 47222 ||
                 code == 47223 || code == 47225 || code == 47226 || code == 47227 || code == 47229 || code == 47230 || code == 47231 || code == 47232 || code == 47233 || code == 47234 ||
                 code == 47235 || code == 47236 || code == 47237 || code == 47238 || code == 47239 || code == 47240 || code == 47241 || code == 47242 || code == 47243 || code == 47244 ||
                 code == 47246 || code == 47247 || code == 47248 || code == 47249 || code == 47250 || code == 47251 || code == 47252 || code == 47253 || code == 47254 || code == 47255 ||
                 code == 47256 || code == 47257 || code == 47258 || code == 47259 || code == 47260 || code == 47261 || code == 47262 || code == 47263 || code == 47264 || code == 47265 ||
                 code == 47266 || code == 47267 || code == 47268 || code == 47269 || code == 47270 || code == 47271 || code == 47273 || code == 47274 || code == 47275 || code == 47276 ||
                 code == 47277 || code == 47278 || code == 47279 || code == 47281 || code == 47282 || code == 47283 || code == 47285 || code == 47286 || code == 47287 || code == 47289 ||
                 code == 47290 || code == 47291 || code == 47292 || code == 47293 || code == 47294 || code == 47295 || code == 47298 || code == 47300 || code == 47302 || code == 47303 ||
                 code == 47304 || code == 47305 || code == 47306 || code == 47307 || code == 47309 || code == 47310 || code == 47311 || code == 47313 || code == 47314 || code == 47315 ||
                 code == 47317 || code == 47318 || code == 47319 || code == 47320 || code == 47321 || code == 47322 || code == 47323 || code == 47324 || code == 47326 || code == 47328 ||
                 code == 47330 || code == 47331 || code == 47332 || code == 47333 || code == 47334 || code == 47335 || code == 47338 || code == 47339 || code == 47341 || code == 47342 ||
                 code == 47343 || code == 47345 || code == 47346 || code == 47347 || code == 47348 || code == 47349 || code == 47350 || code == 47351 || code == 47354 || code == 47356 ||
                 code == 47358 || code == 47359 || code == 47360 || code == 47361 || code == 47362 || code == 47363 || code == 47365 || code == 47366 || code == 47367 || code == 47368 ||
                 code == 47369 || code == 47370 || code == 47371 || code == 47372 || code == 47373 || code == 47374 || code == 47375 || code == 47376 || code == 47377 || code == 47378 ||
                 code == 47379 || code == 47380 || code == 47381 || code == 47382 || code == 47383 || code == 47385 || code == 47386 || code == 47387 || code == 47388 || code == 47389 ||
                 code == 47390 || code == 47391 || code == 47393 || code == 47394 || code == 47395 || code == 47396 || code == 47397 || code == 47398 || code == 47399 || code == 47400 ||
                 code == 47401 || code == 47402 || code == 47403 || code == 47404 || code == 47405 || code == 47406 || code == 47407 || code == 47408 || code == 47409 || code == 47410 ||
                 code == 47411 || code == 47412 || code == 47413 || code == 47414 || code == 47415 || code == 47416 || code == 47417 || code == 47418 || code == 47419 || code == 47422 ||
                 code == 47423 || code == 47425 || code == 47426 || code == 47427 || code == 47429 || code == 47430 || code == 47431 || code == 47432 || code == 47433 || code == 47434 ||
                 code == 47435 || code == 47437 || code == 47438 || code == 47440 || code == 47442 || code == 47443 || code == 47444 || code == 47445 || code == 47446 || code == 47447 ||
                 code == 47450 || code == 47451 || code == 47453 || code == 47454 || code == 47455 || code == 47457 || code == 47458 || code == 47459 || code == 47460 || code == 47461 ||
                 code == 47462 || code == 47463 || code == 47466 || code == 47468 || code == 47470 || code == 47471 || code == 47472 || code == 47473 || code == 47474 || code == 47475 ||
                 code == 47478 || code == 47479 || code == 47481 || code == 47482 || code == 47483 || code == 47485 || code == 47486 || code == 47487 || code == 47488 || code == 47489 ||
                 code == 47490 || code == 47491 || code == 47494 || code == 47496 || code == 47499 || code == 47500 || code == 47503 || code == 47504 || code == 47505 || code == 47506 ||
                 code == 47507 || code == 47508 || code == 47509 || code == 47510 || code == 47511 || code == 47512 || code == 47513 || code == 47514 || code == 47515 || code == 47516 ||
                 code == 47517 || code == 47518 || code == 47519 || code == 47520 || code == 47521 || code == 47522 || code == 47523 || code == 47524 || code == 47525 || code == 47526 ||
                 code == 47527 || code == 47528 || code == 47529 || code == 47530 || code == 47531 || code == 47534 || code == 47535 || code == 47537 || code == 47538 || code == 47539 ||
                 code == 47541 || code == 47542 || code == 47543 || code == 47544 || code == 47545 || code == 47546 || code == 47547 || code == 47550 || code == 47552 || code == 47554 ||
                 code == 47555 || code == 47556 || code == 47557 || code == 47558 || code == 47559 || code == 47562 || code == 47563 || code == 47565 || code == 47571 || code == 47572 ||
                 code == 47573 || code == 47574 || code == 47575 || code == 47578 || code == 47580 || code == 47583 || code == 47584 || code == 47586 || code == 47590 || code == 47591 ||
                 code == 47593 || code == 47594 || code == 47595 || code == 47597 || code == 47598 || code == 47599 || code == 47600 || code == 47601 || code == 47602 || code == 47603 ||
                 code == 47606 || code == 47611 || code == 47612 || code == 47613 || code == 47614 || code == 47615 || code == 47618 || code == 47619 || code == 47620 || code == 47621 ||
                 code == 47622 || code == 47623 || code == 47625 || code == 47626 || code == 47627 || code == 47628 || code == 47629 || code == 47630 || code == 47631 || code == 47632 ||
                 code == 47633 || code == 47634 || code == 47635 || code == 47636 || code == 47638 || code == 47639 || code == 47640 || code == 47641 || code == 47642 || code == 47643 ||
                 code == 47644 || code == 47645 || code == 47646 || code == 47647 || code == 47648 || code == 47649 || code == 47650 || code == 47651 || code == 47652 || code == 47653 ||
                 code == 47654 || code == 47655 || code == 47656 || code == 47657 || code == 47658 || code == 47659 || code == 47660 || code == 47661 || code == 47662 || code == 47663 ||
                 code == 47664 || code == 47665 || code == 47666 || code == 47667 || code == 47668 || code == 47669 || code == 47670 || code == 47671 || code == 47674 || code == 47675 ||
                 code == 47677 || code == 47678 || code == 47679 || code == 47681 || code == 47683 || code == 47684 || code == 47685 || code == 47686 || code == 47687 || code == 47690 ||
                 code == 47692 || code == 47695 || code == 47696 || code == 47697 || code == 47698 || code == 47702 || code == 47703 || code == 47705 || code == 47706 || code == 47707 ||
                 code == 47709 || code == 47710 || code == 47711 || code == 47712 || code == 47713 || code == 47714 || code == 47715 || code == 47718 || code == 47722 || code == 47723 ||
                 code == 47724 || code == 47725 || code == 47726 || code == 47727 || code == 47730 || code == 47731 || code == 47733 || code == 47734 || code == 47735 || code == 47737 ||
                 code == 47738 || code == 47739 || code == 47740 || code == 47741 || code == 47742 || code == 47743 || code == 47744 || code == 47745 || code == 47746 || code == 47750 ||
                 code == 47752 || code == 47753 || code == 47754 || code == 47755 || code == 47757 || code == 47758 || code == 47759 || code == 47760 || code == 47761 || code == 47762 ||
                 code == 47763 || code == 47764 || code == 47765 || code == 47766 || code == 47767 || code == 47768 || code == 47769 || code == 47770 || code == 47771 || code == 47772 ||
                 code == 47773 || code == 47774 || code == 47775 || code == 47776 || code == 47777 || code == 47778 || code == 47779 || code == 47780 || code == 47781 || code == 47782 ||
                 code == 47783 || code == 47786 || code == 47789 || code == 47790 || code == 47791 || code == 47793 || code == 47795 || code == 47796 || code == 47797 || code == 47798 ||
                 code == 47799 || code == 47802 || code == 47804 || code == 47806 || code == 47807 || code == 47808 || code == 47809 || code == 47810 || code == 47811 || code == 47813 ||
                 code == 47814 || code == 47815 || code == 47817 || code == 47818 || code == 47819 || code == 47820 || code == 47821 || code == 47822 || code == 47823 || code == 47824 ||
                 code == 47825 || code == 47826 || code == 47827 || code == 47828 || code == 47829 || code == 47830 || code == 47831 || code == 47834 || code == 47835 || code == 47836 ||
                 code == 47837 || code == 47838 || code == 47839 || code == 47840 || code == 47841 || code == 47842 || code == 47843 || code == 47844 || code == 47845 || code == 47846 ||
                 code == 47847 || code == 47848 || code == 47849 || code == 47850 || code == 47851 || code == 47852 || code == 47853 || code == 47854 || code == 47855 || code == 47856 ||
                 code == 47857 || code == 47858 || code == 47859 || code == 47860 || code == 47861 || code == 47862 || code == 47863 || code == 47864 || code == 47865 || code == 47866 ||
                 code == 47867 || code == 47869 || code == 47870 || code == 47871 || code == 47873 || code == 47874 || code == 47875 || code == 47877 || code == 47878 || code == 47879 ||
                 code == 47880 || code == 47881 || code == 47882 || code == 47883 || code == 47884 || code == 47886 || code == 47888 || code == 47890 || code == 47891 || code == 47892 ||
                 code == 47893 || code == 47894 || code == 47895 || code == 47897 || code == 47898 || code == 47899 || code == 47901 || code == 47902 || code == 47903 || code == 47905 ||
                 code == 47906 || code == 47907 || code == 47908 || code == 47909 || code == 47910 || code == 47911 || code == 47912 || code == 47914 || code == 47916 || code == 47917 ||
                 code == 47918 || code == 47919 || code == 47920 || code == 47921 || code == 47922 || code == 47923 || code == 47927 || code == 47929 || code == 47930 || code == 47935 ||
                 code == 47936 || code == 47937 || code == 47938 || code == 47939 || code == 47942 || code == 47944 || code == 47946 || code == 47947 || code == 47948 || code == 47950 ||
                 code == 47953 || code == 47954 || code == 47955 || code == 47957 || code == 47958 || code == 47959 || code == 47961 || code == 47962 || code == 47963 || code == 47964 ||
                 code == 47965 || code == 47966 || code == 47967 || code == 47968 || code == 47970 || code == 47972 || code == 47973 || code == 47974 || code == 47975 || code == 47976 ||
                 code == 47977 || code == 47978 || code == 47979 || code == 47981 || code == 47982 || code == 47983 || code == 47984 || code == 47985 || code == 47986 || code == 47987 ||
                 code == 47988 || code == 47989 || code == 47990 || code == 47991 || code == 47992 || code == 47993 || code == 47994 || code == 47995 || code == 47996 || code == 47997 ||
                 code == 47998 || code == 47999 || code == 48000 || code == 48001 || code == 48002 || code == 48003 || code == 48004 || code == 48005 || code == 48006 || code == 48007 ||
                 code == 48009 || code == 48010 || code == 48011 || code == 48013 || code == 48014 || code == 48015 || code == 48017 || code == 48018 || code == 48019 || code == 48020 ||
                 code == 48021 || code == 48022 || code == 48023 || code == 48024 || code == 48025 || code == 48026 || code == 48027 || code == 48028 || code == 48029 || code == 48030 ||
                 code == 48031 || code == 48032 || code == 48033 || code == 48034 || code == 48035 || code == 48037 || code == 48038 || code == 48039 || code == 48041 || code == 48042 ||
                 code == 48043 || code == 48045 || code == 48046 || code == 48047 || code == 48048 || code == 48049 || code == 48050 || code == 48051 || code == 48053 || code == 48054 ||
                 code == 48056 || code == 48057 || code == 48058 || code == 48059 || code == 48060 || code == 48061 || code == 48062 || code == 48063 || code == 48065 || code == 48066 ||
                 code == 48067 || code == 48069 || code == 48070 || code == 48071 || code == 48073 || code == 48074 || code == 48075 || code == 48076 || code == 48077 || code == 48078 ||
                 code == 48079 || code == 48081 || code == 48082 || code == 48084 || code == 48085 || code == 48086 || code == 48087 || code == 48088 || code == 48089 || code == 48090 ||
                 code == 48091 || code == 48092 || code == 48093 || code == 48094 || code == 48095 || code == 48096 || code == 48097 || code == 48098 || code == 48099 || code == 48100 ||
                 code == 48101 || code == 48102 || code == 48103 || code == 48104 || code == 48105 || code == 48106 || code == 48107 || code == 48108 || code == 48109 || code == 48110 ||
                 code == 48111 || code == 48112 || code == 48113 || code == 48114 || code == 48115 || code == 48116 || code == 48117 || code == 48118 || code == 48119 || code == 48122 ||
                 code == 48123 || code == 48125 || code == 48126 || code == 48129 || code == 48131 || code == 48132 || code == 48133 || code == 48134 || code == 48135 || code == 48138 ||
                 code == 48142 || code == 48144 || code == 48146 || code == 48147 || code == 48153 || code == 48154 || code == 48160 || code == 48161 || code == 48162 || code == 48163 ||
                 code == 48166 || code == 48168 || code == 48170 || code == 48171 || code == 48172 || code == 48174 || code == 48175 || code == 48178 || code == 48179 || code == 48181 ||
                 code == 48182 || code == 48183 || code == 48185 || code == 48186 || code == 48187 || code == 48188 || code == 48189 || code == 48190 || code == 48191 || code == 48194 ||
                 code == 48198 || code == 48199 || code == 48200 || code == 48202 || code == 48203 || code == 48206 || code == 48207 || code == 48209 || code == 48210 || code == 48211 ||
                 code == 48212 || code == 48213 || code == 48214 || code == 48215 || code == 48216 || code == 48217 || code == 48218 || code == 48219 || code == 48220 || code == 48222 ||
                 code == 48223 || code == 48224 || code == 48225 || code == 48226 || code == 48227 || code == 48228 || code == 48229 || code == 48230 || code == 48231 || code == 48232 ||
                 code == 48233 || code == 48234 || code == 48235 || code == 48236 || code == 48237 || code == 48238 || code == 48239 || code == 48240 || code == 48241 || code == 48242 ||
                 code == 48243 || code == 48244 || code == 48245 || code == 48246 || code == 48247 || code == 48248 || code == 48249 || code == 48250 || code == 48251 || code == 48252 ||
                 code == 48253 || code == 48254 || code == 48255 || code == 48256 || code == 48257 || code == 48258 || code == 48259 || code == 48262 || code == 48263 || code == 48265 ||
                 code == 48266 || code == 48269 || code == 48271 || code == 48272 || code == 48273 || code == 48274 || code == 48275 || code == 48278 || code == 48280 || code == 48283 ||
                 code == 48284 || code == 48285 || code == 48286 || code == 48287 || code == 48290 || code == 48291 || code == 48293 || code == 48294 || code == 48297 || code == 48298 ||
                 code == 48299 || code == 48300 || code == 48301 || code == 48302 || code == 48303 || code == 48306 || code == 48310 || code == 48311 || code == 48312 || code == 48313 ||
                 code == 48314 || code == 48315 || code == 48318 || code == 48319 || code == 48321 || code == 48322 || code == 48323 || code == 48325 || code == 48326 || code == 48327 ||
                 code == 48328 || code == 48329 || code == 48330 || code == 48331 || code == 48332 || code == 48334 || code == 48338 || code == 48339 || code == 48340 || code == 48342 ||
                 code == 48343 || code == 48345 || code == 48346 || code == 48347 || code == 48349 || code == 48350 || code == 48351 || code == 48352 || code == 48353 || code == 48354 ||
                 code == 48355 || code == 48356 || code == 48357 || code == 48358 || code == 48359 || code == 48360 || code == 48361 || code == 48362 || code == 48363 || code == 48364 ||
                 code == 48365 || code == 48366 || code == 48367 || code == 48368 || code == 48369 || code == 48370 || code == 48371 || code == 48375 || code == 48377 || code == 48378 ||
                 code == 48379 || code == 48381 || code == 48382 || code == 48383 || code == 48384 || code == 48385 || code == 48386 || code == 48387 || code == 48390 || code == 48392 ||
                 code == 48394 || code == 48395 || code == 48396 || code == 48397 || code == 48398 || code == 48399 || code == 48401 || code == 48402 || code == 48403 || code == 48405 ||
                 code == 48406 || code == 48407 || code == 48408 || code == 48409 || code == 48410 || code == 48411 || code == 48412 || code == 48413 || code == 48414 || code == 48415 ||
                 code == 48416 || code == 48417 || code == 48418 || code == 48419 || code == 48421 || code == 48422 || code == 48423 || code == 48424 || code == 48425 || code == 48426 ||
                 code == 48427 || code == 48429 || code == 48430 || code == 48431 || code == 48432 || code == 48433 || code == 48434 || code == 48435 || code == 48436 || code == 48437 ||
                 code == 48438 || code == 48439 || code == 48440 || code == 48441 || code == 48442 || code == 48443 || code == 48444 || code == 48445 || code == 48446 || code == 48447 ||
                 code == 48449 || code == 48450 || code == 48451 || code == 48452 || code == 48453 || code == 48454 || code == 48455 || code == 48458 || code == 48459 || code == 48461 ||
                 code == 48462 || code == 48463 || code == 48465 || code == 48466 || code == 48467 || code == 48468 || code == 48469 || code == 48470 || code == 48471 || code == 48474 ||
                 code == 48475 || code == 48476 || code == 48477 || code == 48478 || code == 48479 || code == 48480 || code == 48481 || code == 48482 || code == 48483 || code == 48485 ||
                 code == 48486 || code == 48487 || code == 48489 || code == 48490 || code == 48491 || code == 48492 || code == 48493 || code == 48494 || code == 48495 || code == 48496 ||
                 code == 48497 || code == 48498 || code == 48499 || code == 48500 || code == 48501 || code == 48502 || code == 48503 || code == 48504 || code == 48505 || code == 48506 ||
                 code == 48507 || code == 48508 || code == 48509 || code == 48510 || code == 48511 || code == 48514 || code == 48515 || code == 48517 || code == 48518 || code == 48523 ||
                 code == 48524 || code == 48525 || code == 48526 || code == 48527 || code == 48530 || code == 48532 || code == 48534 || code == 48535 || code == 48536 || code == 48539 ||
                 code == 48541 || code == 48542 || code == 48543 || code == 48544 || code == 48545 || code == 48546 || code == 48547 || code == 48549 || code == 48550 || code == 48551 ||
                 code == 48552 || code == 48553 || code == 48554 || code == 48555 || code == 48556 || code == 48557 || code == 48558 || code == 48559 || code == 48561 || code == 48562 ||
                 code == 48563 || code == 48564 || code == 48565 || code == 48566 || code == 48567 || code == 48569 || code == 48570 || code == 48571 || code == 48572 || code == 48573 ||
                 code == 48574 || code == 48575 || code == 48576 || code == 48577 || code == 48578 || code == 48579 || code == 48580 || code == 48581 || code == 48582 || code == 48583 ||
                 code == 48584 || code == 48585 || code == 48586 || code == 48587 || code == 48588 || code == 48589 || code == 48590 || code == 48591 || code == 48592 || code == 48593 ||
                 code == 48594 || code == 48595 || code == 48598 || code == 48599 || code == 48601 || code == 48602 || code == 48603 || code == 48605 || code == 48606 || code == 48607 ||
                 code == 48608 || code == 48609 || code == 48610 || code == 48611 || code == 48612 || code == 48613 || code == 48614 || code == 48615 || code == 48616 || code == 48618 ||
                 code == 48619 || code == 48620 || code == 48621 || code == 48622 || code == 48623 || code == 48625 || code == 48626 || code == 48627 || code == 48629 || code == 48630 ||
                 code == 48631 || code == 48633 || code == 48634 || code == 48635 || code == 48636 || code == 48637 || code == 48638 || code == 48639 || code == 48641 || code == 48642 ||
                 code == 48644 || code == 48646 || code == 48647 || code == 48648 || code == 48649 || code == 48650 || code == 48651 || code == 48654 || code == 48655 || code == 48657 ||
                 code == 48658 || code == 48659 || code == 48661 || code == 48662 || code == 48663 || code == 48664 || code == 48665 || code == 48666 || code == 48667 || code == 48670 ||
                 code == 48672 || code == 48673 || code == 48674 || code == 48675 || code == 48676 || code == 48677 || code == 48678 || code == 48679 || code == 48680 || code == 48681 ||
                 code == 48682 || code == 48683 || code == 48684 || code == 48685 || code == 48686 || code == 48687 || code == 48688 || code == 48689 || code == 48690 || code == 48691 ||
                 code == 48692 || code == 48693 || code == 48694 || code == 48695 || code == 48696 || code == 48697 || code == 48698 || code == 48699 || code == 48700 || code == 48701 ||
                 code == 48702 || code == 48703 || code == 48704 || code == 48705 || code == 48706 || code == 48707 || code == 48710 || code == 48711 || code == 48713 || code == 48714 ||
                 code == 48715 || code == 48717 || code == 48719 || code == 48720 || code == 48721 || code == 48722 || code == 48723 || code == 48726 || code == 48728 || code == 48732 ||
                 code == 48733 || code == 48734 || code == 48735 || code == 48738 || code == 48739 || code == 48741 || code == 48742 || code == 48743 || code == 48745 || code == 48747 ||
                 code == 48748 || code == 48749 || code == 48750 || code == 48751 || code == 48754 || code == 48758 || code == 48759 || code == 48760 || code == 48761 || code == 48762 ||
                 code == 48766 || code == 48767 || code == 48769 || code == 48770 || code == 48771 || code == 48773 || code == 48774 || code == 48775 || code == 48776 || code == 48777 ||
                 code == 48778 || code == 48779 || code == 48782 || code == 48786 || code == 48787 || code == 48788 || code == 48789 || code == 48790 || code == 48791 || code == 48794 ||
                 code == 48795 || code == 48796 || code == 48797 || code == 48798 || code == 48799 || code == 48800 || code == 48801 || code == 48802 || code == 48803 || code == 48804 ||
                 code == 48805 || code == 48806 || code == 48807 || code == 48809 || code == 48810 || code == 48811 || code == 48812 || code == 48813 || code == 48814 || code == 48815 ||
                 code == 48816 || code == 48817 || code == 48818 || code == 48819 || code == 48820 || code == 48821 || code == 48822 || code == 48823 || code == 48824 || code == 48825 ||
                 code == 48826 || code == 48827 || code == 48828 || code == 48829 || code == 48830 || code == 48831 || code == 48832 || code == 48833 || code == 48834 || code == 48835 ||
                 code == 48836 || code == 48837 || code == 48838 || code == 48839 || code == 48840 || code == 48841 || code == 48842 || code == 48843 || code == 48844 || code == 48845 ||
                 code == 48846 || code == 48847 || code == 48850 || code == 48851 || code == 48853 || code == 48854 || code == 48857 || code == 48858 || code == 48859 || code == 48860 ||
                 code == 48861 || code == 48862 || code == 48863 || code == 48865 || code == 48866 || code == 48870 || code == 48871 || code == 48872 || code == 48873 || code == 48874 ||
                 code == 48875 || code == 48877 || code == 48878 || code == 48879 || code == 48880 || code == 48881 || code == 48882 || code == 48883 || code == 48884 || code == 48885 ||
                 code == 48886 || code == 48887 || code == 48888 || code == 48889 || code == 48890 || code == 48891 || code == 48892 || code == 48893 || code == 48894 || code == 48895 ||
                 code == 48896 || code == 48898 || code == 48899 || code == 48900 || code == 48901 || code == 48902 || code == 48903 || code == 48906 || code == 48907 || code == 48908 ||
                 code == 48909 || code == 48910 || code == 48911 || code == 48912 || code == 48913 || code == 48914 || code == 48915 || code == 48916 || code == 48917 || code == 48918 ||
                 code == 48919 || code == 48922 || code == 48926 || code == 48927 || code == 48928 || code == 48929 || code == 48930 || code == 48931 || code == 48932 || code == 48933 ||
                 code == 48934 || code == 48935 || code == 48936 || code == 48937 || code == 48938 || code == 48939 || code == 48940 || code == 48941 || code == 48942 || code == 48943 ||
                 code == 48944 || code == 48945 || code == 48946 || code == 48947 || code == 48948 || code == 48949 || code == 48950 || code == 48951 || code == 48952 || code == 48953 ||
                 code == 48954 || code == 48955 || code == 48956 || code == 48957 || code == 48958 || code == 48959 || code == 48962 || code == 48963 || code == 48965 || code == 48966 ||
                 code == 48967 || code == 48969 || code == 48970 || code == 48971 || code == 48972 || code == 48973 || code == 48974 || code == 48975 || code == 48978 || code == 48979 ||
                 code == 48980 || code == 48982 || code == 48983 || code == 48984 || code == 48985 || code == 48986 || code == 48987 || code == 48988 || code == 48989 || code == 48990 ||
                 code == 48991 || code == 48992 || code == 48993 || code == 48994 || code == 48995 || code == 48996 || code == 48997 || code == 48998 || code == 48999 || code == 49000 ||
                 code == 49001 || code == 49002 || code == 49003 || code == 49004 || code == 49005 || code == 49006 || code == 49007 || code == 49008 || code == 49009 || code == 49010 ||
                 code == 49011 || code == 49012 || code == 49013 || code == 49014 || code == 49015 || code == 49016 || code == 49017 || code == 49018 || code == 49019 || code == 49020 ||
                 code == 49021 || code == 49022 || code == 49023 || code == 49024 || code == 49025 || code == 49026 || code == 49027 || code == 49028 || code == 49029 || code == 49030 ||
                 code == 49031 || code == 49032 || code == 49033 || code == 49034 || code == 49035 || code == 49036 || code == 49037 || code == 49038 || code == 49039 || code == 49040 ||
                 code == 49041 || code == 49042 || code == 49043 || code == 49045 || code == 49046 || code == 49047 || code == 49048 || code == 49049 || code == 49050 || code == 49051 ||
                 code == 49052 || code == 49053 || code == 49054 || code == 49055 || code == 49056 || code == 49057 || code == 49058 || code == 49059 || code == 49060 || code == 49061 ||
                 code == 49062 || code == 49063 || code == 49064 || code == 49065 || code == 49066 || code == 49067 || code == 49068 || code == 49069 || code == 49070 || code == 49071 ||
                 code == 49073 || code == 49074 || code == 49075 || code == 49076 || code == 49077 || code == 49078 || code == 49079 || code == 49080 || code == 49081 || code == 49082 ||
                 code == 49083 || code == 49084 || code == 49085 || code == 49086 || code == 49087 || code == 49088 || code == 49089 || code == 49090 || code == 49091 || code == 49092 ||
                 code == 49094 || code == 49095 || code == 49096 || code == 49097 || code == 49098 || code == 49099 || code == 49102 || code == 49103 || code == 49105 || code == 49106 ||
                 code == 49107 || code == 49109 || code == 49110 || code == 49111 || code == 49112 || code == 49113 || code == 49114 || code == 49115 || code == 49117 || code == 49118 ||
                 code == 49120 || code == 49122 || code == 49123 || code == 49124 || code == 49125 || code == 49126 || code == 49127 || code == 49128 || code == 49129 || code == 49130 ||
                 code == 49131 || code == 49132 || code == 49133 || code == 49134 || code == 49135 || code == 49136 || code == 49137 || code == 49138 || code == 49139 || code == 49140 ||
                 code == 49141 || code == 49142 || code == 49143 || code == 49144 || code == 49145 || code == 49146 || code == 49147 || code == 49148 || code == 49149 || code == 49150 ||
                 code == 49151 || code == 49152 || code == 49153 || code == 49154 || code == 49155 || code == 49156 || code == 49157 || code == 49158 || code == 49159 || code == 49160 ||
                 code == 49161 || code == 49162 || code == 49163 || code == 49164 || code == 49165 || code == 49166 || code == 49167 || code == 49168 || code == 49169 || code == 49170 ||
                 code == 49171 || code == 49172 || code == 49173 || code == 49174 || code == 49175 || code == 49176 || code == 49177 || code == 49178 || code == 49179 || code == 49180 ||
                 code == 49181 || code == 49182 || code == 49183 || code == 49184 || code == 49185 || code == 49186 || code == 49187 || code == 49188 || code == 49189 || code == 49190 ||
                 code == 49191 || code == 49192 || code == 49193 || code == 49194 || code == 49195 || code == 49196 || code == 49197 || code == 49198 || code == 49199 || code == 49200 ||
                 code == 49201 || code == 49202 || code == 49203 || code == 49204 || code == 49205 || code == 49206 || code == 49207 || code == 49208 || code == 49209 || code == 49210 ||
                 code == 49211 || code == 49213 || code == 49214 || code == 49215 || code == 49216 || code == 49217 || code == 49218 || code == 49219 || code == 49220 || code == 49221 ||
                 code == 49222 || code == 49223 || code == 49224 || code == 49225 || code == 49226 || code == 49227 || code == 49228 || code == 49229 || code == 49230 || code == 49231 ||
                 code == 49232 || code == 49234 || code == 49235 || code == 49236 || code == 49237 || code == 49238 || code == 49239 || code == 49241 || code == 49242 || code == 49243 ||
                 code == 49245 || code == 49246 || code == 49247 || code == 49249 || code == 49250 || code == 49251 || code == 49252 || code == 49253 || code == 49254 || code == 49255 ||
                 code == 49258 || code == 49259 || code == 49260 || code == 49261 || code == 49262 || code == 49263 || code == 49264 || code == 49265 || code == 49266 || code == 49267 ||
                 code == 49268 || code == 49269 || code == 49270 || code == 49271 || code == 49272 || code == 49273 || code == 49274 || code == 49275 || code == 49276 || code == 49277 ||
                 code == 49278 || code == 49279 || code == 49280 || code == 49281 || code == 49282 || code == 49283 || code == 49284 || code == 49285 || code == 49286 || code == 49287 ||
                 code == 49288 || code == 49289 || code == 49290 || code == 49291 || code == 49292 || code == 49293 || code == 49294 || code == 49295 || code == 49298 || code == 49299 ||
                 code == 49301 || code == 49302 || code == 49303 || code == 49305 || code == 49306 || code == 49307 || code == 49308 || code == 49309 || code == 49310 || code == 49311 ||
                 code == 49314 || code == 49316 || code == 49318 || code == 49319 || code == 49320 || code == 49321 || code == 49322 || code == 49323 || code == 49326 || code == 49329 ||
                 code == 49330 || code == 49335 || code == 49336 || code == 49337 || code == 49338 || code == 49339 || code == 49342 || code == 49346 || code == 49347 || code == 49348 ||
                 code == 49350 || code == 49351 || code == 49354 || code == 49355 || code == 49357 || code == 49358 || code == 49359 || code == 49361 || code == 49362 || code == 49363 ||
                 code == 49364 || code == 49365 || code == 49366 || code == 49367 || code == 49370 || code == 49374 || code == 49375 || code == 49376 || code == 49377 || code == 49378 ||
                 code == 49379 || code == 49382 || code == 49383 || code == 49385 || code == 49386 || code == 49387 || code == 49389 || code == 49390 || code == 49391 || code == 49392 ||
                 code == 49393 || code == 49394 || code == 49395 || code == 49398 || code == 49400 || code == 49402 || code == 49403 || code == 49404 || code == 49405 || code == 49406 ||
                 code == 49407 || code == 49409 || code == 49410 || code == 49411 || code == 49413 || code == 49414 || code == 49415 || code == 49417 || code == 49418 || code == 49419 ||
                 code == 49420 || code == 49421 || code == 49422 || code == 49423 || code == 49425 || code == 49426 || code == 49427 || code == 49428 || code == 49430 || code == 49431 ||
                 code == 49432 || code == 49433 || code == 49434 || code == 49435 || code == 49441 || code == 49442 || code == 49445 || code == 49448 || code == 49449 || code == 49450 ||
                 code == 49451 || code == 49454 || code == 49458 || code == 49459 || code == 49460 || code == 49461 || code == 49463 || code == 49466 || code == 49467 || code == 49469 ||
                 code == 49470 || code == 49471 || code == 49473 || code == 49474 || code == 49475 || code == 49476 || code == 49477 || code == 49478 || code == 49479 || code == 49482 ||
                 code == 49486 || code == 49487 || code == 49488 || code == 49489 || code == 49490 || code == 49491 || code == 49494 || code == 49495 || code == 49497 || code == 49498 ||
                 code == 49499 || code == 49501 || code == 49502 || code == 49503 || code == 49504 || code == 49505 || code == 49506 || code == 49507 || code == 49510 || code == 49514 ||
                 code == 49515 || code == 49516 || code == 49517 || code == 49518 || code == 49519 || code == 49521 || code == 49522 || code == 49523 || code == 49525 || code == 49526 ||
                 code == 49527 || code == 49529 || code == 49530 || code == 49531 || code == 49532 || code == 49533 || code == 49534 || code == 49535 || code == 49536 || code == 49537 ||
                 code == 49538 || code == 49539 || code == 49540 || code == 49542 || code == 49543 || code == 49544 || code == 49545 || code == 49546 || code == 49547 || code == 49551 ||
                 code == 49553 || code == 49554 || code == 49555 || code == 49557 || code == 49559 || code == 49560 || code == 49561 || code == 49562 || code == 49563 || code == 49566 ||
                 code == 49568 || code == 49570 || code == 49571 || code == 49572 || code == 49574 || code == 49575 || code == 49578 || code == 49579 || code == 49581 || code == 49582 ||
                 code == 49583 || code == 49585 || code == 49586 || code == 49587 || code == 49588 || code == 49589 || code == 49590 || code == 49591 || code == 49592 || code == 49593 ||
                 code == 49594 || code == 49595 || code == 49596 || code == 49598 || code == 49599 || code == 49600 || code == 49601 || code == 49602 || code == 49603 || code == 49605 ||
                 code == 49606 || code == 49607 || code == 49609 || code == 49610 || code == 49611 || code == 49613 || code == 49614 || code == 49615 || code == 49616 || code == 49617 ||
                 code == 49618 || code == 49619 || code == 49621 || code == 49622 || code == 49625 || code == 49626 || code == 49627 || code == 49628 || code == 49629 || code == 49630 ||
                 code == 49631 || code == 49633 || code == 49634 || code == 49635 || code == 49637 || code == 49638 || code == 49639 || code == 49641 || code == 49642 || code == 49643 ||
                 code == 49644 || code == 49645 || code == 49646 || code == 49647 || code == 49650 || code == 49652 || code == 49653 || code == 49654 || code == 49655 || code == 49656 ||
                 code == 49657 || code == 49658 || code == 49659 || code == 49662 || code == 49663 || code == 49665 || code == 49666 || code == 49667 || code == 49669 || code == 49670 ||
                 code == 49671 || code == 49672 || code == 49673 || code == 49674 || code == 49675 || code == 49678 || code == 49680 || code == 49682 || code == 49683 || code == 49684 ||
                 code == 49685 || code == 49686 || code == 49687 || code == 49690 || code == 49691 || code == 49693 || code == 49694 || code == 49697 || code == 49698 || code == 49699 ||
                 code == 49700 || code == 49701 || code == 49702 || code == 49703 || code == 49706 || code == 49708 || code == 49710 || code == 49712 || code == 49715 || code == 49717 ||
                 code == 49718 || code == 49719 || code == 49720 || code == 49721 || code == 49722 || code == 49723 || code == 49724 || code == 49725 || code == 49726 || code == 49727 ||
                 code == 49728 || code == 49729 || code == 49730 || code == 49731 || code == 49732 || code == 49733 || code == 49734 || code == 49735 || code == 49737 || code == 49738 ||
                 code == 49739 || code == 49740 || code == 49741 || code == 49742 || code == 49743 || code == 49746 || code == 49747 || code == 49749 || code == 49750 || code == 49751 ||
                 code == 49753 || code == 49754 || code == 49755 || code == 49756 || code == 49757 || code == 49758 || code == 49759 || code == 49761 || code == 49762 || code == 49763 ||
                 code == 49764 || code == 49766 || code == 49767 || code == 49768 || code == 49769 || code == 49770 || code == 49771 || code == 49774 || code == 49775 || code == 49777 ||
                 code == 49778 || code == 49779 || code == 49781 || code == 49782 || code == 49783 || code == 49784 || code == 49785 || code == 49786 || code == 49787 || code == 49790 ||
                 code == 49792 || code == 49794 || code == 49795 || code == 49796 || code == 49797 || code == 49798 || code == 49799 || code == 49802 || code == 49803 || code == 49804 ||
                 code == 49805 || code == 49806 || code == 49807 || code == 49809 || code == 49810 || code == 49811 || code == 49812 || code == 49813 || code == 49814 || code == 49815 ||
                 code == 49817 || code == 49818 || code == 49820 || code == 49822 || code == 49823 || code == 49824 || code == 49825 || code == 49826 || code == 49827 || code == 49830 ||
                 code == 49831 || code == 49833 || code == 49834 || code == 49835 || code == 49838 || code == 49839 || code == 49840 || code == 49841 || code == 49842 || code == 49843 ||
                 code == 49846 || code == 49848 || code == 49850 || code == 49851 || code == 49852 || code == 49853 || code == 49854 || code == 49855 || code == 49856 || code == 49857 ||
                 code == 49858 || code == 49859 || code == 49860 || code == 49861 || code == 49862 || code == 49863 || code == 49864 || code == 49865 || code == 49866 || code == 49867 ||
                 code == 49868 || code == 49869 || code == 49870 || code == 49871 || code == 49872 || code == 49873 || code == 49874 || code == 49875 || code == 49876 || code == 49877 ||
                 code == 49878 || code == 49879 || code == 49880 || code == 49881 || code == 49882 || code == 49883 || code == 49886 || code == 49887 || code == 49889 || code == 49890 ||
                 code == 49893 || code == 49894 || code == 49895 || code == 49896 || code == 49897 || code == 49898 || code == 49902 || code == 49904 || code == 49906 || code == 49907 ||
                 code == 49908 || code == 49909 || code == 49911 || code == 49914 || code == 49917 || code == 49918 || code == 49919 || code == 49921 || code == 49922 || code == 49923 ||
                 code == 49924 || code == 49925 || code == 49926 || code == 49927 || code == 49930 || code == 49931 || code == 49934 || code == 49935 || code == 49936 || code == 49937 ||
                 code == 49938 || code == 49942 || code == 49943 || code == 49945 || code == 49946 || code == 49947 || code == 49949 || code == 49950 || code == 49951 || code == 49952 ||
                 code == 49953 || code == 49954 || code == 49955 || code == 49958 || code == 49959 || code == 49962 || code == 49963 || code == 49964 || code == 49965 || code == 49966 ||
                 code == 49967 || code == 49968 || code == 49969 || code == 49970 || code == 49971 || code == 49972 || code == 49973 || code == 49974 || code == 49975 || code == 49976 ||
                 code == 49977 || code == 49978 || code == 49979 || code == 49980 || code == 49981 || code == 49982 || code == 49983 || code == 49984 || code == 49985 || code == 49986 ||
                 code == 49987 || code == 49988 || code == 49990 || code == 49991 || code == 49992 || code == 49993 || code == 49994 || code == 49995 || code == 49996 || code == 49997 ||
                 code == 49998 || code == 49999 || code == 50000 || code == 50001 || code == 50002 || code == 50003 || code == 50004 || code == 50005 || code == 50006 || code == 50007 ||
                 code == 50008 || code == 50009 || code == 50010 || code == 50011 || code == 50012 || code == 50013 || code == 50014 || code == 50015 || code == 50016 || code == 50017 ||
                 code == 50018 || code == 50019 || code == 50020 || code == 50021 || code == 50022 || code == 50023 || code == 50026 || code == 50027 || code == 50029 || code == 50030 ||
                 code == 50031 || code == 50033 || code == 50035 || code == 50036 || code == 50037 || code == 50038 || code == 50039 || code == 50042 || code == 50043 || code == 50046 ||
                 code == 50047 || code == 50048 || code == 50049 || code == 50050 || code == 50051 || code == 50053 || code == 50054 || code == 50055 || code == 50057 || code == 50058 ||
                 code == 50059 || code == 50061 || code == 50062 || code == 50063 || code == 50064 || code == 50065 || code == 50066 || code == 50067 || code == 50068 || code == 50069 ||
                 code == 50070 || code == 50071 || code == 50072 || code == 50073 || code == 50074 || code == 50075 || code == 50076 || code == 50077 || code == 50078 || code == 50079 ||
                 code == 50080 || code == 50081 || code == 50082 || code == 50083 || code == 50084 || code == 50085 || code == 50086 || code == 50087 || code == 50088 || code == 50089 ||
                 code == 50090 || code == 50091 || code == 50092 || code == 50093 || code == 50094 || code == 50095 || code == 50096 || code == 50097 || code == 50098 || code == 50099 ||
                 code == 50100 || code == 50101 || code == 50102 || code == 50103 || code == 50104 || code == 50105 || code == 50106 || code == 50107 || code == 50108 || code == 50109 ||
                 code == 50110 || code == 50111 || code == 50113 || code == 50114 || code == 50115 || code == 50116 || code == 50117 || code == 50118 || code == 50119 || code == 50120 ||
                 code == 50121 || code == 50122 || code == 50123 || code == 50124 || code == 50125 || code == 50126 || code == 50127 || code == 50128 || code == 50129 || code == 50130 ||
                 code == 50131 || code == 50132 || code == 50133 || code == 50134 || code == 50135 || code == 50138 || code == 50139 || code == 50141 || code == 50142 || code == 50145 ||
                 code == 50147 || code == 50148 || code == 50149 || code == 50150 || code == 50151 || code == 50154 || code == 50155 || code == 50156 || code == 50158 || code == 50159 ||
                 code == 50160 || code == 50161 || code == 50162 || code == 50163 || code == 50166 || code == 50167 || code == 50169 || code == 50170 || code == 50171 || code == 50172 ||
                 code == 50173 || code == 50174 || code == 50175 || code == 50176 || code == 50177 || code == 50178 || code == 50179 || code == 50180 || code == 50181 || code == 50182 ||
                 code == 50183 || code == 50185 || code == 50186 || code == 50187 || code == 50188 || code == 50189 || code == 50190 || code == 50191 || code == 50193 || code == 50194 ||
                 code == 50195 || code == 50196 || code == 50197 || code == 50198 || code == 50199 || code == 50200 || code == 50201 || code == 50202 || code == 50203 || code == 50204 ||
                 code == 50205 || code == 50206 || code == 50207 || code == 50208 || code == 50209 || code == 50210 || code == 50211 || code == 50213 || code == 50214 || code == 50215 ||
                 code == 50216 || code == 50217 || code == 50218 || code == 50219 || code == 50221 || code == 50222 || code == 50223 || code == 50225 || code == 50226 || code == 50227 ||
                 code == 50229 || code == 50230 || code == 50231 || code == 50232 || code == 50233 || code == 50234 || code == 50235 || code == 50238 || code == 50239 || code == 50240 ||
                 code == 50241 || code == 50242 || code == 50243 || code == 50244 || code == 50245 || code == 50246 || code == 50247 || code == 50249 || code == 50250 || code == 50251 ||
                 code == 50252 || code == 50253 || code == 50254 || code == 50255 || code == 50256 || code == 50257 || code == 50258 || code == 50259 || code == 50260 || code == 50261 ||
                 code == 50262 || code == 50263 || code == 50264 || code == 50265 || code == 50266 || code == 50267 || code == 50268 || code == 50269 || code == 50270 || code == 50271 ||
                 code == 50272 || code == 50273 || code == 50274 || code == 50275 || code == 50278 || code == 50279 || code == 50281 || code == 50282 || code == 50283 || code == 50285 ||
                 code == 50286 || code == 50287 || code == 50288 || code == 50289 || code == 50290 || code == 50291 || code == 50294 || code == 50295 || code == 50296 || code == 50298 ||
                 code == 50299 || code == 50300 || code == 50301 || code == 50302 || code == 50303 || code == 50305 || code == 50306 || code == 50307 || code == 50308 || code == 50309 ||
                 code == 50310 || code == 50311 || code == 50312 || code == 50313 || code == 50314 || code == 50315 || code == 50316 || code == 50317 || code == 50318 || code == 50319 ||
                 code == 50320 || code == 50321 || code == 50322 || code == 50323 || code == 50325 || code == 50326 || code == 50327 || code == 50328 || code == 50329 || code == 50330 ||
                 code == 50331 || code == 50333 || code == 50334 || code == 50335 || code == 50336 || code == 50337 || code == 50338 || code == 50339 || code == 50340 || code == 50341 ||
                 code == 50342 || code == 50343 || code == 50344 || code == 50345 || code == 50346 || code == 50347 || code == 50348 || code == 50349 || code == 50350 || code == 50351 ||
                 code == 50352 || code == 50353 || code == 50354 || code == 50355 || code == 50356 || code == 50357 || code == 50358 || code == 50359 || code == 50361 || code == 50362 ||
                 code == 50363 || code == 50365 || code == 50366 || code == 50367 || code == 50368 || code == 50369 || code == 50370 || code == 50371 || code == 50372 || code == 50373 ||
                 code == 50374 || code == 50375 || code == 50376 || code == 50377 || code == 50378 || code == 50379 || code == 50380 || code == 50381 || code == 50382 || code == 50383 ||
                 code == 50384 || code == 50385 || code == 50386 || code == 50387 || code == 50388 || code == 50389 || code == 50390 || code == 50391 || code == 50392 || code == 50393 ||
                 code == 50394 || code == 50395 || code == 50396 || code == 50397 || code == 50398 || code == 50399 || code == 50400 || code == 50401 || code == 50402 || code == 50403 ||
                 code == 50404 || code == 50405 || code == 50406 || code == 50407 || code == 50408 || code == 50410 || code == 50411 || code == 50412 || code == 50413 || code == 50414 ||
                 code == 50415 || code == 50418 || code == 50419 || code == 50421 || code == 50422 || code == 50423 || code == 50425 || code == 50427 || code == 50428 || code == 50429 ||
                 code == 50430 || code == 50434 || code == 50435 || code == 50436 || code == 50437 || code == 50438 || code == 50439 || code == 50440 || code == 50441 || code == 50442 ||
                 code == 50443 || code == 50445 || code == 50446 || code == 50447 || code == 50449 || code == 50450 || code == 50451 || code == 50453 || code == 50454 || code == 50455 ||
                 code == 50456 || code == 50457 || code == 50458 || code == 50459 || code == 50461 || code == 50462 || code == 50463 || code == 50464 || code == 50465 || code == 50466 ||
                 code == 50467 || code == 50468 || code == 50469 || code == 50470 || code == 50471 || code == 50474 || code == 50475 || code == 50477 || code == 50478 || code == 50479 ||
                 code == 50481 || code == 50482 || code == 50483 || code == 50484 || code == 50485 || code == 50486 || code == 50487 || code == 50490 || code == 50492 || code == 50494 ||
                 code == 50495 || code == 50496 || code == 50497 || code == 50498 || code == 50499 || code == 50502 || code == 50503 || code == 50507 || code == 50511 || code == 50512 ||
                 code == 50513 || code == 50514 || code == 50518 || code == 50522 || code == 50523 || code == 50524 || code == 50527 || code == 50530 || code == 50531 || code == 50533 ||
                 code == 50534 || code == 50535 || code == 50537 || code == 50538 || code == 50539 || code == 50540 || code == 50541 || code == 50542 || code == 50543 || code == 50546 ||
                 code == 50550 || code == 50551 || code == 50552 || code == 50553 || code == 50554 || code == 50555 || code == 50558 || code == 50559 || code == 50561 || code == 50562 ||
                 code == 50563 || code == 50565 || code == 50566 || code == 50568 || code == 50569 || code == 50570 || code == 50571 || code == 50574 || code == 50576 || code == 50578 ||
                 code == 50579 || code == 50580 || code == 50582 || code == 50585 || code == 50586 || code == 50587 || code == 50589 || code == 50590 || code == 50591 || code == 50593 ||
                 code == 50594 || code == 50595 || code == 50596 || code == 50597 || code == 50598 || code == 50599 || code == 50600 || code == 50602 || code == 50603 || code == 50604 ||
                 code == 50605 || code == 50606 || code == 50607 || code == 50608 || code == 50609 || code == 50610 || code == 50611 || code == 50614 || code == 50615 || code == 50618 ||
                 code == 50623 || code == 50624 || code == 50625 || code == 50626 || code == 50627 || code == 50635 || code == 50637 || code == 50639 || code == 50642 || code == 50643 ||
                 code == 50645 || code == 50646 || code == 50647 || code == 50649 || code == 50650 || code == 50651 || code == 50652 || code == 50653 || code == 50654 || code == 50655 ||
                 code == 50658 || code == 50660 || code == 50662 || code == 50663 || code == 50664 || code == 50665 || code == 50666 || code == 50667 || code == 50671 || code == 50673 ||
                 code == 50674 || code == 50675 || code == 50677 || code == 50680 || code == 50681 || code == 50682 || code == 50683 || code == 50690 || code == 50691 || code == 50692 ||
                 code == 50697 || code == 50698 || code == 50699 || code == 50701 || code == 50702 || code == 50703 || code == 50705 || code == 50706 || code == 50707 || code == 50708 ||
                 code == 50709 || code == 50710 || code == 50711 || code == 50714 || code == 50717 || code == 50718 || code == 50719 || code == 50720 || code == 50721 || code == 50722 ||
                 code == 50723 || code == 50726 || code == 50727 || code == 50729 || code == 50730 || code == 50731 || code == 50735 || code == 50737 || code == 50738 || code == 50742 ||
                 code == 50744 || code == 50746 || code == 50748 || code == 50749 || code == 50750 || code == 50751 || code == 50754 || code == 50755 || code == 50757 || code == 50758 ||
                 code == 50759 || code == 50761 || code == 50762 || code == 50763 || code == 50764 || code == 50765 || code == 50766 || code == 50767 || code == 50770 || code == 50774 ||
                 code == 50775 || code == 50776 || code == 50777 || code == 50778 || code == 50779 || code == 50782 || code == 50783 || code == 50785 || code == 50786 || code == 50787 ||
                 code == 50788 || code == 50789 || code == 50790 || code == 50791 || code == 50792 || code == 50793 || code == 50794 || code == 50795 || code == 50797 || code == 50798 ||
                 code == 50800 || code == 50802 || code == 50803 || code == 50804 || code == 50805 || code == 50806 || code == 50807 || code == 50810 || code == 50811 || code == 50813 ||
                 code == 50814 || code == 50815 || code == 50817 || code == 50818 || code == 50819 || code == 50820 || code == 50821 || code == 50822 || code == 50823 || code == 50826 ||
                 code == 50828 || code == 50830 || code == 50831 || code == 50832 || code == 50833 || code == 50834 || code == 50835 || code == 50838 || code == 50839 || code == 50841 ||
                 code == 50842 || code == 50843 || code == 50845 || code == 50846 || code == 50847 || code == 50848 || code == 50849 || code == 50850 || code == 50851 || code == 50854 ||
                 code == 50856 || code == 50858 || code == 50859 || code == 50860 || code == 50861 || code == 50862 || code == 50863 || code == 50866 || code == 50867 || code == 50869 ||
                 code == 50870 || code == 50871 || code == 50875 || code == 50876 || code == 50877 || code == 50878 || code == 50879 || code == 50882 || code == 50884 || code == 50886 ||
                 code == 50887 || code == 50888 || code == 50889 || code == 50890 || code == 50891 || code == 50894 || code == 50895 || code == 50897 || code == 50898 || code == 50899 ||
                 code == 50901 || code == 50902 || code == 50903 || code == 50904 || code == 50905 || code == 50906 || code == 50907 || code == 50910 || code == 50911 || code == 50914 ||
                 code == 50915 || code == 50916 || code == 50917 || code == 50918 || code == 50919 || code == 50922 || code == 50923 || code == 50925 || code == 50926 || code == 50927 ||
                 code == 50929 || code == 50930 || code == 50931 || code == 50932 || code == 50933 || code == 50934 || code == 50935 || code == 50938 || code == 50939 || code == 50940 ||
                 code == 50942 || code == 50943 || code == 50944 || code == 50945 || code == 50946 || code == 50947 || code == 50950 || code == 50951 || code == 50953 || code == 50954 ||
                 code == 50955 || code == 50957 || code == 50958 || code == 50959 || code == 50960 || code == 50961 || code == 50962 || code == 50963 || code == 50966 || code == 50968 ||
                 code == 50970 || code == 50971 || code == 50972 || code == 50973 || code == 50974 || code == 50975 || code == 50978 || code == 50979 || code == 50981 || code == 50982 ||
                 code == 50983 || code == 50985 || code == 50986 || code == 50987 || code == 50988 || code == 50989 || code == 50990 || code == 50991 || code == 50994 || code == 50996 ||
                 code == 50998 || code == 51000 || code == 51001 || code == 51002 || code == 51003 || code == 51006 || code == 51007 || code == 51009 || code == 51010 || code == 51011 ||
                 code == 51013 || code == 51014 || code == 51015 || code == 51016 || code == 51017 || code == 51019 || code == 51022 || code == 51024 || code == 51033 || code == 51034 ||
                 code == 51035 || code == 51037 || code == 51038 || code == 51039 || code == 51041 || code == 51042 || code == 51043 || code == 51044 || code == 51045 || code == 51046 ||
                 code == 51047 || code == 51049 || code == 51050 || code == 51052 || code == 51053 || code == 51054 || code == 51055 || code == 51056 || code == 51057 || code == 51058 ||
                 code == 51059 || code == 51062 || code == 51063 || code == 51065 || code == 51066 || code == 51067 || code == 51071 || code == 51072 || code == 51073 || code == 51074 ||
                 code == 51078 || code == 51083 || code == 51084 || code == 51085 || code == 51087 || code == 51090 || code == 51091 || code == 51093 || code == 51097 || code == 51099 ||
                 code == 51100 || code == 51101 || code == 51102 || code == 51103 || code == 51106 || code == 51111 || code == 51112 || code == 51113 || code == 51114 || code == 51115 ||
                 code == 51118 || code == 51119 || code == 51121 || code == 51122 || code == 51123 || code == 51125 || code == 51126 || code == 51127 || code == 51128 || code == 51129 ||
                 code == 51130 || code == 51131 || code == 51134 || code == 51138 || code == 51139 || code == 51140 || code == 51141 || code == 51142 || code == 51143 || code == 51146 ||
                 code == 51147 || code == 51149 || code == 51151 || code == 51153 || code == 51154 || code == 51155 || code == 51156 || code == 51157 || code == 51158 || code == 51159 ||
                 code == 51161 || code == 51162 || code == 51163 || code == 51164 || code == 51166 || code == 51167 || code == 51168 || code == 51169 || code == 51170 || code == 51171 ||
                 code == 51173 || code == 51174 || code == 51175 || code == 51177 || code == 51178 || code == 51179 || code == 51181 || code == 51182 || code == 51183 || code == 51184 ||
                 code == 51185 || code == 51186 || code == 51187 || code == 51188 || code == 51189 || code == 51190 || code == 51191 || code == 51192 || code == 51193 || code == 51194 ||
                 code == 51195 || code == 51196 || code == 51197 || code == 51198 || code == 51199 || code == 51202 || code == 51203 || code == 51205 || code == 51206 || code == 51207 ||
                 code == 51209 || code == 51211 || code == 51212 || code == 51213 || code == 51214 || code == 51215 || code == 51218 || code == 51220 || code == 51223 || code == 51224 ||
                 code == 51225 || code == 51226 || code == 51227 || code == 51230 || code == 51231 || code == 51233 || code == 51234 || code == 51235 || code == 51237 || code == 51238 ||
                 code == 51239 || code == 51240 || code == 51241 || code == 51242 || code == 51243 || code == 51246 || code == 51248 || code == 51250 || code == 51251 || code == 51252 ||
                 code == 51253 || code == 51254 || code == 51255 || code == 51257 || code == 51258 || code == 51259 || code == 51261 || code == 51262 || code == 51263 || code == 51265 ||
                 code == 51266 || code == 51267 || code == 51268 || code == 51269 || code == 51270 || code == 51271 || code == 51274 || code == 51275 || code == 51278 || code == 51279 ||
                 code == 51280 || code == 51281 || code == 51282 || code == 51283 || code == 51285 || code == 51286 || code == 51287 || code == 51288 || code == 51289 || code == 51290 ||
                 code == 51291 || code == 51292 || code == 51293 || code == 51294 || code == 51295 || code == 51296 || code == 51297 || code == 51298 || code == 51299 || code == 51300 ||
                 code == 51301 || code == 51302 || code == 51303 || code == 51304 || code == 51305 || code == 51306 || code == 51307 || code == 51308 || code == 51309 || code == 51310 ||
                 code == 51311 || code == 51314 || code == 51315 || code == 51317 || code == 51318 || code == 51319 || code == 51321 || code == 51323 || code == 51324 || code == 51325 ||
                 code == 51326 || code == 51327 || code == 51330 || code == 51332 || code == 51336 || code == 51337 || code == 51338 || code == 51342 || code == 51343 || code == 51344 ||
                 code == 51345 || code == 51346 || code == 51347 || code == 51349 || code == 51350 || code == 51351 || code == 51352 || code == 51353 || code == 51354 || code == 51355 ||
                 code == 51356 || code == 51358 || code == 51360 || code == 51362 || code == 51363 || code == 51364 || code == 51365 || code == 51366 || code == 51367 || code == 51369 ||
                 code == 51370 || code == 51371 || code == 51372 || code == 51373 || code == 51374 || code == 51375 || code == 51376 || code == 51377 || code == 51378 || code == 51379 ||
                 code == 51380 || code == 51381 || code == 51382 || code == 51383 || code == 51384 || code == 51385 || code == 51386 || code == 51387 || code == 51390 || code == 51391 ||
                 code == 51392 || code == 51393 || code == 51394 || code == 51395 || code == 51397 || code == 51398 || code == 51399 || code == 51401 || code == 51402 || code == 51403 ||
                 code == 51405 || code == 51406 || code == 51407 || code == 51408 || code == 51409 || code == 51410 || code == 51411 || code == 51414 || code == 51416 || code == 51418 ||
                 code == 51419 || code == 51420 || code == 51421 || code == 51422 || code == 51423 || code == 51426 || code == 51427 || code == 51429 || code == 51430 || code == 51431 ||
                 code == 51432 || code == 51433 || code == 51434 || code == 51435 || code == 51436 || code == 51437 || code == 51438 || code == 51439 || code == 51440 || code == 51441 ||
                 code == 51442 || code == 51443 || code == 51444 || code == 51446 || code == 51447 || code == 51448 || code == 51449 || code == 51450 || code == 51451 || code == 51454 ||
                 code == 51455 || code == 51457 || code == 51458 || code == 51459 || code == 51463 || code == 51464 || code == 51465 || code == 51466 || code == 51467 || code == 51470 ||
                 code == 51472 || code == 51474 || code == 51475 || code == 51476 || code == 51477 || code == 51478 || code == 51479 || code == 51481 || code == 51482 || code == 51483 ||
                 code == 51484 || code == 51485 || code == 51486 || code == 51487 || code == 51488 || code == 51489 || code == 51490 || code == 51491 || code == 51492 || code == 51493 ||
                 code == 51494 || code == 51495 || code == 51496 || code == 51497 || code == 51498 || code == 51499 || code == 51501 || code == 51502 || code == 51503 || code == 51504 ||
                 code == 51505 || code == 51506 || code == 51507 || code == 51509 || code == 51510 || code == 51511 || code == 51512 || code == 51513 || code == 51514 || code == 51515 ||
                 code == 51516 || code == 51517 || code == 51518 || code == 51519 || code == 51520 || code == 51521 || code == 51522 || code == 51523 || code == 51524 || code == 51525 ||
                 code == 51526 || code == 51527 || code == 51528 || code == 51529 || code == 51530 || code == 51531 || code == 51532 || code == 51533 || code == 51534 || code == 51535 ||
                 code == 51538 || code == 51539 || code == 51541 || code == 51542 || code == 51543 || code == 51545 || code == 51546 || code == 51547 || code == 51548 || code == 51549 ||
                 code == 51550 || code == 51551 || code == 51554 || code == 51556 || code == 51557 || code == 51558 || code == 51559 || code == 51560 || code == 51561 || code == 51562 ||
                 code == 51563 || code == 51565 || code == 51566 || code == 51567 || code == 51569 || code == 51570 || code == 51571 || code == 51573 || code == 51574 || code == 51575 ||
                 code == 51576 || code == 51577 || code == 51578 || code == 51579 || code == 51581 || code == 51582 || code == 51583 || code == 51584 || code == 51585 || code == 51586 ||
                 code == 51587 || code == 51588 || code == 51589 || code == 51590 || code == 51591 || code == 51594 || code == 51595 || code == 51597 || code == 51598 || code == 51599 ||
                 code == 51601 || code == 51602 || code == 51603 || code == 51604 || code == 51605 || code == 51606 || code == 51607 || code == 51610 || code == 51612 || code == 51614 ||
                 code == 51615 || code == 51616 || code == 51617 || code == 51618 || code == 51619 || code == 51620 || code == 51621 || code == 51622 || code == 51623 || code == 51624 ||
                 code == 51625 || code == 51626 || code == 51627 || code == 51628 || code == 51629 || code == 51630 || code == 51631 || code == 51632 || code == 51633 || code == 51634 ||
                 code == 51635 || code == 51636 || code == 51637 || code == 51638 || code == 51639 || code == 51640 || code == 51641 || code == 51642 || code == 51643 || code == 51644 ||
                 code == 51645 || code == 51646 || code == 51647 || code == 51650 || code == 51651 || code == 51653 || code == 51654 || code == 51657 || code == 51659 || code == 51660 ||
                 code == 51661 || code == 51662 || code == 51663 || code == 51666 || code == 51668 || code == 51671 || code == 51672 || code == 51675 || code == 51678 || code == 51679 ||
                 code == 51681 || code == 51683 || code == 51685 || code == 51686 || code == 51688 || code == 51689 || code == 51690 || code == 51691 || code == 51694 || code == 51698 ||
                 code == 51699 || code == 51700 || code == 51701 || code == 51702 || code == 51703 || code == 51706 || code == 51707 || code == 51709 || code == 51710 || code == 51711 ||
                 code == 51713 || code == 51714 || code == 51715 || code == 51716 || code == 51717 || code == 51718 || code == 51719 || code == 51722 || code == 51726 || code == 51727 ||
                 code == 51728 || code == 51729 || code == 51730 || code == 51731 || code == 51733 || code == 51734 || code == 51735 || code == 51737 || code == 51738 || code == 51739 ||
                 code == 51740 || code == 51741 || code == 51742 || code == 51743 || code == 51744 || code == 51745 || code == 51746 || code == 51747 || code == 51748 || code == 51749 ||
                 code == 51750 || code == 51751 || code == 51752 || code == 51754 || code == 51755 || code == 51756 || code == 51757 || code == 51758 || code == 51759 || code == 51760 ||
                 code == 51761 || code == 51762 || code == 51763 || code == 51764 || code == 51765 || code == 51766 || code == 51767 || code == 51768 || code == 51769 || code == 51770 ||
                 code == 51771 || code == 51772 || code == 51773 || code == 51774 || code == 51775 || code == 51776 || code == 51777 || code == 51778 || code == 51779 || code == 51780 ||
                 code == 51781 || code == 51782 || code == 51783 || code == 51784 || code == 51785 || code == 51786 || code == 51787 || code == 51790 || code == 51791 || code == 51793 ||
                 code == 51794 || code == 51795 || code == 51797 || code == 51798 || code == 51799 || code == 51800 || code == 51801 || code == 51802 || code == 51803 || code == 51806 ||
                 code == 51810 || code == 51811 || code == 51812 || code == 51813 || code == 51814 || code == 51815 || code == 51817 || code == 51818 || code == 51819 || code == 51820 ||
                 code == 51821 || code == 51822 || code == 51823 || code == 51824 || code == 51825 || code == 51826 || code == 51827 || code == 51828 || code == 51829 || code == 51830 ||
                 code == 51831 || code == 51832 || code == 51833 || code == 51834 || code == 51835 || code == 51836 || code == 51838 || code == 51839 || code == 51840 || code == 51841 ||
                 code == 51842 || code == 51843 || code == 51845 || code == 51846 || code == 51847 || code == 51848 || code == 51849 || code == 51850 || code == 51851 || code == 51852 ||
                 code == 51853 || code == 51854 || code == 51855 || code == 51856 || code == 51857 || code == 51858 || code == 51859 || code == 51860 || code == 51861 || code == 51862 ||
                 code == 51863 || code == 51865 || code == 51866 || code == 51867 || code == 51868 || code == 51869 || code == 51870 || code == 51871 || code == 51872 || code == 51873 ||
                 code == 51874 || code == 51875 || code == 51876 || code == 51877 || code == 51878 || code == 51879 || code == 51880 || code == 51881 || code == 51882 || code == 51883 ||
                 code == 51884 || code == 51885 || code == 51886 || code == 51887 || code == 51888 || code == 51889 || code == 51890 || code == 51891 || code == 51892 || code == 51893 ||
                 code == 51894 || code == 51895 || code == 51896 || code == 51897 || code == 51898 || code == 51899 || code == 51902 || code == 51903 || code == 51905 || code == 51906 ||
                 code == 51907 || code == 51909 || code == 51910 || code == 51911 || code == 51912 || code == 51913 || code == 51914 || code == 51915 || code == 51918 || code == 51920 ||
                 code == 51922 || code == 51924 || code == 51925 || code == 51926 || code == 51927 || code == 51930 || code == 51931 || code == 51932 || code == 51933 || code == 51934 ||
                 code == 51935 || code == 51937 || code == 51938 || code == 51939 || code == 51940 || code == 51941 || code == 51942 || code == 51943 || code == 51944 || code == 51945 ||
                 code == 51946 || code == 51947 || code == 51949 || code == 51950 || code == 51951 || code == 51952 || code == 51953 || code == 51954 || code == 51955 || code == 51957 ||
                 code == 51958 || code == 51959 || code == 51960 || code == 51961 || code == 51962 || code == 51963 || code == 51964 || code == 51965 || code == 51966 || code == 51967 ||
                 code == 51968 || code == 51969 || code == 51970 || code == 51971 || code == 51972 || code == 51973 || code == 51974 || code == 51975 || code == 51977 || code == 51978 ||
                 code == 51979 || code == 51980 || code == 51981 || code == 51982 || code == 51983 || code == 51985 || code == 51986 || code == 51987 || code == 51989 || code == 51990 ||
                 code == 51991 || code == 51993 || code == 51994 || code == 51995 || code == 51996 || code == 51997 || code == 51998 || code == 51999 || code == 52002 || code == 52003 ||
                 code == 52004 || code == 52005 || code == 52006 || code == 52007 || code == 52008 || code == 52009 || code == 52010 || code == 52011 || code == 52012 || code == 52013 ||
                 code == 52014 || code == 52015 || code == 52016 || code == 52017 || code == 52018 || code == 52019 || code == 52020 || code == 52021 || code == 52022 || code == 52023 ||
                 code == 52024 || code == 52025 || code == 52026 || code == 52027 || code == 52028 || code == 52029 || code == 52030 || code == 52031 || code == 52032 || code == 52034 ||
                 code == 52035 || code == 52036 || code == 52037 || code == 52038 || code == 52039 || code == 52042 || code == 52043 || code == 52045 || code == 52046 || code == 52047 ||
                 code == 52049 || code == 52050 || code == 52051 || code == 52052 || code == 52053 || code == 52054 || code == 52055 || code == 52058 || code == 52059 || code == 52060 ||
                 code == 52062 || code == 52063 || code == 52064 || code == 52065 || code == 52066 || code == 52067 || code == 52069 || code == 52070 || code == 52071 || code == 52072 ||
                 code == 52073 || code == 52074 || code == 52075 || code == 52076 || code == 52077 || code == 52078 || code == 52079 || code == 52080 || code == 52081 || code == 52082 ||
                 code == 52083 || code == 52084 || code == 52085 || code == 52086 || code == 52087 || code == 52090 || code == 52091 || code == 52092 || code == 52093 || code == 52094 ||
                 code == 52095 || code == 52096 || code == 52097 || code == 52098 || code == 52099 || code == 52100 || code == 52101 || code == 52102 || code == 52103 || code == 52104 ||
                 code == 52105 || code == 52106 || code == 52107 || code == 52108 || code == 52109 || code == 52110 || code == 52111 || code == 52112 || code == 52113 || code == 52114 ||
                 code == 52115 || code == 52116 || code == 52117 || code == 52118 || code == 52119 || code == 52120 || code == 52121 || code == 52122 || code == 52123 || code == 52125 ||
                 code == 52126 || code == 52127 || code == 52128 || code == 52129 || code == 52130 || code == 52131 || code == 52132 || code == 52133 || code == 52134 || code == 52135 ||
                 code == 52136 || code == 52137 || code == 52138 || code == 52139 || code == 52140 || code == 52141 || code == 52142 || code == 52143 || code == 52144 || code == 52145 ||
                 code == 52146 || code == 52147 || code == 52148 || code == 52149 || code == 52150 || code == 52151 || code == 52153 || code == 52154 || code == 52155 || code == 52156 ||
                 code == 52157 || code == 52158 || code == 52159 || code == 52160 || code == 52161 || code == 52162 || code == 52163 || code == 52164 || code == 52165 || code == 52166 ||
                 code == 52167 || code == 52168 || code == 52169 || code == 52170 || code == 52171 || code == 52172 || code == 52173 || code == 52174 || code == 52175 || code == 52176 ||
                 code == 52177 || code == 52178 || code == 52179 || code == 52181 || code == 52182 || code == 52183 || code == 52184 || code == 52185 || code == 52186 || code == 52187 ||
                 code == 52188 || code == 52189 || code == 52190 || code == 52191 || code == 52192 || code == 52193 || code == 52194 || code == 52195 || code == 52197 || code == 52198 ||
                 code == 52200 || code == 52202 || code == 52203 || code == 52204 || code == 52205 || code == 52206 || code == 52207 || code == 52208 || code == 52209 || code == 52210 ||
                 code == 52211 || code == 52212 || code == 52213 || code == 52214 || code == 52215 || code == 52216 || code == 52217 || code == 52218 || code == 52219 || code == 52220 ||
                 code == 52221 || code == 52222 || code == 52223 || code == 52224 || code == 52225 || code == 52226 || code == 52227 || code == 52228 || code == 52229 || code == 52230 ||
                 code == 52231 || code == 52232 || code == 52233 || code == 52234 || code == 52235 || code == 52238 || code == 52239 || code == 52241 || code == 52242 || code == 52243 ||
                 code == 52245 || code == 52246 || code == 52247 || code == 52248 || code == 52249 || code == 52250 || code == 52251 || code == 52254 || code == 52255 || code == 52256 ||
                 code == 52259 || code == 52260 || code == 52261 || code == 52262 || code == 52266 || code == 52267 || code == 52269 || code == 52271 || code == 52273 || code == 52274 ||
                 code == 52275 || code == 52276 || code == 52277 || code == 52278 || code == 52279 || code == 52282 || code == 52287 || code == 52288 || code == 52289 || code == 52290 ||
                 code == 52291 || code == 52294 || code == 52295 || code == 52297 || code == 52298 || code == 52299 || code == 52301 || code == 52302 || code == 52303 || code == 52304 ||
                 code == 52305 || code == 52306 || code == 52307 || code == 52310 || code == 52314 || code == 52315 || code == 52316 || code == 52317 || code == 52318 || code == 52319 ||
                 code == 52321 || code == 52322 || code == 52323 || code == 52325 || code == 52327 || code == 52329 || code == 52330 || code == 52331 || code == 52332 || code == 52333 ||
                 code == 52334 || code == 52335 || code == 52337 || code == 52338 || code == 52339 || code == 52340 || code == 52342 || code == 52343 || code == 52344 || code == 52345 ||
                 code == 52346 || code == 52347 || code == 52348 || code == 52349 || code == 52350 || code == 52351 || code == 52352 || code == 52353 || code == 52354 || code == 52355 ||
                 code == 52356 || code == 52357 || code == 52358 || code == 52359 || code == 52360 || code == 52361 || code == 52362 || code == 52363 || code == 52364 || code == 52365 ||
                 code == 52366 || code == 52367 || code == 52368 || code == 52369 || code == 52370 || code == 52371 || code == 52372 || code == 52373 || code == 52374 || code == 52375 ||
                 code == 52378 || code == 52379 || code == 52381 || code == 52382 || code == 52383 || code == 52385 || code == 52386 || code == 52387 || code == 52388 || code == 52389 ||
                 code == 52390 || code == 52391 || code == 52394 || code == 52398 || code == 52399 || code == 52400 || code == 52401 || code == 52402 || code == 52403 || code == 52406 ||
                 code == 52407 || code == 52409 || code == 52410 || code == 52411 || code == 52413 || code == 52414 || code == 52415 || code == 52416 || code == 52417 || code == 52418 ||
                 code == 52419 || code == 52422 || code == 52424 || code == 52426 || code == 52427 || code == 52428 || code == 52429 || code == 52430 || code == 52431 || code == 52433 ||
                 code == 52434 || code == 52435 || code == 52437 || code == 52438 || code == 52439 || code == 52440 || code == 52441 || code == 52442 || code == 52443 || code == 52444 ||
                 code == 52445 || code == 52446 || code == 52447 || code == 52448 || code == 52449 || code == 52450 || code == 52451 || code == 52453 || code == 52454 || code == 52455 ||
                 code == 52456 || code == 52457 || code == 52458 || code == 52459 || code == 52461 || code == 52462 || code == 52463 || code == 52465 || code == 52466 || code == 52467 ||
                 code == 52468 || code == 52469 || code == 52470 || code == 52471 || code == 52472 || code == 52473 || code == 52474 || code == 52475 || code == 52476 || code == 52477 ||
                 code == 52478 || code == 52479 || code == 52480 || code == 52482 || code == 52483 || code == 52484 || code == 52485 || code == 52486 || code == 52487 || code == 52490 ||
                 code == 52491 || code == 52493 || code == 52494 || code == 52495 || code == 52497 || code == 52498 || code == 52499 || code == 52500 || code == 52501 || code == 52502 ||
                 code == 52503 || code == 52506 || code == 52508 || code == 52510 || code == 52511 || code == 52512 || code == 52513 || code == 52514 || code == 52515 || code == 52517 ||
                 code == 52518 || code == 52519 || code == 52521 || code == 52522 || code == 52523 || code == 52525 || code == 52526 || code == 52527 || code == 52528 || code == 52529 ||
                 code == 52530 || code == 52531 || code == 52532 || code == 52533 || code == 52534 || code == 52535 || code == 52536 || code == 52538 || code == 52539 || code == 52540 ||
                 code == 52541 || code == 52542 || code == 52543 || code == 52544 || code == 52545 || code == 52546 || code == 52547 || code == 52548 || code == 52549 || code == 52550 ||
                 code == 52551 || code == 52552 || code == 52553 || code == 52554 || code == 52555 || code == 52556 || code == 52557 || code == 52558 || code == 52559 || code == 52560 ||
                 code == 52561 || code == 52562 || code == 52563 || code == 52564 || code == 52565 || code == 52566 || code == 52567 || code == 52568 || code == 52569 || code == 52570 ||
                 code == 52571 || code == 52573 || code == 52574 || code == 52575 || code == 52577 || code == 52578 || code == 52579 || code == 52581 || code == 52582 || code == 52583 ||
                 code == 52584 || code == 52585 || code == 52586 || code == 52587 || code == 52590 || code == 52592 || code == 52594 || code == 52595 || code == 52596 || code == 52597 ||
                 code == 52598 || code == 52599 || code == 52601 || code == 52602 || code == 52603 || code == 52604 || code == 52605 || code == 52606 || code == 52607 || code == 52608 ||
                 code == 52609 || code == 52610 || code == 52611 || code == 52612 || code == 52613 || code == 52614 || code == 52615 || code == 52617 || code == 52618 || code == 52619 ||
                 code == 52620 || code == 52621 || code == 52622 || code == 52623 || code == 52624 || code == 52625 || code == 52626 || code == 52627 || code == 52630 || code == 52631 ||
                 code == 52633 || code == 52634 || code == 52635 || code == 52637 || code == 52638 || code == 52639 || code == 52640 || code == 52641 || code == 52642 || code == 52643 ||
                 code == 52646 || code == 52648 || code == 52650 || code == 52651 || code == 52652 || code == 52653 || code == 52654 || code == 52655 || code == 52657 || code == 52658 ||
                 code == 52659 || code == 52660 || code == 52661 || code == 52662 || code == 52663 || code == 52664 || code == 52665 || code == 52666 || code == 52667 || code == 52668 ||
                 code == 52669 || code == 52670 || code == 52671 || code == 52672 || code == 52673 || code == 52674 || code == 52675 || code == 52677 || code == 52678 || code == 52679 ||
                 code == 52680 || code == 52681 || code == 52682 || code == 52683 || code == 52685 || code == 52686 || code == 52687 || code == 52689 || code == 52690 || code == 52691 ||
                 code == 52692 || code == 52693 || code == 52694 || code == 52695 || code == 52696 || code == 52697 || code == 52698 || code == 52699 || code == 52700 || code == 52701 ||
                 code == 52702 || code == 52703 || code == 52704 || code == 52705 || code == 52706 || code == 52707 || code == 52708 || code == 52709 || code == 52710 || code == 52711 ||
                 code == 52713 || code == 52714 || code == 52715 || code == 52717 || code == 52718 || code == 52719 || code == 52721 || code == 52722 || code == 52723 || code == 52724 ||
                 code == 52725 || code == 52726 || code == 52727 || code == 52730 || code == 52732 || code == 52734 || code == 52735 || code == 52736 || code == 52737 || code == 52738 ||
                 code == 52739 || code == 52741 || code == 52742 || code == 52743 || code == 52745 || code == 52746 || code == 52747 || code == 52749 || code == 52750 || code == 52751 ||
                 code == 52752 || code == 52753 || code == 52754 || code == 52755 || code == 52757 || code == 52758 || code == 52759 || code == 52760 || code == 52762 || code == 52763 ||
                 code == 52764 || code == 52765 || code == 52766 || code == 52767 || code == 52770 || code == 52771 || code == 52773 || code == 52774 || code == 52775 || code == 52777 ||
                 code == 52778 || code == 52779 || code == 52780 || code == 52781 || code == 52782 || code == 52783 || code == 52786 || code == 52788 || code == 52790 || code == 52791 ||
                 code == 52792 || code == 52793 || code == 52794 || code == 52795 || code == 52796 || code == 52797 || code == 52798 || code == 52799 || code == 52800 || code == 52801 ||
                 code == 52802 || code == 52803 || code == 52804 || code == 52805 || code == 52806 || code == 52807 || code == 52808 || code == 52809 || code == 52810 || code == 52811 ||
                 code == 52812 || code == 52813 || code == 52814 || code == 52815 || code == 52816 || code == 52817 || code == 52818 || code == 52819 || code == 52820 || code == 52821 ||
                 code == 52822 || code == 52823 || code == 52826 || code == 52827 || code == 52829 || code == 52830 || code == 52834 || code == 52835 || code == 52836 || code == 52837 ||
                 code == 52838 || code == 52839 || code == 52842 || code == 52844 || code == 52846 || code == 52847 || code == 52848 || code == 52849 || code == 52850 || code == 52851 ||
                 code == 52854 || code == 52855 || code == 52857 || code == 52858 || code == 52859 || code == 52861 || code == 52862 || code == 52863 || code == 52864 || code == 52865 ||
                 code == 52866 || code == 52867 || code == 52870 || code == 52872 || code == 52874 || code == 52875 || code == 52876 || code == 52877 || code == 52878 || code == 52879 ||
                 code == 52882 || code == 52883 || code == 52885 || code == 52886 || code == 52887 || code == 52889 || code == 52890 || code == 52891 || code == 52892 || code == 52893 ||
                 code == 52894 || code == 52895 || code == 52898 || code == 52902 || code == 52903 || code == 52904 || code == 52905 || code == 52906 || code == 52907 || code == 52910 ||
                 code == 52911 || code == 52912 || code == 52913 || code == 52914 || code == 52915 || code == 52916 || code == 52917 || code == 52918 || code == 52919 || code == 52920 ||
                 code == 52921 || code == 52922 || code == 52923 || code == 52924 || code == 52925 || code == 52926 || code == 52927 || code == 52928 || code == 52930 || code == 52931 ||
                 code == 52932 || code == 52933 || code == 52934 || code == 52935 || code == 52936 || code == 52937 || code == 52938 || code == 52939 || code == 52940 || code == 52941 ||
                 code == 52942 || code == 52943 || code == 52944 || code == 52945 || code == 52946 || code == 52947 || code == 52948 || code == 52949 || code == 52950 || code == 52951 ||
                 code == 52952 || code == 52953 || code == 52954 || code == 52955 || code == 52956 || code == 52957 || code == 52958 || code == 52959 || code == 52960 || code == 52961 ||
                 code == 52962 || code == 52963 || code == 52966 || code == 52967 || code == 52969 || code == 52970 || code == 52973 || code == 52974 || code == 52975 || code == 52976 ||
                 code == 52977 || code == 52978 || code == 52979 || code == 52982 || code == 52986 || code == 52987 || code == 52988 || code == 52989 || code == 52990 || code == 52991 ||
                 code == 52994 || code == 52995 || code == 52997 || code == 52998 || code == 52999 || code == 53001 || code == 53002 || code == 53003 || code == 53004 || code == 53005 ||
                 code == 53006 || code == 53007 || code == 53010 || code == 53012 || code == 53014 || code == 53015 || code == 53016 || code == 53017 || code == 53018 || code == 53019 ||
                 code == 53021 || code == 53022 || code == 53023 || code == 53025 || code == 53026 || code == 53027 || code == 53029 || code == 53030 || code == 53031 || code == 53032 ||
                 code == 53033 || code == 53034 || code == 53035 || code == 53038 || code == 53042 || code == 53043 || code == 53044 || code == 53045 || code == 53046 || code == 53047 ||
                 code == 53049 || code == 53050 || code == 53051 || code == 53052 || code == 53053 || code == 53054 || code == 53055 || code == 53056 || code == 53057 || code == 53058 ||
                 code == 53059 || code == 53060 || code == 53061 || code == 53062 || code == 53063 || code == 53064 || code == 53065 || code == 53066 || code == 53067 || code == 53068 ||
                 code == 53069 || code == 53070 || code == 53071 || code == 53072 || code == 53073 || code == 53074 || code == 53075 || code == 53078 || code == 53079 || code == 53081 ||
                 code == 53082 || code == 53083 || code == 53085 || code == 53086 || code == 53087 || code == 53088 || code == 53089 || code == 53090 || code == 53091 || code == 53094 ||
                 code == 53096 || code == 53098 || code == 53099 || code == 53100 || code == 53101 || code == 53102 || code == 53103 || code == 53106 || code == 53107 || code == 53109 ||
                 code == 53110 || code == 53111 || code == 53113 || code == 53114 || code == 53115 || code == 53116 || code == 53117 || code == 53118 || code == 53119 || code == 53121 ||
                 code == 53122 || code == 53123 || code == 53124 || code == 53126 || code == 53127 || code == 53128 || code == 53129 || code == 53130 || code == 53131 || code == 53133 ||
                 code == 53134 || code == 53135 || code == 53136 || code == 53137 || code == 53138 || code == 53139 || code == 53140 || code == 53141 || code == 53142 || code == 53143 ||
                 code == 53144 || code == 53145 || code == 53146 || code == 53147 || code == 53148 || code == 53149 || code == 53150 || code == 53151 || code == 53152 || code == 53154 ||
                 code == 53155 || code == 53156 || code == 53157 || code == 53158 || code == 53159 || code == 53161 || code == 53162 || code == 53163 || code == 53164 || code == 53165 ||
                 code == 53166 || code == 53167 || code == 53169 || code == 53170 || code == 53171 || code == 53172 || code == 53173 || code == 53174 || code == 53175 || code == 53176 ||
                 code == 53177 || code == 53178 || code == 53179 || code == 53180 || code == 53181 || code == 53182 || code == 53183 || code == 53184 || code == 53185 || code == 53186 ||
                 code == 53187 || code == 53189 || code == 53190 || code == 53191 || code == 53192 || code == 53193 || code == 53194 || code == 53195 || code == 53196 || code == 53197 ||
                 code == 53198 || code == 53199 || code == 53200 || code == 53201 || code == 53202 || code == 53203 || code == 53204 || code == 53205 || code == 53206 || code == 53207 ||
                 code == 53208 || code == 53209 || code == 53210 || code == 53211 || code == 53212 || code == 53213 || code == 53214 || code == 53215 || code == 53218 || code == 53219 ||
                 code == 53221 || code == 53222 || code == 53223 || code == 53225 || code == 53226 || code == 53227 || code == 53228 || code == 53229 || code == 53230 || code == 53231 ||
                 code == 53234 || code == 53236 || code == 53238 || code == 53239 || code == 53240 || code == 53241 || code == 53242 || code == 53243 || code == 53245 || code == 53246 ||
                 code == 53247 || code == 53249 || code == 53250 || code == 53251 || code == 53253 || code == 53254 || code == 53255 || code == 53256 || code == 53257 || code == 53258 ||
                 code == 53259 || code == 53260 || code == 53261 || code == 53262 || code == 53263 || code == 53264 || code == 53266 || code == 53267 || code == 53268 || code == 53269 ||
                 code == 53270 || code == 53271 || code == 53273 || code == 53274 || code == 53275 || code == 53276 || code == 53277 || code == 53278 || code == 53279 || code == 53280 ||
                 code == 53281 || code == 53282 || code == 53283 || code == 53284 || code == 53285 || code == 53286 || code == 53287 || code == 53288 || code == 53289 || code == 53290 ||
                 code == 53291 || code == 53292 || code == 53294 || code == 53295 || code == 53296 || code == 53297 || code == 53298 || code == 53299 || code == 53302 || code == 53303 ||
                 code == 53305 || code == 53306 || code == 53307 || code == 53309 || code == 53310 || code == 53311 || code == 53312 || code == 53313 || code == 53314 || code == 53315 ||
                 code == 53318 || code == 53320 || code == 53322 || code == 53323 || code == 53324 || code == 53325 || code == 53326 || code == 53327 || code == 53329 || code == 53330 ||
                 code == 53331 || code == 53333 || code == 53334 || code == 53335 || code == 53337 || code == 53338 || code == 53339 || code == 53340 || code == 53341 || code == 53342 ||
                 code == 53343 || code == 53345 || code == 53346 || code == 53347 || code == 53348 || code == 53349 || code == 53350 || code == 53351 || code == 53352 || code == 53353 ||
                 code == 53354 || code == 53355 || code == 53358 || code == 53359 || code == 53361 || code == 53362 || code == 53363 || code == 53365 || code == 53366 || code == 53367 ||
                 code == 53368 || code == 53369 || code == 53370 || code == 53371 || code == 53374 || code == 53375 || code == 53376 || code == 53378 || code == 53379 || code == 53380 ||
                 code == 53381 || code == 53382 || code == 53383 || code == 53384 || code == 53385 || code == 53386 || code == 53387 || code == 53388 || code == 53389 || code == 53390 ||
                 code == 53391 || code == 53392 || code == 53393 || code == 53394 || code == 53395 || code == 53396 || code == 53397 || code == 53398 || code == 53399 || code == 53400 ||
                 code == 53401 || code == 53402 || code == 53403 || code == 53404 || code == 53405 || code == 53406 || code == 53407 || code == 53408 || code == 53409 || code == 53410 ||
                 code == 53411 || code == 53414 || code == 53415 || code == 53417 || code == 53418 || code == 53419 || code == 53421 || code == 53422 || code == 53423 || code == 53424 ||
                 code == 53425 || code == 53426 || code == 53427 || code == 53430 || code == 53432 || code == 53434 || code == 53435 || code == 53436 || code == 53437 || code == 53438 ||
                 code == 53439 || code == 53442 || code == 53443 || code == 53445 || code == 53446 || code == 53447 || code == 53450 || code == 53451 || code == 53452 || code == 53453 ||
                 code == 53454 || code == 53455 || code == 53458 || code == 53462 || code == 53463 || code == 53464 || code == 53465 || code == 53466 || code == 53467 || code == 53470 ||
                 code == 53471 || code == 53473 || code == 53474 || code == 53475 || code == 53477 || code == 53478 || code == 53479 || code == 53480 || code == 53481 || code == 53482 ||
                 code == 53483 || code == 53486 || code == 53490 || code == 53491 || code == 53492 || code == 53493 || code == 53494 || code == 53495 || code == 53497 || code == 53498 ||
                 code == 53499 || code == 53500 || code == 53501 || code == 53502 || code == 53503 || code == 53504 || code == 53505 || code == 53506 || code == 53507 || code == 53508 ||
                 code == 53509 || code == 53510 || code == 53511 || code == 53512 || code == 53513 || code == 53514 || code == 53515 || code == 53516 || code == 53518 || code == 53519 ||
                 code == 53520 || code == 53521 || code == 53522 || code == 53523 || code == 53524 || code == 53525 || code == 53526 || code == 53527 || code == 53528 || code == 53529 ||
                 code == 53530 || code == 53531 || code == 53532 || code == 53533 || code == 53534 || code == 53535 || code == 53536 || code == 53537 || code == 53538 || code == 53539 ||
                 code == 53540 || code == 53541 || code == 53542 || code == 53543 || code == 53544 || code == 53545 || code == 53546 || code == 53547 || code == 53548 || code == 53549 ||
                 code == 53550 || code == 53551 || code == 53554 || code == 53555 || code == 53557 || code == 53558 || code == 53559 || code == 53561 || code == 53563 || code == 53564 ||
                 code == 53565 || code == 53566 || code == 53567 || code == 53570 || code == 53574 || code == 53575 || code == 53576 || code == 53577 || code == 53578 || code == 53579 ||
                 code == 53582 || code == 53583 || code == 53585 || code == 53586 || code == 53587 || code == 53589 || code == 53590 || code == 53591 || code == 53592 || code == 53593 ||
                 code == 53594 || code == 53595 || code == 53598 || code == 53600 || code == 53602 || code == 53603 || code == 53604 || code == 53605 || code == 53606 || code == 53607 ||
                 code == 53609 || code == 53610 || code == 53611 || code == 53613 || code == 53614 || code == 53615 || code == 53616 || code == 53617 || code == 53618 || code == 53619 ||
                 code == 53620 || code == 53621 || code == 53622 || code == 53623 || code == 53624 || code == 53625 || code == 53626 || code == 53627 || code == 53629 || code == 53630 ||
                 code == 53631 || code == 53632 || code == 53633 || code == 53634 || code == 53635 || code == 53637 || code == 53638 || code == 53639 || code == 53641 || code == 53642 ||
                 code == 53643 || code == 53644 || code == 53645 || code == 53646 || code == 53647 || code == 53648 || code == 53649 || code == 53650 || code == 53651 || code == 53652 ||
                 code == 53653 || code == 53654 || code == 53655 || code == 53656 || code == 53657 || code == 53658 || code == 53659 || code == 53660 || code == 53661 || code == 53662 ||
                 code == 53663 || code == 53666 || code == 53667 || code == 53669 || code == 53670 || code == 53671 || code == 53673 || code == 53674 || code == 53675 || code == 53676 ||
                 code == 53677 || code == 53678 || code == 53679 || code == 53682 || code == 53684 || code == 53686 || code == 53687 || code == 53688 || code == 53689 || code == 53691 ||
                 code == 53693 || code == 53694 || code == 53695 || code == 53697 || code == 53698 || code == 53699 || code == 53700 || code == 53701 || code == 53702 || code == 53703 ||
                 code == 53704 || code == 53705 || code == 53706 || code == 53707 || code == 53708 || code == 53709 || code == 53710 || code == 53711 || code == 53712 || code == 53713 ||
                 code == 53714 || code == 53715 || code == 53716 || code == 53717 || code == 53718 || code == 53719 || code == 53721 || code == 53722 || code == 53723 || code == 53724 ||
                 code == 53725 || code == 53726 || code == 53727 || code == 53728 || code == 53729 || code == 53730 || code == 53731 || code == 53732 || code == 53733 || code == 53734 ||
                 code == 53735 || code == 53736 || code == 53737 || code == 53738 || code == 53739 || code == 53740 || code == 53741 || code == 53742 || code == 53743 || code == 53744 ||
                 code == 53745 || code == 53746 || code == 53747 || code == 53749 || code == 53750 || code == 53751 || code == 53753 || code == 53754 || code == 53755 || code == 53756 ||
                 code == 53757 || code == 53758 || code == 53759 || code == 53760 || code == 53761 || code == 53762 || code == 53763 || code == 53764 || code == 53765 || code == 53766 ||
                 code == 53768 || code == 53770 || code == 53771 || code == 53772 || code == 53773 || code == 53774 || code == 53775 || code == 53777 || code == 53778 || code == 53779 ||
                 code == 53780 || code == 53781 || code == 53782 || code == 53783 || code == 53784 || code == 53785 || code == 53786 || code == 53787 || code == 53788 || code == 53789 ||
                 code == 53790 || code == 53791 || code == 53792 || code == 53793 || code == 53794 || code == 53795 || code == 53796 || code == 53797 || code == 53798 || code == 53799 ||
                 code == 53800 || code == 53801 || code == 53802 || code == 53803 || code == 53806 || code == 53807 || code == 53809 || code == 53810 || code == 53811 || code == 53813 ||
                 code == 53814 || code == 53815 || code == 53816 || code == 53817 || code == 53818 || code == 53819 || code == 53822 || code == 53824 || code == 53826 || code == 53827 ||
                 code == 53828 || code == 53829 || code == 53830 || code == 53831 || code == 53833 || code == 53834 || code == 53835 || code == 53836 || code == 53837 || code == 53838 ||
                 code == 53839 || code == 53840 || code == 53841 || code == 53842 || code == 53843 || code == 53844 || code == 53845 || code == 53846 || code == 53847 || code == 53848 ||
                 code == 53849 || code == 53850 || code == 53851 || code == 53853 || code == 53854 || code == 53855 || code == 53856 || code == 53857 || code == 53858 || code == 53859 ||
                 code == 53861 || code == 53862 || code == 53863 || code == 53864 || code == 53865 || code == 53866 || code == 53867 || code == 53868 || code == 53869 || code == 53870 ||
                 code == 53871 || code == 53872 || code == 53873 || code == 53874 || code == 53875 || code == 53876 || code == 53877 || code == 53878 || code == 53879 || code == 53880 ||
                 code == 53881 || code == 53882 || code == 53883 || code == 53884 || code == 53885 || code == 53886 || code == 53887 || code == 53890 || code == 53891 || code == 53893 ||
                 code == 53894 || code == 53895 || code == 53897 || code == 53898 || code == 53899 || code == 53900 || code == 53901 || code == 53902 || code == 53903 || code == 53906 ||
                 code == 53907 || code == 53908 || code == 53910 || code == 53911 || code == 53912 || code == 53913 || code == 53914 || code == 53915 || code == 53917 || code == 53918 ||
                 code == 53919 || code == 53921 || code == 53922 || code == 53923 || code == 53925 || code == 53926 || code == 53927 || code == 53928 || code == 53929 || code == 53930 ||
                 code == 53931 || code == 53933 || code == 53934 || code == 53935 || code == 53936 || code == 53938 || code == 53939 || code == 53940 || code == 53941 || code == 53942 ||
                 code == 53943 || code == 53946 || code == 53947 || code == 53949 || code == 53950 || code == 53953 || code == 53955 || code == 53956 || code == 53957 || code == 53958 ||
                 code == 53959 || code == 53962 || code == 53964 || code == 53965 || code == 53966 || code == 53967 || code == 53968 || code == 53969 || code == 53970 || code == 53971 ||
                 code == 53973 || code == 53974 || code == 53975 || code == 53977 || code == 53978 || code == 53979 || code == 53981 || code == 53982 || code == 53983 || code == 53984 ||
                 code == 53985 || code == 53986 || code == 53987 || code == 53990 || code == 53991 || code == 53992 || code == 53993 || code == 53994 || code == 53995 || code == 53996 ||
                 code == 53997 || code == 53998 || code == 53999 || code == 54002 || code == 54003 || code == 54005 || code == 54006 || code == 54007 || code == 54009 || code == 54010 ||
                 code == 54011 || code == 54012 || code == 54013 || code == 54014 || code == 54015 || code == 54018 || code == 54020 || code == 54022 || code == 54023 || code == 54024 ||
                 code == 54025 || code == 54026 || code == 54027 || code == 54031 || code == 54033 || code == 54034 || code == 54035 || code == 54037 || code == 54039 || code == 54040 ||
                 code == 54041 || code == 54042 || code == 54043 || code == 54046 || code == 54050 || code == 54051 || code == 54052 || code == 54054 || code == 54055 || code == 54058 ||
                 code == 54059 || code == 54061 || code == 54062 || code == 54063 || code == 54065 || code == 54066 || code == 54067 || code == 54068 || code == 54069 || code == 54070 ||
                 code == 54071 || code == 54074 || code == 54078 || code == 54079 || code == 54080 || code == 54081 || code == 54082 || code == 54083 || code == 54086 || code == 54087 ||
                 code == 54088 || code == 54089 || code == 54090 || code == 54091 || code == 54092 || code == 54093 || code == 54094 || code == 54095 || code == 54096 || code == 54097 ||
                 code == 54098 || code == 54099 || code == 54100 || code == 54101 || code == 54102 || code == 54103 || code == 54104 || code == 54105 || code == 54106 || code == 54107 ||
                 code == 54108 || code == 54109 || code == 54110 || code == 54111 || code == 54112 || code == 54113 || code == 54114 || code == 54115 || code == 54116 || code == 54117 ||
                 code == 54118 || code == 54119 || code == 54120 || code == 54121 || code == 54122 || code == 54123 || code == 54124 || code == 54125 || code == 54126 || code == 54127 ||
                 code == 54128 || code == 54129 || code == 54130 || code == 54131 || code == 54132 || code == 54133 || code == 54134 || code == 54135 || code == 54136 || code == 54137 ||
                 code == 54138 || code == 54139 || code == 54142 || code == 54143 || code == 54145 || code == 54146 || code == 54147 || code == 54149 || code == 54150 || code == 54151 ||
                 code == 54152 || code == 54153 || code == 54154 || code == 54155 || code == 54158 || code == 54162 || code == 54163 || code == 54164 || code == 54165 || code == 54166 ||
                 code == 54167 || code == 54170 || code == 54171 || code == 54173 || code == 54174 || code == 54175 || code == 54177 || code == 54178 || code == 54179 || code == 54180 ||
                 code == 54181 || code == 54182 || code == 54183 || code == 54186 || code == 54188 || code == 54190 || code == 54191 || code == 54192 || code == 54193 || code == 54194 ||
                 code == 54195 || code == 54197 || code == 54198 || code == 54199 || code == 54201 || code == 54202 || code == 54203 || code == 54205 || code == 54206 || code == 54207 ||
                 code == 54208 || code == 54209 || code == 54210 || code == 54211 || code == 54214 || code == 54215 || code == 54218 || code == 54219 || code == 54220 || code == 54221 ||
                 code == 54222 || code == 54223 || code == 54225 || code == 54226 || code == 54227 || code == 54228 || code == 54229 || code == 54230 || code == 54231 || code == 54233 ||
                 code == 54234 || code == 54235 || code == 54236 || code == 54237 || code == 54238 || code == 54239 || code == 54240 || code == 54242 || code == 54244 || code == 54245 ||
                 code == 54246 || code == 54247 || code == 54248 || code == 54249 || code == 54250 || code == 54251 || code == 54254 || code == 54255 || code == 54257 || code == 54258 ||
                 code == 54259 || code == 54261 || code == 54262 || code == 54263 || code == 54264 || code == 54265 || code == 54266 || code == 54267 || code == 54270 || code == 54272 ||
                 code == 54274 || code == 54275 || code == 54276 || code == 54277 || code == 54278 || code == 54279 || code == 54281 || code == 54282 || code == 54283 || code == 54284 ||
                 code == 54285 || code == 54286 || code == 54287 || code == 54288 || code == 54289 || code == 54290 || code == 54291 || code == 54292 || code == 54293 || code == 54294 ||
                 code == 54295 || code == 54296 || code == 54297 || code == 54298 || code == 54299 || code == 54300 || code == 54302 || code == 54303 || code == 54304 || code == 54305 ||
                 code == 54306 || code == 54307 || code == 54308 || code == 54309 || code == 54310 || code == 54311 || code == 54312 || code == 54313 || code == 54314 || code == 54315 ||
                 code == 54316 || code == 54317 || code == 54318 || code == 54319 || code == 54320 || code == 54321 || code == 54322 || code == 54323 || code == 54324 || code == 54325 ||
                 code == 54326 || code == 54327 || code == 54328 || code == 54329 || code == 54330 || code == 54331 || code == 54332 || code == 54333 || code == 54334 || code == 54335 ||
                 code == 54337 || code == 54338 || code == 54339 || code == 54341 || code == 54342 || code == 54343 || code == 54344 || code == 54345 || code == 54346 || code == 54347 ||
                 code == 54348 || code == 54349 || code == 54350 || code == 54351 || code == 54352 || code == 54353 || code == 54354 || code == 54355 || code == 54356 || code == 54357 ||
                 code == 54358 || code == 54359 || code == 54360 || code == 54361 || code == 54362 || code == 54363 || code == 54365 || code == 54366 || code == 54367 || code == 54369 ||
                 code == 54370 || code == 54371 || code == 54373 || code == 54374 || code == 54375 || code == 54376 || code == 54377 || code == 54378 || code == 54379 || code == 54380 ||
                 code == 54382 || code == 54384 || code == 54385 || code == 54386 || code == 54387 || code == 54388 || code == 54389 || code == 54390 || code == 54391 || code == 54394 ||
                 code == 54395 || code == 54397 || code == 54398 || code == 54401 || code == 54403 || code == 54404 || code == 54405 || code == 54406 || code == 54407 || code == 54410 ||
                 code == 54412 || code == 54414 || code == 54415 || code == 54416 || code == 54417 || code == 54418 || code == 54419 || code == 54421 || code == 54422 || code == 54423 ||
                 code == 54424 || code == 54425 || code == 54426 || code == 54427 || code == 54428 || code == 54429 || code == 54430 || code == 54431 || code == 54432 || code == 54433 ||
                 code == 54434 || code == 54435 || code == 54436 || code == 54437 || code == 54438 || code == 54439 || code == 54440 || code == 54442 || code == 54443 || code == 54444 ||
                 code == 54445 || code == 54446 || code == 54447 || code == 54448 || code == 54449 || code == 54450 || code == 54451 || code == 54452 || code == 54453 || code == 54454 ||
                 code == 54455 || code == 54456 || code == 54457 || code == 54458 || code == 54459 || code == 54460 || code == 54461 || code == 54462 || code == 54463 || code == 54464 ||
                 code == 54465 || code == 54466 || code == 54467 || code == 54468 || code == 54469 || code == 54470 || code == 54471 || code == 54472 || code == 54473 || code == 54474 ||
                 code == 54475 || code == 54477 || code == 54478 || code == 54479 || code == 54481 || code == 54482 || code == 54483 || code == 54485 || code == 54486 || code == 54487 ||
                 code == 54488 || code == 54489 || code == 54490 || code == 54491 || code == 54493 || code == 54494 || code == 54496 || code == 54497 || code == 54498 || code == 54499 ||
                 code == 54500 || code == 54501 || code == 54502 || code == 54503 || code == 54505 || code == 54506 || code == 54507 || code == 54509 || code == 54510 || code == 54511 ||
                 code == 54513 || code == 54514 || code == 54515 || code == 54516 || code == 54517 || code == 54518 || code == 54519 || code == 54521 || code == 54522 || code == 54524 ||
                 code == 54526 || code == 54527 || code == 54528 || code == 54529 || code == 54530 || code == 54531 || code == 54533 || code == 54534 || code == 54535 || code == 54537 ||
                 code == 54538 || code == 54539 || code == 54541 || code == 54542 || code == 54543 || code == 54544 || code == 54545 || code == 54546 || code == 54547 || code == 54550 ||
                 code == 54552 || code == 54553 || code == 54554 || code == 54555 || code == 54556 || code == 54557 || code == 54558 || code == 54559 || code == 54560 || code == 54561 ||
                 code == 54562 || code == 54563 || code == 54564 || code == 54565 || code == 54566 || code == 54567 || code == 54568 || code == 54569 || code == 54570 || code == 54571 ||
                 code == 54572 || code == 54573 || code == 54574 || code == 54575 || code == 54576 || code == 54577 || code == 54578 || code == 54579 || code == 54580 || code == 54581 ||
                 code == 54582 || code == 54583 || code == 54584 || code == 54585 || code == 54586 || code == 54587 || code == 54590 || code == 54591 || code == 54593 || code == 54594 ||
                 code == 54595 || code == 54597 || code == 54598 || code == 54599 || code == 54600 || code == 54601 || code == 54602 || code == 54603 || code == 54606 || code == 54608 ||
                 code == 54610 || code == 54611 || code == 54612 || code == 54613 || code == 54614 || code == 54615 || code == 54618 || code == 54619 || code == 54621 || code == 54622 ||
                 code == 54623 || code == 54625 || code == 54626 || code == 54627 || code == 54628 || code == 54630 || code == 54631 || code == 54634 || code == 54636 || code == 54638 ||
                 code == 54639 || code == 54640 || code == 54641 || code == 54642 || code == 54643 || code == 54646 || code == 54647 || code == 54649 || code == 54650 || code == 54651 ||
                 code == 54653 || code == 54654 || code == 54655 || code == 54656 || code == 54657 || code == 54658 || code == 54659 || code == 54662 || code == 54666 || code == 54667 ||
                 code == 54668 || code == 54669 || code == 54670 || code == 54671 || code == 54673 || code == 54674 || code == 54675 || code == 54676 || code == 54677 || code == 54678 ||
                 code == 54679 || code == 54680 || code == 54681 || code == 54682 || code == 54683 || code == 54684 || code == 54685 || code == 54686 || code == 54687 || code == 54688 ||
                 code == 54689 || code == 54690 || code == 54691 || code == 54692 || code == 54694 || code == 54695 || code == 54696 || code == 54697 || code == 54698 || code == 54699 ||
                 code == 54700 || code == 54701 || code == 54702 || code == 54703 || code == 54704 || code == 54705 || code == 54706 || code == 54707 || code == 54708 || code == 54709 ||
                 code == 54710 || code == 54711 || code == 54712 || code == 54713 || code == 54714 || code == 54715 || code == 54716 || code == 54717 || code == 54718 || code == 54719 ||
                 code == 54720 || code == 54721 || code == 54722 || code == 54723 || code == 54724 || code == 54725 || code == 54726 || code == 54727 || code == 54730 || code == 54731 ||
                 code == 54733 || code == 54734 || code == 54735 || code == 54737 || code == 54739 || code == 54740 || code == 54741 || code == 54742 || code == 54743 || code == 54746 ||
                 code == 54748 || code == 54750 || code == 54751 || code == 54752 || code == 54753 || code == 54754 || code == 54755 || code == 54758 || code == 54759 || code == 54761 ||
                 code == 54762 || code == 54763 || code == 54765 || code == 54766 || code == 54767 || code == 54768 || code == 54769 || code == 54770 || code == 54771 || code == 54774 ||
                 code == 54776 || code == 54778 || code == 54779 || code == 54780 || code == 54781 || code == 54782 || code == 54783 || code == 54786 || code == 54787 || code == 54789 ||
                 code == 54790 || code == 54791 || code == 54793 || code == 54794 || code == 54795 || code == 54796 || code == 54797 || code == 54798 || code == 54799 || code == 54802 ||
                 code == 54806 || code == 54807 || code == 54808 || code == 54809 || code == 54810 || code == 54811 || code == 54813 || code == 54814 || code == 54815 || code == 54817 ||
                 code == 54818 || code == 54819 || code == 54821 || code == 54822 || code == 54823 || code == 54824 || code == 54825 || code == 54826 || code == 54827 || code == 54828 ||
                 code == 54830 || code == 54831 || code == 54832 || code == 54833 || code == 54834 || code == 54835 || code == 54836 || code == 54837 || code == 54838 || code == 54839 ||
                 code == 54842 || code == 54843 || code == 54845 || code == 54846 || code == 54847 || code == 54849 || code == 54850 || code == 54851 || code == 54852 || code == 54854 ||
                 code == 54855 || code == 54858 || code == 54860 || code == 54862 || code == 54863 || code == 54864 || code == 54866 || code == 54867 || code == 54870 || code == 54871 ||
                 code == 54873 || code == 54874 || code == 54875 || code == 54877 || code == 54878 || code == 54879 || code == 54880 || code == 54881 || code == 54882 || code == 54883 ||
                 code == 54884 || code == 54885 || code == 54886 || code == 54888 || code == 54890 || code == 54891 || code == 54892 || code == 54893 || code == 54894 || code == 54895 ||
                 code == 54898 || code == 54899 || code == 54901 || code == 54902 || code == 54903 || code == 54904 || code == 54905 || code == 54906 || code == 54907 || code == 54908 ||
                 code == 54909 || code == 54910 || code == 54911 || code == 54912 || code == 54913 || code == 54914 || code == 54916 || code == 54918 || code == 54919 || code == 54920 ||
                 code == 54921 || code == 54922 || code == 54923 || code == 54926 || code == 54927 || code == 54929 || code == 54930 || code == 54931 || code == 54933 || code == 54934 ||
                 code == 54935 || code == 54936 || code == 54937 || code == 54938 || code == 54939 || code == 54940 || code == 54942 || code == 54944 || code == 54946 || code == 54947 ||
                 code == 54948 || code == 54949 || code == 54950 || code == 54951 || code == 54953 || code == 54954 || code == 54955 || code == 54957 || code == 54958 || code == 54959 ||
                 code == 54961 || code == 54962 || code == 54963 || code == 54964 || code == 54965 || code == 54966 || code == 54967 || code == 54968 || code == 54970 || code == 54972 ||
                 code == 54973 || code == 54974 || code == 54975 || code == 54976 || code == 54977 || code == 54978 || code == 54979 || code == 54982 || code == 54983 || code == 54985 ||
                 code == 54986 || code == 54987 || code == 54989 || code == 54990 || code == 54991 || code == 54992 || code == 54994 || code == 54995 || code == 54997 || code == 54998 ||
                 code == 55000 || code == 55002 || code == 55003 || code == 55004 || code == 55005 || code == 55006 || code == 55007 || code == 55009 || code == 55010 || code == 55011 ||
                 code == 55013 || code == 55014 || code == 55015 || code == 55017 || code == 55018 || code == 55019 || code == 55020 || code == 55021 || code == 55022 || code == 55023 ||
                 code == 55025 || code == 55026 || code == 55027 || code == 55028 || code == 55030 || code == 55031 || code == 55032 || code == 55033 || code == 55034 || code == 55035 ||
                 code == 55038 || code == 55039 || code == 55041 || code == 55042 || code == 55043 || code == 55045 || code == 55046 || code == 55047 || code == 55048 || code == 55049 ||
                 code == 55050 || code == 55051 || code == 55052 || code == 55053 || code == 55054 || code == 55055 || code == 55056 || code == 55058 || code == 55059 || code == 55060 ||
                 code == 55061 || code == 55062 || code == 55063 || code == 55066 || code == 55067 || code == 55069 || code == 55070 || code == 55071 || code == 55073 || code == 55074 ||
                 code == 55075 || code == 55076 || code == 55077 || code == 55078 || code == 55079 || code == 55082 || code == 55084 || code == 55086 || code == 55087 || code == 55088 ||
                 code == 55089 || code == 55090 || code == 55091 || code == 55094 || code == 55095 || code == 55097 || code == 55098 || code == 55099 || code == 55101 || code == 55102 ||
                 code == 55103 || code == 55104 || code == 55105 || code == 55106 || code == 55107 || code == 55109 || code == 55110 || code == 55112 || code == 55114 || code == 55115 ||
                 code == 55116 || code == 55117 || code == 55118 || code == 55119 || code == 55122 || code == 55123 || code == 55125 || code == 55130 || code == 55131 || code == 55132 ||
                 code == 55133 || code == 55134 || code == 55135 || code == 55138 || code == 55140 || code == 55142 || code == 55143 || code == 55144 || code == 55146 || code == 55147 ||
                 code == 55149 || code == 55150 || code == 55151 || code == 55153 || code == 55154 || code == 55155 || code == 55157 || code == 55158 || code == 55159 || code == 55160 ||
                 code == 55161 || code == 55162 || code == 55163 || code == 55166 || code == 55167 || code == 55168 || code == 55170 || code == 55171 || code == 55172 || code == 55173 ||
                 code == 55174 || code == 55175 || code == 55178 || code == 55179 || code == 55181 || code == 55182 || code == 55183 || code == 55185 || code == 55186 || code == 55187 ||
                 code == 55188 || code == 55189 || code == 55190 || code == 55191 || code == 55194 || code == 55196 || code == 55198 || code == 55199 || code == 55200 || code == 55201 ||
                 code == 55202 || code == 55203) {  // start if..

                invalidChars += (str.charAt(idx) + "▦" + idx + "▩");
            }   // end if..
        }  // if조건 숨기기..end
    } // end for

    if (invalidChars == "" || invalidChars == null) {
        return -1;
    }

    var charlist = invalidChars.split("▩");
    var charlistLength = charlist.length;
	var invalidCharResult = "다음 글자는 입력할 수 없습니다.\n\n\n<<연번 : " +part + " >>\n\n";

    for (var idx = 0; idx < charlistLength; idx++ ) {
        if (charlist[idx] == "" || charlist[idx] == null) {
            break;
        }
		 invalidCharResult += ("[ " + (parseInt(charlist[idx].split("▦")[1])+1) + " 번째]\t" + "\'" + charlist[idx].split("▦")[0] + "\'\n");
    }

    // encoding 불가능한 한글 check alert
    alert( invalidCharResult);
    return false;
}