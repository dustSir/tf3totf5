/*
    * 제    목  : 기록검색
    * 설    명  : MRR_GRGeomSaek.xfm - JScript
    * 설 계 자  :
    * 작 성 자  : HIT EMR 개발팀
    * 작 성 일  : 2006-01-12 11:05오전
    * 수정이력  :
    * 기    타  : #!# 작업은 임시작업이므로 사용후 반드시 삭제할 로직임 : 2008-08-18 현재 미반영 상태 (재활의학과 검열요청)
*/

/*
//var sMRR_GRGeomSaek_parm = "";
var sMRR_GRGeomSaek_usyn = "N";

var sMRR_GRGeomSaek_idno = "";
var sMRR_GRGeomSaek_gubn = "";
var sMRR_GRGeomSaek_date = "";
var sMRR_GRGeomSaek_frdt = "";
var sMRR_GRGeomSaek_todt = "";
var sMRR_GRGeomSaek_dept = "";
var sMRR_GRGeomSaek_kind = "";
var sMRR_GRGeomSaek_doct = "";
var sMRR_GRGeomSaek_indd = "";
var sMRR_GRGeomSaek_oudd = "";
var sMRR_GRGeomSaek_outordkind = "";
var sMRR_GRGeomSaek_name = "";
var sMRR_GRGeomSaek_age_sex = "";
var sMRR_GRGeomSaek_deptname = "";

var sMRR_GRGeomSaek_DrugHelp_parm = ""; //약품정보help
var sMRR_GRGeomSaek_DrugHelp_code = ""; //약품정보help
var sMRR_GRGeomSaek_DrugHelp_cdnm = ""; //약품정보help
*/
var sGridBrowser = "1"; //1:그리드및 이미지로 조회, 2:브라우저로 조회
var sAlert = false; //alert 여부

//비트뷰어 관련 변수
var sMRR_GRGeomSaek_FileChange = "N";
var sMRR_GRGeomSaek_FileCnt = 0;
var sMRR_GRGeomSaek_ImageCnt = 1;
var sMRR_GRGeomSaek_DefaultCase = "case_1"; //
var sMRR_GRGeomSaek_ImageView = "N"; // 이미지로 볼것인지 여부
var sMRR_GRGeomSaek_ChrtCd  = "-";
var sMRR_GRGeomSaek_ChrtPSY = "Y";   // 신경정신과 검색권한 check

/*
//복사신청 관련 변수
var sMRR_GRGeomSaekCopy_date = "";
var sMRR_GRGeomSaekCopy_time = "";
var sMRR_GRGeomSaekCopy_idno = "";
var sMRR_GRGeomSaekCopy_idnonm = "";
var sMRR_GRGeomSaekCopy_dept = "";
var sMRR_GRGeomSaekCopy_doct = "";
var sMRR_GRGeomSaekCopy_doctnm = "";
var sMRR_GRGeomSaekCopy_regno1 = "";
var sMRR_GRGeomSaekCopy_regno2 = "";
var sMRR_GRGeomSaekCopy_zipcd1 = "";
var sMRR_GRGeomSaekCopy_zipcd2 = "";
var sMRR_GRGeomSaekCopy_zipcdseqno = "";
var sMRR_GRGeomSaekCopy_firstaddr = "";
var sMRR_GRGeomSaekCopy_secaddr = "";

//Message 관련 변수
var sMRR_GRGeomSaek_Message = "";
*/
//이미지 조회시 사용
var sTotalKind = "";

//20060307 taja78 add 일자별 전체조회시 사용
var iRowCount    = 1;
var g_Separators = "`";
var iGiRokGeomSakModal_GridCnt = 1;
//2006-11-23 3:27오후 김태범 추가 환자의 기록세부내역 조회시 조회중일 경우 기록리스트를 클릭하더라도 조회하지 못하기 위한 전역변수
var sTempSearchFlag = "N";

//XFR을 이용한 출력용 파일수 변환 카운트 2007-01-12 10:41오전 김태범 추가
var iGRGeomSaek_Print_FileChange_Count = 0;
var iMRR_GRGeomSaek_Print_ImageCnt = 0;
var g_Pid = ""; //기록검색 조회시 동일한 등록번호에 대한 내역 조회시

//2007-09-04 1:51오후 기록조회 신청 후 조회하는 패턴용 전역변수
var gGiRokSinCheongFlag = "";
var gGiRokSinCheong_read = "";
var gGiRokSinCheong_print = "";
var gGiRokSinCheong_pid = "";
var gGiRokSinCheong_use = "";

//검사결과조회에서 사용
//var gAction = "/ast/geomsachiryoweb/GyeolGwaJoHoi.live";
//var gMode = "reqGetGyeolGwaJoHoi";
//var gSelectedTab = 1;
//var gPid = "";
//var gUserId = "";
var g_fso     = new ActiveXObject("Scripting.FileSystemObject");
var g_browser = model.control("browser1");
var g_GRSearchVersion = ""; // 다양한 기록검색 버전에 따른 로직분기 : 디폴트 버전 ""\
var g_TotalTestResultTab = "1";

var g_DupChk_Permission = "N"; //appended by OBH on 2008.05.14
var g_DupChk_Pid = ""; //appended by OBH on 2008.05.14
var g_DupChk_Pid_Array = new Array(); //appended by OBH on 2008.05.14
var g_aLength = 0; //appended by OBH on 2008.05.14
var g_ArrCnt = 0; //appended by OBH on 2008.05.14
var g_DupChk_Cnt = 0; //appended by OBH on 2008.05.14
g_DupChk_Pid_Array[0] = ""; //appended by OBH on 2008.05.14
var g_Nst_Right_head = ""; // appended by OBH on 2009.09.03

var g_HistoryButton = false;    // appended by KSJ on 2009.10.16

var prePid = "";

var gImageViewYN = ""; //appended by webkeb on 2013.08.19

var gReadLogCheckCnt = 0; // 응급판독로그 생성
//var gAction = "/ast/geomsachiryoweb/GyeolGwaJoHoi.live";

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 초기화
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fOpen() {

	/* -------------------------------------------------------------------------------- */
    /* 2007-07-31 6:40오후 탭을 누를때 마다 열람내역이 저장되는 부분 막기위한 로직 수정 */
    //model.removeNodeset("/root/MainData/opmcptbsm"); //환자기본정보
    /* -------------------------------------------------------------------------------- */
    // button_date로 초기화
    //f_viewBtnUSN(); // 2008-07-10 1:41오후 제관 : USN 테스트 - 영양정보 버튼을 대신 테스트할 것이므로 숨김
    //f_viewBtnNutri();   // 영양기록 조회 버튼 보기 : 테스트후 버튼을 visible 시키고 이 로직은 삭제
	model.trace("MRR_GRGeomSaek - fOpen 1");
    var vCtrl = model.control("button_date");
    model.toggle(model.getwindowText("button_date"));
    vCtrl.selected = true;
	
	model.trace("MRR_GRGeomSaek - fOpen 2");
    f_SetNavigatorPathInfo("일자별");
    model.toggle("case_1");
    model.enable("radio_all", "true");
    model.enable("radio_otpt", "true");
    model.enable("radio_inpt", "true");
    model.enable("radio_er", "true");
    model.enable("radio_text", "true");
    model.enable("radio_image", "true");
    model.enable("radio_continue", "true");
    model.enable("radio_report", "true");
    model.enable("combo_dept", "true");

    model.enable("input_drugcode", "false"); //투약정보조회
    model.enable("button_drug_help", "false"); //투약정보조회
    model.enable("output8", "false"); //투약정보조회
    model.enable("button_print", "false"); //출력
    model.visible("button_rslt_result", "false"); //검사별
    model.enable("button10", "false"); //조혈모기증자 여부
    model.enable("button11", "false"); //이중번호 여부
    model.enable("button7", "false"); //임상관찰

    //model.enable("button_rslt_silp", false); // 2008-01-08 3:27오후, 정경화씨 요청 임시기능 막음

    model.setAttribute("MRR_GRGeomSaek_Modal.chart_type_view","LN");//진료지원기록 조회플래그 default "N" 설정
	model.setAttribute("gGiRokSinCheong_print", ""); // 출력권한 clear
	var tmp_year = fCommon_FindInit("0045"); //환경 설정 E-20 [EMR] 전체기록조회 기간 설정
	if(tmp_year == "Err")
		tmp_year = 0;
	else
		tmp_year = Number(tmp_year);
    var sTodt0 = new Date();
	var itmp = (1000 * 60 * 60 * 24);
	var sFrdt0 = new Date(); //today
	var sTodt = new Date(sFrdt0 -( - (itmp * 5))); //5일후
    var sFrdt = new Date(sTodt0 - (1000 * 60 * 60 * 24 * 365) - (1000 * 60 * 60 * 24 * 365)* tmp_year ); //today의 365일 + 설정된 x년 전
    sTodt = sTodt.lvFormat();
    sFrdt = sFrdt.lvFormat();
	
	model.trace("MRR_GRGeomSaek - fOpen 3");
    model.setValue("/root/MainData/condition/frdt", sFrdt);
    model.setValue("/root/MainData/condition/todt", sTodt);

    model.setValue("/root/HideData/combodept/deptcd", "%");
    model.setValue("/root/HideData/combodept/deptnm", "전체");
    model.setValue("/root/HideData/combodept/sddeptnm", "전체");
    model.setValue("/root/HideData/combodept/deptlevl", "%");
    model.setValue("/root/HideData/combodept/updeptcd", "%");
    model.setValue("/root/HideData/combodept/tempstat", "-");
    model.setValue("/root/HideData/combodept/tempdate", sTodt);
    model.setValue("/root/MainData/condition/cond2", "%"); //default로 전체를 display해달라고 요청(20050602)
    model.setValue("/root/HideData/glucoseyn","N"); //default로 글루코즈를 N으로 세팅함...

    //출력권한이 있으면 임상관찰 출력 버튼을 enable(요구:20070719 장주임)
    var sBitprtauth = TFGetSessionInfo("bitprtauth"); //출력권한
    if (sBitprtauth == "1" || gGiRokSinCheong_print == "Y") { //
        model.enable("button7", "true"); //임상관찰
    }
    
	model.trace("MRR_GRGeomSaek - fOpen 4");
    var occukind_OBH_CHK = TFGetSessionInfo("occukind"); //model.getValue("/root/MainData/opmcptbsm/occukind")
    var userdeptcd = TFGetSessionInfo("userdeptcd");
    if (userdeptcd == "000178" || userdeptcd == "000109") {
        model.visible("radio_report","true");
    } else {
        //model.visible("radio_report","false");
        if (occukind_OBH_CHK == "10") {
            model.visible("radio_report","true");
        } else {
            model.visible("radio_report","false");
        }
    }
	
	model.trace("MRR_GRGeomSaek - fOpen 5");
	  var gUserId = TFGetSessionInfo("userid");	

    var sPtno = TFGetTopInfo("HJID"); //등록번호

    // 2007-07-03 10:07오전, 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
    if (fEMR_getGeneMode() == true) {
        sPtno = "";
    }

    var sParm = model.getAttribute("sMRR_GRGeomSaek_parm"); //Argument
	//prePid = model.getAttribute("sMRR_GRGeomSaek_prePid"); //Argument
	
	  	
	model.trace("MRR_GRGeomSaek - fOpen 6");
    /*
     * ===========================================================
     *2007.09.04 기록조회 권한신청 관련 환자조회 부분 수정 [김태범]
     */
    gGiRokSinCheong_read = "";
    gGiRokSinCheong_print = "";
    gGiRokSinCheong_use = "";
    gGiRokSinCheongFlag = model.getAttribute("sMRR_GRGeomSaek_parm_GiRokJoHoiSingCheong");
    model.setAttribute("sMRR_GRGeomSaek_parm_GiRokJoHoiSingCheong", "");
    if (gGiRokSinCheongFlag != null && gGiRokSinCheongFlag != "") {
        var tempSinCheongArr = gGiRokSinCheongFlag.split("▦");
        gGiRokSinCheong_pid = sParm;
        gGiRokSinCheong_read = tempSinCheongArr[1];
        gGiRokSinCheong_print = tempSinCheongArr[2];
        gGiRokSinCheong_use   = tempSinCheongArr[3];
        model.setAttribute("gGiRokSinCheong_print", gGiRokSinCheong_print);

        if (gGiRokSinCheong_read != "Y") {
            model.alert("조회할 권한이 없습니다.");
            model.enable("button_date", "false");
            model.enable("button_dept", "false");
            model.enable("button_form", "false");
            model.enable("button_nurse", "false");
            model.enable("button_order1", "false");
            model.enable("button_order2", "false");
            model.enable("button_rslt", "false");
            model.enable("button_pm", "false");
            model.enable("button_acc", "false");
            model.enable("button_ocr", "false");
            model.enable("button_drug", "false");
            model.enable("button_copy", "false");
            model.enable("button_drug2", "false");
            model.enable("button6", "false");
            model.enable("button_date_lt", "false");
            model.enable("button8", "false");
            return;
        } else {
            model.enable("button_date", "true");
            model.enable("button_dept", "true");
            model.enable("button_form", "true");
            model.enable("button_nurse", "true");
            model.enable("button_order1", "true");
            model.enable("button_order2", "true");
            model.enable("button_rslt", "true");
            model.enable("button_pm", "true");
            model.enable("button_acc", "true");
            model.enable("button_ocr", "true");
            model.enable("button_drug", "true");
            model.enable("button_copy", "true");
            model.enable("button_drug2", "true");
            model.enable("button6", "true");
            model.enable("button_date_lt", "true");
            model.enable("button8", "true");
        }
    }
	
	model.trace("MRR_GRGeomSaek - fOpen 7");
	model.enable("input_idno", "true");
  
    /* =========================================================== */
    if (sParm == "") {
        if (sPtno == "") { //TFGetTopInfo에서 등록번호 없는경우 ""로 setting
            model.refresh();
            model.setFocus("input_idno");
        } else {
            model.setValue("/root/MainData/condition/cond1", sPtno);
            model.setValue("/root/HideData/temp_pid", prePid);
            fClicked("button_query");
        }
    } else {
        model.setValue("/root/MainData/condition/cond1", sParm);
				model.setValue("/root/HideData/temp_pid", prePid);
        model.setAttribute("sMRR_GRGeomSaek_parm", "");
        fClicked("button_query");
    }    
    
    model.trace(model.getAttribute("sMRR_GRGeomSaek_parm1"));
    if (model.getAttribute("sMRR_GRGeomSaek_parm1") == "1"){
			model.enable("input_idno", "false");			
	  }
	  model.setAttribute("sMRR_GRGeomSaek_parm1","");
	
	model.trace("MRR_GRGeomSaek - fOpen 8");
    fGetDeptCode();
    fCommon_GetChartGroupColorList();

    //2007.04.19 운영서버반영
    var folder = g_EMRImageTempPath;
    var fso =new ActiveXObject('Scripting.FileSystemObject');
    var foldername = fso.FolderExists(folder); //폴더의 true false 값 저장

    if (foldername == 0) {
        fso.CreateFolder(folder);
    }

	model.trace("MRR_GRGeomSaek - fOpen 9");
    // appended by OBH on 2008.03.06 , 강정규선생 요구사항, 사용자의 소속이 적정진료 지원팀(select * from COMCDEPTM where deptcd = '000109')
    // 일경우 검사탭과 이미지(라디오버튼)버튼에 포커싱이 되도록 요청
    // 의료정보팀으로 테스트 --> '000010'
    //TFGetMsgBox(-1, "userdeptcd = " + userdeptcd, "확인", "I", "OK");
    var sParm_2 = model.getAttribute("sMRR_GRGeomSaek_parm2"); //Argument
    //TFGetMsgBox(-1, "sParm_2 = " + sParm_2, "확인", "I", "OK");
    //if (userdeptcd == "000010") {
    if (userdeptcd == "000109" && sParm_2 == "1") {
        //TFGetMsgBox(-1, "userdeptcd = " + userdeptcd, "확인", "I", "OK");
        // 임시로 막아봅시당............................
        //fClicked("button_rslt");
        model.setValue("/root/HideData/condition/cond6","I");
        //fOnClickPrintFlag();
        var vCtrl = model.control("button_rslt");
        vCtrl.selected = true;

        //model.toggle("case_6");
        //model.visible("radio_report","true");
        //var vCtrl = model.control("button_rslt");
        //model.toggle(model.getwindowText("button_rslt"));
        //model.toggle("case_6");
        //f_SetNavigatorPathInfo("검사");
        //model.setFocus("radio_image");
        //model.setValue("radio_image","1");
        //model.refresh();
    }

	model.trace("MRR_GRGeomSaek - fOpen 10");
    // end append

    // appended by OBH on 2008.05.08
    // 기록검색 권한을 체크한다.
    //var dupChk = "0";
    //fPermissionCheck(gGiRokSinCheong_pid);

    //appended by OBH on 2008.08.11, 의공팀의 경우는 "검사"만 열어 준다.
    //if (userdeptcd == "000167") {
    if (userdeptcd == "000167") {
        model.enable("button_date", "false");
        model.enable("button_form", "false");
        model.enable("button_order1", "false");
        model.enable("button_order2", "false");
        model.enable("button_nurse", "false");
        model.enable("button_ocr", "false");
        model.enable("button_pm", "false");
        model.enable("button_drug", "false");
        model.enable("button_drug2", "false");
        model.enable("button6", "false");
        model.enable("button_date_lt", "false");
        model.enable("button8", "false");
        model.enable("btnUSN", "false");
    }
    
    if (userdeptcd == "000178"){
    	 model.visible("cbPhoto", "true");
    }
	
	model.trace("MRR_GRGeomSaek - fOpen 11");
    fChkCRER(); //응급실호출의 권한 관리

    if(userdeptcd =='000397' && occukind_OBH_CHK == "10"){//유저가 마취통증의학과 이고 의사직인 경우만..
        fChkANE(); //마취과 마취환자의 신경정신과 열람
    }else{
        model.setValue("root/MainData/limit/anecheck","");
        model.refresh();
    }

	model.trace("MRR_GRGeomSaek - fOpen 12");
    fGetOtherHospCheck(); //타병원 진료자료가 있는지 조회(20110929)

  if (fEMR_getGeneMode() == true) {}
	else {
		f_AlertOtherHospBtn(); //타병원 버튼 visible 관리 : 유전자은행일 경우는 제외처리
  }
  
  model.trace("MRR_GRGeomSaek - fOpen 13");
  // Menu Setting
  try{
	//model.setGridMenu("grid_main", "/root/HideData/gridMenu_action", true);   
  } catch(e){}  
}


// 왼쪽 메뉴리스트 by newhope
eval('function grid_main::OnClickedMenuItem( menu, row, col ) {grid_main_OnClickedMenuItem( menu, row, col );}');
function grid_main_OnClickedMenuItem(menu, row, col) {
    var str = "Menu =  " + menu + "  Row is =   " + row +  " Col is =   " + col;
    switch (menu) {      
        case "CHRTDEL" :
			// ** 시스탬 오픈전 **
			var m_checkid     = TFGetSessionInfo("userid");
			var m_checkdeptcd = TFGetSessionInfo("userdeptcd");
			
			/*
			// 시스템오픈. 2015.04.01 by newhope
			if (m_checkid != "29990030" && m_checkdeptcd != "000010"){				
				return;
			}
			*/
			
			model.setAttribute("MRD_GRCHRTDEL.asktype", "M"); // 진료, 간호, 검사, 재증명 구분 식별 방법 
			
			/*
			<mrrfflshh>
				<levl>4</levl>
				<chrtnm>외래초진기록[OS(이석하)/이석하]</chrtnm>
				<chrtkind>E|D|02170473|20141006|O|ODC0000002|201410061500|000312|20020023|외래초진기록|정형외과|이석하|-|||-|-|-|-|-|-|Y</chrtkind>
				<seq>3|5|20141006|20141006|000312|150099|</seq>
				<dept>000312|정형외과|</dept>
			</mrrfflshh>			
			*/						
			var sLevl   = model.getValue("/root/MainData/mrrfflshh[" + row + "]/levl");			
			if (sLevl != "4") return; // 상세기록만 삭제요청 가능
			
			var sPid = model.getValue("/root/MainData/condition/cond1");			
			var sKornmInfo = model.getValue("/root/MainData/opmcptbsm/name");
            var m_chrtkind = model.getValue("/root/MainData/mrrfflshh[" + row + "]/chrtkind");
            var m_seq      = model.getValue("/root/MainData/mrrfflshh[" + row + "]/seq");
            var m_chrtnm   = model.getValue("/root/MainData/mrrfflshh[" + row + "]/chrtnm");
			if(m_seq.substr(13,5) == "focus") return;// focus 기록은 삭제요청 가능하지 않게
			var m_type     = m_chrtkind.substr(0, 1);
			
			// 20200306 김세진 - KRC 전원회신 의무기록삭제요청 (KRC팀만 가능하게)
			if(m_chrtkind.indexOf("|CLX0000001|") != -1){
				if(m_checkdeptcd != "000027"){
					alert("회신이 완료되어 EMR 삭제요청이 불가합니다 (KRC문의 T. 7076)");
					return;
				}
			}
			
			if (m_type == "E" || m_type == "O" || m_type == "N") {        // 진료진료, OCR, 간호기록
				model.setAttribute("MRD_GRCHRTDEL", "AllChartView");      // 진료기록화면에서의 조회
				model.setAttribute("MRD_GRCHRTDEL.pid",      sPid);       // 환자id
				model.setAttribute("MRD_GRCHRTDEL.pname",    sKornmInfo); // 환자성명			
				model.setAttribute("MRD_GRCHRTDEL.asktype",  m_type);     // 진료			
				model.setAttribute("MRD_GRCHRTDEL.chrtkind", m_chrtkind);
				model.setAttribute("MRD_GRCHRTDEL.seq",      m_seq);
				model.setAttribute("MRD_GRCHRTDEL.chrtnm",   m_chrtnm);
				
				// 팝업호출, 해당정보 연동
				var m_xfm = "/emr/girokweb/xfm/MRD_GRChartDel.xfm";
				TFshowModal(null, null, "1", m_xfm, "-", 500, 420, 0, 0, false, false);
			}
			            
            break;
	}
}			


/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 이미지/XFR 출력 버튼 click
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fOnClickPrintFlag() {
    var sPrintFlag = model.getValue("/root/HideData/condition/cond6");
    if (sPrintFlag == "I" || sPrintFlag == "C") {
        model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
        model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
    } else if (sPrintFlag == "P") {
        model.enable("button_print", "false"); //출력
        model.setAttribute("MRR_GRGeomSaek_Print.function", "clear");
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
    } else {
        model.alert("이미지(page,연속보기)/출력 버튼 선택 시 오류");
        return;
    }
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 이미지 출력 권한 체크
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCheckPrePrint() {
    if (gGiRokSinCheongFlag != null && gGiRokSinCheongFlag != "") {
        if (gGiRokSinCheong_print == "Y") {
            return true;
        } else {
            model.alert("출력 권한이 없습니다. 의무기록팀에 출력 권한을 부여 받아야 출력 하실 수 있습니다.");
            return false;
        }
    }
    return true;
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 화면 Clear
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fReset() {
    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Clear.xfm","replace");

    model.removeNodeset("/root/MainData/mrrfflshh");
    model.removeNodeset("/root/MainData/mrrfflshh_copy");
    model.removeNodeset("/root/MainData/copylist");

    model.gridRebuild("grid_main");
    model.gridRebuild("grid_copy");
    model.gridRebuild("grid_list");

    var sTodt0 = new Date();
	var itmp = (1000 * 60 * 60 * 24);
	var sFrdt0 = new Date(); //today
	var sTodt = new Date(sFrdt0 -( - (itmp * 5))); //5일후
    var sFrdt = new Date(sTodt0 - (1000 * 60 * 60 * 24 * 365)); //today의 365일전
    sTodt = sTodt.lvFormat();
    sFrdt = sFrdt.lvFormat();

    model.setValue("/root/MainData/condition/frdt", sFrdt);
    model.setValue("/root/MainData/condition/todt", sTodt);

    model.setValue("/root/HideData/combodept/deptcd", "%");
    model.setValue("/root/HideData/combodept/deptnm", "전체");
    model.setValue("/root/HideData/combodept/sddeptnm", "전체");
    model.setValue("/root/HideData/combodept/deptlevl", "%");
    model.setValue("/root/HideData/combodept/updeptcd", "%");
    model.setValue("/root/HideData/combodept/tempstat", "-");
    model.setValue("/root/HideData/combodept/tempdate", sTodt);
    model.setValue("/root/MainData/condition/cond2", "%");  //default로 전체를 display해달라고 요청(20050602)
    model.setValue("/root/HideData/glucoseyn","N");         //default로 글루코즈를 N으로 세팅함...

    TFSetMessage("");
    model.refresh();
    model.setFocus("input_idno");
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 입력 항목의 유효성 검사
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fIsValid() {
    var srhTxtSize = model.getValue("/root/MainData/condition/cond1").length;
    if (srhTxtSize == 0) {
        //TFGetMsgBox(-1, "등록번호를 입력하세요.", "확인");
        model.setFocus("input_idno");
        return false;
    } else if (srhTxtSize != 8) {
        var idno = model.getValue("/root/MainData/condition/cond1");
        model.setValue("/root/MainData/condition/cond1",idno.lvGetZeroString(8));
    }
    return true;
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 클릭시 처리
 * @param  : pControlID
 * @return :
 * @---------------------------------------------------
 */
function fClicked(pControlID) {
    var arr_info1 = null;
    var arr_info2 = null;

    var gOption = "2";   //검사결과조회시 이용  ==> 실시[접수]일자 기준 기준
    //var gOption = "1";     //검사결과조회시 이용  ==> 결과일자 기준 2006.06.14 주석처리함 taja

	//prePid = model.getAttribute("sMRR_GRGeomSaek_prePid"); //Argument

    sMRR_GRGeomSaek_FileCnt = 0;
    //결과일자 or 실시일자 조회기준 get
    var sInfo = TFGetSessionInfo("userEnvInfo"); sInfo = sInfo.lvStripWhite();

    if (sInfo.length > 0) {
        arr_info1 = sInfo.split("▩");
        for (var i = 0; i < arr_info1.length; i++) {
            arr_info2 = arr_info1[i].split("▦");
            if (arr_info2.length > 0) {
                if (arr_info2[0] == "0014") {          //결과일자 or 실시일자 조회기준 코드
					/*
                    if (arr_info2[1] == "0") {         //미설정
                        gOption = "1";
                    } else if (arr_info2[1] == "1") {  //결과일자기준
                        gOption = "1";
                    } else if (arr_info2[1] == "2") {  //실시일자기준
                        gOption = "2";
                    } else {                          //default값  ==>> 2006.06.14 실시[접수]일자 기준으로 default값 변경함 taja
                        gOption = "2";
                    }
					*/
					gOption = "2";
                } else if(arr_info2[0] == "0035") {
					if(arr_info2[1] == "1") {
						g1ption = "1";
					} else if(arr_info2[1] == "2") {
						g1ption = "2";
					}
					else{
						g1ption = "1";                //default값
					}
				}
            }
        }
    }

	var sLanguage = model.getValue("/root/HideData/condition/cond15"); //한글 혹은 영어로 조회

    if (pControlID == "button_nurse_all") { //간호 전체 조회(임시테스트용)
        model.removeNodeset("/root/HideData/savedata_nurse");
        model.removeNodeset("/root/HideData/savedata_nurse_all");
        TFclearNodeValue("/root/SendData"); //이전의 서버 호출에 사용된 정보 지움
        model.setValue("/root/SendData/Mode", "MRR_reqGetMrnipkidh2"); //Action Method
        model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1")); //등록번호
        if (model.submitInstance("MRR_reqGetMrnipkidh")) {
            TFSetMessage("root/HideData/savedata_nurse_all");
        } else {
            return;
        }

        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
        var emr_Right = TFGetModel("body_girokgeomsaek_Right", "Y");
        if (emr_Right) {
            var rowCnt = getNodesetCnt("/root/HideData/savedata_nurse_all");

            for (var i = 0; i < rowCnt; i++) {
                var idx = i + 1;

                var sChrtName = "===== " + model.getValue("/root/HideData/savedata_nurse_all[" + idx + "]/chrtname") + " =====";
                emr_Right.javascript.fMakeGridNurse(sChrtName);

                copyNode("/root/HideData/savedata_nurse_all[" + idx + "]/signdata", "/root/HideData/savedata_nurse[" + idx + "]");

                var xPath = "/root/HideData/savedata_nurse[" + idx + "]";
                var sText = fCommon_getInstanceStringSignValue(xPath);
                emr_Right.javascript.fMakeGridNurse(sText);

                if (i != rowCnt - 1) emr_Right.javascript.fMakeGridNurse(" \n \n \n");
            }
            emr_Right.refresh();
            return;
        } else { // model 객체를 얻어오지 못하였을 경우.
            model.alert("조회에 실패하였습니다! 다시 한번 선택해 주시기 바랍니다.");
            return;
        }
    }

    if (pControlID == "button_query" || pControlID == "button_date" || pControlID == "button_dept" || pControlID == "button_form" || pControlID == "button_nurse" || pControlID == "button_order1" || pControlID == "button_order2" || pControlID == "button_acc" || pControlID == "button_ocr") {
		model.toggle("case_1");
        model.enable("radio_all", "true");
        model.enable("radio_otpt", "true");
        model.enable("radio_inpt", "true");
        model.enable("radio_er", "true");
		model.enable("radio_language2", "false");
        if (pControlID == "button_query" || pControlID == "button_date" || pControlID == "button_dept" || pControlID == "button_form" || pControlID == "button_nurse") {
            model.enable("radio_text", "true");
            model.enable("radio_image", "true");
            model.enable("radio_continue", "true");
            model.enable("radio_report", "true");
			model.enable("radio_language", "true");
        } else {
			model.enable("radio_language", "false");
            model.enable("radio_text", "false");
            model.enable("radio_image", "false");
            model.enable("radio_continue", "false");
            model.enable("radio_report", "false");
        }
        model.enable("combo_dept", "true");
    } else if (pControlID == "button_rslt") { //검사결과조회
        // appended by OBH on 2008.04.03
        //TFGetMsgBox(-1, "여기가 문제일 까요????????? line 537", "확인", "I", "OK");

        model.toggle("case_2");
        model.enable("radio_all", "false");
        model.enable("radio_otpt", "false");
        model.enable("radio_inpt", "false");
        model.enable("radio_er", "false");
        model.enable("radio_text", "true");
        model.enable("radio_image", "true");
        model.enable("radio_continue", "true");
        model.enable("radio_report", "true");
        model.enable("combo_dept", "false");
		model.enable("radio_language", "true");
		model.enable("radio_language2", "true");
    } else if (pControlID == "button_drug" || pControlID == "button_drug2") { //Drug조회
        if (pControlID == "button_drug") {                            //ALTERED BY taja78 start... 2005.09.13
            model.toggle("case_3");
            model.setValue("/root/HideData/condition/cond3", "A");
            model.setValue("/root/HideData/condition/cond4", "");
            model.setValue("/root/HideData/condition/cond5", "");
        } else if (pControlID == "button_drug2") {
            model.toggle("case_5");
            model.setValue("/root/HideData/condition/cond8","-");
            model.setValue("/root/HideData/condition/cond9","-");
            model.setValue("/root/HideData/condition/cond10","A");
            model.setValue("/root/HideData/condition/cond11", "");
            model.setValue("/root/HideData/condition/cond12", "");
            model.setValue("/root/HideData/condition/cond13","A");
            model.setValue("/root/HideData/condition/cond14","A");  //ALTERED BY taja78 end... 2005.09.13
            //진료정보 초기화추가                                   //ALTERED BY taja78 start... 2006.02.13
            model.removeNodeset("/root/MainData/jinryojeongbo_twoyak");
            model.gridrebuild("grid2");                             //ALTERED BY taja78 end...   2006.02.13
        }
		model.enable("radio_language", "false");
		model.enable("radio_language2", "false");
        model.enable("radio_all", "false");
        model.enable("radio_otpt", "false");
        model.enable("radio_inpt", "false");
        model.enable("radio_er", "false");
        model.enable("radio_text", "false");
        model.enable("radio_image", "false");
        model.enable("radio_continue", "false");
        model.enable("radio_report", "false");
        model.enable("combo_dept", "false");
    } else if (pControlID == "button_copy") { //복사신청
		model.enable("radio_language", "false");
		model.enable("radio_language2", "false");
        model.toggle("case_4");
        model.enable("radio_all", "true");
        model.enable("radio_otpt", "true");
        model.enable("radio_inpt", "true");
        model.enable("radio_er", "true");
        model.enable("radio_text", "false");
        model.enable("radio_image", "false");
        model.enable("radio_continue", "false");
        model.enable("radio_report", "false");
        model.enable("combo_dept", "true");
    }

    if (pControlID == "button_query") { //********** 검색대상자조회

		//---- 2010.12 PSY 기록검색권한 제한처리
		//---- 2012.09 특정인 진료정보조회 권한점검 처리
		/*
		var sLimitCheck = fGetPSYGRCheck();
		if ( sLimitCheck == "N" ) {
			model.removeNodeset("/root/MainData/mrrfflshh");
			model.gridRebuild("grid_main");
			return;
		}
		*/
		
		gGiRokSinCheong_print = "";
		model.setAttribute("gGiRokSinCheong_print", "");
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
		model.enable("button7", "false"); //출력(임상관찰출력)(무조건 disable시킨다.)
		
		if (!fSelect()) return; // 등록번호 Validation 체크

		sMRR_GRGeomSaek_ChrtPSY = model.getValue("/root/MainData/limit/grcheck");
        var sUsyn = model.getValue("/root/MainData/opmcptbsm/usyn"); //권한체크

		//alert(model.getValue("/root/MainData/opmcptbsm/usyn"));

        if (sUsyn == "N") {
            var MsgRtn = TFGetMsgBox(-1, "조회권한이 없습니다.\n사유입력 후 조회가 가능합니다.\n사유를 입력하시겠습니까?", "확인", "Q", "YN"); //6:예 7:아니오
            if (MsgRtn == '6') {
                model.setAttribute("sMRR_GRGeomSaek_usyn", "N");
                model.setAttribute("sMRR_GRGeomSaek_parm", model.getValue("/root/MainData/condition/cond1"));
				        model.setAttribute("sMRR_GRGeomSaek_priv", model.getValue("/root/MainData/opmcptbsm/priv"));
                model.setAttribute("sMRR_GRGeomSaek_Reason_param",gGiRokSinCheong_use);
				model.setAttribute("sMRR_GRGeomSaek_psycheck", model.getValue("/root/MainData/limit/grcheck"));

                TFshowModal(null, null, "1", "/emr/girokweb/xfm/MRR_GRGeomSaek_Reason.xfm", "-", 538, 210, 0, 0, false, false);
                var sUsyn = model.getAttribute("sMRR_GRGeomSaek_usyn");
                model.setAttribute("sMRR_GRGeomSaek_parm", "");
                model.setAttribute("sMRR_GRGeomSaek_usyn", "N");

                if (sUsyn == "N") {
                    model.enable("button_date", "false");
                    model.enable("button_dept", "false");
                    model.enable("button_form", "false");
                    model.enable("button_nurse", "false");
                    model.enable("button_order1", "false");
                    model.enable("button_order2", "false");
                    model.enable("button_rslt", "false");
                    model.enable("button_pm", "false");
                    model.enable("button_acc", "false");
                    model.enable("button_ocr", "false");
                    model.enable("button_drug", "false");
                    model.enable("button_copy", "false");
                    model.enable("button_drug2", "false");
                    model.enable("button6", "false");
                    model.enable("button_date_lt", "false");
					model.enable("button8", "false");
					model.enable("button_nutri", "false");

                    return;
                } else {
                    model.enable("button_date", "true");
                    model.enable("button_dept", "true");
                    model.enable("button_form", "true");
                    model.enable("button_nurse", "true");
                    model.enable("button_order1", "true");
                    model.enable("button_order2", "true");
                    model.enable("button_rslt", "true");
                    model.enable("button_pm", "true");
                    model.enable("button_acc", "true");
                    model.enable("button_ocr", "true");
                    model.enable("button_drug", "true");
                    model.enable("button_copy", "true");
                    model.enable("button_drug2", "true");
                    model.enable("button6", "true");
                    model.enable("button_date_lt", "true");

                    //사유 입력을 제대로 했을 경우 2007-07-31 7:01오후 김태범
                    //현재 전역변수의 등록번호와 조회할 등록번호가 같을 경우 다시 조회 하지 않는다. 2007-07-05 1:21오후 김태범
                    var sPid = model.getValue("/root/MainData/condition/cond1");
                    if (g_Pid == sPid) {
                        var sTempUsyn = model.getValue("/root/MainData/opmcptbsm/usyn");
                        if (sTempUsyn == "N") {
                            model.setValue("/root/MainData/opmcptbsm/usyn","Y");
                        }
                    }
                }
            } else { //if (MsgRtn == '6')
                model.enable("button_date", "false");
                model.enable("button_dept", "false");
                model.enable("button_form", "false");
                model.enable("button_nurse", "false");
                model.enable("button_order1", "false");
                model.enable("button_order2", "false");
                model.enable("button_rslt", "false");
                model.enable("button_pm", "false");
                model.enable("button_acc", "false");
                model.enable("button_ocr", "false");
                model.enable("button_drug", "false");
                model.enable("button_copy", "false");
                model.enable("button_drug2", "false");
                model.enable("button6", "false");
                model.enable("button_date_lt", "false");
                return;
            }
        } else if(sUsyn == "Author") {
			model.alert("조회권한이 없습니다.\n필요시 '진료용이외 의무기록 열람신청' 메뉴를 이용하세요" );
			model.setAttribute("sMRR_GRGeomSaek_usyn", "N");

			model.enable("button_date", "false");
			model.enable("button_dept", "false");
			model.enable("button_form", "false");
			model.enable("button_nurse", "false");
			model.enable("button_order1", "false");
			model.enable("button_order2", "false");
			model.enable("button_rslt", "false");
			model.enable("button_pm", "false");
			model.enable("button_acc", "false");
			model.enable("button_ocr", "false");
			model.enable("button_drug", "false");
			model.enable("button_copy", "false");
			model.enable("button_drug2", "false");
			model.enable("button6", "false");
			model.enable("button_date_lt", "false");
			model.enable("button8", "false");
			model.enable("button_nutri", "false");

			return;

		} else {
            model.enable("button_date", "true");
            model.enable("button_dept", "true");
            model.enable("button_form", "true");
            model.enable("button_nurse", "true");
            model.enable("button_order1", "true");
            model.enable("button_order2", "true");
            model.enable("button_rslt", "true");
            model.enable("button_pm", "true");
            model.enable("button_acc", "true");
            model.enable("button_ocr", "true");
            model.enable("button_drug", "true");
            model.enable("button_copy", "true");
            model.enable("button_drug2", "true");
            model.enable("button6", "true");
            model.enable("button_date_lt", "true");

            // appended by OBH on 2008.05.09
            // 권한 조회가 제대로 되지 않아 함수를 추가함.
            // Noted by OBH on 2008.05.22
            // 부서간, 의료정보팀 내부 팀원간 이견이 있어 일단 원래대로 되돌림.
            // 조율되어 확정될때까지 막음
            // if (!fPermissionCheck(g_Pid)) return;
            // append completed by OBH on 20080509
            // appended by OBH on 2008.06.04
            // 위의 사항이 해결될때 까지 임시적으로 간호대 실습생의 경우는 권한을 조회한다.
            // 일단, 간호대 실습생의 유저아이디로 체크한다.
            var gUserId = TFGetSessionInfo("userid");
            //model.alert("gUserId = " + gUserId);
            if (gUserId == "39990008") {
                if (!fPermissionCheck(g_Pid)) return;
            }
        }
        //****************************************************************************************//
        //****************** 의료기관 평가 관련 의무기록팀 요청사항(2007.10.19) ******************//
        //****************************************************************************************//
        var userdeptcd = TFGetSessionInfo("userdeptcd");
        if (userdeptcd == "000178") {
            var vCtrl = model.control("button_form");
            model.toggle(model.getwindowText("button_form"));
            vCtrl.selected = true;
            model.setValue("/root/HideData/condition/cond1", "form");
        } else {
            var vCtrl = model.control("button_date");
            model.toggle(model.getwindowText("button_date"));
            vCtrl.selected = true;
            model.setValue("/root/HideData/condition/cond1", "date");
        }
        //복구할 소스
        /*
        //조회버튼 클릭 시 항상 기록조회를 선택한다.
        var vCtrl = model.control("button_date");
        model.toggle(model.getwindowText("button_date"));
        vCtrl.selected = true;
        model.setValue("/root/HideData/condition/cond1", "date");
        */

        sMRR_GRGeomSaek_DefaultCase = "case_1";
        sMRR_GRGeomSaek_ImageView = "Y";
        model.setValue("/root/HideData/condition/cond6", "T");
        model.setValue("/root/HideData/condition/cond7", "Y");

		if (!fSelect2()) return; //
        fGetDept();

        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Clear.xfm","replace");
        model.setFocus("input_idno");

        // appended by OBH 2008.03.06 강정규 요구사항을 위해 삽입
        //if (userdeptcd == "000010") {
        var sParm_2 = model.getAttribute("sMRR_GRGeomSaek_parm2"); //Argument
        //if (userdeptcd == "000010" && sParm_2 == "1") {
        if (userdeptcd == "000109" && sParm_2 == "1") {
            f_initViewImage("userdeptcd", gOption);
        }
        //model.setFocus("input_idno");
        //end append
        // appended by OBH on 2008.04.22
        // MRR_GRGeomSaek_Result.xfm에 환자이름을 넘겨주기위해 추가했슴

        // 남홍기과장의 요구사항
        var sMRR_GRGeomSaek_PtName = "";
        model.setAttribute("sMRR_GRGeomSaek_PtName", model.getValue("/root/MainData/opmcptbsm/name"));

        //appended by OBH on 2008.08.11, 의공팀의 경우는 "검사"만 열어 준다.
        var userdeptcd = TFGetSessionInfo("userdeptcd");
        //if (userdeptcd == "000167") {
        if (userdeptcd == "000167") {
            model.enable("button_date", "false");
            model.enable("button_form", "false");
            model.enable("button_order1", "false");
            model.enable("button_order2", "false");
            model.enable("button_nurse", "false");
            model.enable("button_ocr", "false");
            model.enable("button_pm", "false");
            model.enable("button_drug", "false");
            model.enable("button_drug2", "false");
            model.enable("button6", "false");
            model.enable("button_date_lt", "false");
            model.enable("button8", "false");
            model.enable("btnUSN", "false");

            model.removeNodeset("/root/MainData/mrrfflshh");
            model.gridRebuild("grid_main");
            model.refresh();
        }
        // end append
    } else if (pControlID == "button_query2") { //********** 서식지조회
        if (!fSelect2()) return; // 등록번호 Validation 체크
        fGetDept();
        model.setFocus("input_idno");
    } else if (pControlID == "button_print") { //********** 출력
        /* ====== 기록조회 권한신청 관련 추가 부분 ===== */
        if (!fCheckPrePrint()) return;

        /* ============================================= */
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            emr_Right.javascript.printChart("차트 출력");
            var sPrint = model.getAttribute("sCMR_Param");
            if (sPrint == "") {
                return;
            }
            fChartPrint(sPrint);
        } else {
            model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
            return;
        }
    } else if (pControlID == "combo_dept") { // 진료과 변경
        fSelect2();
    } else if (pControlID == "button_date") { //********** 검색대상자조회(일자별)
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.setValue("/root/HideData/condition/cond1", "date");
        sMRR_GRGeomSaek_DefaultCase = "case_1";
        sMRR_GRGeomSaek_ImageView = "Y";
        model.setValue("/root/HideData/condition/cond7", "Y");

        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분
        if (sTextImage == "T") { //
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Grid.function", "clear");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
            }
        } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") {
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
            }
        } else if (sTextImage == "P") {
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
        }

        if (!fSelect()) return; // 등록번호 Validation 체크
        if (!fSelect2()) return; // 등록번호 Validation 체크
        fGetDept();
        // model.alert("여깁니다요.......");
        model.setFocus("input_idno");
    } else if (pControlID == "button_dept") { //********** 검색대상자조회(과별)
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.setValue("/root/HideData/condition/cond1", "dept");
        sMRR_GRGeomSaek_DefaultCase = "case_1";
        sMRR_GRGeomSaek_ImageView = "Y";
        model.setValue("/root/HideData/condition/cond7", "Y");

        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분
        if (sTextImage == "T") { //
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Grid.function", "clear");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
            }
        } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") {
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
            }
        }

        if (!fSelect()) return; // 등록번호 Validation 체크
        if (!fSelect2()) return; // 등록번호 Validation 체크
        fGetDept();
        model.setFocus("input_idno");
    } else if (pControlID == "button_form") { //********** 검색대상자조회(서식지별)
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.setValue("/root/HideData/condition/cond1", "form");
        sMRR_GRGeomSaek_DefaultCase = "case_1";
        sMRR_GRGeomSaek_ImageView = "Y";
        model.setValue("/root/HideData/condition/cond7", "Y");

        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

        if (sTextImage == "T") { //
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Grid.function", "clear");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
            }
        } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") {
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
            }
        } else if (sTextImage == "P") {
            if (fGetIP_xfm() != "MRR_GRGeomSaek_ReportView") { //시점문제로 앞으로 가져옴
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
            }
        }

        if (!fSelect()) return; // 등록번호 Validation 체크
        if (!fSelect2()) return; // 등록번호 Validation 체크
        fGetDept();
        model.setFocus("input_idno");
    } else if (pControlID == "button_nurse") { //********** 검색대상자조회(간호)
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.setValue("/root/HideData/condition/cond1", "nurse");
        sMRR_GRGeomSaek_DefaultCase = "case_1";
        sMRR_GRGeomSaek_ImageView = "Y";
        model.setValue("/root/HideData/condition/cond7", "Y");

        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

        // temp, appended by OBH on 2008.03.05 for 간호기록검색 프로그램수정
        //TFGetMsgBox(-1, "sTextImage = " + sTextImage, "확인", "I", "OK");
        // end append

        if (sTextImage == "T") { //
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") { //시점문제로 앞으로 가져옴
                // temp, appended by OBH on 2008.03.05 for 간호기록검색 프로그램수정
                //TFGetMsgBox(-1, "fGetIP_xfm = " + fGetIP_xfm(), "확인", "I", "OK");
                //end append

                model.setAttribute("MRR_GRGeomSaek_Grid.function", "clear");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
            }
        } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") {
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
            }
        } else if (sTextImage == "P") {
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
        }

        if (!fSelect()) return; // 등록번호 Validation 체크

        // appended by OBH on 2008.04.10 for 여기를 막아봅시당.......
        //TFGetMsgBox(-1, "라인 878", "확인", "I", "OK");
        if (!fSelect2()) return; // 등록번호 Validation 체크
        fGetDept();
        model.setFocus("input_idno");
    } else if (pControlID == "button_order1") { //********** 검색대상자조회(오더-일자별)
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.setValue("/root/HideData/condition/cond1", "order1");
        sMRR_GRGeomSaek_DefaultCase = "case_1";
        sMRR_GRGeomSaek_ImageView = "N";
        model.setValue("/root/HideData/condition/cond7", "N");

        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Clear.xfm","replace");
        if (!fSelect()) return; // 등록번호 Validation 체크
        if (!fSelect2()) return; // 등록번호 Validation 체크
        fGetDept();
        model.setFocus("input_idno");
    } else if (pControlID == "button_order2") { //********** 검색대상자조회(오더-부서별)
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.setValue("/root/HideData/condition/cond1", "order2");
        sMRR_GRGeomSaek_DefaultCase = "case_1";
        sMRR_GRGeomSaek_ImageView = "N";
        model.setValue("/root/HideData/condition/cond7", "N");

        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Clear.xfm","replace");
        if (!fSelect()) return; // 등록번호 Validation 체크
        if (!fSelect2()) return; // 등록번호 Validation 체크
        fGetDept();
        model.setFocus("input_idno");
    } else if (pControlID == "button_rslt") { //********** 검색대상조회(검사)
        // appended by OBH on 2008.04.03
        //TFGetMsgBox(-1, "아니면 여기가 문제일 까요????????? line 907", "확인", "I", "OK");
		model.trace("[KIS] check process:1");
		
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.visible("button_rslt_result", "false"); //검사별
        model.setValue("/root/HideData/condition/cond1", "rslt");
        sMRR_GRGeomSaek_DefaultCase = "case_2";
        model.toggle("result_case1");
        sMRR_GRGeomSaek_ImageView = "Y";
        model.setValue("/root/HideData/condition/cond7", "Y");
        model.removenodeset("/root/MainData/hist");   model.gridrebuild("grid_list");
		
		model.trace("[KIS] check process:2");
        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분
        if (sTextImage == "T") { //
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Result.kind", "clear");
				model.trace("[KIS] body_girokgeomsaek_Right loadURI 2");
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm", "replace");
            }
        } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") {
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
            }
        } else if (sTextImage == "P") {
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
        }
		
		model.trace("[KIS] check process:3");
        if (!fSelect()) return; // 등록번호 Validation 체크
		model.trace("[KIS] check process:3-0");
        if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") { //이미지 조회인 경우
            fDeleteFile("RESULT_");
            sMRR_GRGeomSaek_FileCnt = 0;
        }
		model.trace("[KIS] check process:3-1");
        var ctrl = model.control("button_rslt_date");
        ctrl.selected = "true";
        ctrl.refresh();
        model.trace("[KIS] check process:3-2");
        if (!fGetGyeolGwaList(gOption, "1")) {//조회 기간이 2년을 넘을 경우 자동 return
            return;
        }
        
		model.trace("[KIS] check process:4");
        var sTempGSGridCnt = TFGetGridCount("grid_list");
        if (sTempGSGridCnt > 10) {
            //2007-07-04 1:23오후 해당 기간안의 모든 검사를 조회하는 부분 선택 처리함(기간이 길어질 수록 오래 걸리는 문제 해결)
            // modified by OBH on 2009.04.03, 메시지를 물어 보지 않는다. 박진희 주임 요구사항.
            //if (model.messageBox ("모든 세부내역을 조회 하려면 '예'를 ... 단, 검사내역이 많을 경우 조회시간이 길어질 수 있습니다.", "선택", 1) != 1) {                             //재확인
            //    return;
            //}
        } else if (sTempGSGridCnt == 0) {
            return;
        }

        if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") {
            model.setAttribute("MRR_GRGeomSaek_Result.kind", "date"); //일자별 검사결과조회
			model.trace("[KIS] body_girokgeomsaek_Right loadURI 10");			
            model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm", "replace");
        } else {
            var emr_Right = TFGetModel("body_girokgeomsaek_Right", "Y");
            if (emr_Right) {
                emr_Right.removenodeset("/root/MainData/result");
                emr_Right.gridrebuild("grid_result");
                var gPid = model.getValue("/root/MainData/condition/cond1");
                var gUserId = TFGetSessionInfo("userid");
                var sGsFromdd = model.getValue("/root/MainData/condition/frdt"); //2007.07.04 김태범 추가
                var sGsTodd   = model.getValue("/root/MainData/condition/todt"); //2007.07.04  김태범 추가

                // 의사확인 출력여부 설정
                var sPrint = "0";
                //if (sTextImage != "T") sPrint = "0";
				
				return; // 일단 주석!
				
                fWaitBegin("검사결과 조회중...*");
                emr_Right.javascript.fGetResult(1, 0, "1", gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd, sLanguage, sPrint);
                fWaitEnd();

                if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I") { //이미지 조회인 경우
                    fWriteGyeolGwa();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
                } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "C") { //이미지 조회인 경우
                    fWriteGyeolGwa2();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
                }
            } else {
                //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
                return;
            }
        }
    } else if (pControlID == "button_rslt_date") { //********** 검색대상조회(검사)
        g_TotalTestResultTab = "1";
        model.visible("button_rslt_result", "false"); //검사별
        model.toggle("result_case1");
        model.removenodeset("/root/MainData/hist");   model.gridrebuild("grid_list");

        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

        if (sTextImage == "T") { //
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Result.kind", "clear");
				model.trace("[KIS] body_girokgeomsaek_Right loadURI 11");				
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm", "replace");
            }
        } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") {
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
            }
        } else if (sTextImage == "P") {
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
        }

        if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") { //이미지 조회인 경우
            fDeleteFile("RESULT_");
            sMRR_GRGeomSaek_FileCnt = 0;
        }

        //2007-07-04 1:23오후 해당 기간안의 모든 검사를 조회하는 부분 선택 처리함(기간이 길어질 수록 오래 걸리는 문제 해결) - 김태범
        if (!fGetGyeolGwaList(gOption, "1")) {//조회 기간이 2년을 넘을 경우 자동 return
            return;
        }

        var sTempGSGridCnt = TFGetGridCount("grid_list");
        if (sTempGSGridCnt > 10) {
            //2007-07-04 1:23오후 해당 기간안의 모든 검사를 조회하는 부분 선택 처리함(기간이 길어질 수록 오래 걸리는 문제 해결)
            //modified by OBH on 2009.04.03,출력메시지는 막는다. 박진희주임.
            //if (model.messageBox ("모든 일자별-세부내역을 조회 하려면 '예'를 ... 단, 검사내역이 많을 경우 조회시간이 길어질 수 있습니다.", "선택", 1) != 1) {                             //재확인
            //    return;
            //}
        } else if (sTempGSGridCnt == 0) {
            return;
        }

        if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") {
            model.setAttribute("MRR_GRGeomSaek_Result.kind", "date"); //일자별 검사결과조회
			model.trace("[KIS] body_girokgeomsaek_Right loadURI 12");			
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm","replace");
        } else {
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                emr_Right.removenodeset("/root/MainData/result"); emr_Right.gridrebuild("grid_result");
                var gPid = model.getValue("/root/MainData/condition/cond1");
                var gUserId = TFGetSessionInfo("userid");
                var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
                var sGsTodd   = model.getValue("/root/MainData/condition/todt");

                // 의사확인 출력여부 설정
                var sPrint = "0";
                //if (sTextImage != "T") sPrint = "0";

                fWaitBegin("검사결과 조회중...**");
                emr_Right.javascript.fGetResult(1, 0, "1", gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd, sLanguage, sPrint);
                fWaitEnd();

                if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I") { //이미지 조회인 경우
                    fWriteGyeolGwa();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "C") { //이미지 조회인 경우
                    fWriteGyeolGwa2();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                }
            } else {
                //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
                return;
            }
        }
    } else if (pControlID == "button_rslt_dept") { //********** 검색대상조회(검사부서별)
        g_TotalTestResultTab = "2";
        model.visible("button_rslt_result", "true"); //검사별
        model.toggle("result_case1");

        model.removenodeset("/root/MainData/hist");   model.gridrebuild("grid_list");
        //model.removenodeset("/root/MainData/result"); model.gridrebuild("grid_result");

        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

        if (sTextImage == "T") { //
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Result.kind", "clear");
				model.trace("[KIS] body_girokgeomsaek_Right loadURI 3");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm","replace");
            }
        } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") {
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
            }
        } else if (sTextImage == "P") {
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
        }

        if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") { //이미지 조회인 경우
            fDeleteFile("RESULT_");
            sMRR_GRGeomSaek_FileCnt = 0;
        }
        //기록검색 왼쪽 검사부서별 리스트 조회
        //2007-07-04 1:23오후 해당 기간안의 모든 검사를 조회하는 부분 선택 처리함(기간이 길어질 수록 오래 걸리는 문제 해결) - 김태범
        if (!fGetGyeolGwaList(gOption, "2")) {//조회 기간이 2년을 넘을 경우 자동 return
            return;
        }

        var sTempGSGridCnt = TFGetGridCount("grid_list");
        if (sTempGSGridCnt > 10) {
            //modified by OBH on 2009.04.03,출력메시지는 막는다. 박진희주임.
            //2007-07-04 1:23오후 해당 기간안의 모든 검사를 조회하는 부분 선택 처리함(기간이 길어질 수록 오래 걸리는 문제 해결)
            //if (model.messageBox ("모든 검사부서별-세부내역을 조회 하려면 '예'를 ... 단, 검사내역이 많을 경우 조회시간이 길어질 수 있습니다.", "선택", 1) != 1) {                             //재확인
            //    return;
            //}
        } else if (sTempGSGridCnt == 0) {
            return;
        }

        if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") {
            model.setAttribute("MRR_GRGeomSaek_Result.kind", "dept"); //과별검사결과 조회
			model.trace("[KIS] body_girokgeomsaek_Right loadURI 4");
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm","replace");
        } else {
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                emr_Right.removenodeset("/root/MainData/result"); emr_Right.gridrebuild("grid_result");
                var gPid = model.getValue("/root/MainData/condition/cond1");
                var gUserId = TFGetSessionInfo("userid");
                var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
                var sGsTodd   = model.getValue("/root/MainData/condition/todt");

                // 의사확인 출력여부 설정
                var sPrint = "0";
                //if (sTextImage != "T") sPrint = "0";

                fWaitBegin("검사결과 조회중...***");
                emr_Right.javascript.fGetResult(1, 0, "2", gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd, sLanguage, sPrint);
                fWaitEnd();

                if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I") { //이미지 조회인 경우
                    fWriteGyeolGwa();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "C") { //이미지 조회인 경우
                    fWriteGyeolGwa2();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                }
            } else {
                //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
                return;
            }
        }
    } else if (pControlID == "button_rslt_result") { //********** 검색대상조회(검사별)
        model.toggle("result_case1");
        model.removenodeset("/root/MainData/hist");   model.gridrebuild("grid_list");

        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

        if (sTextImage == "T") { //
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Result.kind", "clear");
				model.trace("[KIS] body_girokgeomsaek_Right loadURI 5");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm","replace");
            }
        } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") {
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
            }
        } else if (sTextImage == "P") {
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
        }

        if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") { //이미지 조회인 경우
            fDeleteFile("RESULT_");
            sMRR_GRGeomSaek_FileCnt = 0;
        }
        fGetGyeolGwaList(gOption, "3");

        if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") {
            model.setAttribute("MRR_GRGeomSaek_Result.kind", "result"); //검사별검사결과 조회
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm","replace");
        } else {
            //2006-08-16 1:51오후 get Model 관련 수정 김태범
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                emr_Right.removenodeset("/root/MainData/result"); emr_Right.gridrebuild("grid_result");
                var gPid = model.getValue("/root/MainData/condition/cond1");
                var gUserId = TFGetSessionInfo("userid");
                var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
                var sGsTodd   = model.getValue("/root/MainData/condition/todt");

                // 의사확인 출력여부 설정
                var sPrint = "0";
                //if (sTextImage != "T") sPrint = "0";

                fWaitBegin("검사결과 조회중...****");
                emr_Right.javascript.fGetResult(1, 0, "3", gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd, sLanguage, sPrint);
                fWaitEnd();

                if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I") { //이미지 조회인 경우
                    fWriteGyeolGwa();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "C") { //이미지 조회인 경우
                    fWriteGyeolGwa2();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                }
            } else {
                //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
                return;
            }
        }
    } else if (pControlID == "button_rslt_slip") { //********** 검색대상조회(검사별)
        //**********************************************************************************************************************
        g_TotalTestResultTab = "3";
        //alert("slip별 조회")
        //TFGetMsgBox(-1, "slip별 조회", "확인", "I", "OK");
        model.visible("button_rslt_result", "false"); //검사별
        model.toggle("result_case1");
        model.removenodeset("/root/MainData/hist");   model.gridrebuild("grid_list");

        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

        if (sTextImage == "T") { //
            //TFGetMsgBox(-1, "slip별 조회11111", "확인", "I", "OK");
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Result.kind", "clear");
				model.trace("[KIS] body_girokgeomsaek_Right loadURI 6");
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm", "replace");
            }
        } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") {
            //TFGetMsgBox(-1, "slip별 조회22222", "확인", "I", "OK");
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
                //TFGetMsgBox(-1, "slip별 조회22222-11111", "확인", "I", "OK");
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
            }
        } else if (sTextImage == "P") {
            //TFGetMsgBox(-1, "slip별 조회33333", "확인", "I", "OK");
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
        }

        if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") { //이미지 조회인 경우
            //TFGetMsgBox(-1, "slip별 조회44444", "확인", "I", "OK");
            fDeleteFile("RESULT_");
            sMRR_GRGeomSaek_FileCnt = 0;
        }

        //2007-07-04 1:23오후 해당 기간안의 모든 검사를 조회하는 부분 선택 처리함(기간이 길어질 수록 오래 걸리는 문제 해결) - 김태범
        // if (!fGetGyeolGwaList(gOption, "1")) {//조회 기간이 2년을 넘을 경우 자동 return
        //if (!fGetGyeolGwaList(gOption, "2")) {//조회 기간이 2년을 넘을 경우 자동 return
        if (!fGetGyeolGwaList(gOption, "3")) {//조회 기간이 2년을 넘을 경우 자동 return
            return;
        }

        var sTempGSGridCnt = TFGetGridCount("grid_list");
        if (sTempGSGridCnt > 10) {
            //modified by OBH on 2009.04.03,출력메시지는 막는다. 박진희주임.
            //2007-07-04 1:23오후 해당 기간안의 모든 검사를 조회하는 부분 선택 처리함(기간이 길어질 수록 오래 걸리는 문제 해결)
            //if (model.messageBox ("모든 slip별-세부내역을 조회 하려면 '예'를 ... 단, 검사내역이 많을 경우 조회시간이 길어질 수 있습니다.", "선택", 1) != 1) {                             //재확인
            //    return;
            //}
        } else if (sTempGSGridCnt == 0) {
            return;
        }

        if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") {
            //TFGetMsgBox(-1, "slip별 조회---------------------------------->>>>>>>>>>>>>>>>>>>>>>11111", "확인", "I", "OK");
            // OBH wrote, 2008.03.11
            // 분명 여기만 바뀐것이 아니라 MRR_GRGeomSaek_Result.xfm의 init... 부분에 추가가 있었을것으로 생각되는데 뭐가 변경된것인지 모르겠슴.
            // 우선 아래의 "slip"을 "date"로 변겅.........
            model.setAttribute("MRR_GRGeomSaek_Result.kind", "date"); //일자별 검사결과조회
            //model.setAttribute("MRR_GRGeomSaek_Result.kind", "slip"); //일자별 검사결과조회
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm","replace");
            // 0313
            //TFGetMsgBox(-1, "slip별 조회 5555555555", "확인", "I", "OK");
            //fClicked_gridList_OBH(gOption);
        } else {
            //TFGetMsgBox(-1, "slip별 조회---------------------------------->>>>>>>>>>>>>>>>>>>>>>2222222222", "확인", "I", "OK");
			model.trace("[KIS] click Process 1");
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
				model.trace("[KIS] click Process 2");
                emr_Right.removenodeset("/root/MainData/result"); emr_Right.gridrebuild("grid_result");
                var gPid = model.getValue("/root/MainData/condition/cond1");
                var gUserId = TFGetSessionInfo("userid");
                var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
                var sGsTodd   = model.getValue("/root/MainData/condition/todt");

                // 의사확인 출력여부 설정
                var sPrint = "0";
                //if (sTextImage != "T") sPrint = "0";				
                fWaitBegin("검사결과 조회중...*****");
                //TFGetMsgBox(-1, "gOption =: "+gOption, "확인", "I", "OK");
                //emr_Right.javascript.fGetResult(1, 0, "1", gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd);
                //emr_Right.javascript.fGetResult(1, 0, "2", gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd);
                //emr_Right.javascript.fGetResult(1, 0, "3", gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd);				
                emr_Right.javascript.fGetResult(0, 1, "3", gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd, sLanguage, sPrint);
				
                fWaitEnd();

                //0327
                //TFGetMsgBox(-1, "slip별 조회 666666---1111111111111111111111111111111111", "확인", "I", "OK");
                //fClicked_gridList_OBH(gOption);

                if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I") { //이미지 조회인 경우
                    fWriteGyeolGwa();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                    //0327
                    //TFGetMsgBox(-1, "slip별 조회 666666", "확인", "I", "OK");
                    //fClicked_gridList_OBH(gOption);
                } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "C") { //이미지 조회인 경우
                    fWriteGyeolGwa2();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                    //0327
                    //TFGetMsgBox(-1, "slip별 조회 777777", "확인", "I", "OK");
                    //fClicked_gridList_OBH(gOption);
                }
            } else {
                //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
                return;
            }
        }

        //***********************************************************************************************************************
    } else if (pControlID == "button_rslt_silsi") { //********** 검색대상조회(검사 실시내역)
        model.toggle("result_case2");
        fGetSilSi(1);
    } else if (pControlID == "button_rslt_process") { //********** 검색대상조회(진행내역)
        model.toggle("result_case3");
        fGetJinHaeng();
    } else if (pControlID == "button_rslt_silsi_date") { //********** 검색대상조회(검사 실시내역-일자별)
        fGetSilSi(1);
    } else if (pControlID == "button_rslt_silsi_dept") { //********** 검색대상조회(검사 실시내역-과별)
        fGetSilSi(2);
    } else if (pControlID == "button_rslt_silsi_pacs") { //********** 검색대상조회(검사 실시내역-PACS)
        var row = 0;
        var pacsno_list = "";

        row = TFGetGridPos("grid_silsi");
        if (row <= 0) {
            model.alert("선택된 내역이 없습니다.");
            return;
        }

        pacsno_list = model.getValue("/root/MainData/silsi/list[" + row + "]/accession_no");
        if (pacsno_list == "") {
            model.alert("선택된 PACS 검사가 없습니다.");
            return;
        }
        var sIdno = model.getValue("/root/MainData/condition/cond1"); //등록번호
        TFPACSExecute(sIdno, pacsno_list); // PACS Program 호출

    } else if (pControlID == "button_rslt_silsi_ocr") { //********** 검색대상조회(검사 실시내역-OCR)
        var row = TFGetGridPos("grid_silsi");
        if (row <= 0) {
            model.alert("보고자하는 항목을 선택해 주십시요.");
            return;
        }

        var acptdd = model.getValue("/root/MainData/silsi/list[" + row + "]/acptdd");
        var suppdept = model.getValue("/root/MainData/silsi/list[" + row + "]/suppdept");
        var acptno = model.getValue("/root/MainData/silsi/list[" + row + "]/acptno");
        var acptseqno = model.getValue("/root/MainData/silsi/list[" + row + "]/acptseqno");

        var receiveno = acptdd + suppdept + acptno.lvGetZeroString(5) + acptseqno.lvGetZeroString(3);
        var sIdno = model.getValue("/root/MainData/condition/cond1"); //등록번호
        model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fResultCall_2");
        model.setAttribute("MRR_GRGeomSaek_Ocr.sData", receiveno);
        model.setAttribute("MRR_GRGeomSaek_Ocr.sPid", sIdno); //20060218추가
        TFshowModal(null, "ResultPopup"+receiveno, "2", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "-", 890, 950, 0, 0, false, false);
    } else if (pControlID == "button_rslt_silsi_ocr2") { //********** 검색대상조회(검사 실시내역-OCR)
        var row = TFGetGridPos("grid_silsi");
        if (row <= 0) {
            model.alert("보고자하는 항목을 선택해 주십시요.");
            return;
        }

        var acptdd = model.getValue("/root/MainData/silsi/list[" + row + "]/acptdd");
        var suppdept = model.getValue("/root/MainData/silsi/list[" + row + "]/suppdept");
        var acptno = model.getValue("/root/MainData/silsi/list[" + row + "]/acptno");
        var acptseqno = model.getValue("/root/MainData/silsi/list[" + row + "]/acptseqno");

        var receiveno = acptdd + suppdept + acptno.lvGetZeroString(5) + acptseqno.lvGetZeroString(3);
        var pid = model.getValue("/root/MainData/condition/cond1"); //등록번호
        var userid = TFGetSessionInfo("userid");
        
        fCommonShowKisInterface2("OCR,"+ userid + "," + pid + "," + receiveno + ",Y"); 
        
    } else if (pControlID == "button_rslt_cath") { //********** 검색대상조회(검사 실시내역-CATH)
        var row = TFGetGridPos("grid_silsi");
        if (row <= 0) {
            model.alert("선택된 내역이 없습니다.");
            return;
        }

        var gPid = model.getValue("/root/MainData/condition/cond1");
        var pacsno = model.getValue("/root/MainData/silsi/list[" + row + "]/accession_no");

        //model.setAttribute("MRR_GRGeomSaek_CatchViewModal_param",gPid+"▦"+pacsno+"▦"+"MRR_GRGeomSaek.js");
        //TFshowModal(null, "WaitPopup", "2", "/emr/girokweb/xfm/MRR_GRGeomSaek_CatchViewModal.xfm", "-",1080, 830, 0, 0, false, false, false);

		var arg_str = " -O" + pacsno + " -AWorkServer -Wnexus -Dnexus";
		//model.executeProgramEx("", "C:\\Program Files\\his\\NexusX\\NexusX.exe", arg_str);
		var m_ver = model.getBranchName();
		if (m_ver == "3.0_KunDae") {
    		model.executeProgramEx("", "C:\\Program Files\\his\\NexusX\\NexusX.exe", arg_str);
		} else {
			model.executeProgramEx("C:\\Program Files\\his\\NexusX\\NexusX.exe", arg_str);
		}
	} else if (pControlID == "button_rslt_cath_infinitt") { //2014.03.26
        var row = TFGetGridPos("grid_silsi");
        if (row <= 0) {
            model.alert("선택된 내역이 없습니다.");
            return;
        }

        var gPid = model.getValue("/root/MainData/condition/cond1");
        var pacsno = model.getValue("/root/MainData/silsi/list[" + row + "]/accession_no");

		//model.showModal("text/html", "http://10.10.150.185/pkg_pacs/external_interface.aspx?MODE=CPACS&LID=admin&LPW=nimda&AN=" + pacsno, 205, 105, 684, 748, true, true);
		fCommonShowCATH_infinitt(pacsno);

    } else if (pControlID == "button_rslt_silsi_fluoro") { //2017.08.02
        var row = TFGetGridPos("grid_silsi");
        if (row <= 0) {
            model.alert("선택된 내역이 없습니다.");
            return;
        }

        var gPid = model.getValue("/root/MainData/condition/cond1");
        var pacsno = model.getValue("/root/MainData/silsi/list[" + row + "]/accession_no");

		if(pacsno == "" || pacsno.length <= 8) {
			model.alert("검사정보가 없습니다.(pacsno)");
			return;
		}
		
		//Fluoro Path 구하기
		var datapath ="";
		
		model.setValue("/root/SendData/Data", pacsno);
		model.setValue("/root/SendData/Mode", "reqGetFluoroDataPath");

		if (model.submitInstance("getFluoroDataPath") == true) 
		{
			datapath = model.getValue("/root/HideData/fluoro/datapath");
			
			if (datapath != ""){
				fCommonShowFluoro(datapath); 
			} else {
				alert("영상 데이터가 없습니다");
			}
		}

		TFSetMessage("/root/HideData/fluoro"); 

    } else if (pControlID == "button_pm") { //********** 검색대상자조회(제증명)
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        //20061018 해당 제증명화면을 open하는 것으로 변경.
        var sSendPid = model.getValue("/root/MainData/condition/cond1");
        if (sSendPid == "" || sSendPid == null) {
            return;
        } else {
			fOCS_TotalEMRView2(sSendPid);
			/*
            model.setAttribute("MRR_GRGeomSaek_param","GRGeomSaek▦"+sSendPid);
            var tempEmr_Popup = TFGetModel("OPM_JeJeungMyeongGwanRi","N");
            if (tempEmr_Popup == null) {
                model.showModeless("OPM_JeJeungMyeongGwanRi", "/pam/weonmuweb/xfm/OPM_JeJeungMyeongGwanRi.xfm", 50, 50, 1220, 800, false, false);
            } else {
                tempEmr_Popup.javascript.fInitJejeungMyoung();
            }
			*/
        }
    } else if (pControlID == "button_acc") { //********** 검색대상자조회(누적)
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.setValue("/root/HideData/condition/cond1", "acc");
        sMRR_GRGeomSaek_DefaultCase = "case_1";
        sMRR_GRGeomSaek_ImageView = "N";
        model.setValue("/root/HideData/condition/cond7", "N");
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Clear.xfm","replace");

        if (!fSelect()) return; // 등록번호 Validation 체크
        if (!fSelect2()) return; // 등록번호 Validation 체크
        fGetDept();
        model.setFocus("input_idno");
    } else if (pControlID == "button_ocr") { //********** 검색대상자조회(기타)
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.setValue("/root/HideData/condition/cond1", "ocr");
        sMRR_GRGeomSaek_DefaultCase = "case_1";
        sMRR_GRGeomSaek_ImageView = "N";
        model.setValue("/root/HideData/condition/cond7", "N");

        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분
        if (sTextImage == "T") { //
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Grid.function", "clear");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
            }
        } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") {
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
            }
        }
        if (!fSelect()) return; // 등록번호 Validation 체크
        if (!fSelect2()) return; // 등록번호 Validation 체크
        fGetDept();
        model.setFocus("input_idno");
    } else if (pControlID == "button_drug") { //********** 투약정보
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.setValue("/root/HideData/condition/cond1", "drug");
        sMRR_GRGeomSaek_DefaultCase = "case_3";
        sMRR_GRGeomSaek_ImageView = "N";
        model.setValue("/root/HideData/condition/cond7", "N");
		    //model.setValue("/root/SendData/psychk", model.getValue("/root/MainData/limit/grcheck"));
        model.setAttribute("MRR_GRGeomSaek_PSYChk", model.getValue("/root/MainData/limit/grcheck"));
        //TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Clear.xfm","replace");

        if (!fSelect()) return; // 등록번호 Validation 체크
        fSelectDrug("A");
    } else if (pControlID == "button_drug2") { //********** 투약기록
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)

        var sPid  = model.getValue("/root/MainData/condition/cond1");    //altered by taja78 start... 2006.02.13
        var sFrdt = model.getValue("/root/MainData/condition/frdt");
        var sTodt = model.getValue("/root/MainData/condition/todt");

		model.setAttribute("MRR_GRGeomSaek_PSYChk", model.getValue("/root/MainData/limit/grcheck"));

        if (sPid != "") {    //등록번호 입력시 해당환자의 진료정보 조회
            TFclearNodeValue("/root/SendData");
            model.setValue("/root/SendData/Mode", "reqGetTwoYakGiRokJinRyoJeongBo");
            model.setValue("/root/SendData/Data1",sPid);
            model.setValue("/root/SendData/Data2",sFrdt);
            model.setValue("/root/SendData/Data3",sTodt);
			//model.setValue("/root/SendData/psychk",model.getValue("/root/MainData/limit/grcheck"));
            if (model.submitInstance("MRR_reqGetTwoYakGiRokJinRyoJeongBo")) {
                if (TFGetGridCount("grid2") > 0) {
                    fSellMergeReft("twgirok_jinryo", "grid2");
                }
            }

        }
        model.setValue("/root/HideData/condition/cond1", "drug");
        sMRR_GRGeomSaek_DefaultCase = "case_5";
        sMRR_GRGeomSaek_ImageView = "N";
        model.setValue("/root/HideData/condition/cond7", "N");
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Clear.xfm","replace");

        if (!fSelect()) return; // 등록번호 Validation 체크
        fSelectDrug2("A");
    } else if (pControlID == "button_copy") { //********** 복사신청
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.setValue("/root/HideData/condition/cond1", "copy");
        sMRR_GRGeomSaek_DefaultCase = "case_4";
        sMRR_GRGeomSaek_ImageView = "Y";
        model.setValue("/root/HideData/condition/cond6", "I"); //Text, Image 조회구분
        model.setValue("/root/HideData/condition/cond7", "Y");
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Clear.xfm","replace");

        if (!fSelect()) return; // 등록번호 Validation 체크
        fSelectCopy();

        model.removeNodeset("/root/HideData/combodept");
        model.resetInstanceNode("/root/SendData");
        model.setValue("/root/SendData/Mode", "reqGetComboCOMCDEPTM");
        model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1")); //등록번호
        model.setValue("/root/SendData/Data2", model.getValue("/root/MainData/condition/frdt")); //시작일자
        model.setValue("/root/SendData/Data3", model.getValue("/root/MainData/condition/todt")); //종료일자
        model.setValue("/root/SendData/Data4", model.getValue("/root/HideData/condition/cond1")); //일자별,서식지별...
        model.setValue("/root/SendData/Data5", model.getValue("/root/HideData/condition/cond2")); //전체,외래,입원,응급
        model.setValue("/root/SendData/Data6", "girok"); //전체과 가져오는 구분

        //처음에는 "전체" 한 data만 추가하고 조회시 해당과만 insert해달라고 요청(20050602)
        if (model.submitInstance("getComboCommon")) { //사용자부서를 가져온다.
            //TFSetMessage("/root/HideData/combodept");
        } else {
        }
        TFInsertInstance("/root/HideData/combodept",  "combodept", "deptcd,deptnm,sddeptnm,deptlevl,updeptcd,tempstat,tempdate", "%,전체,전체,%,%,-,"+model.getValue("/root/MainData/condition" + "/todt"));
        model.setValue("/root/MainData/condition/cond2", "%");
        model.refresh();
    } else if (pControlID == "button_drug_help") { //********** 투약정보 help
        var sArg = "2▦-▦A▦M▦%▦%▦D▦00000000▦O▦";
        var sCode = model.getValue("/root/HideData/condition/cond4");
        model.setAttribute("sMRR_GRGeomSaek_DrugHelp_parm", sArg);
        if (sCode != "") {
            model.setAttribute("sMRR_GRGeomSaek_DrugHelp_code", sCode);
        }
        TFshowModal(null, null, "1", "/emr/girokweb/xfm/MRR_GRGeomSaek_DrugHelp.xfm", "-", 965, 760, 0, 0, false, false);
        //model.setValue("/root/HideData/condition/cond4", model.getAttribute("sMRR_GRGeomSaek_DrugHelp_code"));
        //model.setValue("/root/HideData/condition/cond5", model.getAttribute("sMRR_GRGeomSaek_DrugHelp_cdnm"));
        var i = TFGridInsertLast("grid5");
        model.setValue("/root/HideData/druggirok_durg1["+i+"]/drugcd",model.getAttribute("sMRR_GRGeomSaek_DrugHelp_code"));
        model.setValue("/root/HideData/druggirok_durg1["+i+"]/drugnm",model.getAttribute("sMRR_GRGeomSaek_DrugHelp_cdnm"));
        model.gridRebuild("grid5");
        model.setAttribute("sMRR_GRGeomSaek_DrugHelp_parm", "");
        model.setAttribute("sMRR_GRGeomSaek_DrugHelp_code", "");
        model.setAttribute("sMRR_GRGeomSaek_DrugHelp_cdnm", "");
        model.refresh();
        fSelectDrug("BB");
    //투약기록 약품코드 헬프 추가  2005-09-14 changed by taja78
    } else if (pControlID == "button_drug_help1") { //********** 투약정보 help
        var sDrugFlag = model.getValue("/root/HideData/condition/cond8"); // 약/주사/수혈구분
        if (sDrugFlag == "20") { //수혈인 경우
            var sArg = "2▦-▦A▦B▦%▦%▦D▦00000000▦O▦";
        } else {
            var sArg = "2▦-▦A▦M▦%▦%▦D▦00000000▦O▦";
        }

        var sCode = model.getValue("/root/HideData/condition/cond11");
        model.setAttribute("sMRR_GRGeomSaek_DrugHelp_parm", sArg);
        if (sCode != "") {
            model.setAttribute("sMRR_GRGeomSaek_DrugHelp_code", sCode);
        }
        TFshowModal(null, null, "1", "/emr/girokweb/xfm/MRR_GRGeomSaek_DrugHelp.xfm", "-", 965, 760, 0, 0, false, false);
        //2005-10-01 약품코드를 여러개 조회할수 있도록 변경
        if (model.getAttribute("sMRR_GRGeomSaek_DrugHelp_code") !="" && model.getAttribute("sMRR_GRGeomSaek_DrugHelp_cdnm") !="") {
            var i = TFGridInsertLast("grid1");
            model.setValue("/root/HideData/druggirok_durg["+i+"]/drugcd",model.getAttribute("sMRR_GRGeomSaek_DrugHelp_code"));
            model.setValue("/root/HideData/druggirok_durg["+i+"]/drugnm",model.getAttribute("sMRR_GRGeomSaek_DrugHelp_cdnm"));
            model.gridRebuild("grid1");
            model.setAttribute("sMRR_GRGeomSaek_DrugHelp_parm", "");
            model.setAttribute("sMRR_GRGeomSaek_DrugHelp_code", "");
            model.setAttribute("sMRR_GRGeomSaek_DrugHelp_cdnm", "");
            model.refresh();
            fSelectDrug2("BB");
        }
    } else if (pControlID == "button_copylist") { //********** 신청내역조회
        var emr_Right = null;
        if (fGetIP_xfm() != "MRR_GRGeomSaek_Copy") {
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_CopyList.xfm","replace");
            emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                model.setAttribute("sMRR_GRGeomSaekCopy_doct", TFGetSessionInfo("userid"));
                emr_Right.javascript.fCopy_Open();
                model.setAttribute("sMRR_GRGeomSaekCopy_doct", "");
            } else {
                return;
            }
        } else {
            emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                model.setAttribute("sMRR_GRGeomSaekCopy_doct", TFGetSessionInfo("userid"));
                emr_Right.javascript.fCopy_Open();
                model.setAttribute("sMRR_GRGeomSaekCopy_doct", "");
            } else {
                return;
            }
        }
    } else if (pControlID == "button_copyinput") { //********** 복사신청입력
        model.setAttribute("sMRR_GRGeomSaek_usyn", "N");
        model.setAttribute("sMRR_GRGeomSaekCopy_date", "-");
        model.setAttribute("sMRR_GRGeomSaekCopy_time", "-");
        model.setAttribute("sMRR_GRGeomSaekCopy_idno", model.getValue("/root/MainData/condition/cond1"));
        model.setAttribute("sMRR_GRGeomSaekCopy_idnonm", model.getValue("/root/MainData/opmcptbsm/name"));
        model.setAttribute("sMRR_GRGeomSaekCopy_dept", "-");
        model.setAttribute("sMRR_GRGeomSaekCopy_doct", "-");
        model.setAttribute("sMRR_GRGeomSaekCopy_doctnm", "-");
        model.setAttribute("sMRR_GRGeomSaekCopy_regno1", model.getValue("/root/MainData/opmcptbsm/regno1"));
        model.setAttribute("sMRR_GRGeomSaekCopy_regno2", model.getValue("/root/MainData/opmcptbsm/regno2"));
        model.setAttribute("sMRR_GRGeomSaekCopy_zipcd1", model.getValue("/root/MainData/opmcptbsm/zipcd1"));
        model.setAttribute("sMRR_GRGeomSaekCopy_zipcd2", model.getValue("/root/MainData/opmcptbsm/zipcd2"));
        model.setAttribute("sMRR_GRGeomSaekCopy_zipcdseqno", model.getValue("/root/MainData/opmcptbsm/zipcdseqno"));
        model.setAttribute("sMRR_GRGeomSaekCopy_firstaddr", model.getValue("/root/MainData/opmcptbsm/firstaddr"));
        model.setAttribute("sMRR_GRGeomSaekCopy_secaddr", model.getValue("/root/MainData/opmcptbsm/secaddr"));

        TFshowModal(null, null, "1", "/emr/girokweb/xfm/MRR_GRGeomSaek_CopyInput.xfm", "-", 696, 256, 0, 0, false, false);

        var sCopyUsyn = model.getAttribute("sMRR_GRGeomSaek_usyn");
        var sCopyIdno = model.getValue("/root/MainData/condition/cond1");
        var sCopyDate = model.getAttribute("sMRR_GRGeomSaekCopy_date");
        var sCopyTime = model.getAttribute("sMRR_GRGeomSaekCopy_time");
        var sCopyDept = model.getAttribute("sMRR_GRGeomSaekCopy_dept");
        var sCopyDoct = model.getAttribute("sMRR_GRGeomSaekCopy_doct");
        model.setAttribute("sMRR_GRGeomSaekCopy_date", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_time", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_idno", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_idnonm", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_dept", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_doct", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_doctnm", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_regno1", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_regno2", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_zipcd1", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_zipcd2", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_zipcdseqno", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_firstaddr", "");
        model.setAttribute("sMRR_GRGeomSaekCopy_secaddr", "");

        if (sCopyUsyn == "N") {
            return;
        } else {
            model.resetInstanceNode("/root/SendData");
            model.setValue("/root/SendData/Mode", "MRR_reqSetOprcchrah"); //복사신청입력

            var fg = model.vsGrid("grid_copy");
            for (var i = fg.FixedRows; i < fg.Rows; i++) {
                var sCopyLevl = model.getValue("/root/MainData/mrrfflshh_copy[" + i + "]/levl");
                if (sCopyLevl == "4" && ((model.getValue("/root/MainData/mrrfflshh_copy[" + i + "]/boolflag") == "TRUE") || (model.getValue("/root/MainData/mrrfflshh_copy[" + i + "]/boolflag") == "true"))) {
                    model.setValue("/root/MainData/mrrfflshh_copy[" + i + "]/tempflag", "I");
                    model.setValue("/root/MainData/mrrfflshh_copy[" + i + "]/copyidno", sCopyIdno);
                    model.setValue("/root/MainData/mrrfflshh_copy[" + i + "]/copydate", sCopyDate);
                    model.setValue("/root/MainData/mrrfflshh_copy[" + i + "]/copytime", sCopyTime);
                    model.setValue("/root/MainData/mrrfflshh_copy[" + i + "]/copydept", sCopyDept);
                } else {
                    model.setValue("/root/MainData/mrrfflshh_copy[" + i + "]/tempflag", "-");
                }
            }
            model.setValue("/root/SendData/Data1", TFNodeSendDataMake2("/root/MainData/mrrfflshh_copy", "tempflag"));

            //복사신청입력
            if (model.submitInstance("MRR_reqGetMrrfflshhCopy")) {
                TFSetMessage("/root/MainData/iudmsg");
            }

            var emr_Right = null;
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Copy") {
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_CopyList.xfm","replace");
                emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
                if (emr_Right) {
                    model.setAttribute("sMRR_GRGeomSaekCopy_doct", sCopyDoct);
                    emr_Right.javascript.fCopy_Open();
                    model.setAttribute("sMRR_GRGeomSaekCopy_doct", "");
                } else {
                    return;
                }
            } else {
                emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
                if (emr_Right) {
                    model.setAttribute("sMRR_GRGeomSaekCopy_doct", sCopyDoct);
                    emr_Right.javascript.fCopy_Open();
                    model.setAttribute("sMRR_GRGeomSaekCopy_doct", "");
                } else {
                    return;
                }
            }
        }
    } else if (pControlID == "grid_main") { //********** grid_main 선택(오른쪽화면에 display하는 루틴)		
        var iRow = TFGetGridPos("grid_main");		
        if (iRow > 0) {
            var fg      = model.vsGrid(pControlID);
            var sLevl   = model.getValue("/root/MainData/mrrfflshh[" + iRow + "]/levl");
            var sGubn   = model.getValue("/root/MainData/mrrfflshh[" + iRow + "]/chrtkind").substr(0,1);
            var sIofg   = model.getValue("/root/MainData/mrrfflshh[" + iRow + "]/seq").substr(0,1);
            var schrtnm = model.getValue("/root/MainData/mrrfflshh[" + iRow + "]/chrtnm");

                  //alert(model.getValue("/root/MainData/mrrfflshh[" + iRow + "]/chrtkind"));

            // temp, appended by OBH on 2008.02.27
            //TFGetMsgBox(-1, "sGubn = " + sGubn + " : " + "pControlID = " + pControlID + " : " + "iRow = " + iRow + "sLevl = " + sLevl, "확인", "I", "OK");
            // appended by OBH on 2009.08.10
            // 타과회신지 출력시 "NST"라는 헤드를 추가하기위한 변수처리.
            var schrtnm_split = schrtnm.split("/");
            //model.alert("schrtnm := " + schrtnm);
            //model.alert("schrtnm_split := " + schrtnm_split);
            //model.alert("AAAAAAAAAAAAAAAAAAAAA");
            if (schrtnm_split.length > 3) {
                //model.alert("length := " + schrtnm_split[3].length);
                if (schrtnm_split[3].length > 3) {
                    var schrtnm_title = schrtnm_split[3].substr(0,3);
                    if (schrtnm_title == "NST") {
                        model.setAttribute("MRR_GRGeomSaek_ReportView.right",schrtnm_title);
                        g_Nst_Right_head = schrtnm_title;
                        schrtnm_title = "";
                    } else {
                        model.setAttribute("MRR_GRGeomSaek_ReportView.right","");
                        g_Nst_Right_head = "";
                        schrtnm_title = "";
                    }
                }
            } else {
                model.setAttribute("MRR_GRGeomSaek_ReportView.right","");
                g_Nst_Right_head = "";
                schrtnm_title = "";
            }
            //model.alert("CCCCCCCCCCCCCCCCCCCCCCCC");
            //model.alert(schrtnm_split.length);
            if (schrtnm_split.length > 1) {
                if (sLevl == "4" && schrtnm_split[1].substr(0,3) == "NST") {
                    //model.alert("AAAAAAAAAAAAAAAAAAAAA");
                    model.setAttribute("MRR_GRGeomSaek_ReportView.right","NST");
                    g_Nst_Right_head = "NST";
                    schrtnm_title = "";
                }
            }
            // 의무기록열람/사본신청 제한 (2014.11.04)
            var userdeptcd = TFGetSessionInfo("userdeptcd");
            
            if(schrtnm_split[0] == "의무기록 열람" && userdeptcd != "000178"){
                alert("조회 권한이 없습니다. 권한을 확인해 주시길 바랍니다!"
      	               + "\r\n\r\n문의 : 의무기록팀(Tel:5973)");
                return;
            }
            
            //model.alert("EEEEEEEEE");
            //model.alert(schrtnm);
            //model.alert(schrtnm_split.length);
            //model.alert(schrtnm_split[3].length);
            //model.alert("schrtnm := " + schrtnm);
            //model.alert("schrtnm_split := " + schrtnm_OBH[3].substr(0,3));


			//2013.09.16 정신과 조회 오류로 인하여 추가함. webkebi
			//sMRR_GRGeomSaek_ChrtPSY = model.getValue("/root/MainData/limit/grcheck");

            if (sGubn == "Z") { //처방
                if (sLevl != "2" && sLevl != "3") {
                    if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                    else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                    return;
                }
            } else if (sGubn == "E" || sGubn == "O") { //EMR or OCR
                if (sIofg == "3" || sIofg == "2") { //외래, 응급
                    if (sLevl != "2" && sLevl != "3" && sLevl != "4") {
                        if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                        else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                        return;
                    }
                } else { //입원
                    if (sLevl != "2" && sLevl != "3" && sLevl != "4") {
                        if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                        else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                        return;
                    }
                }

                // 2007-07-12 1:29오후 - 동의서 여부 Check
                if (f_SearchChartName(schrtnm) != -1) {
                    if (fEMR_getGeneMode() == true) {
                        alert("연구검색에서는 동의서 정보를 조회할 수 없습니다");
                        return;
                    }
                }

								//---- 2010.12 PSY 기록검색권한 제한처리

                          if (sLevl == "4") {
					      	var psychk = model.getValue("/root/MainData/mrrfflshh[" + iRow + "]/chrtkind");
                            var anecheck = model.getValue("root/MainData/limit/anecheck");
                            var arr_psychk = psychk.split("|");
                            var psychk1 = arr_psychk[10];
                            var psychk2 = arr_psychk[21];
					      	if(sMRR_GRGeomSaek_ChrtPSY != "Y" && (psychk1 == "N" || psychk2 == "N") && anecheck != "Y" ){
					      		TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
					      		return;
					      	}else if(sMRR_GRGeomSaek_ChrtPSY == "S" && (psychk1 =="S"||psychk2 == "S") && anecheck !="Y"){
								TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
								return
							}
					      }

					      if (sLevl == "3") {
					      	var psychk = model.getValue("/root/MainData/mrrfflshh[" + iRow + "]/chrtkind");
                            var anecheck = model.getValue("root/MainData/limit/anecheck");
                            var arr_psychk = psychk.split("|");
                            var psychk1 = arr_psychk[8];
                            var psychk2 = arr_psychk[9];
					      	if(sMRR_GRGeomSaek_ChrtPSY != "Y" && (psychk1 == "N" || psychk2 == "N") && anecheck !="Y"){
                  				TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
					      		return;
					      	}else if(sMRR_GRGeomSaek_ChrtPSY == "S" && (psychk1 =="S"||psychk2 == "S") && anecheck !="Y"){
								TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
								return
							}
					      }
            } else if (sGubn == "N") { //간호
                var sGubn2 = model.getValue("/root/MainData/mrrfflshh[" + iRow + "]/chrtkind").substr(3,1); //간호기록(FOCUS),정보조사지,TPR 구분을 한다.
                // appended by OBH on 2008.06.25 for 간호기록지 수정
                // 혈액투석기록지도 Focus간호기록과 동일하게 날짜별로 한꺼번에 오른쪽그리드에 출력되도록, 의무기록팀 장헤숙주임요청
                // TFGetMsgBox(-1, "sGubn = " + sGubn + " : " + "sGubn2 = " + sGubn2, "확인", "I", "OK");
                // if (sGubn2 == "2") { //간호기록(FOCUS)

                if (sGubn2 == "1" || sGubn2 == "2") { //간호기록(FOCUS)
                    if (sLevl != "2" && sLevl != "3" && sLevl != "4") {
                        if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                        else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
						            //model.setValue("/root/SendData/Data10", "Y");
                        return;
                    }

                    //---- 2010.12 PSY 기록검색권한 제한처리
					          if (sLevl == "4") {
					          	var psychk = model.getValue("/root/MainData/mrrfflshh[" + iRow + "]/chrtkind");
                                var anecheck = model.getValue("root/MainData/limit/anecheck");
                                var arr_psychk = psychk.split("|");
                                psychk = arr_psychk[8];

					          	if(sMRR_GRGeomSaek_ChrtPSY != "Y" && psychk == "N" && anecheck !="Y"){
					          		TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
					          		return;
					          	}else if(sMRR_GRGeomSaek_ChrtPSY == "S" && psychk == "S" && anecheck !="Y"){
									TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
					          		return
								}
					          }
                } else { //TPR, 정보조사지
                    if (sLevl != "4") {//3레벨 조회가능. 우선 주석처리 sLevl != "3"
                        if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                        else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                        return;
                    }
                }
            } else if (sGubn == "A") { //누적
                if (sLevl != "4") {
                    if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                    else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                    return;
                }
            } else if (sGubn == "P") { //제증명
                if (sLevl != "4") {
                    if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                    else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                    return;
                }
            } else {
                if (sLevl != "4") {
                    if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                    else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                    return;
                }
            }
						
            fSelect_getLoadURL(pControlID, iRow, sLevl, "grid_main");
        }

        model.setFocus("grid_main");
    } else if (pControlID == "grid_copy") { //********** grid_copy 선택(오른쪽화면에 display하는 루틴)
        var iRow = TFGetGridPos("grid_copy");
        var iCol = TFGetGridCol("grid_copy");
        if (iRow > 0) {
            var fg = model.vsGrid(pControlID);
            var sLevl = model.getValue("/root/MainData/mrrfflshh_copy[" + iRow + "]/levl");
            var sGubn =  model.getValue("/root/MainData/mrrfflshh_copy[" + iRow + "]/chrtkind").substr(0,1);
            var sIofg =  model.getValue("/root/MainData/mrrfflshh_copy[" + iRow + "]/seq").substr(0,1);

            if (iCol == 0) {
                var sBoolflag = model.getValue("/root/MainData/mrrfflshh_copy[" + iRow + "]/boolflag");
                if (sLevl == "1") {
                    model.setValue("/root/MainData/mrrfflshh_copy[" + iRow + "]/boolflag", ""); //check box를 안보이게하는 trick
                    model.refresh();
                } else if (sLevl == "2") {
                    for (var i = iRow; i < fg.Rows; i++) {
                        var sLevlCopy = model.getValue("/root/MainData/mrrfflshh_copy[" + i + "]/levl");
                        if (i > iRow && sLevlCopy == "1") break;
                        if (sBoolflag == "true") {
                            TFGridFontColor("grid_copy", i, 1, i, 1, 1, 255, 0, 0);
                            model.setValue("/root/MainData/mrrfflshh_copy[" + i + "]/boolflag", "true");
                        } else {
                            TFGridFontColor("grid_copy", i, 1, i, 1, 1, 0, 0, 0);
                            model.setValue("/root/MainData/mrrfflshh_copy[" + i + "]/boolflag", "false");
                        }
                    }
                } else if (sLevl == "3") {
                    for (var i = iRow; i < fg.Rows; i++) {
                        var sLevlCopy = model.getValue("/root/MainData/mrrfflshh_copy[" + i + "]/levl");
                        if (i > iRow && (sLevlCopy == "2" || sLevlCopy == "3")) break;
                        if (sBoolflag == "true") {
                            TFGridFontColor("grid_copy", i, 1, i, 1, 1, 255, 0, 0);
                            model.setValue("/root/MainData/mrrfflshh_copy[" + i + "]/boolflag", "true");
                        } else {
                            TFGridFontColor("grid_copy", i, 1, i, 1, 1, 0, 0, 0);
                            model.setValue("/root/MainData/mrrfflshh_copy[" + i + "]/boolflag", "false");
                        }
                    }
                } else if (sLevl == "4") {
                    var i = iRow
                    if (sBoolflag == "true") {
                        TFGridFontColor("grid_copy", i, 1, i, 1, 1, 255, 0, 0);
                    } else {
                        TFGridFontColor("grid_copy", i, 1, i, 1, 1, 0, 0, 0);
                    }
                }
                model.gridrefresh("grid_copy");
                return;
            }
            if (iCol == 1) return;

            if (sGubn == "Z") { //처방
                if (sLevl != "2" && sLevl != "3") {
                    if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                    else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                    return;
                }
            } else if (sGubn == "E" || sGubn == "O") { //EMR or OCR
                if (sIofg == "3" || sIofg == "2") { //외래, 응급
                    if (sLevl != "2" && sLevl != "3" && sLevl != "4") {
                        if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                        else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                        return;
                    }
                } else { //입원
                    if (sLevl != "2" && sLevl != "3" && sLevl != "4") {
                        if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                        else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                        return;
                    }
                }
            } else if (sGubn == "N") { //간호
                var sGubn2 = model.getValue("/root/MainData/mrrfflshh_copy[" + iRow + "]/chrtkind").substr(3,1); //간호기록(FOCUS),정보조사지,TPR 구분을 한다.
                if (sGubn2 == "2") { //간호기록(FOCUS)
                    if (sLevl != "2" && sLevl != "3" && sLevl != "4") {
                        if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                        else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                        return;
                    }
                } else { //TPR, 정보조사지
                    if (sLevl != "4") {
                        if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                        else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                        return;
                    }
                }
            } else if (sGubn == "A") { //누적
                if (sLevl != "4") {
                    if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                    else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                    return;
                }
            } else if (sGubn == "P") { //제증명
                if (sLevl != "4") {
                    if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                    else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                    return;
                }
            } else {
                if (sLevl != "4") {
                    if (fg.IsCollapsed(iRow) == 0) fg.IsCollapsed(iRow) = 2;
                    else if (fg.IsCollapsed(iRow) == 2) fg.IsCollapsed(iRow) = 1;
                    return;
                }
            }
            fSelect_getLoadURL(pControlID, iRow, sLevl, "grid_copy");
        }
        model.setFocus("grid_copy");
    } else if (pControlID == "grid_list") { //********** grid_list 선택(검사결과조회)
		model.trace("[KIS] 검사결과 상세 1");
        var iRow_f = TFGetGridPos("grid_list");
        var iRow_s = iRow_f;
        var iTotalCount = TFGetGridCount("grid_list");
        var fg = model.vsGrid("grid_list");
		
		model.trace("[KIS] 검사결과 상세 2");
        if (iRow_s > 0) {
            var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

			model.trace("[KIS] 검사결과 상세 2-1");
            if (sTextImage == "P") {
                model.removeNodeset("/root/MainData/result_print/result_print_interface");
                var sLevel = model.getValue("/root/MainData/hist/list["+iRow_s+"]/level");

                var strDest = "/root/MainData/result_print";
                var strAppendTmp = "/root/MainData/result_print_tmp";

                model.makenode(strAppendTmp+"/result_print_interface");
                model.makenode(strAppendTmp+"/result_print_interface/data");
                model.makenode(strAppendTmp+"/result_print_interface/ordstatnm");
                model.makenode(strAppendTmp+"/result_print_interface/execdd");
                model.makenode(strAppendTmp+"/result_print_interface/suppdept");
                model.makenode(strAppendTmp+"/result_print_interface/rsltdd");
                model.makenode(strAppendTmp+"/result_print_interface/rsltno");
                model.makenode(strAppendTmp+"/result_print_interface/printparam");
                model.makenode(strAppendTmp+"/result_print_interface/ordcd");
                model.makenode(strAppendTmp+"/result_print_interface/ordcdnm");
                model.makenode(strAppendTmp+"/result_print_interface/level");
				
				model.trace("[KIS] 검사결과 상세 2-1-1");
                var m_node = findNode("/root/MainData/result_print_tmp");
				
				model.trace("[KIS] 검사결과 상세 2-1-2:" + sLevel);
                // sLevel이 1인경우. 다음 row가 1일때까지 roof
                // sLevel이 2인경우. 다음 row가 1이나 2일때까지 roof
                // sLevel이 3인경우. 현재 row만
                if (sLevel=="1") {
                    for (var i = iRow_s; i < iTotalCount; i++) {
                        iRow_s++;
                        var sLevel = model.getValue("/root/MainData/hist/list["+iRow_s+"]/level");

                        if (sLevel=="" || sLevel=="1") {
                            break;
                        } else if (sLevel=="2") {
                            continue;
                        }

                        var sTempOrdcdnm = model.getValue("/root/MainData/hist/list["+iRow_s+"]/data");
                        var sOrdcdnm = sTempOrdcdnm.split("【");
                        var sSuppDept= model.getValue("/root/MainData/hist/list["+iRow_s+"]/suppdept");
                        var sSuppDeptnm = model.getValue("/root/HideData/deptList[cd = '"+sSuppDept+"']/nm");
                        if (sSuppDeptnm != "") {
                            sSuppDept = sSuppDeptnm;
                        }

                        model.setValue(strAppendTmp+"/result_print_interface/data",       sOrdcdnm[0]);
                        model.setValue(strAppendTmp+"/result_print_interface/ordstatnm",  model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordstatnm"));
                        model.setValue(strAppendTmp+"/result_print_interface/execdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/execdd"));
                        model.setValue(strAppendTmp+"/result_print_interface/suppdept",   model.getValue("/root/MainData/hist/list["+iRow_s+"]/suppdept"));
                        model.setValue(strAppendTmp+"/result_print_interface/rsltdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltdd"));
                        model.setValue(strAppendTmp+"/result_print_interface/rsltno",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltno"));
                        model.setValue(strAppendTmp+"/result_print_interface/printparam", model.getValue("/root/MainData/hist/list["+iRow_s+"]/printparam"));
                        model.setValue(strAppendTmp+"/result_print_interface/ordcd",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcd"));
                        model.setValue(strAppendTmp+"/result_print_interface/ordcdnm",    model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcdnm"));
                        model.setValue(strAppendTmp+"/result_print_interface/level",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/level"));

                        var m_saveLists = m_node.childNodes;
                        var m_SaveJob    = findNode(strDest);
                        for (var jCnt = 0; jCnt < m_saveLists.length; jCnt++) {
                            var m_SaveChild = m_saveLists.item(jCnt);
                            m_SaveJob.appendChild(m_SaveChild.cloneNode(true));
                        }
                    }
                } else if (sLevel=="2") {
                    for (var i = iRow_s; i < iTotalCount; i++) {
                        iRow_s++;
                        var sLevel = model.getValue("/root/MainData/hist/list["+iRow_s+"]/level");

                        if (sLevel=="") {
                            break;
                        } else if (sLevel=="2" || sLevel=="1") {
                            break;
                        }

                        var sTempOrdcdnm = model.getValue("/root/MainData/hist/list["+iRow_s+"]/data");
                        var sOrdcdnm = sTempOrdcdnm.split("【");
                        var sSuppDept= model.getValue("/root/MainData/hist/list["+iRow_s+"]/suppdept");
                        var sSuppDeptnm = model.getValue("/root/HideData/deptList[cd = '"+sSuppDept+"']/nm");
                        if (sSuppDeptnm != "") {
                            sSuppDept = sSuppDeptnm;
                        }

                        model.setValue(strAppendTmp+"/result_print_interface/data",       sOrdcdnm[0]);
                        model.setValue(strAppendTmp+"/result_print_interface/ordstatnm",  model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordstatnm"));
                        model.setValue(strAppendTmp+"/result_print_interface/execdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/execdd"));
                        model.setValue(strAppendTmp+"/result_print_interface/suppdept",   model.getValue("/root/MainData/hist/list["+iRow_s+"]/suppdept"));
                        model.setValue(strAppendTmp+"/result_print_interface/rsltdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltdd"));
                        model.setValue(strAppendTmp+"/result_print_interface/rsltno",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltno"));
                        model.setValue(strAppendTmp+"/result_print_interface/printparam", model.getValue("/root/MainData/hist/list["+iRow_s+"]/printparam"));
                        model.setValue(strAppendTmp+"/result_print_interface/ordcd",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcd"));
                        model.setValue(strAppendTmp+"/result_print_interface/ordcdnm",    model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcdnm"));
                        model.setValue(strAppendTmp+"/result_print_interface/level",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/level"));

                        var m_saveLists = m_node.childNodes;
                        var m_SaveJob    = findNode(strDest);

                        for (var jCnt = 0; jCnt < m_saveLists.length; jCnt++) {
                            var m_SaveChild = m_saveLists.item(jCnt);
                            m_SaveJob.appendChild(m_SaveChild.cloneNode(true));
                        }
                    }
                } else if (sLevel=="3") {
                    var sTempOrdcdnm = model.getValue("/root/MainData/hist/list["+iRow_s+"]/data");
                    var sOrdcdnm = sTempOrdcdnm.split("【");
                    var sSuppDept= model.getValue("/root/MainData/hist/list["+iRow_s+"]/suppdept");
                    var sSuppDeptnm = model.getValue("/root/HideData/deptList[cd = '"+sSuppDept+"']/nm");
                    if (sSuppDeptnm != "") {
                        sSuppDept = sSuppDeptnm;
                    }

                    model.setValue(strAppendTmp+"/result_print_interface/data",       sOrdcdnm[0]);
                    model.setValue(strAppendTmp+"/result_print_interface/ordstatnm",  model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordstatnm"));
                    model.setValue(strAppendTmp+"/result_print_interface/execdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/execdd"));
                    model.setValue(strAppendTmp+"/result_print_interface/suppdept",   sSuppDept);
                    model.setValue(strAppendTmp+"/result_print_interface/rsltdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltdd"));
                    model.setValue(strAppendTmp+"/result_print_interface/rsltno",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltno"));
                    model.setValue(strAppendTmp+"/result_print_interface/printparam", model.getValue("/root/MainData/hist/list["+iRow_s+"]/printparam"));
                    model.setValue(strAppendTmp+"/result_print_interface/ordcd",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcd"));
                    model.setValue(strAppendTmp+"/result_print_interface/ordcdnm",    model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcdnm"));
                    model.setValue(strAppendTmp+"/result_print_interface/level",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/level"));

                    var m_saveLists = m_node.childNodes;
                    var m_SaveJob    = findNode(strDest);

                    for (var jCnt = 0; jCnt < m_saveLists.length; jCnt++) {
                        var m_SaveChild = m_saveLists.item(jCnt);
                        m_SaveJob.appendChild(m_SaveChild.cloneNode(true));
                    }
                }
            }
			
			model.trace("[KIS] 검사결과 상세 2-2");
            if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") { //이미지 조회인 경우
                fDeleteFile("RESULT_");
                sMRR_GRGeomSaek_FileCnt = 0;
            }
			
			model.trace("[KIS] 검사결과 상세 2-3");
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") {
				model.trace("[KIS] 검사결과 상세 2-3-1");
                if(g_TotalTestResultTab != "6"){
					model.setAttribute("MRR_GRGeomSaek_Result.kind", iRow_f);
					model.setAttribute("MRR_GRGeomSaek_Ocr.goption", "false");
				} else {
					model.setAttribute("MRR_GRGeomSaek_Ocr.goption", "6");
					model.setAttribute("MRR_GRGeomSaek_Result.kind", iRow_f);
				}
				model.trace("[KIS] body_girokgeomsaek_Right loadURI 7");
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm", "replace");
            } else {
				model.trace("[KIS] 검사결과 상세 2-3-2");
                //2006-08-16 1:51오후 get Model 관련 수정 김태범				
                var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");

                if (emr_Right) {
                    emr_Right.removenodeset("/root/MainData/result"); 
					emr_Right.gridrebuild("grid_result");
					
                    var gPid = model.getValue("/root/MainData/condition/cond1");
                    var gUserId = TFGetSessionInfo("userid");
                    //조회 기간이 2년을 넘길 경우 메세지 호출 2007-07-04 1:56오후 - 김태범
                    var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
                    var sGsTodd   = model.getValue("/root/MainData/condition/todt");
                    var sDayCount = lvGetDateTerm(sGsFromdd,sGsTodd);

                    //조회 시작일 종료일 세팅
                    model.setValue("/root/HideData/fromdd", sGsFromdd);
                    model.setValue("/root/HideData/todd"  , sGsTodd);
					model.trace("[KIS] 검사결과 상세 2-3-3");
                    fWaitBegin("검사결과 조회중...******");

                    // 2008-01-04 11:28오전 : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
                    var sTempFlag = "1";

                    switch (g_TotalTestResultTab) {
                        case "1" :
                            sTempFlag = g_TotalTestResultTab;
                            break;
                        case "2" :
                            sTempFlag = g_TotalTestResultTab;
                            break;
                        case "3" :
                            sTempFlag = g_TotalTestResultTab;
                            break;
						case "6" :
                            sTempFlag = g_TotalTestResultTab;
							gOption = "1";
                            break;
                        default :
                            sTempFlag = "2";
                    }

                    /*
                    if (g_TotalTestResultTab != "1") {
                        sTempFlag = "2";
                    }
                    */

                    //alert("g_TotalTestResultTab:" + g_TotalTestResultTab + " sTempFlag:" + sTempFlag + "iRow_f:" + iRow_f);
                    //alert("g_TotalTestResultTab:3 / sTempFlag:3 / iRow_f:1");

                    // 의사확인 출력여부 설정
                    var sPrint = "0";
					//if (sTextImage != "T") sPrint = "0";
					model.trace("[KIS] 검사결과 상세 2-3-4:" + iRow_f);					
                    emr_Right.javascript.fGetResult(0, iRow_f, sTempFlag, gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd, sLanguage, sPrint);
					model.trace("[KIS] 검사결과 상세 2-3-5");
                    fWaitEnd();

                    // appended by OBH on 2009.06.12
                    /*
                    var iRow_f_OBH = TFGetGridPos("grid_result");
                    var iRow_s_OBH = iRow_f_OBH;
                    var iTotalCount_OBH = TFGetGridCount("grid_result");
                    var Data_OBH = model.getValue("/root/MainData/result/list["+iRow_s_OBH+"]/data");
                    var pacsno_OBH = model.getValue("/root/MainData/result/list["+iRow_s_OBH+"]/pacsno");
                    model.alert(Data_OBH);
                    model.alert(pacsno_OBH);
                    model.alert(iRow_s_OBH);
                    model.alert(iRow_f_OBH);
                    model.alert(iTotalCount_OBH);
                    */

                    if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I") {  //이미지 조회인 경우
                        fWriteGyeolGwa();
                        model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                        sTotalKind = sTotalKind + "▩";
                        model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                    } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "C") {  //이미지 조회인 경우
                        fWriteGyeolGwa2();
                        model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                        sTotalKind = sTotalKind + "▩";
                        model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                    }
                } else {
                    return;
                }
            }
        }
        // 병리검사 글자 잘리는 문제 수정 (2014.09.25)
		
		model.trace("[KIS] 검사결과 상세 3");
        if(sTextImage == "T"){
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                var fg1 = emr_Right.vsGrid("grid_result");

                fg1.row = 0;
                fg1.col = 0;
                fg1.WordWrap      = true;
                fg1.AutoSizeMode  = 1;
                fg1.RowHeightMin  = 370;
                fg1.AutoSize(0, fg.Cols -1);
            }            
        }
                         
        model.setFocus("grid_list");
    } else if (pControlID == "grid_silsi") { //********** grid_list 선택(검사결과조회-실시내역)
        model.setFocus("grid_silsi");
    } else if (pControlID == "light_equipment") {    //경장비 내역클릭시
        model.enable("button_print", "false"); //출력(무조건 disable시킨다.)
        model.toggle("case_10");
        fGetEquipmentData();
    } else if (pControlID == "EquipmentCatchView") {
        // 2007-07-12 1:32오후 : 연구검색 예외
        if (fEMR_getGeneMode() != true) {
            fClickEquipmentCatchView();
        }
    } else if (pControlID == "grid4") {
        var iRow = TFGetGridPos(pControlID);
        var fg = model.vsGrid(pControlID);
        if (fg.IsCollapsed(iRow) == 0) {
            fg.IsCollapsed(iRow) = 2;
        } else if (fg.IsCollapsed(iRow) == 2) {
            fg.IsCollapsed(iRow) = 1;
        }
    } else if (pControlID == "grid_UHealthcare") {
        model.toggle("U-HEALTH");

        var fg = model.vsGrid(pControlID);
        for (var iCnt = fg.FixedRows; iCnt < fg.Rows; iCnt++) {
            fg.IsSubtotal(iCnt) = true;
            fg.RowOutlineLevel(iCnt) = fg.TextMatrix(iCnt, 0); //인스턴스 levl 값임

            //fg.Cell(5,i, 1) = 2;
            if (fg.TextMatrix(iCnt, 0) == "1") {
                fg.Cell(7, iCnt, 1) = model.rgb(51,0,187);
                fg.Cell(13,iCnt, 1) = true;
            } else if (fg.TextMatrix(iCnt, 0) == "2") {
                fg.Cell(7, iCnt, 1) = model.rgb(150,17,187);
                fg.Cell(7, iCnt, 2) = model.rgb(230,30,30);
                fg.Cell(13,iCnt, 1) = true;
                fg.Cell(13,iCnt, 2) = true;
            }
        }

        fg.OutlineBar = 4;        //+ , - , 사각형 안보이게 하려면 0 으로 세팅
        fg.OutlineCol = 1; //returns or sets the column userd to display the outline tree
        fg.Row = 0;
    } else if (pControlID == "button_nutri") {  // 영양정보 조회
        model.toggle("case_nutri");

        model.enable("radio_all", "false");
        model.enable("radio_otpt", "false");
        model.enable("radio_inpt", "false");
        model.enable("radio_er", "false");
        model.enable("radio_text", "false");
        model.enable("radio_image", "false");
        model.enable("radio_continue", "false");
        model.enable("radio_report", "false");
        model.enable("combo_dept", "false");
		model.enable("radio_language", "false");
		model.enable("radio_language2", "false");

        model.setgroupBtnStatus("button_nutri_date", true);      // 일자별 버튼이 기본 셋팅

        if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") {        // 브라우저 쪽에는 텍스트 기반 출력용 xfm으로
            model.setAttribute("MRR_GRGeomSaek_Grid.function", "clear");
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
        }

        f_SetNavigatorPathInfo("영양", "일자별");
        f_SelectNutri("D");
    } else if (pControlID == "grid_nutri") {    // 영양정보 그리드 클릭시 (오른쪽화면에 display하는 루틴)
        fNutriSelect_getLoadURL();
        model.setFocus("grid_nutri");
    } else if (pControlID == "close") {
        //이준환
        model.setAttribute("sMRR_GRGeomSaek_prePid", "");
        model.removeNodeset("/root/MainData/opmcptbsm");
        model.resetInstanceNode("/root/SendData");
        model.setValue("/root/SendData/Mode", "MRR_reqGetOpmcptbsm"); // Action Method
        model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1")); //등록번호
        var sDate = new Date();
        sDate = sDate.lvFormat();
        model.setValue("/root/SendData/Data2", sDate); // 시스템일자
        //model.setValue("/root/SendData/Data3", model.getAttribute("sMRR_GRGeomSaek_from")); // 접근패턴 : PACS OR 연구검색일 경우 값이 들어옵니다
        model.setValue("/root/SendData/Data11", "close");
        //model.setValue("/root/MainData/opmcptbsm/pre_pid", model.getAttribute("/root/MainData/condition/cond1")); // 현재 환자 등록번호를 이전 등록번호로 옮긴다
        //model.setAttribute("sMRR_GRGeomSaek_prePid", model.getValue("/root/MainData/condition/cond1")); // 현재 환자 등록번호를 이전 등록번호로 옮긴다
        model.setValue("/root/HideData/temp_pid", model.getValue("/root/MainData/condition/cond1")); // 현재 환자 등록번호를 이전 등록번호로 옮긴다
        model.submitInstance("MRR_reqGetOpmcptbsm");
        //model.closeBrowser();
        TFCloseBrowser();

    } else if (pControlID == "button_rslt_date_2nd") { //********** 검사일자별 조회
		gOption = "1";
        g_TotalTestResultTab = "6";
        model.visible("button_rslt_result", "false"); //검사별
        model.toggle("result_case1");
        model.removenodeset("/root/MainData/hist");   model.gridrebuild("grid_list");

        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

        if (sTextImage == "T") { //
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Result.kind", "clear");
				model.trace("[KIS] body_girokgeomsaek_Right loadURI 8");
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm", "replace");
            }
        } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") {
            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
            }
        } else if (sTextImage == "P") {
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
        }

        if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") { //이미지 조회인 경우
            fDeleteFile("RESULT_");
            sMRR_GRGeomSaek_FileCnt = 0;
        }

        //2007-07-04 1:23오후 해당 기간안의 모든 검사를 조회하는 부분 선택 처리함(기간이 길어질 수록 오래 걸리는 문제 해결) - 김태범
        if (!fGetGyeolGwaList(gOption, "6")) {//조회 기간이 2년을 넘을 경우 자동 return
            return;
        }

        var sTempGSGridCnt = TFGetGridCount("grid_list");
        if (sTempGSGridCnt > 10) {
            //2007-07-04 1:23오후 해당 기간안의 모든 검사를 조회하는 부분 선택 처리함(기간이 길어질 수록 오래 걸리는 문제 해결)
            //modified by OBH on 2009.04.03,출력메시지는 막는다. 박진희주임.
            //if (model.messageBox ("모든 일자별-세부내역을 조회 하려면 '예'를 ... 단, 검사내역이 많을 경우 조회시간이 길어질 수 있습니다.", "선택", 1) != 1) {                             //재확인
            //    return;
            //}
        } else if (sTempGSGridCnt == 0) {
            return;
        }

        if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") {
            model.setAttribute("MRR_GRGeomSaek_Result.kind", "date"); //일자별 검사결과조회
			model.trace("[KIS] body_girokgeomsaek_Right loadURI 13");			
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm","replace");
        } else {
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                emr_Right.removenodeset("/root/MainData/result"); emr_Right.gridrebuild("grid_result");
                var gPid = model.getValue("/root/MainData/condition/cond1");
                var gUserId = TFGetSessionInfo("userid");
                var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
                var sGsTodd   = model.getValue("/root/MainData/condition/todt");

                // 의사확인 출력여부 설정
                var sPrint = "0";
                //if (sTextImage != "T") sPrint = "0";

                fWaitBegin("검사결과 조회중...*******");
                emr_Right.javascript.fGetResult(1, 0, "6", gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd, sLanguage, sPrint);
                fWaitEnd();

                if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I") { //이미지 조회인 경우
                    fWriteGyeolGwa();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "C") { //이미지 조회인 경우
                    fWriteGyeolGwa2();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                }
            } else {
                //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
                return;
            }
        }
    }


    // temp, 20090210
    //var chkUserId = TFGetSessionInfo("userid");
    //model.alert(chkUserId);
    //if (chkUserId == "19950004")
    //{
    //    model.enable("button_print", "true");
    //    model.visible("radio_report","true");
    //}
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 진료과를 가져온다.
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fGetDept() {
    model.removeNodeset("/root/HideData/combodept");
    model.resetInstanceNode("/root/SendData");
    model.setValue("/root/SendData/Mode", "reqGetComboCOMCDEPTM");
    model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1")); //등록번호
    model.setValue("/root/SendData/Data2", model.getValue("/root/MainData/condition/frdt")); //시작일자
    model.setValue("/root/SendData/Data3", model.getValue("/root/MainData/condition/todt")); //종료일자
    model.setValue("/root/SendData/Data4", model.getValue("/root/HideData/condition/cond1")); //일자별,서식지별...
    model.setValue("/root/SendData/Data5", model.getValue("/root/HideData/condition/cond2")); //전체,외래,입원,응급
    model.setValue("/root/SendData/Data6", "girok"); //전체과 가져오는 구분

    if (model.submitInstance("getComboCommon")) { //사용자부서를 가져온다.
    }
    TFInsertInstance("/root/HideData/combodept",  "combodept", "deptcd,deptnm,sddeptnm,deptlevl,updeptcd,tempstat,tempdate", "%,전체,전체,%,%,-,"+model.getValue("/root/MainData/condition" + "/todt"));
    model.setValue("/root/MainData/condition/cond2", "%");
    model.refresh();

    return;
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 더블클릭시 처리
 * @param  : pControlID
 * @return :
 * @---------------------------------------------------
 */
function fDoubleClicked(pControlID) {
    var fg = model.vsGrid(pControlID);
    var iRow = TFGetGridPos(pControlID);
    var iCol = TFGetGridCol(pControlID);
    if (iRow > 0) {
        if (pControlID == "grid_main") {
            if (fg.IsCollapsed(iRow) == 0) {
                fg.IsCollapsed(iRow) = 2;
            } else if (fg.IsCollapsed(iRow) == 2) {
                fg.IsCollapsed(iRow) = 1;
            }
        } else if (pControlID == "grid_copy") {
            if (fg.IsCollapsed(iRow) == 0) {
                fg.IsCollapsed(iRow) = 2;
            } else if (fg.IsCollapsed(iRow) == 2) {
                fg.IsCollapsed(iRow) = 1;
            }
            var sLevl = model.getValue("/root/MainData/mrrfflshh_copy[" + iRow + "]/levl");
            if (sLevl == "1") {
                model.setValue("/root/MainData/mrrfflshh_copy[" + iRow + "]/boolflag", ""); //check box를 안보이게하는 trick
                model.refresh();
            }
        } else if ("grid4") {
            if (fg.IsCollapsed(iRow) == 0) {
                fg.IsCollapsed(iRow) = 2;
            } else if (fg.IsCollapsed(iRow) == 2) {
                fg.IsCollapsed(iRow) = 1;
            }
        } else {
            return;
        }
    } //(iRow > 0)
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : KeyBoard Key가 눌린경우
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fKeyDown() {
    var curCtrlID = model.getCurId();
    var curKey = model.getCurKeyValue();
    var systemKey = model.getCurSysKeyValue();  // 시스템키 (Ctrl, Alt, Shift등)

    // ※ Tab(9), Enter(13), 하향키(40)
    if (curKey == 13) {
        if (curCtrlID == "input_idno") {
            fClicked("button_query");
        //} else if (curCtrlID == "input_drugcode") {
        //    fSelectDrug("BB");
        } else if (curCtrlID == "input_copycnt") {
            fCopyCnt();
        }
    }
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : MouseEvent('LButtonClick'/'RButtonClick'/'LButtonDBLClick'/'RButtonDBLClick')
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fMouseEvent() {
    var curCtrlID = model.getCurId();
    var curKey = model.getCurMouseState(); // 'LButtonClick'/'RButtonClick'/'LButtonDBLClick'/'RButtonDBLClick'
    if (curCtrlID == "grid_copy") {
        if (curKey == "RButtonClick") {
            var fg = model.vsGrid("grid_copy");
            if (fg.Row < 1) return;
            model.setGridMenu ("grid_copy", "/root/HideData/gridcopymenu1", false);
            model.displayGridMenu("grid_copy");
        }
    } else if (curCtrlID == "grid_order") {
        if (curKey == "RButtonClick") {
            var fg = model.vsGrid("grid_order");
            if (fg.Row < 1) return;
            model.setGridMenu ("grid_order", "/root/HideData/gridordermenu1", false);
            model.displayGridMenu("grid_order");

        } else if (curKey == "LButtonClick") {   //2007.10.16 ley추가 수혈반납/폐기요청서조회

            var fg = model.vsGrid("grid_order");
            if (fg.Row < 1) return;

            var pid = model.getValue("root/MainData/order["+fg.Row+"]/opod_pid");
            var indd = model.getValue("root/MainData/order["+fg.Row+"]/opod_indd");
            var orddd = model.getValue("root/MainData/order["+fg.Row+"]/opod_orddd");
            var orddeptcd = model.getValue("root/MainData/order["+fg.Row+"]/opod_orddeptcd");
            var mskind = model.getValue("root/MainData/order["+fg.Row+"]/opod_mskind");
            var orddoctcd = model.getValue("root/MainData/order["+fg.Row+"]/opod_orddoctcd");
            var seqno = model.getValue("root/MainData/order["+fg.Row+"]/opod_seqno");
            var sData1 = "m▦pid▦indd▦orddd▦orddeptcd▦mskind▦orddoctcd▦seqno▩▦"
                       + pid + "▦"
                       + indd + "▦"
                       + orddd + "▦"
                       + orddeptcd + "▦"
                       + mskind + "▦"
                       + orddoctcd + "▦"
                       + seqno + "▩";

            model.removeNodeset("/root/HideData/shbncb");
            model.resetInstanceNode("/root/SendData");
            model.setValue("/root/SendData/Mode", "reqGetIWCheoBangSHYN"); // Action Method
            model.setValue("/root/SendData/Data1", sData1); //

            if (model.submitInstance("GetIWCheoBangSHYN")) {
                TFSetMessage("/root/HideData/shbncb");

                if (model.getValue("/root/HideData/shbncb/ordstat") == "P" || model.getValue("/root/HideData/shbncb/ordstat") == "N") {
                     model.enable("button1", "true");
                     var sData2 = "m▦pid▦indd▦orddd▦orddeptcd▦mskind▦orddoctcd▦seqno▦ordstat▦spcmno▩▦"
                                + pid + "▦"
                                + indd + "▦"
                                + orddd + "▦"
                                + orddeptcd + "▦"
                                + mskind + "▦"
                                + orddoctcd + "▦"
                                + seqno + "▦"
                                + model.getValue("/root/HideData/shbncb/ordstat") + "▦"
                                + model.getValue("/root/HideData/shbncb/spcmno") + "▩";
                    model.setAttribute("OER_SHBNPGJoHoi_parm",sData2);
                 } else model.enable("button1", "false");
            } else {
                model.enable("button1", "false");
            }

            model.refresh();
        }
    } else if (curCtrlID == "grid_focus") {
        if (curKey == "RButtonClick") {
            var fg = model.vsGrid("grid_focus");
            if (fg.Row < 1) return;
            model.setGridMenu ("grid_focus", "/root/HideData/gridfocusmenu1", false);
            model.displayGridMenu("grid_focus");
        }
    } else if (curCtrlID == "grid_drug") {
        if (curKey == "RButtonClick") {
            var fg = model.vsGrid("grid_drug");
            if (fg.Row < 1) return;
            model.setGridMenu ("grid_drug", "/root/HideData/griddrugmenu1", false);
            model.displayGridMenu("grid_drug");
        }
    }
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 복사신청매수 수정
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCopyCnt() {
    var sCopyCnt = 0;
    if (model.getValue("/root/HideData/copycnt").length > 0) sCopyCnt = eval(model.getValue("/root/HideData/copycnt"));

    if (sCopyCnt < 1) {
        TFGetMsgBox(-1, "매수를 확인하세요.", "확인", "I", "OK");
        model.setFocus("input_copycnt");
        return;
    }

    var i_ForCnt = 0;
    var fg = model.vsGrid("grid_copy");
    for (var i = fg.FixedRows; i < fg.Rows; i++) {
        if (model.getValue("/root/MainData/mrrfflshh_copy[" + i + "]/boolflag") == "true") {
            model.setValue("/root/MainData/mrrfflshh_copy[" + i + "]/cnt", sCopyCnt);
            i_ForCnt = i_ForCnt + 1;
        }
    }

    if (i_ForCnt < 1) {
        TFGetMsgBox(-1, "복사신청하고자 하는 챠트를 선택후 작업하십시요.", "확인", "I", "OK");
        model.setFocus("input_copycnt");
        return;
    }

    model.gridrefresh("grid_copy");
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검색대상자조회
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSelect() {
    if (!fIsValid()) return false; // 등록번호 Validation 체크
	
	model.trace("[KIS] fSelect 1");
    fChkCRER();//응급실 호출의 출력권한
    var occukind_OBH_CHK = TFGetSessionInfo("occukind"); //model.getValue("/root/MainData/opmcptbsm/occukind")
    var userdeptcd = TFGetSessionInfo("userdeptcd");

	model.trace("[KIS] fSelect 1-1");
    if(userdeptcd =='000397' && occukind_OBH_CHK == "10"){//유저가 마취통증의학과 이고 의사직인 경우만..
        fChkANE(); //마취과 마취환자의 신경정신과 열람
    }else{
        model.setValue("root/MainData/limit/anecheck","");
        model.refresh();
    }	

	model.trace("[KIS] fSelect 2");
    //특정인 진료정보조회 권한점검 처리
	var sLimitCheck = fGetPSYGRCheck();
	if ( sLimitCheck == "N" ) {
		model.removeNodeset("/root/MainData/mrrfflshh");
		model.gridRebuild("grid_main");
		return;
	}

	model.trace("[KIS] fSelect 3");
	//prePid = model.getValue("/root/HideData/temp_pid");
	var sPid = model.getValue("/root/MainData/condition/cond1");

    if (g_Pid == sPid) {
        return true;
    } else {
        model.removeNodeset("/root/MainData/opmcptbsm"); //환자기본정보 삭제
    }
	
	model.trace("[KIS] fSelect 4");
    /* 권한신청 연동 관련 추가 부분. - 2007-09-13 2:05오후 김태범 */
    if (gGiRokSinCheongFlag != "" && gGiRokSinCheongFlag != null) {
        if (gGiRokSinCheong_pid != sPid) {
            //권한신청에서 조회하려고 하는 환자가 아닌 다른 환자를 조회하려고 하는 케이스 이므로 조회가 안되도록 막는다.
            model.alert(gGiRokSinCheong_pid +" 환자가 아닌 다른 환자를 조회하시려면 권한신청화면에서 조회하시기 바랍니다.");
            model.setValue("/root/MainData/condition/cond1", "");
            model.refresh();
            return false;
        }
    }
	
	model.trace("[KIS] fSelect 5");

    //기록조회 완료시간 기능개선(2010.10.21)
	//등록번호가 같은 기록 조회 하려고 하면 update 하지 말아야 함
	if(prePid == "" && prePid != sPid){
		model.removeNodeset("/root/MainData/opmcptbsm");
		model.resetInstanceNode("/root/SendData");
		model.setValue("/root/SendData/Mode", "MRR_reqGetOpmcptbsm"); // Action Method
		model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1")); //등록번호
		var sDate = new Date();
		sDate = sDate.lvFormat();
		model.setValue("/root/SendData/Data2", sDate); // 시스템일자
		model.setValue("/root/SendData/Data3", model.getAttribute("sMRR_GRGeomSaek_from")); // 접근패턴 : PACS OR 연구검색일 경우 값이 들어옵니다
		//model.setValue("/root/SendData/Data11", model.getValue("/root/HideData/temp_pid"));
		model.setValue("/root/SendData/Data11", model.getAttribute("sMRR_GRGeomSaek_prePid"));
		//model.setValue("/root/MainData/opmcptbsm/pre_pid", model.getAttribute("/root/MainData/condition/cond1")); // 현재 환자 등록번호를 이전 등록번호로 옮긴다
		//model.setAttribute("sMRR_GRGeomSaek_prePid", model.getValue("/root/MainData/condition/cond1")); // 현재 환자 등록번호를 이전 등록번호로 옮긴다
		model.setValue("/root/HideData/temp_pid", model.getValue("/root/MainData/condition/cond1")); // 현재 환자 등록번호를 이전 등록번호로 옮긴다
		model.setAttribute("sMRR_GRGeomSaek_prePid", model.getValue("/root/MainData/condition/cond1"));

		//기록검색 사유 입력시 신경정신과 권한여부 추가
		model.setValue("/root/SendData/Data10", model.getValue("/root/MainData/limit/grcheck"));

	}
	else if(prePid != "" && prePid == sPid){
		return;
	}

    if (model.submitInstance("MRR_reqGetOpmcptbsm")) {
        TFSetMessage("/root/MainData/opmcptbsm");
        if (model.getValue("/root/MainData/opmcptbsm/donate") == "Y") model.enable("button10", "true"); //조혈모기증자 여부
        else model.enable("button10", "false");
        if (model.getValue("/root/MainData/opmcptbsm/dup") == "Y") model.enable("button11", "true"); //이중번호 여부
        else model.enable("button11", "false");

        fSexAge();
        g_Pid = sPid;
    } else {
        model.enable("button10", "false");
        model.enable("button11", "false");
        return false;
    }
	
	model.trace("[KIS] fSelect 6");

    model.refresh();
    return true;
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 서식지조회
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSelect2() {
    model.resetInstanceNode("/root/SendData"); //인자로 들어간 인스턴스 path의 첫번째 depth의 자식 노드들까지의 값을 지우고 두번째 depth의 자식 노드들은 아예 노드 자체를 삭제
    model.setValue("/root/SendData/Mode",  "MRR_reqGetMrrfflshh"); // Action Method
    model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1")); //등록번호
    model.setValue("/root/SendData/Data2", model.getValue("/root/MainData/condition/frdt")); //시작일자
    model.setValue("/root/SendData/Data3", model.getValue("/root/MainData/condition/todt")); //종료일자
    model.setValue("/root/SendData/Data4", model.getValue("/root/HideData/condition/cond1")); //일자별,서식지별...
    model.setValue("/root/SendData/Data5", model.getValue("/root/HideData/condition/cond2")); //전체,외래,입원,응급
    model.setValue("/root/SendData/Data6", model.getValue("/root/MainData/condition/cond2")); //진료과

    var sJiWeonGiRokFlag = model.getAttribute("MRR_GRGeomSaek_Modal.chart_type_view"); // 진료지원기록 조회플래그
    model.setValue("/root/SendData/Data7", sJiWeonGiRokFlag);

    // appended by OBH on 2008.03.05 입력받은 정보를 임시로 출력해 본다. 반영시에는 막아야함.
    //TFGetMsgBox(-1, "Mode  = " + model.getValue("/root/SendData/Mode"), "확인", "I", "OK");
    //TFGetMsgBox(-1, "Data1(cond1) = " + model.getValue("/root/SendData/Data1"), "확인", "I", "OK");
    //TFGetMsgBox(-1, "Data2(cond2) = " + model.getValue("/root/SendData/Data2"), "확인", "I", "OK");
    //TFGetMsgBox(-1, "Data3(cond3) = " + model.getValue("/root/SendData/Data3"), "확인", "I", "OK");
    //TFGetMsgBox(-1, "Data4(cond4) = " + model.getValue("/root/SendData/Data4"), "확인", "I", "OK");
    //TFGetMsgBox(-1, "Data5(cond5) = " + model.getValue("/root/SendData/Data5"), "확인", "I", "OK");
    //TFGetMsgBox(-1, "Data6(cond6) = " + model.getValue("/root/SendData/Data6"), "확인", "I", "OK");
    //TFGetMsgBox(-1, "Data7(cond7) = " + model.getValue("/root/SendData/Data7"), "확인", "I", "OK");
    // end append

    if (model.submitInstance("MRR_reqGetMrrfflshh")) {
        if (model.getvalue("/root/MainData/mrrfflshh/levl") == "") { //조회실패
            fReset(); //화면정리
            return false;
        } else {
            TFSetMessage("/root/MainData/mrrfflshh");
        }
    } else {
        fReset(); //화면정리
        return false;
    }

    model.gridrebuild("grid_main");

    var iRow = TFGetGridCount("grid_main");
    if (iRow > 0) {
        var sTree = 0; //default로 Tree를 접어달라고 요청(20050602)
        var fg = model.vsGrid("grid_main");
        for (var i = fg.FixedRows; i < fg.Rows; i++) {
            var sLevl = model.getValue("/root/MainData/mrrfflshh[" + i + "]/levl");
            var sKind = model.getValue("/root/MainData/mrrfflshh[" + i + "]/chrtkind").substr(0,1);
            var sIofg =  model.getValue("/root/MainData/mrrfflshh[" + i + "]/seq").substr(0,1);
            var sKind2 = model.getValue("/root/MainData/mrrfflshh[" + i + "]/chrtkind").substr(3,1); //간호기록(FOCUS),정보조사지,TPR 구분을 한다.

            //재원여부를 check begin
            var sSeq = model.getValue("/root/MainData/mrrfflshh[" + i + "]/seq");
            sSeq = sSeq.lvReplaceWord("|", "▦");
            var sOudt = TFGetMatrixData(sSeq, 0, 5); //퇴원일자
            //재원여부를 check begin

            if (sKind == "Z") { //처방
                if (sLevl != "2" && sLevl != "3") {
                    //TFGridRowColor("grid_main", i, 1, i, 1, 1, 230, 230, 230);
                } else {
                    TFGridFontColor("grid_main", i, 0, i, 1, 1, 0, 0, 255);
                }
            } else if (sKind == "E" || sKind == "O") { //EMR or OCR
                if (sIofg == "3" || sIofg == "2") { //외래, 응급
                    if (sLevl != "2" && sLevl != "3" && sLevl != "4") {
                        //TFGridRowColor("grid_main", i, 1, i, 1, 1, 230, 230, 230);
                    } else {
                        TFGridFontColor("grid_main", i, 0, i, 1, 1, 0, 0, 255);
                    }
                } else { //입원
                    if (sLevl != "2" && sLevl != "3" && sLevl != "4") {
                        //TFGridRowColor("grid_main", i, 1, i, 1, 1, 230, 230, 230);
                    } else {
                        if (sLevl == "2" && sOudt == "29991231") { //재원중일경우
                            TFGridFontColor("grid_main", i, 0, i, 1, 1, 255, 0, 255);
                        } else {
                            TFGridFontColor("grid_main", i, 0, i, 1, 1, 0, 0, 255);
                        }
                    }
                }

                // #!# 지원기록은 sJiWeonGiRokFlag 에 의해 구분, 심평원확인건으로 인한 임시처리
                // 2008-08-18, 기록자정보 삭제, 요청자 (장혜숙:의무기록실, 장재연:재활의학과)
                if (sJiWeonGiRokFlag == "LY" && sLevl == "4") {
                    var s_Temp = fg.TextMatrix(i,0);
                    if (s_Temp == "") countinue;

                    var s_Temps = s_Temp.split("[");
                    fg.TextMatrix(i,0) = s_Temps[0];
                }
            } else if (sKind == "N") { //간호
                if (sKind2 == "2") { //간호기록(FOCUS)
                    if (sLevl != "2" && sLevl != "3" && sLevl != "4") {
                        //TFGridRowColor("grid_main", i, 1, i, 1, 1, 230, 230, 230);
                    } else {
                        TFGridFontColor("grid_main", i, 0, i, 1, 1, 0, 0, 255);
                    }
                } else { //TPR, 정보조사지
                    if (sLevl != "4") {
                        //TFGridRowColor("grid_main", i, 1, i, 1, 1, 230, 230, 230);
                    } else {
                        TFGridFontColor("grid_main", i, 0, i, 1, 1, 0, 0, 255);
                    }
                }
            } else if (sKind == "A") { //누적
                if (sLevl != "4") {
                    //TFGridRowColor("grid_main", i, 1, i, 1, 1, 230, 230, 230);
                } else {
                    TFGridFontColor("grid_main", i, 0, i, 1, 1, 0, 0, 255);
                }
            } else if (sKind == "A") { //제증명
                if (sLevl != "4") {
                    //TFGridRowColor("grid_main", i, 1, i, 1, 1, 230, 230, 230);
                } else {
                    TFGridFontColor("grid_main", i, 0, i, 1, 1, 0, 0, 255);
                }
            } else {
                if (sLevl != "4") {
                    //TFGridRowColor("grid_main", i, 1, i, 1, 1, 230, 230, 230);
                } else {
                    TFGridFontColor("grid_main", i, 0, i, 1, 1, 0, 0, 255);
                }
            }

            if (sTree == 0) {  //Tree를 접는다. default로 Tree를 접어달라고 요청(20050602)
                fg.IsSubtotal(i) = true;
                fg.RowOutlineLevel(i) = sLevl; //Returns or sets the outline level for a subtotal row.
            } else if (sTree == 1) { //1단계 Tree를 편다.
                if (sIofg == "3" || sIofg == "2") { //외래, 응급
                    fg.IsSubtotal(i) = true;
                    fg.RowOutlineLevel(i) = sLevl; //Returns or sets the outline level for a subtotal row.
                } else { //입원
                    if (sLevl == "1" || sLevl == "2") {
                        fg.IsSubtotal(i) = true;
                        fg.RowOutlineLevel(i) = sLevl; //Returns or sets the outline level for a subtotal row.
                    }
                }
            } else { //Tree를 편다.
                fg.IsSubtotal(i) = true;
                fg.RowOutlineLevel(i) = sLevl; //Returns or sets the outline level for a subtotal row.
            }
        } // for문

        if (sTree == 0) { //Tree를 접는다.
            for (var i = fg.FixedRows; i < fg.Rows; i++) fg.IsCollapsed(i) = 2;
        } else if (sTree == 1) { //1단계 Tree를 편다.
            for (var i = fg.FixedRows; i < fg.Rows; i++) fg.IsCollapsed(i) = 1;
        } else { //Tree를 편다.
            for (var i = fg.FixedRows; i < fg.Rows; i++) fg.IsCollapsed(i) = 0;
        }

        if (sTree == 1) { //전체적인 Tree구조를 다시 생성
            for (var i = fg.FixedRows; i < fg.Rows; i++) {
                var sLevl = model.getValue("/root/MainData/mrrfflshh[" + i + "]/levl");
                fg.IsSubtotal(i) = true;
                fg.RowOutlineLevel(i) = sLevl; //Returns or sets the outline level for a subtotal row.
            } //for
        }

        fg.PicturesOver = false; //글자와 이미지를 겹치지 않게.
        fg.OutlineBar = 4; //+ , - , 사각형 안보이게 하려면 0 으로 세팅
        fg.OutlineCol = 0; //returns or sets the column used to display the outline tree
        fg.Row = 0;
    }

    fWordWrap("grid_main");
    return true;
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과조회
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fGetGyeolGwaList(gOption, gSelectedTab) {
    TFclearNodeValue("/root/SendData");

    //model.setValue("/root/HideData/option", gOption);
    //model.setValue("/root/HideData/gubn", gSelectedTab);

	/*결과일자 Tab추가로 인하여 추가*/
	if (gSelectedTab != "6"){
		model.setValue("/root/HideData/option", gOption);
		model.setValue("/root/HideData/gubn", gSelectedTab);
	} else {
		model.setValue("/root/HideData/option", gOption);
		model.setValue("/root/HideData/gubn", "1");
	}

    var gPid = model.getValue("/root/MainData/condition/cond1");
    model.setValue("/root/HideData/pid", gPid);

    //조회 기간이 2년을 넘길 경우 메세지 호출 2007-07-04 1:56오후 - 김태범
    var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
    var sGsTodd   = model.getValue("/root/MainData/condition/todt");
    var sDayCount = lvGetDateTerm(sGsFromdd,sGsTodd);

    if (sDayCount > 735) {
        model.alert("조회 기간은 2년을 넘길 수 없습니다. 조회기간을 변경 하시기 바랍니다.");
        model.setFocus("input_frdt");
        return false;
    } else {
        //조회 시작일 종료일 세팅
        model.setValue("/root/HideData/fromdd", sGsFromdd);
        model.setValue("/root/HideData/todd"  , sGsTodd);
    }

    var sGlucose = model.getValue("/root/HideData/glucoseyn");
    if (sGlucose != "Y") model.setValue("/root/HideData/glucoseyn", "N");
    copyNode("/root/HideData", "/root/SendData/Data");
    //fSetFunction("reqGetGyeolGwaJoHoi", "getList", "OAC_GyeolGwaJoHoi_View1");

    //fRequestAction("reqAction", "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "/root/SendData", "/root/MainData/hist");
    //2015.04.30 lsm. 호출방식변경
    model.setValue("/root/SendData/Mode", "reqGetGyeolGwaHistJoHoi");
    if (model.submitInstance("getGyeolGwaHistJoHoi") == true) { 
    model.resetInstanceNode("/root/SendData");
    TFSetMessage("/root/MainData");

    var row = TFGetGridCount("grid_list");

    if (row > 0) {
        fMakeTree("grid_list", 2);

        // 내과만 적용해제 : 2012-01-10
        var usrdtcd = TFGetSessionInfo("userdeptcd");
        //if (fAuthCheck(usrdtcd) == "true") {
    		// 의사확인 색상변경 - 붉은색
    		var fg = model.vsGrid("grid_list");
    		for (var i = 1; i < fg.rows;i++) {
                if (parseInt(fg.TextMatrix(i, 5)) > 20140831) {//20120111 -> 20140831 인증관련 김윤숙 샘 요청.
                           if ((fg.TextMatrix(i, 10) != "-") || (fg.TextMatrix(i, 2) == "-")) { //
                                TFGridFontColor("grid_list", i, 0, i, 0, 1, 255, 0, 0);
					}
				}
    		}
    	//}
        }
    }
    return true;
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과조회(실시내역)
 * @param  : gubn(1:일자별, 2:부서별)
 * @return :
 * @---------------------------------------------------
 */
function fGetSilSi(gubn) {
    //조회 기간이 2년을 넘길 경우 메세지 호출 2007-07-04 1:56오후 - 김태범
    var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
    var sGsTodd   = model.getValue("/root/MainData/condition/todt");
    var sDayCount = lvGetDateTerm(sGsFromdd,sGsTodd);

    if (sDayCount > 735) {
        model.alert("조회 기간은 2년을 넘길 수 없습니다. 조회기간을 변경 하시기 바랍니다.");
        model.setFocus("input_frdt");
        return false;
    } else {
        //조회 시작일 종료일 세팅
        model.setValue("/root/HideData/fromdd", sGsFromdd);
        model.setValue("/root/HideData/todd"  , sGsTodd);
    }

    model.setValue("/root/HideData/gubn", gubn);
    copyNode("/root/HideData", "/root/SendData/Data");
    //fSetFunction("reqGetGyeolGwaJoHoi", "getSilSiList", "OAC_GyeolGwaJoHoi_View3");

    // 2010.12 권한별 신경정신과 접수 기능검사 제외처리
    //fRequestAction("reqAction", "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "/root/SendData", "/root/MainData/silsi");
    if( model.getValue("/root/MainData/limit/grcheck").substr(0,1) == "Y" ){
        //fRequestAction("reqAction", "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "/root/SendData", "/root/MainData/silsi");
        model.setValue("/root/SendData/Mode", "reqGetGyeolGwaSilSiJoHoi_M");
        model.submitInstance("getGyeolGwaSilSiJoHoi_M");
    } else {
        //fRequestAction("reqAction", "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "/root/SendData", "/root/HideData/silsi");
        model.setValue("/root/SendData/Mode", "reqGetGyeolGwaSilSiJoHoi_H");
        if (model.submitInstance("getGyeolGwaSilSiJoHoi_H") == true) {
            for( var i = 1 ; i < getNodesetCnt("/root/HideData/silsi/list")+1; i++){
                if( model.getValue("/root/HideData/silsi/list["+ i + "]/suppdept") == "000268" ){
                    //model.alert(model.getValue("/root/HideData/silsi/list["+ i + "]/suppdept"));
                    model.removenodeset("/root/HideData/silsi/list["+ i + "]");
                }
            }
        }
        model.copyNode("/root/HideData/silsi","/root/MainData/silsi");
    }
    model.removeNodeset("/root/HideData/silsi");
    model.gridRebuild("grid_silsi");
    model.refresh();

    model.resetInstanceNode("/root/SendData");
    TFSetMessage("/root/MainData");

    fSetBackColor("grid_silsi");

    var fg = model.vsGrid("grid_silsi");    //Cell Merge
    for (var i = 0; i < fg.cols; i++) fg.MergeCol(i)= false; //초기화
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1)= true;

    return true;
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과조회(진행내역)
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fGetJinHaeng() {
    //조회 기간이 2년을 넘길 경우 메세지 호출 2007-07-04 1:56오후 - 김태범
    var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
    var sGsTodd   = model.getValue("/root/MainData/condition/todt");
    var sDayCount = lvGetDateTerm(sGsFromdd,sGsTodd);

    if (sDayCount > 735) {
        model.alert("조회 기간은 2년을 넘길 수 없습니다. 조회기간을 변경 하시기 바랍니다.");
        model.setFocus("input_frdt");
        return false;
    } else {
        //조회 시작일 종료일 세팅
        model.setValue("/root/HideData/fromdd", sGsFromdd);
        model.setValue("/root/HideData/todd"  , sGsTodd);
    }

    copyNode("/root/HideData", "/root/SendData/Data");
    //fSetFunction("reqGetGyeolGwaJoHoi", "getJinHaengList", "OAC_GyeolGwaJoHoi_View5");
    model.setValue("/root/SendData/BizName", "getJinHaengList");

    // 2010.12 권한별 신경정신과 접수 기능검사 제외처리
    //fRequestAction("reqAction", "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "/root/SendData", "/root/MainData/jinhaeng");
    if( model.getValue("/root/MainData/limit/grcheck").substr(0,1) == "Y" ){
        //fRequestAction("reqAction", "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "/root/SendData", "/root/MainData/jinhaeng");
        model.setValue("/root/SendData/Mode", "reqGetGyeolGwaJinHaengJoHoi_M");
        model.submitInstance("getGyeolGwaJinHaengJoHoi_M");
    } else{
        //fRequestAction("reqAction", "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "/root/SendData", "/root/HideData/jinhaeng");
        model.setValue("/root/SendData/Mode", "reqGetGyeolGwaJinHaengJoHoi_H");
        if (model.submitInstance("getGyeolGwaJinHaengJoHoi_H") == true) { 
            var list_content =  model.getValue("/root/HideData/jinhaeng/list").split("▩");
            var list = "";

            //model.alert(model.getValue("/root/HideData/jinhaeng/list"));

            list = list_content[0]+"▩";

            for( var i = 1 ; i < list_content.length-1; i++){

                if( list_content[i].substr(list_content[i].length-6,6) == "000268" ){
                    list_content[i] = "";
                    list= list.replace( list.substr(0,1),list.substr(0,1) - 1 );
                }
                else{
                    list = list+list_content[i] + "▩";
                }

            }

            model.setValue("/root/HideData/jinhaeng/list",list);

            model.copyNode("/root/HideData/jinhaeng","/root/MainData/jinhaeng");
        }
        model.removeNodeset("/root/HideData/jinhaeng");
        model.gridRebuild("grid_jinhaeng");
        model.refresh();
    }

    TFclearNodeValue("/root/SendData");
    TFSetMessage("/root/MainData");
    TFGridSort("grid_jinhaeng","0^2▦6^1▦");
    fSetBackColor("grid_jinhaeng");

    var fg = model.vsGrid("grid_jinhaeng"); //Cell Merge
    for (var i = 0; i < fg.cols; i++) fg.MergeCol(i)= false; //초기화
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1)= true;

    return true;
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과조회(실시내역)
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSetBackColor(grid) {
    var fg = model.vsGrid(grid);
    var cur_orddd = "";
    var orddd = "";
    var row = 0;
    var mod = 0;

    for (var i = fg.fixedrows; i < fg.rows; i++) {
        row++;
        if (grid == "grid_jinhaeng") {
            orddd = fg.TextMatrix(i, 0);
        } else if (grid == "grid_silsi") {
            orddd = fg.TextMatrix(i, 0);
        }

        if (cur_orddd != orddd) {
            cur_orddd = orddd;
            mod++;
        }

        if ((mod % 2) == 1) {
            TFGridRowColor(grid, i, 0, i, fg.cols - 1, 1, 238, 238, 255);  //eeeeff
        } else {
            TFGridRowColor(grid, i, 0, i, fg.cols - 1, 1, 255, 255, 255);
        }
    }
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 투약정보조회
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSelectDrug(pGubn) {
    var sIdno = model.getValue("/root/MainData/condition/cond1");

    var sTodt = model.getValue("/root/MainData/condition/todt");
    var sTodt = sTodt.lvToDate("YYYYMMDD");
    if (pGubn == "A") { //최근일자별
        model.enable("input_drugcode", "false"); //투약정보조회
        model.enable("button_drug_help", "false"); //투약정보조회
        model.enable("output8", "false"); //투약정보조회
        model.setValue("/root/HideData/condition/cond4", "");
        model.setValue("/root/HideData/condition/cond5", "");
        model.refresh();
        var sFrdt = new Date(sTodt - (1000 * 60 * 60 * 24 * 30)); //종료일자의 30일전
        var sDrug = "-"; //약품
    } else if (pGubn == "B") { //최근약품별(체크박스만 클릭했을경우)
        model.enable("input_drugcode", "true"); //투약정보조회
        model.enable("button_drug_help", "true"); //투약정보조회
        model.enable("output8", "true"); //투약정보조회
        return;
    } else if (pGubn == "BB") { //최근약품별(약품코드 입력후)
        var sFrdt = new Date(sTodt - (1000 * 60 * 60 * 24 * 150)); //종료일자의 150일전
        //var sDrug = model.getValue("/root/HideData/condition/cond4"); //약품
        var sDrug = "";
        var sGridCount = TFGetGridCount("grid5");
        if (sGridCount < 1) {
            sDrug = "";
        } else {
            for (var j = 1 ; j < sGridCount+1; j++) {
                if (sGridCount == j) {
                    sDrug += model.getValue("/root/HideData/druggirok_durg1["+j+"]/drugcd");
                } else {
                    sDrug += model.getValue("/root/HideData/druggirok_durg1["+j+"]/drugcd")+",";
                }
            }
        }
    } else if (pGubn == "C") { //진단명별
        model.enable("input_drugcode", "false"); //투약정보조회
        model.enable("button_drug_help", "false"); //투약정보조회
        model.enable("output8", "false"); //투약정보조회
        model.setValue("/root/HideData/condition/cond4", "");
        model.setValue("/root/HideData/condition/cond5", "");
        model.refresh();
        var sFrdt = new Date(sTodt - (1000 * 60 * 60 * 24 * 30)); //종료일자의 30일전
        var sDrug = "-"; //약품
    }

    sTodt = sTodt.lvFormat();
    sFrdt = sFrdt.lvFormat();

    var sGubn = "-"; //내원구분
    var sKind = "-"; //제형
    var sDept = "%"; //처방과
    var sDiag = "-"; //진단명

    if (sDrug == ""){
    		sDrug = "-";
    }

    model.setAttribute("sMRR_GRGeomSaek_idno", sIdno);
    model.setAttribute("sMRR_GRGeomSaek_frdt", sFrdt);
    model.setAttribute("sMRR_GRGeomSaek_todt", sTodt);
    model.setAttribute("sMRR_GRGeomSaek_gubn", sGubn);
    model.setAttribute("sMRR_GRGeomSaek_kind", sKind);
    model.setAttribute("sMRR_GRGeomSaek_dept", sDept);
    model.setAttribute("sMRR_GRGeomSaek_drug", sDrug);
    model.setAttribute("sMRR_GRGeomSaek_diag", sDiag);
    model.setAttribute("sMRR_GRGeomSaek_name", model.getValue("/root/MainData/opmcptbsm/name"));
    model.setAttribute("sMRR_GRGeomSaek_age_sex", model.getValue("/root/MainData/opmcptbsm/age_sex"));

    //---- 2010.12 PSY 기록검색권한 제한처리
	  model.setAttribute("MRR_GRGeomSaek_PSYChk", model.getValue("/root/MainData/limit/grcheck"));

    if (fGetIP_xfm() != "MRR_GRGeomSaek_Drug") {

        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Drug.xfm","replace");
    } else {
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            emr_Right.setValue("/root/MainData/condition/cond1", sIdno);
            emr_Right.setValue("/root/MainData/condition/frdt", sFrdt);
            emr_Right.setValue("/root/MainData/condition/todt", sTodt);
            emr_Right.setValue("/root/MainData/condition/gubn", sGubn);
            emr_Right.setValue("/root/MainData/condition/kind", sKind);
            emr_Right.setValue("/root/MainData/condition/dept", sDept);
            emr_Right.setValue("/root/MainData/condition/drug", sDrug);
            emr_Right.setValue("/root/MainData/condition/diag", sDiag);
            emr_Right.javascript.fSelect_drug();
        } else {
            return;
        }
    }
    return;
}

/**
 * @group  :
 * @ver    : 2005.09.13
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 투약기록조회
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSelectDrug2(pGubn) {
    var sDrugFlag = model.getValue("/root/HideData/condition/cond8"); // 약/주사구분
    if (sDrugFlag =="") {
        model.setValue("/root/HideData/condition/cond8","-");
    }

    var sPayFlag = model.getValue("/root/HideData/condition/cond9"); // 급여/비급여구분
    if (sPayFlag =="") {
        model.setValue("/root/HideData/condition/cond9","A");
    }
    var sEarnclssFlag = model.getValue("/root/HideData/condition/cond13");
    if (sEarnclssFlag =="") {
        model.setValue("/root/HideData/condition/cond13","A");
    }

    var sEarnclssFlag2 = model.getValue("/root/HideData/condition/cond14");
    if (sEarnclssFlag2 =="") {
        model.setValue("/root/HideData/condition/cond14","A");
    }

    var sIdno = model.getValue("/root/MainData/condition/cond1");
    var sTodt = model.getValue("/root/MainData/condition/todt"); //sTodt는 요구사항에 의해 종료일에서 시작일로 의미를 변경 2006.02.13 taja78 altered...

    if (sIdno =="") {
        return;
    }

    if (sTodt =="") {
        model.alert("조회 종료일자를 입력하셔야 합니다...");
        return;
    }

    var sGubn = "-";   //내원구분
    var sKind = "-";   //제형
    var sDept = "%";   //처방과
    var sDiag = "-";   //진단명
    var sOrdcd = ''   //처방코드
    var sOrdcdnm = '%';//처방명

    var sFrdt;
    var sOIEflag = "";

    if (pGubn == "A") { //최근일자별
        //model.enable("input_drugcode", "false"); //투약정보조회
        //model.enable("button_drug_help", "false"); //투약정보조회
        model.enable("input2", "false"); //투약정보조회
        model.enable("button4", "false"); //투약정보조회
        model.enable("output8", "false"); //투약정보조회
        model.setValue("/root/HideData/condition/cond11", "");
        model.setValue("/root/HideData/condition/cond12", "");
        model.refresh();
        var iCurrentCur = TFGetGridCount("grid2");
        if (iCurrentCur > 0) {
            var sGridid = "grid2";
            sTodt = model.getValue("/root/MainData/jinryojeongbo_twoyak["+iCurrentCur+"]/orddd").lvToDate("YYYYMMDD");
            var fg = model.vsGrid(sGridid);
            fg.select(iCurrentCur,1);
            fg.TopRow = iCurrentCur;
            sFrdt = sTodt;
            sOIEflag = model.getValue("/root/MainData/jinryojeongbo_twoyak["+iCurrentCur+"]/ordtype");
        } else {
            sFrdt = new Date(sTodt - (1000 * 60 * 60 * 24 * 30)); //종료일자의 30일전
        }

        var sDrug = "-"; //약품
    } else if (pGubn == "B") { //최근약품별(체크박스만 클릭했을경우)
        model.enable("input2" , "true"); //투약정보조회
        model.enable("button4", "true"); //투약정보조회
        model.enable("output8", "true"); //투약정보조회
        var sGridCount = TFGetGridCount("grid1");
        if (sGridCount < 1) {
            return;
        } else {
            for (var j = 1 ; j < sGridCount+1; j++) {
                if (sGridCount == j) {
                    sOrdcd += model.getValue("/root/HideData/druggirok_durg["+j+"]/drugcd");
                } else {
                    sOrdcd += model.getValue("/root/HideData/druggirok_durg["+j+"]/drugcd")+",";
                }
            }
        }
        sTodt = fGetTwoYakGiRokRightTodt();
        sFrdt = sTodt;
    } else if (pGubn == "BB") { //최근약품별(약품코드 입력후)
        var sFrdt = new Date(sTodt - (1000 * 60 * 60 * 24 * 150)); //종료일자의 150일전
        //2005-10-01 여러개의 약품코드 조회가능하게 하기위해 변경....
        //화면 오른쪽의 Acting 일자를 가져오기
        sTodt = fGetTwoYakGiRokRightTodt();
        sFrdt = sTodt;
        var sGridCount = TFGetGridCount("grid1");
        for (var j = 1 ; j < sGridCount+1; j++) {
            if (sGridCount == j) {
                sOrdcd += model.getValue("/root/HideData/druggirok_durg["+j+"]/drugcd");
            } else {
                sOrdcd += model.getValue("/root/HideData/druggirok_durg["+j+"]/drugcd")+",";
            }
        }
    } else if (pGubn == "C") { //진단명별
        model.enable("input_drugcode", "false"); //투약정보조회
        model.enable("button_drug_help", "false"); //투약정보조회
        model.enable("output8", "false"); //투약정보조회
        model.setValue("/root/HideData/condition/cond11", "");
        model.setValue("/root/HideData/condition/cond12", "");
        model.refresh();
        var sDrug = "-"; //약품
    } else if (pGubn =="J") {
        var sFrdt = sTodt;//.lvGetRelativeDate(-3, "M"); //.lvFormat("YYYYMMDD")
        sKind = model.getValue("/root/HideData/condition/cond8");
        if (sKind == "-") {
            sEarnclssFlag  = "A";
            model.setValue("/root/HideData/condition/cond13","A");
            sEarnclssFlag2 = "A";
            model.setValue("/root/HideData/condition/cond14","A");
            fGetEarnclssVauleChaned();
        } else if (sKind =="11") {
            sEarnclssFlag  = "05";
            model.setValue("/root/HideData/condition/cond13","05");
            sEarnclssFlag2 = "A";
            model.setValue("/root/HideData/condition/cond14","A");
            fGetEarnclssVauleChaned();
        } else if (sKind =="12") {
            sEarnclssFlag  = "04";
            model.setValue("/root/HideData/condition/cond13","04");
            sEarnclssFlag2 = "A";
            model.setValue("/root/HideData/condition/cond14","A");
            fGetEarnclssVauleChaned();
        }
        //화면 오른쪽의 Acting 일자를 가져오기
        sTodt = fGetTwoYakGiRokRightTodt();
        sFrdt = sTodt;
    } else if (pGubn =="S") {//2006-12-05 3:20오후 적정진료팀 요구사항 [처방구분 : 수혈] 김태범 작성
        var sFrdt = sTodt;
        sKind = model.getValue("/root/HideData/condition/cond8");
        if (sKind == "-") {
            sEarnclssFlag  = "A";
            model.setValue("/root/HideData/condition/cond13","A");
            sEarnclssFlag2 = "A";
            model.setValue("/root/HideData/condition/cond14","A");
            fGetEarnclssVauleChaned();
        } else if (sKind == "20") {
            sEarnclssFlag  = "17";
            model.setValue("/root/HideData/condition/cond13","17");
            sEarnclssFlag2 = "A";
            model.setValue("/root/HideData/condition/cond14","A");
            fGetEarnclssVauleChaned();
        }
    } else if (pGubn =="G") {
        sPayFlag  = model.getValue("/root/HideData/condition/cond9");
        //제형의 약,주사 선택시 수익분류의 대분류가 같이 setting 된다.
        //화면 오른쪽의 Acting 일자를 가져오기
        sTodt = fGetTwoYakGiRokRightTodt();
        sFrdt = sTodt;
    } else if (pGubn == "E") {
        var sFrdt = sTodt;
        sEarnclssFlag  = model.getValue("/root/HideData/condition/cond13");
        sEarnclssFlag2 = model.getValue("/root/HideData/condition/cond14");
        //화면 오른쪽의 Acting 일자를 가져오기
        sTodt = fGetTwoYakGiRokRightTodt();
        sFrdt = sTodt;
    } else if (pGubn == "DD") { //진료정보 일자별
        if (TFGetGridCount("grid2") > 0) {
            var iCurrentCur = TFGetGridPos("grid2");
            sTodt = model.getValue("/root/MainData/jinryojeongbo_twoyak["+iCurrentCur+"]/orddd").lvToDate("YYYYMMDD");
            sFrdt = sTodt;
        }
    }

    sOIEflag = model.getValue("/root/MainData/jinryojeongbo_twoyak["+iCurrentCur+"]/ordtype");
    sKind = model.getValue("/root/HideData/condition/cond8");

    if (sTodt =="" || sOIEflag == "") {
        if (TFGetGridCount("grid2") > 0) {
            var iCurrentCur = TFGetGridPos("grid2");
            sTodt = model.getValue("/root/MainData/jinryojeongbo_twoyak["+iCurrentCur+"]/orddd").lvToDate("YYYYMMDD");
            sOIEflag = model.getValue("/root/MainData/jinryojeongbo_twoyak["+iCurrentCur+"]/ordtype");
            sFrdt = sTodt;
        } else {
            //화면 오른쪽의 Acting 일자를 가져오기
            sTodt = fGetTwoYakGiRokRightTodt();
            sFrdt = sTodt;
        }
    }

    sTodt = sTodt.lvFormat();
    sFrdt = sFrdt.lvFormat();

    if (sOrdcd =="") {
        sOrdcd = "-";
    }
    sOrdcdnm  = model.getValue("/root/HideData/condition/cond12");

    model.setAttribute("sMRR_GRGeomSaek_idno", sIdno);
    model.setAttribute("sMRR_GRGeomSaek_todt", sTodt); //sTodt는 요구사항에 의해 종료일에서 시작일로 의미를 변경 2006.02.13 taja78 altered...
    model.setAttribute("sMRR_GRGeomSaek_gubn", sGubn);
    model.setAttribute("sMRR_GRGeomSaek_kind", sKind);
    model.setAttribute("sMRR_GRGeomSaek_dept", sDept);
    model.setAttribute("sMRR_GRGeomSaek_drug", sDrug);
    model.setAttribute("sMRR_GRGeomSaek_diflag",sDrugFlag);
    model.setAttribute("sMRR_GRGeomSaek_payflag",sPayFlag);
    model.setAttribute("sMRR_GRGeomSaek_ordcd", sOrdcd);
    model.setAttribute("sMRR_GRGeomSaek_ordcdnm", sOrdcdnm);
    model.setAttribute("sMRR_GRGeomSaek_earnclss1",sEarnclssFlag);
    model.setAttribute("sMRR_GRGeomSaek_earnclss2",sEarnclssFlag2);
    model.setAttribute("sMRR_GRGeomSaek_oieflag",sOIEflag);
    var sTitle = "";

    if (sDrugFlag =="20") {
        sTitle = "수 혈 기 록";
    } else {
        sTitle = "투 약 기 록";
    }

    if (sEarnclssFlag != "A") {
        model.setAttribute("sMRR_GRGeomSaek_earnclss_detail","Y");
    }
    if (fGetIP_xfm() != "MRR_GRGeomSaek_DrugGiRok") {
        model.setAttribute("sMRR_GRGeomSaek_DrugGirok_Title",sTitle);
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_DrugGiRok.xfm","replace");
    } else {
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            emr_Right.setValue("/root/HideData/condition/todd", sTodt);
            emr_Right.setValue("/root/HideData/condition/gubn", sGubn);
            emr_Right.setValue("/root/HideData/condition/kind", sKind);
            emr_Right.setValue("/root/HideData/condition/dept", sDept);
            emr_Right.setValue("/root/HideData/condition/cond1", sIdno);
            emr_Right.setValue("/root/HideData/condition/diflag",sDrugFlag);
            emr_Right.setValue("/root/HideData/condition/payflag",sPayFlag);
            emr_Right.setValue("/root/HideData/condition/ordcd",sOrdcd);
            emr_Right.setValue("/root/HideData/condition/ordcdnm",sOrdcdnm);
            emr_Right.setValue("/root/HideData/condition/earnclssflag",sEarnclssFlag);
            emr_Right.refresh();
            emr_Right.javascript.fGetEarnclssValeChaned();
            emr_Right.setValue("/root/HideData/condition/earnclssflag2",sEarnclssFlag2);
            emr_Right.setValue("/root/HideData/condition/oieflag",sOIEflag);
            emr_Right.setValue("/root/HideData/title",sTitle);
            emr_Right.javascript.fSelect_drug();
        } else {
            //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
            return;
        }
    }
    return;
}

/**
 * @group  :
 * @ver    : 2005.09.14
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 오른쪽 투약기록화면의 Acting일자를 가져오는 함수
 * @param  :
 * @return : Actiong 일자
 * @---------------------------------------------------
 */
function fGetTwoYakGiRokRightTodt() {
    var sTodt ;
    var emr_RightTemp = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_RightTemp) {
        sTodt = emr_RightTemp.getValue("/root/HideData/condition/todd").lvToDate("YYYYMMDD");
        return sTodt;
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }
}

/**
 * @group  :
 * @ver    : 2005.09.14
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 수익대분류값이 변경되었을 경우 이벤트
 * @param  : pSearchYN : 조회여부 Y일경우 조회연결
 * @return : 해당 수익중분류 리스트
 * @---------------------------------------------------
 */
function fGetEarnclssVauleChaned(pSearchFlag) {
    var sCond = model.getValue("/root/HideData/condition/cond13");
    if (sCond == "04") {
        fGetEarnclssCodeList("04");
        model.enable("combo2",true);
    } else if (sCond == "05") {
        fGetEarnclssCodeList("05");
        model.enable("combo2",true);
    } else if (sCond == "17") {
        fGetEarnclssCodeList("17");
        model.enable("combo2",true)
    } else if (sCond == "A") {
    //전체를 선택할 경우 중분류도 전체로 선택한후 고정한다.
        model.setValue("/root/HideData/cond13","A");
        model.setValue("/root/HideData/cond14","A");
        model.enable("combo2",false);
    }
    model.refresh();
    if (pSearchFlag == "Y") {
        //해당 조건에 맞는 조회
        fSelectDrug2("DD");
    }
}

/**
 * @group  :
 * @ver    : 2005.09.14
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 수익중분류
 * @param  : 04 : 투약 , 05: 주사
 * @return : 수익중분류 리스트
 * @---------------------------------------------------
 */
function fGetEarnclssCodeList(pCond) {
    model.removeNodeSet("/root/InitData/earnclss");
    model.refresh();
    TFclearNodeValue("/root/SendData");
    model.setValue("/root/SendData/Mode","reqGetEarnclssCodeList");
    model.setValue("/root/SendData/Data1",pCond);
    model.submitInstance("MRR_reqGetEarnclssCodeList");
    TFInsertInstance("/root/InitData/earnclss",  "earnclss", "cd,nm", "A ,전체");
    model.setValue("/root/HideData/condition/cond14","A");
}

/**
 * @group  :
 * @ver    : 2005.10.01
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 수익중분류
 * @param  : 04 : 투약 , 05: 주사
 * @return : 수익중분류 리스트
 * @---------------------------------------------------
 */
function fOnClickDrugDelBtn() {
    if (TFGetGridCount("grid1")==0) {
        model.alert("삭제할 약품코드가 존재하지 않습니다...");
        return;
    }
    var sCur = TFGetGridPos("grid1");
    if (sCur == 0) {
       alert("삭제할 약품코드를 선택하세요...");
       return;
    }
    model.gridDelete("/root/HideData/druggirok_durg",sCur+"");
    model.gridRebuild("grid1");
    fSelectDrug2("BB");
}

/**
 * @group  :
 * @ver    : 2005.10.01
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 수익중분류
 * @param  : 04 : 투약 , 05: 주사
 * @return : 수익중분류 리스트
 * @---------------------------------------------------
 */
function fOnClickDrugDelBtn1() {
    if (TFGetGridCount("grid5")==0) {
        model.alert("삭제할 약품코드가 존재하지 않습니다...");
        return;
    }
    var sCur = TFGetGridPos("grid5");
    if (sCur == 0) {
       alert("삭제할 약품코드를 선택하세요...");
       return;
    }
    model.gridDelete("/root/HideData/druggirok_durg1",sCur+"");
    model.gridRebuild("grid5");
    fSelectDrug("BB");
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 복사신청
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSelectCopy() {
    model.resetInstanceNode("/root/SendData"); //인자로 들어간 인스턴스 path의 첫번째 depth의 자식 노드들까지의 값을 지우고 두번째 depth의 자식 노드들은 아예 노드 자체를 삭제
    //TFclearNodeValue("/root/SendData"); //여러 depth의 자식 노드들을 가진 노드의 값만을 지움
    model.setValue("/root/SendData/Mode",  "MRR_reqGetMrrfflshhCopy"); // Action Method
    model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1")); //등록번호
    model.setValue("/root/SendData/Data2", model.getValue("/root/MainData/condition/frdt")); //시작일자
    model.setValue("/root/SendData/Data3", model.getValue("/root/MainData/condition/todt")); //종료일자
    model.setValue("/root/SendData/Data4", model.getValue("/root/HideData/condition/cond1")); //일자별,서식지별...
    model.setValue("/root/SendData/Data5", model.getValue("/root/HideData/condition/cond2")); //전체,외래,입원,응급
    model.setValue("/root/SendData/Data6", model.getValue("/root/MainData/condition/cond2")); //진료과

    if (model.submitInstance("MRR_reqGetMrrfflshhCopy")) {
        if (model.getvalue("/root/MainData/mrrfflshh_copy/levl") == "") { //조회실패
            fReset(); //화면정리
            return;
        } else {
            TFSetMessage("/root/MainData/mrrfflshh_copy");
        }
    } else {
        fReset(); //화면정리
        return;
    }
    model.gridrebuild("grid_copy");

    var iRow = TFGetGridCount("grid_copy");
    if (iRow > 0) {
        var sTree = 0; //default로 Tree를 접어달라고 요청(20050602)
        var fg = model.vsGrid("grid_copy");
        for (var i = fg.FixedRows; i < fg.Rows; i++) {
            var sLevl = model.getValue("/root/MainData/mrrfflshh_copy[" + i + "]/levl");
            var sKind = model.getValue("/root/MainData/mrrfflshh_copy[" + i + "]/chrtkind").substr(0,1);
            var sIofg =  model.getValue("/root/MainData/mrrfflshh_copy[" + iRow + "]/seq").substr(0,1);
            var sKind2 = model.getValue("/root/MainData/mrrfflshh_copy[" + i + "]/chrtkind").substr(3,1); //간호기록(FOCUS),정보조사지,TPR 구분을 한다.
            if (sKind == "Z") { //처방
                if (sLevl != "2" && sLevl != "3") {
                    //TFGridRowColor("grid_copy", i, 1, i, 1, 1, 230, 230, 230);
                } else {
                    TFGridFontColor("grid_copy", i, 2, i, 2, 1, 0, 0, 255);
                }
            } else if (sKind == "E" || sKind == "O") { //EMR or OCR
                if (sIofg == "3" || sIofg == "2") { //외래, 응급
                    TFGridFontColor("grid_copy", i, 2, i, 2, 1, 0, 0, 255);
                } else { //입원
                    if (sLevl != "2" && sLevl != "3" && sLevl != "4") {
                        //TFGridRowColor("grid_copy", i, 1, i, 1, 1, 230, 230, 230);
                    } else {
                        TFGridFontColor("grid_copy", i, 2, i, 2, 1, 0, 0, 255);
                    }
                }
            } else if (sKind == "N") { //간호
                if (sKind2 == "2") { //간호기록(FOCUS)
                    if (sLevl != "2" && sLevl != "3" && sLevl != "4") {
                        //TFGridRowColor("grid_copy", i, 1, i, 1, 1, 230, 230, 230);
                    } else {
                        TFGridFontColor("grid_copy", i, 2, i, 2, 1, 0, 0, 255);
                    }
                } else { //TPR, 정보조사지
                    if (sLevl != "4") {
                        //TFGridRowColor("grid_copy", i, 1, i, 1, 1, 230, 230, 230);
                    } else {
                        TFGridFontColor("grid_copy", i, 2, i, 2, 1, 0, 0, 255);
                    }
                }
            } else if (sKind == "A") { //누적
                if (sLevl != "4") {
                    //TFGridRowColor("grid_copy", i, 1, i, 1, 1, 230, 230, 230);
                } else {
                    TFGridFontColor("grid_copy", i, 2, i, 2, 1, 0, 0, 255);
                }
            } else {
                if (sLevl != "4") {
                    //TFGridRowColor("grid_copy", i, 1, i, 1, 1, 230, 230, 230);
                } else {
                    TFGridFontColor("grid_copy", i, 2, i, 2, 1, 0, 0, 255);
                }
            }

            if (sTree == 0) {  //Tree를 접는다. default로 Tree를 접어달라고 요청(20050602)
                fg.IsSubtotal(i) = true;
                fg.RowOutlineLevel(i) = sLevl; //Returns or sets the outline level for a subtotal row.
            } else if (sTree == 1) { //1단계 Tree를 편다.
                if (sIofg == "3" || sIofg == "2") { //외래, 응급
                    fg.IsSubtotal(i) = true;
                    fg.RowOutlineLevel(i) = sLevl; //Returns or sets the outline level for a subtotal row.
                } else { //입원
                    if (sLevl == "1" || sLevl == "2") {
                        fg.IsSubtotal(i) = true;
                        fg.RowOutlineLevel(i) = sLevl; //Returns or sets the outline level for a subtotal row.
                    }
                }
            } else { //Tree를 편다.
                fg.IsSubtotal(i) = true;
                fg.RowOutlineLevel(i) = sLevl; //Returns or sets the outline level for a subtotal row.
            }
        } // for문

        if (sTree == 0) { //Tree를 접는다.
            for (var i = fg.FixedRows; i < fg.Rows; i++) fg.IsCollapsed(i) = 2;
        } else if (sTree == 1) { //1단계 Tree를 편다.
            for (var i = fg.FixedRows; i < fg.Rows; i++) fg.IsCollapsed(i) = 1;
        } else { //Tree를 편다.
            for (var i = fg.FixedRows; i < fg.Rows; i++) fg.IsCollapsed(i) = 0;
        }

        if (sTree == 1) { //전체적인 Tree구조를 다시 생성
            for (var i = fg.FixedRows; i < fg.Rows; i++) {
                var sLevl = model.getValue("/root/MainData/mrrfflshh_copy[" + i + "]/levl");
                fg.IsSubtotal(i) = true;
                fg.RowOutlineLevel(i) = sLevl; //Returns or sets the outline level for a subtotal row.
            } //for
        }

        fg.PicturesOver = false; //글자와 이미지를 겹치지 않게.
        fg.OutlineBar = 4; //+ , - , 사각형 안보이게 하려면 0 으로 세팅
        fg.OutlineCol = 2; //returns or sets the column used to display the outline tree
        fg.Row = 0;
    }

    fWordWrap("grid_copy");
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 서식지상세조회
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSelect_getLoadURL(pControlID, pRow, pLevl, pGrid) {
    if (g_HistoryButton == true) {      // 이력 조회 버튼이 있을 때만 작동하도록
        model.enable("button_history", false);
    }

    // temp, appended by OBH on 2008.02.27
    //TFGetMsgBox(-1, "pControlID = " + pControlID + " : " + "pRow = " + pRow + " : " + "pLevl = " + pLevl + " : " + "pGrid = " + pGrid, "확인", "I", "OK");

    var pNode = "";
    if (pGrid == "grid_copy") {
        pNode = "/root/MainData/mrrfflshh_copy[";
    } else {
        pNode = "/root/MainData/mrrfflshh[";
        iRowCount = 1; //TextEMR 이미지저장
    }

    var sPtno      = model.getValue("/root/MainData/condition/cond1");
    var sChrtkind  = model.getValue(pNode + pRow + "]/chrtkind").substr(0,1);
    var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

    gImageViewYN = model.getValue("/root/HideData/condition/cond16");

    // xfr 파일count 초기화.
    iGRGeomSaek_Print_FileChange_Count = 0;
    model.setAttribute("sMRR_GRGeomSaek_Print_FileChange_Count",iGRGeomSaek_Print_FileChange_Count);
    iMRR_GRGeomSaek_Print_ImageCnt = 0;

    // temp, appended by OBH on 2008.02.27
    //TFGetMsgBox(-1, "fSelect_getLoadURL sPtno = " + sPtno + " : " + "sChrtkind = " + sChrtkind + " : " + "sTextImage = " + sTextImage, "확인", "I", "OK");
	
    // RightView Object 에 브라우저 xfm load(서식지별 조회구현으로 chrtkind로 비교)
    if (sChrtkind == "E") {
        if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") { //이미지 조회인 경우
            fDeleteFile("EMR_");
            fDeleteFile("ReportView_");
            sMRR_GRGeomSaek_FileCnt = 0;
            sMRR_GRGeomSaek_ImageCnt = 1;
            model.removeNodeset("/root/SubData/viewinfo");
        } else if (sTextImage == "T") {    //Text로 조회시 출력은 항상 visible false...
            model.enable("button_print", "false");
        }
        model.setAttribute("MRR_GRGeomSaek_EMR.oudt", "");

        if (pLevl == "2") { //2depth click(입원일자)
            var sEmr = 0;
            var sOcr = 0;
            var fg = model.vsGrid(pControlID);

            var sSeq = model.getValue(pNode + pRow + "]/seq");
            var sSeq = sSeq.lvReplaceWord("|", "▦");
            var sOudt = TFGetMatrixData(sSeq, 0, 5); //퇴원일자
            model.setAttribute("MRR_GRGeomSaek_EMR.oudt", sOudt);

            for (var i = pRow + 1; i < fg.Rows; i++) {
                var sLevl = model.getValue(pNode + i + "]/levl");
                if (sLevl == "1" || sLevl == "2") break;
                if (sLevl == "3") continue;
                var sKind = model.getValue(pNode + i + "]/chrtkind").substr(0,1);
                if (sKind == "E") {
                    sEmr = sEmr + 1;
                } else if (sKind == "O") {
                    sOcr = sOcr + 1;
                }
            } // for문

            //alert(sEmr + "$$" + sOcr);
            if (sEmr != 0 && sOcr == 0) { //Text EMR만 존재
                var iForCnt = 0;
                fCall_EMR_text(pRow, pRow, pNode,pLevl);
            } else if (sEmr == 0 && sOcr != 0) { //영상 EMR만 존재
                fCall_OCR(pRow, pNode, "Y"); //Y:영상만 존재, N:영상과 TextEMR이 같이 존재
            } else if (sEmr != 0 && sOcr != 0) { //일자별,과별,서식지별에서 2단계를 클릭했을때 Text EMR 및 영상 EMR이 같이 존재?경우 사용자에게 선택하도록한다.
                if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") { //bit view로 Text EMR 및 영상 EMR을 같이 볼경우
                    var iForCnt = 0;
                    fCall_EMR_text(pRow, pRow, pNode,pLevl);
                    fCall_OCR(pRow, pNode, "N");
                    for (var i = pRow + 1; i < fg.Rows; i++) {
                        var sLevl = model.getValue(pNode + i + "]/levl");
                        if (sLevl == "1" || sLevl == "2") break;
                        if (sLevl == "3") continue;
                        var sKind = model.getValue(pNode + i + "]/chrtkind").substr(0,1);
                        if (sKind == "O") {
                            //fCall_OCR(pRow, pNode, "N");
                        }
                        iForCnt = iForCnt + 1;
                    }

                } else { //Text로 조회하는 경우
                    var MsgRtn = TFGetMsgBox(-1, "Text EMR과 영상 EMR이 존재합니다.\nText EMR을 조회할려면 '예'버튼을\n영상 EMR을 조회할려면 '아니오'버튼을 선택하십시요.", "확인", "Q", "YNC"); //6:예 7:아니오
                    if (MsgRtn == '6') { //Text EMR
                        var iForCnt = 0;
                        fCall_EMR_text(pRow, pRow, pNode,pLevl); //taja78 add...
                    } else if (MsgRtn == '7') { //영상 EMR
                        fCall_OCR(pRow, pNode, "Y");
                    } else {
                        //return;  //이부분 주석처리 하지않으면 취소선택시 먹통이 되어버림
                    }
                }
            } else {
                TFGetMsgBox(-1, "데이타가 존재하지않습니다.", "확인", "I", "OK");
            }
        } else if (pLevl == "3") { //3depth click(진료일자)
            var sEmr = 0;
            var sOcr = 0;
            var fg = model.vsGrid(pControlID);

            for (var i = pRow; i > 0; i--) {
                var sLevl = model.getValue(pNode + i + "]/levl");
                if (sLevl == "1") {
                    model.setAttribute("MRR_GRGeomSaek_EMR.oudt", "");
                    break;
                }
                if (sLevl == "2") {
                    var sSeq = model.getValue(pNode + i + "]/seq");
                    sSeq = sSeq.lvReplaceWord("|", "▦");
                    var sOudt = TFGetMatrixData(sSeq, 0, 5); //퇴원일자
                    model.setAttribute("MRR_GRGeomSaek_EMR.oudt", sOudt);
                    break;
                }
                if (sLevl == "3" || sLevl == "4") continue;
            } // for문

            for (var i = pRow + 1; i < fg.Rows; i++) {
                var sLevl = model.getValue(pNode + i + "]/levl");
                if (sLevl == "1" || sLevl == "2" || sLevl == "3") break;
                var sKind = model.getValue(pNode + i + "]/chrtkind").substr(0,1);
                if (sKind == "E") {
                    sEmr = sEmr + 1;
                } else if (sKind == "O") {
                    sOcr = sOcr + 1;
                }
            } // for문

            if (sEmr != 0 && sOcr == 0) { //Text EMR만 존재
                fCall_EMR_text(pRow, pRow, pNode,pLevl); //taja78 add... 2006.03.13
            } else if (sEmr == 0 && sOcr != 0) { //영상 EMR만 존재
                fCall_OCR(pRow, pNode, "Y");
                return;    // 2006.05.17 리턴추가안할 경우 밑의 4레벨 이후의 로직을 호출하여 다시 downloadchart() 로직을 실행하여 오류가 발생한다.
            } else if (sEmr != 0 && sOcr != 0) { //일자별,과별,서식지별에서 2단계를 클릭했을때 Text EMR 및 영상 EMR이 같이 존재?경우 사용자에게 선택하도록한다.
                if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") { //bit view로 Text EMR 및 영상 EMR을 같이 볼경우
                    var iForCnt = 0;
                    fCall_EMR_text(pRow, pRow, pNode,pLevl); //taja78 add...
                    fCall_OCR(pRow, pNode, "N");
                    for (var i = pRow + 1; i < fg.Rows; i++) {
                        var sLevl = model.getValue(pNode + i + "]/levl");
                        if (sLevl == "1" || sLevl == "2" || sLevl == "3") break;
                        var sKind = model.getValue(pNode + i + "]/chrtkind").substr(0,1);
                        if (sKind == "E") {
                        } else if (sKind == "O") {
                        }
                        iForCnt = iForCnt + 1;
                    } // for문
                } else {
                    var MsgRtn = TFGetMsgBox(-1, "Text EMR과 영상 EMR이 존재합니다.\nText EMR을 조회할려면 '예'버튼을\n영상 EMR을 조회할려면 '아니오'버튼을 선택하십시요.", "확인", "Q", "YNC"); //6:예 7:아니오
                    if (MsgRtn == '6') { //Text EMR
                        var iForCnt = 0;
                        fCall_EMR_text(pRow, pRow, pNode,pLevl); //taja78 add...
                    } else if (MsgRtn == '7') { //영상 EMR
                        fCall_OCR(pRow, pNode, "Y");
                    } else {
                        //return; //이부분 주석처리 하지않으면 취소선택시 먹통이 되어버림
                    }
                }
            } else {
                TFGetMsgBox(-1, "데이타가 존재하지않습니다.", "확인", "I", "OK");
            }
        } else { //4depth click
            //출력시 입퇴원요약지에 퇴원일자 추가 관련 bagin
            for (var i = pRow; i > 0; i--) {
                var sLevl = model.getValue(pNode + i + "]/levl");
                if (sLevl == "1") {
                    model.setAttribute("MRR_GRGeomSaek_EMR.oudt", "");
                    break;
                }
                if (sLevl == "2") {
                    var sSeq = model.getValue(pNode + i + "]/seq");
                    var sSeq = sSeq.lvReplaceWord("|", "▦");
                    var sOudt = TFGetMatrixData(sSeq, 0, 5); //퇴원일자
                    model.setAttribute("MRR_GRGeomSaek_EMR.oudt", sOudt);
                    break;
                }
                if (sLevl == "3" || sLevl == "4") continue;
            } // for문

            fCall_EMR_text(pRow, 0, pNode);
            iForCnt = 1;
        }

        model.toggle(sMRR_GRGeomSaek_DefaultCase); //object를 숨기기위해서 toggle(MRR_GRGeomSaek_TextSave.js에 있는부분을 가져옴)

        var sPid = model.getValue("/root/MainData/condition/cond1");
        model.setAttribute("MRR_GRGeomSaek_ocr_write_param",sPid);

        // XFM출력일 경우 이미지를 만드는 작업. 2007-01-12 1:39오후, 2007-01-22 1:23오후 프린트일 경우 시점문제로 소스 밑에서 이동시킴.
        if (sTextImage == "P") {
            model.setAttribute("MRR_GRGeomSaek_ReportView.function", "fMakeImage_XFR_Print");
            model.setAttribute("MRR_GRGeomSaek_ReportView.function_param", "EMR_JinRyo");
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
        }

        if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I") { //kkk
        	model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_EMR_text");
        	
            model.setAttribute("MRR_GRGeomSaek_Ocr.sMRR_GRGeomSaek_FileCnt", sMRR_GRGeomSaek_FileCnt);
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                        
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                sTotalKind = sTotalKind + "▩";
              
                emr_Right.javascript.fViewerSetting(sTotalKind, "EMR_");
              
                //return;
                fWaitBegin("이미지 Loading중...");
                emr_Right.javascript.downloadChart();
                fWaitEnd();
                emr_Right.javascript.setupChart("ChartView"); //연속보기로 설정
                
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "");
            } else {
                //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
                return;
            }
         } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "C") { //kkk
        	model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_EMR_text");
            model.setAttribute("MRR_GRGeomSaek_Ocr.sMRR_GRGeomSaek_FileCnt", sMRR_GRGeomSaek_FileCnt);
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");

            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                sTotalKind = sTotalKind + "▩";
                emr_Right.javascript.fViewerSetting(sTotalKind, "EMR_");

                fWaitBegin("이미지 Loading중...");
                emr_Right.javascript.downloadChart();
                fWaitEnd();

                emr_Right.javascript.setupChart("ChartView"); //연속보기로 설정
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "");
            } else {
                //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
                return;
            }
        }
    } else if (sChrtkind == "O") {
        fCall_OCR(pRow, pNode, "Y");
    } else if (sChrtkind == "Z") {		
        fCall_OCS(pRow, pNode);		
    } else if (sChrtkind == "N") {
        var sChrtkind2 = model.getValue(pNode + pRow + "]/chrtkind").substr(3,1); //간호기록(FOCUS),정보조사지,TPR 구분을 한다
        var sGubn = model.getValue(pNode + pRow + "]/chrtkind");
        var sParm = sGubn.lvReplaceWord("|", "▦");
        var sIndt = TFGetMatrixData(sParm, 0, 2);
        var sDate = TFGetMatrixData(sParm, 0, 3);
        var sItemCd = TFGetMatrixData(sParm, 0, 4);
        var sOptrustdt = TFGetMatrixData(sParm, 0, 5);
        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

        // appended by OBH on 2009.04.16
        // 현재 라인 부터
        // "} else if (sChrtkind2 == "2") { //간호기록(FOCUS)"이전 까지 로직은
        // 간호에서 사용하는 카테터기록을 따로 보여 주시위해 추가한 로직, 적진 강정규 요청사항.
        // 아래의 두줄이 원래의 로직임.
        //if (sChrtkind2 == "1") { //정보조사지
        //  fCall_NUR1(pRow, pNode); //정보조사지는 목록만 보여준다.
        var sChrtkind3  = model.getValue(pNode + pRow + "]/chrtkind");
        var sChrtkind4  = model.getValue(pNode + pRow + "]/chrtkind").substr(1,4);
        //appended by OBH on 2009.08.17
        //model.alert("sChrtkind2 := " + sChrtkind2);
        //model.alert("sChrtkind3 := " + sChrtkind3);
        //model.alert("sChrtkind4 := " + sChrtkind4);
        //model.alert(sChrtkind3.substr(25,10));
        //클릭한 범위 안에서 입원일과 퇴원일을 찾는다.
        for (var i = pRow; i > 0; i--) {
            var sLevl = model.getValue(pNode + i + "]/levl");

            if (sLevl == "2") {
                var sSeqCatheter = model.getValue(pNode + i + "]/seq");
                var sSeqCatheter2 = sSeqCatheter.lvReplaceWord("|", "▦");
                var sendCatheterIndate = TFGetMatrixData(sSeqCatheter2, 0, 4); //입원일자
                var sendCatheterOutdate = TFGetMatrixData(sSeqCatheter2, 0, 5); //퇴원일자

                //model.alert(sSeqCatheter);
                //model.alert(sSeqCatheter2);
                //model.alert(sendCatheterIndate);
                //model.alert(sendCatheterOutdate);
                break;
            }
            if (sLevl == "1" || sLevl == "3" || sLevl == "4") continue;
        }

        if (sChrtkind3.substr(3,1) == "1") {
            var sendCatheter = "Y"+"▦"+sPtno+"▦"+sendCatheterIndate+"▦"+"I";
        } else if (sChrtkind3.substr(3,1) == "2") {
            var sendCatheter = "Y"+"▦"+sPtno+"▦"+sendCatheterIndate+"▦"+"I";
        } else {
            var sendCatheter = "Y"+"▦"+sPtno+"▦"+sendCatheterIndate+"▦"+"O";
        }
        //model.alert(sChrtkind2);
        //model.alert(sChrtkind3);
        //model.alert(sChrtkind3.substr(16,9));
        //model.alert(sChrtkind4);
        //model.alert(sendCatheter);
        //model.alert(sChrtkind3.substr(25,10));

        if (sChrtkind2 == "1") { //정보조사지
            if (sChrtkind3.substr(25,10) == "INC0000003")   //카테터기록지
            {
                fCall_NUR1(pRow, pNode); //카테터기록
                // model.setAttribute("MRN_CatheterGwanRi_parm", sendCatheter);
                TFSetGlobalAttribute("MRN_CatheterGwanRi_parm", sendCatheter);
                TFshowModal(null, null, "1", "/emr/ganhogirokweb/xfm/MRN_CatheterGwanRi.xfm", "-", 1220, 798, 0, 0, false, false);
                // model.setAttribute("MRN_CatheterGwanRi_parm", "");
                TFSetGlobalAttribute("MRN_CatheterGwanRi_parm", "");
            }
            // appended by OBH on 2009.08.17
            // 간호기록검색중 중환자실 간호기록이 기록지의 특성상 한 개의 그리드에 너무많은 텍스트가 들어가
            // 기록이 모두 보이지 않는 현상이 발생하고 있음.
            // 이를 수정하기위하여 한개의 그리드를 여러개의 그리드로 분리하는 로직을 작성하기 위해서 아래와 같은
            // 함수를 사용하여 처리하였슴.
            else if (sChrtkind3.substr(25,10) == "INS0000009") //중환자실간호기록
            {
                //fCall_NUR4(pRow, pNode);
                //fCall_NUR1(pRow, pNode);
                fCall_NUR5(pRow, pNode);
            }
            else if (sChrtkind3.substr(16,9) == "중환자실 간호기록") //중환자실간호기록
            {
                fCall_NUR5(pRow, pNode);
            }
            // Append terminated, 2009.08.19, OBH
            else
            {
                fCall_NUR1(pRow, pNode); //정보조사지는 목록만 보여준다.
            }
        } else if (sChrtkind2 == "2") { //간호기록(FOCUS)
            fCall_NUR2(pRow, pNode);
        } else if (sChrtkind2 == "3") { //TPR(임상관찰)
            model.setAttribute("sMRN_GISGCGiRok_idno", sPtno);
            model.setAttribute("sMRN_GISGCGiRok_iwdt", sIndt);
            model.setAttribute("sMRN_GISGCGiRok_johoidd", sDate);
            model.setAttribute("sMRN_GISGCGiRok_johoi", "Y");
            if (sAlert) model.alert("임상 sMRN_GISGCGiRok_idno["+sPtno+"] sMRN_GISGCGiRok_iwdt["+sIndt+"]");
            TFshowModal(null, null, "1", "/emr/ganhogirokweb/xfm/MRN_GISGCGiRok.xfm", "-", 1220, 798, 0, 0, false, false);
            model.setAttribute("sMRN_GISGCGiRok_idno", "");
            model.setAttribute("sMRN_GISGCGiRok_iwdt", "");
            model.setAttribute("sMRN_GISGCGiRok_johoidd", "");
            model.setAttribute("sMRN_GISGCGiRok_johoi", "");
        } else if (sChrtkind2 == "4") { //TPR(중환자 임상관찰)
            model.setAttribute("sMRN_GISGCGiRok_idno", sPtno);
            model.setAttribute("sMRN_GISGCGiRok_iwdt", sIndt);
            model.setAttribute("sMRN_GISGCGiRok_johoidd", sDate);
            model.setAttribute("sMRN_GISGCGiRok_johoi", "Y");
            if (sAlert) model.alert("중환자 sMRN_GISGCGiRok_idno["+sPtno+"] sMRN_GISGCGiRok_iwdt["+sIndt+"]");
            TFshowModal(null, null, "1", "/emr/ganhogirokweb/xfm/MRN_GJHJGCGiRok.xfm", "-", 1220, 798, 0, 0, false, false);
            model.setAttribute("sMRN_GISGCGiRok_idno", "");
            model.setAttribute("sMRN_GISGCGiRok_iwdt", "");
            model.setAttribute("sMRN_GISGCGiRok_johoidd", "");
            model.setAttribute("sMRN_GISGCGiRok_johoi", "");
        } else if (sChrtkind2 == "5") { //마취기록
            if (sTextImage == "T") {
                model.setAttribute("GRGeomSaek","Y");
                model.setAttribute("sMRN_CAnesthesiaGiRok_idno", sPtno);
                model.setAttribute("sMRN_CAnesthesiaGiRok_iwdt", sIndt);
                model.setAttribute("sMRN_CAnesthesiaGiRok_recpcl", sItemCd);
                model.setAttribute("sMRN_CAnesthesiaGiRok_optrustdt", sOptrustdt); //추가(20050725)
				model.setAttribute("sMRN_CAnesthesiaGiRok_kornm", model.getValue("/root/MainData/opmcptbsm/name")); //추가(20140114 jung12)// 수술기록 환자이름
                model.setAttribute("sMRN_CAnesthesiaGiRok_johoi", "Y");
                if (sAlert) model.alert("마취기록 sMRN_GISGCGiRok_idno["+sPtno+"] sMRN_GISGCGiRok_iwdt["+sIndt+"] sMRN_CAnesthesiaGiRok_recpcl["+sItemCd+"]");
                TFshowModal(null, null, "1", "/emr/girokweb/xfm/MRD_CAnesthesiaGiRok.xfm", "-", 1220, 798, 0, 0, false, false, false); //2: modaless, 1: modal
                model.setAttribute("sMRN_CAnesthesiaGiRok_idno", "");
                model.setAttribute("sMRN_CAnesthesiaGiRok_iwdt", "");
                model.setAttribute("sMRN_CAnesthesiaGiRok_recpcl", "");
                model.setAttribute("sMRN_CAnesthesiaGiRok_optrustdt", ""); //추가(20050725)
                model.setAttribute("sMRN_CAnesthesiaGiRok_johoi", "");
            } else {
                fCall_NUR3(pRow, pNode);
            }
            
        } else if (sChrtkind2 == "6") { //20181128 doublej 외래마취기록
            if (sTextImage == "T") {
                model.setAttribute("GRGeomSaek","Y");
                model.setAttribute("sMRN_GISGCGiRok_idno", sPtno);
                model.setAttribute("sMRN_GISGCGiRok_iwdt", sIndt);
                model.setAttribute("sMRN_GISGCGiRok_johoidd", sDate);
                model.setAttribute("sMRN_GISGCGiRok_johoi", "Y");
                model.setAttribute("sMRN_GISGCGiRok_recpcl", "LIT");
                //model.setAttribute("sMRN_CAnesthesiaGiRok_optrustdt", sOptrustdt); //추가(20050725)
				model.setAttribute("sMRN_GISGCGiRok_kornm", model.getValue("/root/MainData/opmcptbsm/name")); //추가(20140114 jung12)// 수술기록 환자이름
                model.setAttribute("sMRN_GISGCGiRok_johoi", "Y");
                if (sAlert) model.alert("LIT/KIT기록 sMRN_GISGCGiRok_idno["+sPtno+"] sMRN_GISGCGiRok_iwdt["+sIndt+"] sMRN_GISGCGiRok_recpcl["+sItemCd+"]");
                TFshowModal(null, null, "1", "/emr/ganhogirokweb/xfm/MRN_CAnesthesiaGiRok_O.xfm", "-", 1220, 798, 0, 0, false, false); //2: modaless, 1: modal
                model.setAttribute("sMRN_GISGCGiRok_idno", "");
                model.setAttribute("sMRN_GISGCGiRok_iwdt", "");
                model.setAttribute("sMRN_GISGCGiRok_recpcl", "");
                //model.setAttribute("sMRN_GISGCGiRok_optrustdt", ""); //추가(20050725)
                model.setAttribute("sMRN_GISGCGiRok_johoi", "");
            } else {
                fCall_NUR3(pRow, pNode);
            }    
            
            
            
        } else { //내원구분 또는 기록일자를 선택했을경우
            return;
        }
    } else if (sChrtkind == "R") { //
        //alert("=== 지원부서 결과 미리보기 화면입니다.. ===");
    } else if (sChrtkind == "P") { //제증명 
        fCall_PM(pRow, pNode);
    } else if (sChrtkind == "A") { //누적
        fCall_ACC(pRow, pNode);
    } else if (sChrtkind == "D") { //투약정보
        //fCall_DRUG(pRow, pNode);
    } else if (sChrtkind == "V") { //마취전 환자 방문기록
		var levl = model.getValue("/root/MainData/mrrfflshh["+TFGetGridPos("grid_main")+"]/levl");
        if (levl == "4") {
            fDeleteFile("EMR_");
            fGetAnstPatientVist();
        }
	    /*
		// 2017.10 마취방문기록지 임시 조회
			var sGubn = model.getValue(pNode + pRow + "]/seq");
			var sParm = sGubn.lvReplaceWord("|", "▦");
			var sIndt = TFGetMatrixData(sParm, 0, 2);

            model.setAttribute("GRGeomSaek","Y");
            model.setAttribute("sMRN_CAnesthesiaGiRok_idno", sPtno);
            model.setAttribute("sMRN_CAnesthesiaGiRok_iwdt", sIndt);
            model.setAttribute("sMRN_CAnesthesiaGiRok_recpcl", "");
            model.setAttribute("sMRN_CAnesthesiaGiRok_optrustdt", "");
		    model.setAttribute("sMRN_CAnesthesiaGiRok_kornm", model.getValue("/root/MainData/opmcptbsm/name")); //추가(20140114 jung12)// 수술기록 환자이름
            model.setAttribute("sMRN_CAnesthesiaGiRok_johoi", "Y");
            if (sAlert) model.alert("마취방문기록 sMRN_GISGCGiRok_idno["+sPtno+"] sMRN_GISGCGiRok_iwdt["+sIndt+"] sMRN_CAnesthesiaGiRok_recpcl["+sItemCd+"]");
            TFshowModal(null, null, "1", "/emr/ganhogirokweb/xfm/MRD_MaBeforeHJBMGiRok.xfm", "-", 1220, 798, 0, 0, false, false, true);
            model.setAttribute("sMRN_CAnesthesiaGiRok_idno", "");
            model.setAttribute("sMRN_CAnesthesiaGiRok_iwdt", "");
            model.setAttribute("sMRN_CAnesthesiaGiRok_recpcl", "");
            model.setAttribute("sMRN_CAnesthesiaGiRok_optrustdt", ""); //추가(20050725)
            model.setAttribute("sMRN_CAnesthesiaGiRok_johoi", "");
		*/
    } else {
        TFGetMsgBox(-1, "구분오류입니다.", "확인", "I", "OK");
    }
	
    //조회가 끝났을 경우 플래그 세팅.
    sTempSearchFlag = "N";
    model.setAttribute("girokgeomsaek_searchflag_param","N");
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 서식지상세조회(MRD_GRJoHoi.js(fSelectGiRok) 참조)
 *               20060306 pLevl 추가 by taja78
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCall_EMR_text(pRow, pForCnt, pNode, pLevl) {
    var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분
    if (sTextImage == "T") { //
        if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") { //시점문제로 앞으로 가져옴
            model.setAttribute("MRR_GRGeomSaek_Grid.function", "clear");
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
        }
    }

    var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
    var sParm = sChrtkind.lvReplaceWord("|", "▦");
    var sEmr = TFGetMatrixData(sParm, 0, 0); //EMR, OCR 구분
    var sDpfg = TFGetMatrixData(sParm, 0, 1); //진료기록,간호기록 구분
    var sIdno = TFGetMatrixData(sParm, 0, 2);
    var sPtdt = TFGetMatrixData(sParm, 0, 3);
    var sIofg = TFGetMatrixData(sParm, 0, 4);
    var sChrt = TFGetMatrixData(sParm, 0, 5);
    var sDate = TFGetMatrixData(sParm, 0, 6);
    var sDept = TFGetMatrixData(sParm, 0, 7);
    var sDoct = TFGetMatrixData(sParm, 0, 8);
    var sChrtNm = TFGetMatrixData(sParm, 0, 9);
    var sDeptNm = TFGetMatrixData(sParm, 0, 10);
    var sDoctNm = TFGetMatrixData(sParm, 0, 11);
    var sOtdt = TFGetMatrixData(sParm, 0, 12); //수술일시
    var sExdp = TFGetMatrixData(sParm, 0, 13); //수신부서
    var sExdc = TFGetMatrixData(sParm, 0, 14); //수신의사
    var sExdt = TFGetMatrixData(sParm, 0, 15); //수신일시

    var sTgerchrt = TFGetMatrixData(sParm, 0, 16); //타괴회신시 의뢰내용
    var sTgerdate = TFGetMatrixData(sParm, 0, 17); //타괴회신시 의뢰내용
    var sTgerdept = TFGetMatrixData(sParm, 0, 18); //타괴회신시 의뢰내용
    var sTgerdoct = TFGetMatrixData(sParm, 0, 19); //타괴회신시 의뢰내용
    var sTgerexfg = TFGetMatrixData(sParm, 0, 20); //타괴회신시 의뢰내용

    TFclearNodeValue("/root/SendData"); //이전의 서버 호출에 사용된 정보 지움
    model.removeNodeset("/root/HideData/savedata");
    model.removeNodeset("/root/HideData/savedatas");
    model.removeNodeset("root/HideData/receiveData/MainData");

    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        emr_Right.removeNodeSet("/root/SubData/viewinfo");
        model.removeNodeset("/root/SubData");
        emr_Right.gridRebuild("grid_view");
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }

    //temp,OBH,2009.08.18
    //model.alert("sChrt := " + sChrt);

    TFclearNodeValue("/root/MainData/mrdrptchh"); //추가(20050725)
    TFclearNodeValue("/root/MainData/mrdrptith"); //추가(20050725)
    TFclearNodeValue("/root/MainData/otherinfo"); //추가(20050725)

    model.setValue("/root/InitData/mrdrptchh/ptchptno", sIdno);
    model.setValue("/root/InitData/mrdrptchh/ptchptdt", sPtdt);
    model.setValue("/root/InitData/mrdrptchh/ptchiofg", sIofg);
    model.setValue("/root/InitData/mrdrptchh/ptchchrt", sChrt);
    model.setValue("/root/InitData/mrdrptchh/chrtname", "");
    model.setValue("/root/InitData/mrdrptchh/ptchdate", sDate);
    model.setValue("/root/InitData/mrdrptchh/ptchdept", sDept);
    model.setValue("/root/InitData/mrdrptchh/deptname", "");
    model.setValue("/root/InitData/mrdrptchh/ptchdoct", sDoct);
    model.setValue("/root/InitData/mrdrptchh/doctname", "");
    model.setValue("/root/InitData/mrdrptchh/ptchsign", "");
    model.setValue("/root/InitData/mrdrptchh/ptchcsyn", "");
    model.setValue("/root/InitData/mrdrptchh/ptchdefy", "");
    model.setValue("/root/InitData/mrdrptchh/ptchrgst", "");
    model.setValue("/root/InitData/mrdrptchh/rgstname", "");
    model.setValue("/root/InitData/mrdrptchh/ptchrgdt", "");
    model.setValue("/root/InitData/mrdrptchh/ptchuser", "");
    model.setValue("/root/InitData/mrdrptchh/ptchupdt", "");
    model.setValue("/root/InitData/mrdrptchh/chrtxfrm", "");
    model.setValue("/root/InitData/mrdrptchh/ptchacyn", "");
    model.setValue("/root/InitData/mrdrptchh/ptchotdt", sOtdt);
    model.setValue("/root/InitData/mrdrptchh/ptchexdp", sExdp);
    model.setValue("/root/InitData/mrdrptchh/ptchexdc", sExdc);
    model.setValue("/root/InitData/mrdrptchh/tgerchrt", sTgerchrt);
    model.setValue("/root/InitData/mrdrptchh/tgerdate", sTgerdate);
    model.setValue("/root/InitData/mrdrptchh/tgerdept", sTgerdept);
    model.setValue("/root/InitData/mrdrptchh/tgerdoct", sTgerdoct);
    model.setValue("/root/InitData/mrdrptchh/tgerexfg", sTgerexfg);
    model.setValue("/root/InitData/mrdrptchh/ptchexdt", sExdt);
    model.setValue("/root/InitData/mrdrptchh/tempflag", "");
    model.setValue("/root/InitData/mrdrptchh/signrgst", "");
	model.setValue("/root/SendData/psychk", model.getValue("/root/MainData/limit/grcheck"));
    model.setValue("/root/SendData/anecheck", model.getValue("/root/MainData/limit/anecheck"));

    // 2-1. 지원기록목록 관련처리 작업
    if (sChrt.substr(1, 2) != "LT") {
        model.setValue("/root/InitData/mrdrptchh/chart_type_view", "LY"); // 지원기록만 조회
    } else {
        model.setValue("/root/InitData/mrdrptchh/chart_type_view", "LN"); // 지원기록만 빼고 조회
    }

    var sDateFlag = model.getValue("/root/HideData/condition/cond1");
    if (pLevl == "2") {
        var sTempDate = fGet_FrDateToDate(pRow, pLevl, sDateFlag).split("▦");
        model.setValue("/root/InitData/mrdrptchh/ptchfrdt", sTempDate[0]);
        model.setValue("/root/InitData/mrdrptchh/ptchtodt", sTempDate[1]);

        model.setValue("/root/SendData/Mode","reqGetGiRokViewAll");
        copyNode("/root/InitData/mrdrptchh","/root/SendData/Data1");
        var sJiWeonGiRokFlag = model.getAttribute("MRR_GRGeomSaek_Modal.chart_type_view");
        model.setValue("/root/SendData/Data2", sJiWeonGiRokFlag);

        if (model.submitInstance("reqGetGiRokViewAll")) {
            iGiRokGeomSakModal_GridCnt = 1;
            f_ViewingProcess(pRow, pForCnt, pNode, pLevl);
        } else {
            false;
        }
    } else if (pLevl == "3") {
        var sTempDate = fGet_FrDateToDate(pRow, pLevl, sDateFlag).split("▦");

        model.setValue("/root/InitData/mrdrptchh/ptchfrdt", sTempDate[0]);
        model.setValue("/root/InitData/mrdrptchh/ptchtodt", sTempDate[1]);

        //외래이면서 서식지별일 경우에는 내원일자가 없기 때문에 가장 최근의 기록일자를 넘긴다.
        if (model.getValue("/root/HideData/condition/cond1")=="form" && sIofg != "I") {
            model.setValue("/root/InitData/mrdrptchh/ptchptdt", sTempDate[1]);
        }

        //입원이면서 서식지별일 경우에 내원일자 세팅...
        if (model.getValue("/root/HideData/condition/cond1")=="form" && sIofg == "I") {
            for (var k = pRow-1; k >= 0; k--) {
                var sLevl = model.getValue("/root/MainData/mrrfflshh[" + k + "]/levl");
                if (sLevl == "2") {
                    model.setValue("/root/InitData/mrdrptchh/ptchptdt",model.getValue("/root/MainData/mrrfflshh[" + k + "]/chrtnm").lvReplaceWord("/","").substr(0,8));
                    break;
                }
            }
        }

        model.setValue("/root/SendData/Mode","reqGetGiRokViewAll");

        copyNode("/root/InitData/mrdrptchh","/root/SendData/Data1");
        var sJiWeonGiRokFlag = model.getAttribute("MRR_GRGeomSaek_Modal.chart_type_view");
        model.setValue("/root/SendData/Data2", sJiWeonGiRokFlag);

        if (model.submitInstance("reqGetGiRokViewAll")) {
            iGiRokGeomSakModal_GridCnt = 1;// 기록검색_grid 의 grid_view에 insert 될 count 수 초기화
            f_ViewingProcess(pRow, pForCnt, pNode, pLevl);
        } else {
            false;
        }
    } else { // levl 4
        //alert("["+model.getValue("/root/InitData/mrdrptchh/ptchptno")+"]");
        model.setValue("/root/InitData/mrdrptchh/ptchfrdt", "");
        model.setValue("/root/InitData/mrdrptchh/ptchtodt", "");

        model.setValue("/root/SendData/Mode","reqGetGiRokViewAll");
        copyNode("/root/InitData/mrdrptchh","/root/SendData/Data1");

        var sJiWeonGiRokFlag = model.getAttribute("MRR_GRGeomSaek_Modal.chart_type_view");
        model.setValue("/root/SendData/Data2", sJiWeonGiRokFlag);

        if (model.submitInstance("reqGetGiRokViewAll")) {
            //appended by OBH on 2009.08.18
            //model.alert("pRowp = " + pRow);
            //model.alert("pForCnt = " + pForCnt);
            //model.alert("pNode = " + pNode);
            //model.alert("pLevl = " + pLevl);
            iGiRokGeomSakModal_GridCnt = 1;// 기록검색_grid 의 grid_view에 insert 될 count 수 초기화
            f_ViewingProcess(pRow, pForCnt, pNode, pLevl);
        } else {
            false;
        }
    }

    //텍스트보기로 조회할 경우 조회 sTempSearchFlag를 N로 변경한다.
    var sTextImgFlag = model.getValue("/root/HideData/condition/cond6");
    if (sTextImgFlag == "T") {
        sTempSearchFlag = "N";
        model.setAttribute("girokgeomsaek_searchflag_param","N"); //전역변수 설정.

        // 이력 조회 버튼이 있고, 표시 차트가 1개 이며, 확정일 때만 버튼 활성화
        if (g_HistoryButton == true) {
            if (getNodesetCnt("/root/HideData/savedatas/savedata") == 1) {
                if (model.getValue("/root/HideData/savedatas/savedata/mrdrptchh/ptchsign") == "C") {
                    model.enable("button_history", true);
                }
            }
        }
    }
}

/**
 * @group  :
 * @ver    : 2005-10-06 3:06오후
 * @by     : 현대정보기술 의료기술팀, 꿈꾸는 HIS 프로그래머 이제관 (je2kwan2@hanmir.com)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function f_ViewingProcess(pRow, pForCnt, pNode, pLevl) {
    var m_XPath = "/root/HideData/savedata";
    var m_node = findNode("/root/HideData/savedatas");
    var m_saveLists = m_node.childNodes;
    var sTextImgFlag = model.getValue("/root/HideData/condition/cond6");
    var fg;

    // 2009.09.02
    //model.alert("sTextImgFlag := " + sTextImgFlag + "sTextImgFlag := " + sTextImgFlag);

    if (sTextImgFlag == "T" || sTextImgFlag == "P") { //2006-12-29 3:11오후 프린트 관련 추가(테스트...)
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            if (sTextImgFlag == "T") {
                fg = emr_Right.vsGrid("grid_view");
            }

            for (var iCnt = 0; iCnt < m_saveLists.length; iCnt++) {
                var m_SaveList  = m_saveLists.nextNode();
                model.removeNodeset(m_XPath);
                model.makenode(m_XPath);

                var m_SaveJob    = findNode(m_XPath);
                var m_SaveChilds = m_SaveList.childNodes;

                for (var jCnt = 0; jCnt < m_SaveChilds.length; jCnt++) {
                    var m_SaveChild = m_SaveChilds.item(jCnt);
                    m_SaveJob.appendChild(m_SaveChild.cloneNode(true));
                }

                var m_chartcode = model.getValue(m_XPath + "/mrdrptchh/ptchchrt");
                f_ViewingDetailProcess(m_chartcode, fg, sTextImgFlag);
            }

            if (sTextImgFlag == "T") {
                emr_Right.gridRebuild("grid_view");
                fMakeImage_girokView(fg, sTextImgFlag);

                fg.WordWrap = true;
                fg.AutoSizeMode  = 1;
                fg.AutoSize(0, fg.Cols -1);
            }
            fCommon_ShowMessage("기록세부내역 조회가 " + iCnt + "건 되었습니다");
        } else {
            //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
            return;
        }
    } else {
        var iDataCnt = 0;
        var nodeList = findNodeset("/root/HideData/savedatas/savedata");

        if (nodeList == null) {
            iDataCnt = 0;
        } else {
            iDataCnt = nodeList.length;
        }

        for (var i = 1; i < iDataCnt+1; i++) {
            var sCurrentChrt = model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchchrt");
            model.copyNode("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh","/root/HideData/Data1");
            model.copyNode("/root/HideData/savedatas/savedata["+i+"]/mrdrptith","/root/HideData/Data2");
            model.removeNodeset("/root/HideData/Data3");
            model.makenode("/root/HideData/Data3");
            model.removeNodeset("/root/HideData/Data4");
            model.makenode("/root/HideData/Data4");
            model.removeNodeset("/root/HideData/Data5");
            model.makenode("/root/HideData/Data5");
            if (findNode("/root/HideData/savedatas/savedata["+i+"]/otherinfo") != null) {
                if (sCurrentChrt == "IDC0000006" || sCurrentChrt == "IDC0000007" || sCurrentChrt == "IDC0000004") { //입퇴원요약조회, 단기입퇴원기록
                    if (findNode("/root/HideData/savedatas/savedata["+i+"]/otherinfo/baseinfo") != null) {
                        model.copyNode("/root/HideData/savedatas/savedata["+i+"]/otherinfo/baseinfo","/root/HideData/Data5"); //기본정보를 뒤로 빼기위해서 따로 저장
                        model.removeNode("/root/HideData/savedatas/savedata["+i+"]/otherinfo/baseinfo");
                        model.copyNode("/root/HideData/savedatas/savedata["+i+"]/otherinfo","/root/HideData/Data3");

                    } else {
                        model.copyNode("/root/HideData/savedatas/savedata["+i+"]/otherinfo","/root/HideData/Data3");
                    }
                } else {
                    model.copyNode("/root/HideData/savedatas/savedata["+i+"]/otherinfo","/root/HideData/Data3");
                }
            }

            var m_ptchchrt1 = model.getValue("/root/HideData/savedatas/savedata[" + i + "]/mrdrptchh/ptchchrt");
            var m_ptchchrt2 = model.getValue("/root/HideData/savedata/mrdrptchh/ptchchrt");
            if (m_ptchchrt1 == "CDC0000009" || m_ptchchrt1 == "CLX0000001"
            	|| m_ptchchrt1 == "OLX0000001"
            	|| m_ptchchrt1 == "OLX0000002"
        		|| m_ptchchrt1 == "OLX0000003"
        		|| m_ptchchrt1 == "OLX0000004"
        		|| m_ptchchrt1 == "OLX0000005"
        		|| m_ptchchrt1 == "OLX0000007") {  // 타과회신
                model.copyNode("/root/HideData/savedatas/savedata["+i+"]/otherinfo/tger_mrdrptith","/root/HideData/Data4");
            } else if (fEMR_ChrtChk_ERI(m_ptchchrt2) == "Y") { //타과의뢰
                model.copyNode("/root/HideData/savedatas/savedata["+i+"]/otherinfo/tghs_mrdrptith","/root/HideData/Data4");
            }
            // value 없는 node 삭제
            var node   = findNode("/root/HideData/Data1");
            var node2  = findNode("/root/HideData/Data2");
            var node3  = findNode("/root/HideData/Data3");
            var node4  = findNode("/root/HideData/Data4");
            var node5  = findNode("/root/HideData/Data5");

            var node2path = "/root/HideData/Data2";
            var node3path = "/root/HideData/Data3";
            var node4path = "/root/HideData/Data4";
            var node5path = "/root/HideData/Data5";

            if (node == null || node2 == null) {
                return;
            }
            TFRemoveNoChild(node2);
            TFRemoveNoChild(node3);
            TFRemoveNoChild(node4);
            TFRemoveNoChild(node5);

            //alert(sGridBrowser + ":" + sMRR_GRGeomSaek_ImageView + ":" + sTextImgFlag);
            if (sGridBrowser == "1") { //default:grid로 display:
                if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImgFlag == "I") { //이미지 조회인경우
                    if (sMRR_GRGeomSaek_FileCnt == 0) { //첫장인 경우
                        sMRR_GRGeomSaek_FileChange = "Y";
                        sMRR_GRGeomSaek_FileCnt = sMRR_GRGeomSaek_FileCnt + 1;
                        sMRR_GRGeomSaek_ChrtCd = sCurrentChrt;
                    } else {
                        sMRR_GRGeomSaek_FileChange = "Y";
                        sMRR_GRGeomSaek_FileCnt = sMRR_GRGeomSaek_FileCnt + 1;
                        sMRR_GRGeomSaek_ChrtCd = sCurrentChrt;
                        /* 20050801 임시로 막음
                        if (sMRR_GRGeomSaek_ChrtCd == "IDC0000005" && sChrt == "IDC0000005") { //이전서식지와 다음서식지가 입원경과기록지인경우
                            sMRR_GRGeomSaek_FileChange = "N";
                        } else {
                            sMRR_GRGeomSaek_FileChange = "Y";
                            sMRR_GRGeomSaek_FileCnt = sMRR_GRGeomSaek_FileCnt + 1;
                            sMRR_GRGeomSaek_ChrtCd = sChrt;
                        }
                        */
                    }

                    //타과 회신일 경우 회신부서와 회신의사 출력 요청(2006.05.24) by taja78
                    if (sCurrentChrt == "CDC0000009" || sCurrentChrt == "CLX0000001") {
                       //타과회신 부서를 넘기기위한 변수
                        var hrgstdept = model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/rgstdept");
                        var hrgstdeptnm = model.getValue("/root/HideData/deptList[cd = '"+hrgstdept+"']/nm");
                        model.setAttribute("MRR_GRGeomSaek_TextSave_TGHSDept",hrgstdeptnm);
                    }

                    fMakeGridMain(node, node2, node2path, pForCnt, node3, node3path, node4, node4path, node5, node5path, sCurrentChrt); //MRR_GRGeomSaek_TextSave.js호출

                    if (sMRR_GRGeomSaek_FileCnt == 1) {
                        sTotalKind = "▦" + (model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchchrt")).substr(2,1)
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchptno")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchptdt")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchiofg")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchchrt")
                                   + "▦" + (model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchdate")).substr(0,8)
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchdept")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchdoct")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/chrtname")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/deptname")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/doctname") + "▦▦▦▦▦▦▦▦▦▦▦";
                    } else {
                        sTotalKind = sTotalKind + "▩" + "▦" + (model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchchrt")).substr(2,1)
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchptno")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchptdt")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchiofg")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchchrt")
                                                       + "▦" + (model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchdate")).substr(0,8)
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchdept")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/ptchdoct")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/chrtname")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/deptname")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+ i+"]/mrdrptchh/doctname") + "▦▦▦▦▦▦▦▦▦▦▦";
                    }
                } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImgFlag == "C") { //이미지 조회인경우
                    if (sMRR_GRGeomSaek_FileCnt == 0) { //첫장인 경우
                        sMRR_GRGeomSaek_FileChange = "Y";
                        sMRR_GRGeomSaek_FileCnt = sMRR_GRGeomSaek_FileCnt + 1;
                        sMRR_GRGeomSaek_ChrtCd = sCurrentChrt;
                    } else {
                        sMRR_GRGeomSaek_FileChange = "N";
                        sMRR_GRGeomSaek_ChrtCd = sCurrentChrt;
                    }

                    //타과 회신일 경우 회신부서와 회신의사 출력 요청(2006.05.24) by taja78
                   if (sCurrentChrt == "CDC0000009" || sCurrentChrt == "CLX0000001") {
                       //타과회신 부서를 넘기기위한 변수
                        var hrgstdept = model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/rgstdept");
                        var hrgstdeptnm = model.getValue("/root/HideData/deptList[cd = '"+hrgstdept+"']/nm");
                        model.setAttribute("MRR_GRGeomSaek_TextSave_TGHSDept",hrgstdeptnm);
                   }

                    fMakeGridMain(node, node2, node2path, pForCnt, node3, node3path, node4, node4path, node5, node5path, sCurrentChrt); //MRR_GRGeomSaek_TextSave.js호출
                    if (sMRR_GRGeomSaek_FileCnt == 1) {
                        sTotalKind = "▦" + (model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchchrt")).substr(2,1)
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchptno")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchptdt")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchiofg")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchchrt")
                                   + "▦" + (model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchdate")).substr(0,8)
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchdept")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchdoct")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/chrtname")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/deptname")
                                   + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/doctname") + "▦▦▦▦▦▦▦▦▦▦▦";
                    } else {
                        sTotalKind = sTotalKind + "▩" + "▦" + (model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchchrt")).substr(2,1)
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchptno")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchptdt")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchiofg")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchchrt")
                                                       + "▦" + (model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchdate")).substr(0,8)
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchdept")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/ptchdoct")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/chrtname")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/deptname")
                                                       + "▦" + model.getValue("/root/HideData/savedatas/savedata["+i+"]/mrdrptchh/doctname") + "▦▦▦▦▦▦▦▦▦▦▦";
                    }
                } else { //if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I")
                    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
                    if (emr_Right) {
                        if (sAlert) model.alert("["+node+"]["+node2+"]["+node2path+"]");

                        emr_Right.javascript.fMakeGridMain(node, node2, node2path, pForCnt, node3, node3path, node4, node4path, node5, node5path, sCurrentChrt); //MRR_GRGeomSaek_Grid에 있는 function호출
                        emr_Right.refresh();
                    } else {
                        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
                        return;
                    }
                }
            } else if (sGridBrowser == "2") {  //browser로 display
                var viewSet =  fEMR_MakeMRBogiString(node, node2, node3, node2path, node3path, "M", "");
                model.setAttribute("preview_data",viewSet);

                if (fGetIP_xfm() != "MRR_GRGeomSaek_MRBogi") {
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_MRBogi.xfm","replace");
                } else {
                    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
                    if (emr_Right) {
                        var bsr = emr_Right.control("browser1");
                        bsr.src = "/emr/girokweb/jsp/MRD_MRBoGi.jsp?reqParam=MRBogi";
                        emr_Right.refresh();
                    } else {
                        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
                        return;
                    }
                }
            }//if (sGridBrowser == "1") { //default:grid로 display:
        }//for
    }//end of  else { // if (sTextImgFlag == "T")
}

/**
 * @group  :
 * @ver    : 2005.04.30
 * @by     : 이혜정
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : XML구조를 Grid형태로 변환한 내용 중 이미지를 생성한다.
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fMakeImage_girokView(pGrid, sTextImgFlag) {
    //2006.04.01 taja78 수정중...
    if (sTextImgFlag == "T") {
        //2006-08-16 1:51오후 get Model 관련 수정 김태범
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            var sTempPid = model.getValue("/root/MainData/condition/cond1");
            model.setAttribute("GirokGeomSaek_fMakeImage_girok_param",sTempPid);
            model.setAttribute("GirokGeomSaek_fMakeImage_girok_param1",model.getValue("/root/HideData/condition/cond16"));
            //pGrid = emr_Right.vsGrid("grid_view");
            emr_Right.javascript.fMakeImage_girok(g_CGCSet); // Common_EMR_JinRyo 선언함수 호출 (x) <- xfm안에 선언되어 있은
        } else {
            //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
            return;
        }
    } else {
        if (sTextImgFlag == "P") {
            // 프린트 할 경우 이미지 만들기...
            return;
        }
        //이미지로 볼 경우 이쪽 프로세스를...
    }
}

/**
 * @group  :
 * @ver    : 2005-10-06 4:15오후
 * @by     : 현대정보기술 의료기술팀, 꿈꾸는 HIS 프로그래머 이제관 (je2kwan2@hanmir.com)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function f_ViewingDetailProcess(pChartCode, pGrid, pTextImgFlag) {
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        if (pTextImgFlag == "T") {
            pGrid = emr_Right.vsGrid("grid_view");
        }
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }

    var m_xPath   = "/root/HideData/savedata";
    var m_Data1   = "/root/HideData/Data1";
    var m_Data2   = "/root/HideData/Data2";
    var m_Data3   = "/root/HideData/Data3";
    var m_Data4   = "/root/HideData/Data4";

    // Data2 정리
    model.removeNodeset(m_Data1);
    model.makenode(m_Data1);

    // 전송받은 데이터 Move : Data1, Data2 정리
    copyNode(m_xPath + "/mrdrptchh", m_Data1);

    // Data2 정리
    model.removeNodeset(m_Data2);
    model.makenode(m_Data2);

    // 과별서식 누적조회는 다른 경로로 데이터가 전달되므로 이를 Sync 시킨다.
    if (pChartCode.substring(1, 3) == "DT" && pChartCode != "CDT0000021") {
        fCommon_dumpCopyNode(m_xPath + "/mrdrptith/G999/tpls/tpl", m_Data2);
    } else {
        copyNode(m_xPath + "/mrdrptith", m_Data2);
    }

    // Data3 정리
    model.removeNodeset(m_Data3);
    model.makenode(m_Data3);

    // 과별서식 누적조회는 다른 경로로 데이터가 전달되므로 이를 Sync 시킨다.
    if (pChartCode.substring(1, 3) == "DT" && pChartCode != "CDT0000021") {
        copyNode(m_xPath + "/otherinfo/otherinfo", m_Data3);
    } else {
        copyNode(m_xPath + "/otherinfo", m_Data3);
    }

    // Data4 정리
    model.removeNodeset(m_Data4);
    model.makenode(m_Data4);
    //appended by OBH on 2009.08.18
    //model.alert("pChartCode := " + pChartCode);
    switch (pChartCode) {
        case "CDC0000009" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tger_mrdrptith", m_Data4);
            break;
        case "CLX0000001" :
        	var m_ins        = model.instance;
        	var node_list = m_ins.selectNodes(m_xPath + "/otherinfo/tger_mrdrptith");
        	for(var i = 1; i < node_list.length + 1; i++) {
        		f_dumpCopyNode(m_xPath + "/otherinfo/tger_mrdrptith[" + i +"]", m_Data4);
        	}
            break;
        case "OLX0000001" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tger_mrdrptith", m_Data4);
            break;
        case "OLX0000002" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tger_mrdrptith", m_Data4);
            break;
        case "OLX0000003" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tger_mrdrptith", m_Data4);
            break;
        case "OLX0000004" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tger_mrdrptith", m_Data4);
            break;
		case "OLX0000005" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tger_mrdrptith", m_Data4);
            break;
		case "OLX0000007" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tger_mrdrptith", m_Data4);
            break;
        case "CDC0000008" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tghs_mrdrptith", m_Data4);
            break;
        case "CDC0000012" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tghs_mrdrptith", m_Data4);
            break;
        case "CDC0000019" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tghs_mrdrptith", m_Data4);
            break;
        case "CDX0000020" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tghs_mrdrptith", m_Data4);
            break;
        case "CDX0000002" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tghs_mrdrptith", m_Data4);
            break;
        case "CDC0000004" :
            fCommon_dumpCopyNode(m_xPath + "/otherinfo/tghs_mrdrptith", m_Data4);
            break;
    }

    // Data 1, 2, 3, 4 : value 없는 node 삭제
    var m_node1 = findNode(m_Data1);
    var m_node2 = findNode(m_Data2);
    var m_node3 = findNode(m_Data3);
    var m_node4 = findNode(m_Data4);

    if (m_node1 == null || m_node2 == null) return;

    TFRemoveNoChild(m_node2); // 값이 없으면 삭제하는 로직
    TFRemoveNoChild(m_node3); // 값이 없으면 삭제하는 로직
    TFRemoveNoChild(m_node4); // 값이 없으면 삭제하는 로직

    //==============================================================================================
    // Header 표기
    //==============================================================================================
    // 2009.09.02
    //model.alert("iRowCount := " + iRowCount);
    //model.alert("m_node1 := " + m_node1);
    //model.alert("pGrid := " + pGrid);
    //model.alert("pTextImgFlag := " + pTextImgFlag);

    f_MakeGridForHeader(iRowCount, m_node1, pGrid, pTextImgFlag);

    //model.alert("pChartCode =" + pChartCode);
    /* 2006.05.10 기록조회 변경사항 반영 */
    // A. 내용을 뒤에 표현하고 싶은 서식정보 내역
    // appended by OBH on 2008.06.30 , KRC의뢰지(CDC0000012)의 출력순서도 타과의뢰지와 동일하게, 의무기록 장혜숙주임 요청
    //if (pChartCode == "CDC0000007" || pChartCode == "IDC0000006" || pChartCode == "IDC0000007" ||
    //    pChartCode == "CDC0000008" || pChartCode == "CDX0000002" || pChartCode == "ODC0000004" ||
    //    pChartCode == "CDC0000009" || pChartCode == "OLX0000001" || pChartCode == "OLX0000002" ||
    //    pChartCode == "OLX0000003" || pChartCode == "OLX0000004" || pChartCode == "OLX0000005" ) {
    if (fEMR_ChrtChk_ERHSSSIT(pChartCode) == "Y") {
    } else {
        fMakeGridForDetail_girokView(m_node2, m_Data2, pGrid, pTextImgFlag);
    }

    if (m_node3 != null) {
        if (pChartCode.substring(1, 3) == "DT" && pChartCode != "CDT0000021") {
            fMakeGridForOther_Dept_girokView(m_node3, m_Data3, pGrid, pTextImgFlag);
        } else {
            // 기본정보등의 데이터를 표현
            fMakeGridForOther_girokView(m_node3, m_Data3, pGrid, pTextImgFlag);
        }
    }

    // appended by OBH on 2008.06.30 , KRC의뢰지(CDC0000012)의 출력순서도 타과의뢰지와 동일하게, 의무기록 장혜숙주임 요청
    if (fEMR_ChrtChk_ERSSIT(pChartCode) == "Y") {
        fMakeGridForDetail_girokView(m_node2, m_Data2, pGrid, pTextImgFlag);
    }

    // D. OTHER-INFO / m_node2 이후 서식내역
    if (m_node4 != null) {
        if (fEMR_ChrtChk_ERHS(pChartCode) == "Y") {
            fMakeGridForDetail_girokView(m_node4, m_Data4, pGrid, pTextImgFlag);
        }
    }

    // E. OTHER-INFO / m_node2 / m_node4 이후 서식내역
    if (fEMR_ChrtChk_HS(pChartCode) == "Y") {
        fMakeGridForDetail_girokView(m_node2, m_Data2, pGrid, pTextImgFlag);
    }

    //==============================================================================================
    // Footer 표기 [처리예정 : 2008.05.16 : newhope]
    //==============================================================================================
    //alert("check f_ViewingDetailProcess 3");
    //f_MakeGridForFooter(iRowCount, m_node1, pGrid, pTextImgFlag);
}

/**
 * @group  :
 * @ver    : 2004.11.18
 * @by     : 이혜정
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fMakeGridForOther_girokView (mtemp, parentPath, pGrid , pTextImgFlag) {
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        if (pTextImgFlag == "T") {
            pGrid = emr_Right.vsGrid("grid_view");
        }
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }

    var rootTemp    = mtemp;
    var temp        = mtemp.firstChild;
    var namevalue   = "";
    var AttNode     = "";
    var groupcheck  = "";  //진단정보, 수술정보 CHECK

    if (temp == null)
        return;

    nodeCheck = 1;
    checkcheck = "0";
    //tempPath  = parentPath;

    var ctrlID = 1;

    var sTempString = "";

    while((temp != rootTemp)) {
        if (temp.nodeName != "#text") {
            AttNode = temp.selectSingleNode("./@SelNm");

            // SelNm Attribute가 있는 경우만 미리보기
            if (AttNode != null) {
                namevalue = AttNode.nodeValue;
                if (nodeCheck == "1" && checkcheck == "0" && groupcheck != namevalue) {
                    fSetGridValue_girokView(iRowCount,2,sTempString,"T","","", pGrid, pTextImgFlag, null, null, null);
                    sTempString = "";
                    sTempString = tt + namevalue;
                    fSetGridValue_girokView(iRowCount,1,sTempString,"T","","", pGrid, pTextImgFlag, null, null, temp.nodeName);
                    sTempString = "";

                    checkcheck = "1";
                    groupcheck =  namevalue;
                } else if (nodeCheck == "1"  && checkcheck == "1" && groupcheck != namevalue ) {
                    fSetGridValue_girokView(iRowCount,2,sTempString,"T","","", pGrid, pTextImgFlag, null, null, null);
                    sTempString = "";
                    sTempString = tt + namevalue;
                    fSetGridValue_girokView(iRowCount,1,sTempString,"T","","", pGrid, pTextImgFlag, null, null, temp.nodeName);
                    sTempString = "";

                    groupcheck =  namevalue;
                } else if (groupcheck != namevalue) {
                    fSetGridValue_girokView(iRowCount, 2, sTempString, "T","","", pGrid, pTextImgFlag, null, null, null);
                    sTempString = "";
                    //sTempString = tt + "  " + namevalue; //2007-02-05 5:33오후 주석처리 김태범
                    sTempString = tt + "　" + namevalue;
                }
            }
        } else {
            AttNode = temp.parentNode.selectSingleNode("./@SelNm");
            if (AttNode != null) { // SelNm Attribute가 있는 경우만 미리보기
                //sTempString = sTempString + "  " + temp.nodeValue; //2007-02-05 5:33오후 주석처리 김태범
                sTempString = sTempString + "　" + temp.nodeValue;
            }
        }

        temp = fGetNode(temp, rootTemp);

        if (temp == null) break;
    }
    fSetGridValue_girokView(iRowCount,2,sTempString,"T","","", pGrid, pTextImgFlag, null, null, null);

    return;
}

/**
 * @group  :
 * @ver    : 2004.11.18
 * @by     : 이혜정
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fMakeGridForOther_Dept_girokView(mtemp, parentPath, pGrid, pTextImgFlag) {
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        if (pTextImgFlag == "T") {
            pGrid = emr_Right.vsGrid("grid_view");
        }
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }

    var rootTemp    = mtemp;
    var temp        = mtemp.firstChild;
    var namevalue   = "";
    var AttNode     = "";
    var ViewNode    = "";
    var viewvalue   = "";
    var groupcheck  = "";  //진단정보, 수술정보 CHECK

    if (temp == null)
        return;

    nodeCheck = 1;
    checkcheck = "0";
    //tempPath  = parentPath;

    var ctrlID = 1;

    var sTempString = "";

    while((temp != rootTemp)) {
        if (temp.nodeName != "#text") {

            AttNode  = temp.selectSingleNode("./@name");
            ViewNode = temp.selectSingleNode("./@view");

            if (ViewNode == null) viewvalue = "Y";
            else viewvalue = ViewNode.nodeValue;

            // SelNm Attribute가 있는 경우만 미리보기
            if (AttNode != null && viewvalue == "Y") {

                namevalue = AttNode.nodeValue;

                if (nodeCheck == "1" && checkcheck == "0" && groupcheck != namevalue) {
                    fSetGridValue_girokView(iRowCount,2,sTempString,"T","","", pGrid, pTextImgFlag, null, null, null);
                    sTempString = "";
                    sTempString = tt + namevalue;
                    fSetGridValue_girokView(iRowCount,1,sTempString,"T","","", pGrid, pTextImgFlag, null, null, temp.nodeName);
                    sTempString = "";

                    checkcheck = "1";
                    groupcheck =  namevalue;
                } else if (nodeCheck == "1"  && checkcheck == "1" && groupcheck != namevalue ) {
                    fSetGridValue_girokView(iRowCount,2,sTempString,"T","","", pGrid, pTextImgFlag, null, null, null);
                    sTempString = "";
                    sTempString = tt + namevalue;
                    fSetGridValue_girokView(iRowCount,1,sTempString,"T","","", pGrid, pTextImgFlag, null, null, temp.nodeName);
                    sTempString = "";

                    groupcheck =  namevalue;
                } else if (groupcheck != namevalue) {
                    fSetGridValue_girokView(iRowCount, 2, sTempString, "T","","", pGrid, pTextImgFlag, null, null, null);
                    sTempString = "";
                    sTempString = tt + namevalue;
                }
            }
        } else {
            AttNode  = temp.parentNode.selectSingleNode("./@name");
            ViewNode = temp.parentNode.selectSingleNode("./@view");

            if (ViewNode == null) viewvalue = "Y";
            else viewvalue = ViewNode.nodeValue;

            // SelNm Attribute가 있는 경우만 미리보기
            if (AttNode != null && viewvalue == "Y") {
                sTempString = sTempString + "　" + temp.nodeValue; // 2007-02-05 5:36오후 공백대신 특수문자
            }

            if (namevalue == "기록일자") {
                fSetGridValue_girokView(iRowCount,2,"--------------------------------------------------","T","","", pGrid, pTextImgFlag, null, null, null);
            }
        }

        temp = fGetNode(temp, rootTemp);

        if (temp == null) break;
    }

    fSetGridValue_girokView(iRowCount, 2, sTempString, "T", "", "", pGrid, pTextImgFlag, null, null, null);
    return;
}

/**
 * @group  :
 * @ver    : 2005-10-06 4:55오후
 * @by     :
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Hearder정보를 Grid형태로 변환
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function f_MakeGridForHeader(pNo, pNode, pGrid, pTextImgFlag) {
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        if (pTextImgFlag == "T") {
            pGrid = emr_Right.vsGrid("grid_view");
        }
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }

    if (pTextImgFlag =="T") {
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/data");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/path");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgpath");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgexvg");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/type");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/size");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/InfoCode");

        emr_Right.setValue("/root/SubData/viewinfo[" +pNo+ "]/path", 0);
    } else { //else if (pTextImgFlag =="P") {}
        model.makenode("/root/SubData/viewinfo[" + pNo + "]");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/data");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/path");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/imgpath");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/imgexvg");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/type");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/size");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/InfoCode");

        model.setValue("/root/SubData/viewinfo[" +pNo+ "]/path", 0);
    }

    if (pTextImgFlag =="P") { //pTextImgFlag =="P" 일 경우에는 filechange 속성, 기록리스트속성추가.
        model.setAttribute("sMRR_GRGeomSaek_Print_FileChange","Y");
        iGRGeomSaek_Print_FileChange_Count = iGRGeomSaek_Print_FileChange_Count+1;
        model.setAttribute("sMRR_GRGeomSaek_Print_FileChange_Count",iGRGeomSaek_Print_FileChange_Count);
    }

    var hptchdate = pNode.selectSingleNode("ptchdate").nodeTypedValue;
    var hptchrgdt = pNode.selectSingleNode("ptchrgdt").nodeTypedValue;
    var hptchupdt = pNode.selectSingleNode("ptchupdt").nodeTypedValue;
    var hptchsign = pNode.selectSingleNode("ptchsign").nodeTypedValue;
    var hchrtname = pNode.selectSingleNode("chrtname").nodeTypedValue;
    var hrgstdept = pNode.selectSingleNode("rgstdept").nodeTypedValue;
    var hptchchrt = pNode.selectSingleNode("ptchchrt").nodeTypedValue;
    var hptchsbnm = pNode.selectSingleNode("ptchsbnm").nodeTypedValue;  //부서식명

    var hrgstname = pNode.selectSingleNode("rgstname").nodeTypedValue;  // 최초 작성자
    var hptchuser = pNode.selectSingleNode("ptchuser").nodeTypedValue;  // 최종 작성자
    var hptchdoct = pNode.selectSingleNode("doctname").nodeTypedValue;  // 지정의 (주치의 / 스탭)
    var hptchrenm = pNode.selectSingleNode("ptchrenm").nodeTypedValue;  // 전공의 (레지던트)
    var hptchsnst = pNode.selectSingleNode("ptchsnst").nodeTypedValue;  // 공동인증 상태 (YY : 둘다인증)
    
    var hsignrgst = "";
    if (pNode.selectSingleNode("signrgst") != null) {
    	var hsignrgst  = pNode.selectSingleNode("signrgst").nodeTypedValue;  // 최초 작성자 인증상태
    }
    
    if(hptchchrt == "EDC0000001"  || hptchchrt == "EDC0000002" || hptchchrt == "EDC0000003" || hptchchrt == "EDC0000004"){
        var hptchopft = pNode.selectSingleNode("ptchopft").nodeTypedValue;  // 응급실 입실일시
    }

    var hptchcodt = "";     // 공동인증 의사
    
    // Mod by jykim 2019-05-31 공동인증 의사 존재 로직 제거, 단독 인증 서식들이 여러명 인증으로 변경됨으로 인해 생략
    // 입퇴원요약, 단기입퇴원, 수술기록은 공동인증 의사가 존재함
//    if (hptchchrt == "IDC0000006" || hptchchrt == "IDC0000007" || hptchchrt == "CDC0000007" || hptchchrt == "CDC0000014"
//            || hptchchrt == "EDC0000001" || hptchchrt == "EDC0000002" || hptchchrt == "EDC0000003" || hptchchrt == "IDC0000004") {
//        if (hrgstname == hptchuser) {       //최초작성자와 최종작성자가 같은경우
//        	if (hptchrenm == null || hptchrenm == "") { // 전공의가 널인경우 최종
//        		hptchcodt = hptchuser;
//        	} else {
//        		if (hrgstname == hptchrenm) {   // 최초작성자와 전공의와 같은경우  -> 최초작성 == 최종작성 == 전공의
//        			hptchcodt = hptchuser;
//        		} else {                          // (최초작성 == 최종작성), (최초작성 != 전공의)
//        			hptchcodt = hptchrenm;
//        		}
//        	}
//        } else {                               // 최초작성자와 최종작성자가 같지 않은 경우
//            if (hrgstname == hptchrenm) {   // (최초작성 != 최종작성), (최초작성 == 전공의)
//                hptchcodt = hptchuser;
//            } else {                          // (최초작성 != 최종작성), (최초작성 != 전공의), [지정의 -> 전공의]
//                if (hptchrenm == null || hptchrenm == "") {    // 전공의가 널인경우와 그렇지 않은경우(전공의 임시저장)
//                    hptchcodt = hptchuser;
//                } else {
//                    if (hptchuser == hptchrenm) {
//                        hptchcodt = hptchrenm;      // 레지던트A-레지던트B-스탭일 경우 문제됨
//                    } else {
//                        hptchcodt = hptchuser;
//                    }
//                }
//            }
//        }
//    } else {
//    	hptchcodt = hptchuser;
//    }
    hptchcodt = hptchuser;

    if (hrgstdept !="" && hrgstdept != null) {
        hrgstdept = model.getValue("/root/HideData/deptList[cd = '"+hrgstdept+"']/nm");
    }
    var hrgstname = pNode.selectSingleNode("rgstname").nodeTypedValue;

    // String 날짜처리 함수사용
    var sPtchDate  = fCommon_setStringType(hptchdate, "FULL12");
    var sPtchRgdt  = fCommon_setStringType(hptchrgdt, "FULL12");
    var sPtchUpdt  = fCommon_setStringType(hptchupdt, "FULL12");

    var sPtchDate1 = hptchdate.substr(0,4) + "-" +
                     hptchdate.substr(4,2) + "-" +
                     hptchdate.substr(6,2) ;

    var m_ViewDate = sPtchRgdt; // 표기할 일자 : 기본적으로 기록일자를 보이는 날짜로 Setting

    var sPtchSignRgst = "";
    var sPtchSign = "";
    if (hptchsign == "T") {
        sPtchSign = "(임시)";
    } else if (hptchsign == "C") {
        sPtchSign = "(확정)";
    }
    
    if(hsignrgst == "N"){
    	sPtchSignRgst = "(임시)";
    	hsignrgst = "T";
    }else{
    	sPtchSignRgst = "(확정)";
    	hsignrgst = "C";
    }
    
    var sPtchDate2 =  "[기록일:" + sPtchDate1 + "]";
        
    // 2007-08-23 11:41오전 : 지원기록일때 지원기록 서식명을 표기함
    if (hptchchrt.substr(1,2) == "LT") {
        m_ViewDate = sPtchDate; // 진료지원기록은 실제기록일자를 표기함 : 실시일자 표기

        if (hptchsbnm != "" && hptchsbnm != "-") {
            hchrtname = hchrtname + ":" + hptchsbnm;
        }
    }else{ // 지원기록이 아니여도 부제목 표시 (2015.06.15)
        if (hptchsbnm != "" && hptchsbnm != "-") {
            hchrtname = hchrtname + ":" + hptchsbnm;
        }
    }

    //****************************************************************************//
    //*******    의료기관 평가 관련 수정 사항 (2007.10.19 김태범)       **********//
    //*******    [로직 Simple 처리 : 이제관 [2008.04.25]                **********//
    // title정보 최초 등록일자를 기록일자로 변경하여 보여주도록 수정 요청
    // 2018.11 3주기 인증시 기록일자에서 최초기록일자로 표시되도록 (의무기록팀 장혜숙파트장) 
    //****************************************************************************//
    /*
    var m_ProcessCode = "TestMode"; // 의평인경우 "TestMode"  ELSE "Normal";
    if (m_ProcessCode == "TestMode") {
        m_ViewDate = sPtchDate; //**의평관련**
    }*/
    
    var m_Temp = "";
    var m_UpdateDateView = "";
	
	// 2017.09 의평관련 최종기록일시 미표시 2017.09 원복
    if (sPtchRgdt != sPtchUpdt) m_UpdateDateView = " 수정>" + sPtchUpdt;
	
    if(hptchchrt == "EDC0000001"  || hptchchrt == "EDC0000002" || hptchchrt == "EDC0000003" || hptchchrt == "EDC0000004"){
        m_UpdateDateView = m_UpdateDateView + "  입실일시>" + hptchopft;
    }
    var m_titleinfo = hchrtname + "  " +  "/"; // + hrgstname;

    //
    if ((hptchchrt == "IDC0000006" || hptchchrt == "IDC0000007" || hptchchrt == "CDC0000007"
             || hptchchrt == "EDC0000001"  || hptchchrt == "EDC0000002" || hptchchrt == "EDC0000003" || hptchchrt == "CDC0000014" || hptchchrt == "IDC0000004") && hptchcodt != "") {
    	//Mod by jykim 2019-06-03 Title 출력 수정
    	m_Temp = m_titleinfo + hrgstname + sPtchSignRgst + ((hptchcodt == null || hptchcodt == "" || (hptchcodt == hrgstname && hsignrgst == hptchsign)) ? "" : "|" + hptchcodt + sPtchSign) + "  " + sPtchDate2 + "  " + sPtchRgdt + m_UpdateDateView;
//        if (hrgstname == hptchcodt) {
//            m_Temp = m_titleinfo + hrgstname + sPtchSign + "  " + sPtchDate2 + "  " + sPtchRgdt + m_UpdateDateView;
//        } else {
//            // 기록일자 = 수정일자
//            if (sPtchRgdt == sPtchUpdt) {
//                m_Temp = m_titleinfo + hrgstname + sPtchSign + "|" + hptchcodt + sPtchSign + "  " + sPtchDate2 + "  " + sPtchRgdt + m_UpdateDateView; //[2]
//            } else {
//            // 기록일자 != 수정일자
//                if (hptchrenm == "") { // 전공의 없음
//                        m_Temp = m_titleinfo + hrgstname + "(임시)"  + "|" + hptchcodt + sPtchSign + "  " + sPtchDate2 + "  " + sPtchRgdt + m_UpdateDateView;
//                } else {
//                    if (hptchsnst == "YY") {
//                        m_Temp = m_titleinfo + hrgstname + sPtchSign + "|" + hptchcodt + sPtchSign + "  " + sPtchDate2 + "  " + sPtchRgdt + m_UpdateDateView; //[2]
//                    } else {
//                        m_Temp = m_titleinfo + hrgstname +             "|" + hptchcodt + sPtchSign + "  " + sPtchDate2 + "  " + sPtchRgdt + m_UpdateDateView;
//                    }
//                }
//            }
//        }
	} else if(hptchchrt == "CDS0000001" || hptchchrt == "CDS0000002") {
		if (hptchsign != "C") { //최종이 임시저장인 경우
			if (hrgstname == hptchuser) { //최초작성자와 최종작성자가 같은 경우
				m_Temp = hchrtname + "  " + "/" + hrgstname + sPtchSign + "  " + sPtchDate2 + "  " + sPtchRgdt+ "  수정>" + sPtchUpdt;
			} else { //최초, 최종 작성자가 다른 경우
				m_Temp = hchrtname + "  " + "/" + hrgstname + "|" + hptchuser + sPtchSign + "  " + sPtchDate2 + "  " + sPtchRgdt+ "  수정>" + sPtchUpdt;
			}
			//m_Temp = hchrtname + "  " + "/" + hrgstname + sPtchSign + "  " + sPtchRgdt+ "  수정>" + sPtchUpdt;
		} else {
			if (hrgstname == hptchuser) {
				m_Temp = hchrtname + "  " + "/" + hrgstname + sPtchSign + "  " + sPtchDate2 + "  " + sPtchRgdt+ "  수정>" + sPtchUpdt;
			} else {
				m_Temp = hchrtname + "  " + "/" + hrgstname + "|" + hptchuser + sPtchSign + "  " + sPtchDate2 + "  " + sPtchRgdt+ "  수정>" + sPtchUpdt;
			}
			//m_Temp = hchrtname + "  " + "/" + hrgstname + sPtchSign + "  " + sPtchRgdt+ "  수정>" + sPtchUpdt;
		}
    } else {
        //m_Temp = m_titleinfo + hrgstname + sPtchSign + "  " + sPtchDate2 + "  " + m_ViewDate + m_UpdateDateView;
    	// Mod by jykim 2019-05-31 두명이상 인증 서식 조건 그 외에도 최종이 있으면 출력, 최초 최종이 같으면 하나만 출력
    	m_Temp = m_titleinfo + hrgstname + sPtchSignRgst + ((hptchcodt == null || hptchcodt == "" || (hptchcodt == hrgstname && hsignrgst == hptchsign)) ? "" : "|" + hptchcodt + sPtchSign) + "  " 
    			+ sPtchDate2 + "  " + sPtchRgdt + m_UpdateDateView;
        // #!# 지원기록은 sJiWeonGiRokFlag 에 의해 구분, 심평원확인건으로 인한 임시처리
        // 2008-08-18, 기록자정보 삭제, 요청자 (장혜숙:의무기록실, 장재연:재활의학과)
        if (hptchchrt.substr(1,2) == "LT") {
            m_Temp = hchrtname;
        }
    }

    //

    // appended by OBH on 2009.09.02
    // 타과회신 NST의 경우 헤드에 회신과를 출력하여준다.
    //model.alert("hptchchrt := " + hptchchrt);
    //model.alert("m_Temp := " + m_Temp);
    //model.alert("hptchsbnm := " + hptchsbnm);
    //model.alert("hrgstname := " + hrgstname);
    //model.alert("hptchuser := " + hptchuser);
    //model.alert("hrgstdept := " + hrgstdept);

    if (hptchchrt == "CDC0000009" || hptchchrt == "CLX0000001") {   // 타과회신
        var m_Temp_NST = m_Temp.split("/");
        //model.alert("m_Temp_NST := " + m_Temp_NST);
        //model.alert("g_Nst_Right_head := " + g_Nst_Right_head);
        if (g_Nst_Right_head == "NST") {
            m_Temp = m_Temp_NST[0] + g_Nst_Right_head + "-" + m_Temp_NST[1];
            g_Nst_Right_head = "";
        } else if (hrgstname == "박경지" && hrgstdept == "조제팀") {
            m_Temp = m_Temp_NST[0] + "/" + "NST" + "-" + m_Temp_NST[1];
            g_Nst_Right_head = "";
            //model.setAttribute("MRR_GRGeomSaek_ReportView.right","NST");
        } else if (hrgstname == "임소진" && hrgstdept == "영양팀") {
            m_Temp = m_Temp_NST[0] + "/" + "NST" + "-" + m_Temp_NST[1];
            g_Nst_Right_head = "";
            //model.setAttribute("MRR_GRGeomSaek_ReportView.right","NST");
        } else {
            g_Nst_Right_head = "";
            //model.setAttribute("MRR_GRGeomSaek_ReportView.right","");
        }
    }

    /*
    if (sPtchRgdt == sPtchUpdt) {
        if ((hptchchrt == "IDC0000006" || hptchchrt == "IDC0000007" || hptchchrt == "CDC0000007") && hptchcodt != "") {
            if (hrgstname == hptchcodt) {
                m_Temp = m_titleinfo + sPtchSign + "  " + sPtchRgdt + m_UpdateDateView;
            }
            else {
                m_Temp = m_titleinfo + sPtchSign + "|" + hptchcodt + sPtchSign + "  " + sPtchRgdt + m_UpdateDateView; //[2]
            }
        } else {
            m_Temp = m_titleinfo + sPtchSign + "  " + m_ViewDate + m_UpdateDateView;
        }
    } else {
        if ((hptchchrt == "IDC0000006" || hptchchrt == "IDC0000007" || hptchchrt == "CDC0000007") && hptchcodt != "") {
            if (hrgstname == hptchcodt) {
                m_Temp = m_titleinfo + sPtchSign + "  " + sPtchRgdt + m_UpdateDateView;
            } else {
                if (hptchrenm == "") {
                    m_Temp = m_titleinfo + "(임시)" + "|" + hptchcodt + sPtchSign + "  " + sPtchRgdt + m_UpdateDateView;
                } else {
                    if (hptchsnst == "YY") {
                        m_Temp = m_titleinfo + sPtchSign + "|" + hptchcodt + sPtchSign + "  " + sPtchRgdt + m_UpdateDateView; //[2]
                    } else {
                        m_Temp = m_titleinfo + "|" + hptchcodt + sPtchSign + "  " + sPtchRgdt + m_UpdateDateView;
                    }
                }
            }
        } else {
            m_Temp = hchrtname + "  " + "/" + hrgstname + sPtchSign + "  " + m_ViewDate + m_UpdateDateView;
        }
    }
    */

    if (pTextImgFlag =="T") {
        emr_Right.setValue("/root/SubData/viewinfo[" + pNo + "]/data", m_Temp);
        emr_Right.setValue("/root/SubData/viewinfo[" + pNo + "]/InfoCode", hptchchrt);
    } else {
        model.setValue("/root/SubData/viewinfo[" + pNo + "]/data", m_Temp);
        model.setValue("/root/SubData/viewinfo[" + pNo + "]/InfoCode", hptchchrt);

        if (pTextImgFlag == "P") {
            var m_HeaderInfo = "기록일자: " + m_ViewDate; // [출력물] 기록일자정보 표기 [이제관]
            fWriteFile(0, m_HeaderInfo, "T", "HeaderInfo");
        }
    }

    iRowCount = iRowCount + 1;
    iGiRokGeomSakModal_GridCnt = iGiRokGeomSakModal_GridCnt + 1;
}

/**
 * @group  :
 * @ver    : 2008-04-25 8:45오후
 * @by     :
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Footer정보를 Grid형태로 변환
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function f_MakeGridForFooter(pNo, pNode, pGrid, pTextImgFlag) {
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        if (pTextImgFlag == "T") {
            pGrid = emr_Right.vsGrid("grid_view");
        }
    } else {
        return;
    }

    if (pTextImgFlag =="T") {
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/data");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/path");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgpath");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgexvg");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/type");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/size");
        emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/InfoCode");

        emr_Right.setValue("/root/SubData/viewinfo[" +pNo+ "]/path", 0);
    } else { //else if (pTextImgFlag =="P") {}
        model.makenode("/root/SubData/viewinfo[" + pNo + "]");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/data");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/path");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/imgpath");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/imgexvg");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/type");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/size");
        model.makenode("/root/SubData/viewinfo[" + pNo + "]/InfoCode");

        model.setValue("/root/SubData/viewinfo[" +pNo+ "]/path", 0);
    }

    if (pTextImgFlag =="P") { //pTextImgFlag =="P" 일 경우에는 filechange 속성, 기록리스트속성추가.
        model.setAttribute("sMRR_GRGeomSaek_Print_FileChange","Y");
        iGRGeomSaek_Print_FileChange_Count = iGRGeomSaek_Print_FileChange_Count+1;
        model.setAttribute("sMRR_GRGeomSaek_Print_FileChange_Count",iGRGeomSaek_Print_FileChange_Count);
    }

    var hptchdate = pNode.selectSingleNode("ptchdate").nodeTypedValue;
    var hptchrgdt = pNode.selectSingleNode("ptchrgdt").nodeTypedValue;
    var hptchupdt = pNode.selectSingleNode("ptchupdt").nodeTypedValue;
    var hptchsign = pNode.selectSingleNode("ptchsign").nodeTypedValue;
    var hchrtname = pNode.selectSingleNode("chrtname").nodeTypedValue;
    var hrgstdept = pNode.selectSingleNode("rgstdept").nodeTypedValue;
    var hptchchrt = pNode.selectSingleNode("ptchchrt").nodeTypedValue;

    var hrgstname =  pNode.selectSingleNode("rgstname").nodeTypedValue;   // 최초 작성자
    var hptchsbnm = pNode.selectSingleNode("ptchsbnm").nodeTypedValue;    // 부서식명
    var hptchuser = pNode.selectSingleNode("ptchuser").nodeTypedValue;    // 최종작성자
    var hptchdoct = pNode.selectSingleNode("doctname").nodeTypedValue;    // 지정의
    var hptchrenm = pNode.selectSingleNode("ptchrenm").nodeTypedValue;    // 전공의
    var hptchsnst = pNode.selectSingleNode("ptchsnst").nodeTypedValue;    // 공동인증 상태

    var hptchcodt = "";     // 공동인증 의사

    // 입퇴원요약, 단기입퇴원, 수술기록은 공동인증 의사가 존재함
    if (hptchchrt == "IDC0000006" || hptchchrt == "IDC0000007" || hptchchrt == "CDC0000007" || hptchchrt == "CDC0000014" || hptchchrt == "IDC0000004") {
        // 1-1 최초작성자와 최종작성자가 같은경우
        if (hrgstname == hptchuser) {
            if (hrgstname == hptchrenm) {   // 최초작성자와 전공의와 같은경우  -> 최초작성 == 최종작성 == 전공의
                hptchcodt = hptchuser; // [최종작성자]
            } else {                        // (최초작성 == 최종작성), (최초작성 != 전공의)
                hptchcodt = hptchrenm; // [전공의]
            }
        // 1-2 최초작성자와 최종작성자가 같지 않은 경우
        } else {
            if (hrgstname == hptchrenm) {   // (최초작성 != 최종작성), (최초작성 == 전공의)
                hptchcodt = hptchuser;
            } else {                        // (최초작성 != 최종작성), (최초작성 != 전공의), [지정의 -> 전공의]
                if (hptchrenm == null || hptchrenm == "") {    // 전공의가 널인경우와 그렇지 않은경우(전공의 임시저장)
                    hptchcodt = hptchuser;
                } else {
                    if (hptchuser == hptchrenm) {
                        hptchcodt = hptchrenm;      // 레지던트A-레지던트B-스탭일 경우 문제됨
                    } else {
                        hptchcodt = hptchuser;
                    }
                }
            }
        }
    }

    if (hrgstdept !="" && hrgstdept != null) {
        hrgstdept = model.getValue("/root/HideData/deptList[cd = '"+hrgstdept+"']/nm");
    }
    var hrgstname = pNode.selectSingleNode("rgstname").nodeTypedValue;

    // String 날짜처리 함수사용
    var sPtchDate  = fCommon_setStringType(hptchdate, "FULL12");
    var sPtchRgdt  = fCommon_setStringType(hptchrgdt, "FULL12");
    var sPtchUpdt  = fCommon_setStringType(hptchupdt, "FULL12");

    var m_ViewDate = sPtchRgdt; // 표기할 일자 : 기본적으로 기록일자를 보이는 날짜로 Setting

    var sPtchSign = "";
    if (hptchsign == "T") {
        sPtchSign = "(임시)";
    } else if (hptchsign == "C") {
        sPtchSign = "(확정)";
    }

    // 2007-08-23 11:41오전 : 지원기록일때 지원기록 서식명을 표기함
    if (hptchchrt.substr(1,2) == "LT") {
        m_ViewDate = sPtchDate; // 진료지원기록은 실제기록일자를 표기함 : 실시일자 표기

        if (hptchsbnm != "" && hptchsbnm != "-") {
            hchrtname = hchrtname + ":" + hptchsbnm;
        }
    }

    //****************************************************************************//
    //*******    의료기관 평가 관련 수정 사항 (2007.10.19 김태범)       **********//
    //*******    [로직 Simple 처리 : 이제관 [2008.04.25]                **********//
    // title정보 최초 등록일자를 기록일자로 변경하여 보여주도록 수정 요청
    // 2018.11 3주기 인증시 기록일자에서 최초기록일자로 표시되도록 (의무기록팀 장혜숙파트장) 
    //****************************************************************************//
    /*
    var m_ProcessCode = "TestMode"; // 의평인경우 "TestMode"  ELSE "Normal";
    if (m_ProcessCode == "TestMode") {
        m_ViewDate = sPtchDate; //**의평관련**
    }*/

    
    var m_Temp = "";
    if (sPtchRgdt == sPtchUpdt) {
        if ((hptchchrt == "IDC0000006" || hptchchrt == "IDC0000007" || hptchchrt == "CDC0000007" || hptchchrt == "CDC0000014" || hptchchrt == "IDC0000004") && hptchcodt != "") {
            if (hrgstname == hptchcodt) {
                m_Temp = hchrtname + "  " +  "/" + hrgstname + sPtchSign + "  " + sPtchRgdt;
            } else {
                m_Temp = hchrtname + "  " +  "/" + hrgstname + sPtchSign + "|" + hptchcodt + sPtchSign + "  " + sPtchRgdt;
            }
        } else {
            m_Temp = hchrtname + "  " + "/" + hrgstname + sPtchSign + "  " + m_ViewDate;
        }
    } else {
        if ((hptchchrt == "IDC0000006" || hptchchrt == "IDC0000007" || hptchchrt == "CDC0000007" || hptchchrt == "CDC0000014" || hptchchrt == "IDC0000004") && hptchcodt != "") {
            if (hrgstname == hptchcodt) {
                m_Temp = hchrtname + "  " +  "/" + hrgstname + sPtchSign + "  " + sPtchRgdt+ "  수정>" + sPtchUpdt;
            } else {
                if (hptchrenm == "") {
                    m_Temp = hchrtname + "  " +  "/" + hrgstname + "(임시)" + "|" + hptchcodt + sPtchSign + "  " + sPtchRgdt+ "  수정>" + sPtchUpdt;
                } else {
                    if (hptchsnst == "YY") {
                        m_Temp = hchrtname + "  " +  "/" + hrgstname + sPtchSign + "|" + hptchcodt + sPtchSign + "  " + sPtchRgdt+ "  수정>" + sPtchUpdt;
                    } else {
                        m_Temp = hchrtname + "  " +  "/" + hrgstname + "|" + hptchcodt + sPtchSign + "  " + sPtchRgdt+ "  수정>" + sPtchUpdt;
                    }
                }
            }
        } else {
            m_Temp = hchrtname + "  " + "/" + hrgstname + sPtchSign + "  " + m_ViewDate+ "  수정>" + sPtchUpdt;
        }
    }

    if (pTextImgFlag =="T") {
        emr_Right.setValue("/root/SubData/viewinfo[" + pNo + "]/data", m_Temp);
        emr_Right.setValue("/root/SubData/viewinfo[" + pNo + "]/InfoCode", hptchchrt);
    } else {
        model.setValue("/root/SubData/viewinfo[" + pNo + "]/data", m_Temp);
        model.setValue("/root/SubData/viewinfo[" + pNo + "]/InfoCode", hptchchrt);

        if (pTextImgFlag == "P") {
            var m_HeaderInfo = "기록정보: " + m_ViewDate; // [출력물] 기록일자정보 표기 [이제관]
            fWriteFile(0, m_HeaderInfo, "T", "HeaderInfo");
        }
    }

    iRowCount = iRowCount + 1;
    iGiRokGeomSakModal_GridCnt = iGiRokGeomSakModal_GridCnt + 1;
}
/**
 * @group  :
 * @ver    : 2005.04.30
 * @by     : 이혜정
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : XML구조를 Grid형태로 변환
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fMakeGridForDetail_girokView(pNode, pPath, pGrid, pTextImgFlag) {
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        if (pTextImgFlag == "T") {
            pGrid = emr_Right.vsGrid("grid_view");
        }
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }

    var rootTemp = pNode;
    var temp     = pNode.firstChild;
    var elemcheck = "";
    var tempcheck = "";
    var groupcheck = "";
    var rowlengthcheck = "";
    var temptxt = "";
    var pauselvcheck = "";
    var tempNodeCheck = 0;
    var nodetempCheck = 2;


    if (temp == null)
        return;

    nodeCheck = 1;
    checkcheck = "0";
    tempPath  = pPath;

    var ctrlID = 1;

    var sTempString = "";

    var sSpace = "  ";

    while((temp != rootTemp)) {
        if (temp.nodeName != "#text") {
            if (temp.nodeName == "#cdata-section") {
            } else if (temp.nodeName.substring(0, 5) == "image" && temp.nodeName != "images") { //image tag를 만나면.. 다른 루틴을 탄다..
                var imgChList = temp.childNodes;
                var orgImg = "";
                var xvgData = "";
                var m_fhspace     = "   ";
                for (var tmpIdx = 0; tmpIdx < imgChList.length; tmpIdx++) {
                    if (imgChList.item(tmpIdx).nodeName == "path") {
                        orgImg = imgChList.item(tmpIdx).nodeTypedValue;
                    } else if (imgChList.item(tmpIdx).nodeName == "exvg") {
                        xvgData = imgChList.item(tmpIdx).nodeTypedValue;
                    }
                }
                fSetGridValue_girokView(iRowCount, 2, sTempString, "T", "", "", pGrid, pTextImgFlag, "Y", null, null);
                sTempString ="";
                sTempString ="<object id=\"imgCtrl" + ctrlID +"\" classid=\"CLSID:72e5d9ed-0d6a-46e8-aead-23144bfef878\" width=\"240px\" height=\"150px\">\n"+
                             "<PARAM name=\"Location\" value=\"" + model.getURI() + "\"/>"+
                             "<PARAM name=\"ImgPath\" value=\"" + orgImg + "\"/>"+
                             "<PARAM name=\"ShapeData\" value=\"" + xvgData + "\"/>"+
                             "</object>";
                fSetGridValue_girokView(iRowCount, 2, sTempString, "I", orgImg, xvgData, pGrid, pTextImgFlag, "Y", null, null);
                sTempString ="";
            } else if (temp.nodeName.substring(0, 2) == "FH") {
                if (temp.nodeName == "FHimages") {
                    var m_FHImgList   = temp.childNodes;
                    var m_XMDData     = "";
                    var m_Info        = "";
                    var m_CommentData = "";
                    var m_fhspace     =   "　"; //2007-02-05 5:40오후 공백대신 특수문자 추가. 김태범

                    for (var tmpIdx = 0; tmpIdx < m_FHImgList.length; tmpIdx++) {
                        if (m_FHImgList.item(tmpIdx).nodeName == "FHimage") {
                            m_XMDData = m_FHImgList.item(tmpIdx).nodeTypedValue;
                        }
                        if (m_FHImgList.item(tmpIdx).nodeName == "FHInfo") {
                            m_Info    = m_FHImgList.item(tmpIdx).nodeTypedValue;
                        }
                        if (m_FHImgList.item(tmpIdx).nodeName == "FHComment") {
                            m_CommentData = m_FHImgList.item(tmpIdx).nodeTypedValue;
                        }
                    }

                    fSetGridValue_girokView(iRowCount, 2, sTempString, "T", "", "", pGrid, pTextImgFlag, "Y", null, null);
                    fSetGridValue_girokView(iRowCount, 2, "※ 가계도", "T", "", "", pGrid, pTextImgFlag, "Y", m_fhspace, null);
                    sTempString = "familyHistroyMaker"; // 가계도 공백 그리드

                    fSetGridValue_girokView(iRowCount, 2, sTempString, "F", "", m_XMDData, pGrid, pTextImgFlag, "Y", null, null);

                    sTempString = "";

                    if (m_Info != "") {
                        fSetGridValue_girokView(iRowCount, 2, "※ 가계도 부연정보", "T", "", "", pGrid, pTextImgFlag, "Y", m_fhspace, null);
                        fSetGridValue_girokView(iRowCount, 2, m_Info, "T", "", "", pGrid, pTextImgFlag,  "Y", m_fhspace + m_fhspace, null);
                    }

                    if (m_CommentData != "") {
                        fSetGridValue_girokView(iRowCount, 2, "※ 가계도 Comment", "T", "", "", pGrid, pTextImgFlag, "Y", m_fhspace, null);
                        fSetGridValue_girokView(iRowCount, 2, m_CommentData, "T", "", "", pGrid, pTextImgFlag, "Y", m_fhspace + m_fhspace, null);
                    }
                }
            } else {
                //2006-11-21 2:17오후 end
                // name attribute value get
                var nameAttNode = temp.selectSingleNode("./@name");
                var nodedesc = "";
                var m_fhspace = "   ";
                if (nameAttNode != null) {
                    nodedesc = nameAttNode.nodeValue;
                } else {
                    nodedesc = temp.nodeName;
                }

                // view attribute value get
                var viewAttNode = temp.selectSingleNode("./@view");
                var viewcheck = "";
                if (viewAttNode != null) {
                    viewcheck = viewAttNode.nodeValue;
                } else {
                    viewcheck = "Y";
                }

                // next attribute value get
                var nextAttNode = temp.selectSingleNode("./@next");
                var nextcheck = "";
                var nextValue   = "";
                if (nextAttNode != null) {
                    if (nextAttNode.nodeValue == "Y") {
                        //lhj_07/06
                        nextcheck = "\n" + tt;
                        nextValue      = "Y";
                        rowlengthcheck = "N";
                    } else {
                        if (pauselvcheck == nodeCheck) {
                            nextcheck = ",";
                        } else {
                            nextcheck = "  ";
                        }
                        rowlengthcheck = "Y";
                        nextValue = "N";
                    }
                } else {
                    nextcheck = "\n" + tt;
                    rowlengthcheck = "N";
                    nextValue      = "Y";
                }

                // 주호소일 경우 '-' 추가
                if  (nodedesc == "주호소" && viewcheck == "N") {
                    fSetGridValue_girokView(iRowCount, 2, sTempString, "T","","", pGrid, pTextImgFlag, nextValue, null, null);
                    sTempString = "";
                    sTempString = sSpace + "-";
                    //fSetGridValue(iRowCount, nodeCheck, sTempString, "T","","");
                }
                
                // view attribute check
                if (viewcheck == "Y") {
                    pauselvcheck = nodeCheck;
                    if (nodeCheck == "1" && checkcheck == "0") {
                        fSetGridValue_girokView(iRowCount,2,sTempString,"T","","", pGrid, pTextImgFlag, nextValue, null, null);
                        sTempString = "";
                        sTempString = tt + nodedesc;
                        fSetGridValue_girokView(iRowCount,1,sTempString,"T","","", pGrid, pTextImgFlag, nextValue, null, temp.nodeName);
                        sTempString = "";
                        checkcheck = "1";
                        groupcheck = nodedesc;
                    } else if (nodeCheck == "1"  && checkcheck == "1") {
                    	  // 2014.06 value instance view='N'인 마지막 parent instacne nodename 표시시 들여쓰기 문제해결 ex) 01897822 5/15 경과기록 치료
                        if ( nodetempCheck < 3) nodetempCheck = 2;                     	                    	
                        fSetGridValue_girokView(iRowCount,nodetempCheck,sTempString,"T","","", pGrid, pTextImgFlag, nextValue, null, null);
                        sTempString = "";
                        sTempString = tt + nodedesc;
                        fSetGridValue_girokView(iRowCount,1,sTempString,"T","","", pGrid, pTextImgFlag, nextValue, null, temp.nodeName);
                        sTempString = "";
                        groupcheck = nodedesc;
                    // group명과 동일한 template 명은 제외
                    } else if (groupcheck != nodedesc) {
                        //초진기록 주호소그룹 처리
                        if  (nodedesc == "발생일") {
                            sTempString = sTempString + "  " + nodedesc+" :";
                        } else {
                            if (nodeCheck == "2"  && temp.firstChild.nodeTypedValue != "None") {
                                if (sTempString != "") {
                                        fSetGridValue_girokView(iRowCount,nodeCheck,sTempString,"T","","", pGrid, pTextImgFlag, nextValue, null, null);
                                        sTempString = "";
                                        nodetempCheck = nodeCheck;
                                }
                                temptxt = nodedesc+"(+)";
                                sTempString = sTempString + nextcheck + temptxt;
                                //fSetGridValue(iRowCount,nodeCheck,temptxt,"T","","");
                                tempcheck = nodedesc;
                            } else {
                                temptxt = nodedesc;
                                if (temp.nodeName == "path" || temp.nodeName == "exvg") {
                                } else {
                                    if (sTempString != "") {
                                        fSetGridValue_girokView(iRowCount,nodeCheck,sTempString,"T","","", pGrid, pTextImgFlag, nextValue, null, null);
                                        sTempString = "";
                                        nodetempCheck = nodeCheck;
                                    }
                                    temptxt = tt + nodedesc;
                                    //fSetGridValue(iRowCount,nodeCheck,sTempString,"T","","");
                                    sTempString = temptxt;
                                    tempcheck = nodedesc;
                                    tempNodeCheck = nodeCheck;
                                }
                            }
                        }
                    }
                }
            }
            temptxt = "";
        } else {
            var parent1elem = temp.parentNode.nodeName;
            var parent2node = temp.parentNode.parentNode;
            var parent2elem = parent2node.nodeName;
            var parent2name = parent2node.selectSingleNode("./@name");
            var parent2val  = "";

            if (parent2name == null) {
                parent2val = "none";
            } else {
                parent2val = parent2name.nodeValue;
            }
            if (parent1elem == "path" || parent1elem == "exvg" || parent1elem.substring(0, 2) == "FH") {

            // 2.나.b.(1) : 상위노드이름이 freetext 이고 3Level 인경우
            } else if (parent1elem == "freetext" && nodeCheck == "3") {
                if (sTempString != "") fSetGridValue_girokView(iRowCount,nodeCheck,sTempString,"T","","", pGrid, pTextImgFlag, "Y", null, null);

                sTempString = tt;

                temptxt = "";
                temptxt = temp.nodeValue;
                temptxt = temptxt.lvReplaceWord("\n", "\n" + tt);
                sTempString = sTempString + temptxt;
            } else if (parent1elem == "Y" ||  parent1elem == "N" || parent1elem == "valueL" || parent1elem == "valueR") {
                if (temp.nodeValue == "Y") {
                    sTempString = sTempString + "(+)";
                } else if (temp.nodeValue == "N") {
                    sTempString = sTempString + "(-)";
                } else if (tempcheck != temp.nodeValue) {
                    temptxt = "";
                    temptxt = temp.nodeValue;
                    temptxt = temptxt.lvReplaceWord("\n", "\n" + tt);
                    sTempString = sTempString + "  " + temptxt;
               }
            } else if (parent1elem == "value" && tempcheck != temp.nodeValue && parent2elem != "freetext" && parent2elem != "valueL" && parent2val != "freetext" && temp.nodeValue != "None") {//checkbox bind 제외
                //None은 (-)로 표시
                temptxt = "";
                temptxt = temp.nodeValue;
                temptxt = temptxt.lvReplaceWord("\n", "\n" + tt);
                sTempString = sTempString + "  " + temptxt;
            } else if (temp.nodeValue != "None" && tempcheck != temp.nodeValue) {//checkbox bind 제외
                temptxt = "";
                temptxt = temp.nodeValue;
                temptxt = temptxt.lvReplaceWord("\n", "\n" + tt);
                sTempString = sTempString + "  " + temptxt;
            } else if (temp.nodeValue == "None") {
                if (sTempString == ""){
                	sTempString = "  None(-)";
                } else {
                  sTempString = sTempString + "(-)";
                }
            }
            temptxt = "";
        }

        temp = fGetNode(temp, rootTemp);
        if (temp == null) break;
        //2009.09.02
        //model.alert("sTempString := " + sTempString);
        //model.alert("tt := " + tt);
    }
    
    // 2014.06 value instance view='N'인 마지막 parent instacne nodename 표시시 들여쓰기 문제해결 ex) 01897822 5/15 경과기록 치료
    if ( nodetempCheck < 3) nodetempCheck = 2;
    fSetGridValue_girokView(iRowCount, nodetempCheck, sTempString, "T", "", "", pGrid, pTextImgFlag, "Y", null, null);
    return;
}

/**
 * @group  :
 * @ver    : 2005-11-24 2:31오후
 * @by     : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : XML구조를 Grid형태로 변환
 * @param  : pNo - 추가 행번호
 * @param  : pPath - 레벨Path
 * @param  : pData - 데이터
 * @param  : pType - 이미지/텍스트
 * @param  : pImgPath - 이미지경로
 * @param  : pImgExvg - 이비지편집
 * @param  : pNextMode - 아래로 보내기 구현(그리드 다음줄 판단)
 * @param  : pSpaceTab -
 * @param  : pCodeInfo -
 * @return :
 * @---------------------------------------------------
 */
 var g_OldMode = "";

function fSetGridValue_girokView(pNo, pPath, pData, pType, pImgPath, pImgExvg, pGrid, pTextImgFlag, pNextMode, pSpaceTab, pCodeInfo) {
    // 2009.09.02
    //model.alert("pData := " + pData);
    //model.alert("pType := " + pType);

    pData = f_NoDisplayPT(pData);

    var sTempTab = "　"; //*** 특수문자 ㄱ 1번의 공백으로 사용하였음...
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        if (pTextImgFlag == "T") {
            pGrid = emr_Right.vsGrid("grid_view");
        }
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }

    if (pData == ""||pData == "G999") {
        return;
    }

    if (pType == "T") {
        if (pData.indexOf("exvg") != -1 || pData.indexOf("Xvg") != -1) {
            return;
        }
    }

    var m_CodeInfo = "";
    if (pCodeInfo != null) {
        m_CodeInfo = pCodeInfo;
    }

   if (pPath == 0) { //맨첫줄
       fWriteFile(pPath, pData, pType)
       return;
   } else if (pPath == 1) {
       pData = pData + ">";
   } else {
   }

    var re = new RegExp("[▩]",["gi"])
    pData  = pData.replace(re, "\n   ");

    if (pType == "T") {
        var m_TempData = pData.split("\n");
        var m_Temp = "";

        if (g_OldMode == "") {
            g_OldMode = pNextMode;
        }
/*
        if (g_OldMode == "N") {
            for (var iCnt = 0; iCnt < m_TempData.length; iCnt++) {
                //var m_Temp = pGrid.TextMatrix(pGrid.Rows - pGrid.FixedRows, 0);
                //pGrid.TextMatrix(pGrid.Rows - pGrid.FixedRows, 0) = m_Temp + m_TempData[iCnt];
            }
        } else {
            // 단순히 한줄로만 표현하고자 한다면, 위로직을 쓰지 않고 아래처럼 간단히 구현함
            for (var iCnt = 0; iCnt < m_TempData.length; iCnt++) {
                //pGrid.AddItem(m_TempData[iCnt] + g_Separators + pPath + g_Separators + g_Separators + g_Separators + pType + g_Separators + "");
            }
        }
*/
        for (var iCnt = 0; iCnt < m_TempData.length; iCnt++) {
            if (pPath == 2) {
                m_TempData[iCnt] = sTempTab + m_TempData[iCnt];
            }
            if (pTextImgFlag == "T") {
                emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]");
                emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/data");
                emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/path");
                emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgpath");
                emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgexvg");
                emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/type");
                emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/size");
                emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/InfoCode");

                emr_Right.setValue("/root/SubData/viewinfo[" +pNo+ "]/path", pPath);
                emr_Right.setValue("/root/SubData/viewinfo[" +pNo+ "]/type", pType);
                emr_Right.setValue("/root/SubData/viewinfo[" +pNo+ "]/InfoCode", m_CodeInfo);
            } else {
                model.makenode("/root/SubData/viewinfo[" +pNo+ "]");
                model.makenode("/root/SubData/viewinfo[" +pNo+ "]/data");
                model.makenode("/root/SubData/viewinfo[" +pNo+ "]/path");
                model.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgpath");
                model.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgexvg");
                model.makenode("/root/SubData/viewinfo[" +pNo+ "]/type");
                model.makenode("/root/SubData/viewinfo[" +pNo+ "]/size");
                model.makenode("/root/SubData/viewinfo[" +pNo+ "]/InfoCode");

                model.setValue("/root/SubData/viewinfo[" +pNo+ "]/path", pPath);
                model.setValue("/root/SubData/viewinfo[" +pNo+ "]/type", pType);
                model.setValue("/root/SubData/viewinfo[" +pNo+ "]/InfoCode", m_CodeInfo);
            }

            //model.alert("["+m_TempData[iCnt]+"]");
            if (m_TempData[iCnt] == "G999>" || m_TempData[iCnt] == "G024>") {
                if (pTextImgFlag == "T") {
                    pData = "    "; //종합판정이거나 과별서식인경우 타이틀을 빈공란으로 보여준다. //추가(20050725)
                } else {
                    pData = "    "; //종합판정이거나 과별서식인경우 타이틀을 빈공란으로 보여준다. //추가(20050725)
                    fWriteFile(pPath, pData, pType); //2006.03.31 taja78 추가 ===========
                }
            } else {
                pData = m_TempData[iCnt];
            }

            if (pTextImgFlag == "T") {
                //2006-11-21 2:23오후 add taja78
                emr_Right.setValue("/root/SubData/viewinfo[" +pNo+ "]/data",pData);
            } else if (pTextImgFlag == "P") {
                model.setValue("/root/SubData/viewinfo[" +pNo+ "]/data", pData);
                fWriteFile(pPath, pData, pType);
            } else {
                model.setValue("/root/SubData/viewinfo[" +pNo+ "]/data", pData);
                fWriteFile(pPath, pData, pType);
            }

            pNo = pNo + 1;
            iRowCount = iRowCount + 1;
        }//for
    } else if (pType == "I"||pType == "F") {
        if (pTextImgFlag == "T") {
            emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]");
            emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/data");
            emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/path");
            emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgpath");
            emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgexvg");
            emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/type");
            emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/size");
            emr_Right.makenode("/root/SubData/viewinfo[" +pNo+ "]/InfoCode");

            emr_Right.setValue("/root/SubData/viewinfo[" +pNo+ "]/path", pPath);
            emr_Right.setValue("/root/SubData/viewinfo[" +pNo+ "]/type", pType);

            emr_Right.setValue("/root/SubData/viewinfo[" +pNo+ "]/imgpath", pImgPath);
            emr_Right.setValue("/root/SubData/viewinfo[" +pNo+ "]/imgexvg", pImgExvg);
            emr_Right.setValue("/root/SubData/viewinfo[" +pNo+ "]/InfoCode", m_CodeInfo);
        } else {
            model.makenode("/root/SubData/viewinfo[" +pNo+ "]");
            model.makenode("/root/SubData/viewinfo[" +pNo+ "]/data");
            model.makenode("/root/SubData/viewinfo[" +pNo+ "]/path");
            model.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgpath");
            model.makenode("/root/SubData/viewinfo[" +pNo+ "]/imgexvg");
            model.makenode("/root/SubData/viewinfo[" +pNo+ "]/type");
            model.makenode("/root/SubData/viewinfo[" +pNo+ "]/size");
            model.makenode("/root/SubData/viewinfo[" +pNo+ "]/InfoCode");

            model.setValue("/root/SubData/viewinfo[" +pNo+ "]/path", pPath);
            model.setValue("/root/SubData/viewinfo[" +pNo+ "]/type", pType);

            model.setValue("/root/SubData/viewinfo[" +pNo+ "]/imgpath", pImgPath);
            model.setValue("/root/SubData/viewinfo[" +pNo+ "]/imgexvg", pImgExvg);
            model.setValue("/root/SubData/viewinfo[" +pNo+ "]/InfoCode", m_CodeInfo);
        }

        if (pTextImgFlag != "T") {
            if (pTextImgFlag == "P") {
                iMRR_GRGeomSaek_Print_ImageCnt++;
                // 2008-05-16 4:37오후, 실제파일이 존재하는지 점검후 처리해야 함 : newhope
                var m_imgFile = g_EMRImageTempPath+"\\EMR_IMG_TEMP_G" + iMRR_GRGeomSaek_Print_ImageCnt + ".jpg";
                if (g_fso.FileExists(m_imgFile)) {
                    pData = "[<image path="+g_EMRImageTempPath+"\\EMR_IMG_TEMP_G"+iMRR_GRGeomSaek_Print_ImageCnt+".jpg, size=200>]";
                    fWriteFile(pPath, pData, pType);
                } else {
                }
            } else {
                fWriteFile(pPath, pData, pType);
            }
        }

        pNo = pNo + 1;
        iRowCount = iRowCount + 1;
    }
}

/**
 * @group  :
 * @ver    : 2006.03.02
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 해당 정보 클릭시 시작일, 종료일 조회
 * @param  : pRow : current row
 * @param  : pLevl : levl
 * @param  : pDateFlag : "date, form"(일자별/서식지별)
 * @return : 시작, 종료일
 * @remark : 2006.04.17 일자별/서식지별 시작일, 종료일 구하는 법이 틀리므로 Flag처리로 로직변경(김태범)
 * @---------------------------------------------------
 */
function fGet_FrDateToDate(pRow, pLevl, pDateFlag) {
    var iGridCnt = TFGetGridCount("grid_main");
    var sTempstr1;
    var sTempstr2;
    var sTemplevl = "";

    var sTempArr;
    var iCnt = 0;

    if (pDateFlag == "date") { //일자별 조회일 경우
        if (pLevl =="2") {
            sTempstr1 = model.getValue("/root/MainData/mrrfflshh["+pRow+"]/chrtnm").lvReplaceWord("/","");
            sTemplevl = model.getValue("/root/MainData/mrrfflshh["+(pRow+1)+"]/levl");

            if (sTemplevl =="3") {
                sTempstr2 = model.getValue("/root/MainData/mrrfflshh["+(pRow+1)+"]/chrtnm").lvReplaceWord("/","");
            } else {
                sTempstr2 = sTempstr1;
            }
        } else if (pLevl =="3") {  //levl 이 3일 경우 한날짜만 Display되기 때문에 시작일 종료일을 동일하게 한다.
            sTempstr1 = model.getValue("/root/MainData/mrrfflshh["+pRow+"]/chrtnm").lvReplaceWord("/","");
            sTempstr2 = sTempstr1;
        } else if (pLevl =="4") {
            sTempstr1 = model.getValue("/root/MainData/mrrfflshh["+i+"]/chrtnm").lvReplaceWord("/","");
            sTempstr2 = sTempstr1;
        } else {
            return; // levl이 1일 경우 return
        }
    } else if (pDateFlag == "form") { //서식지별 조회일 경우 *********** 수정중이었음 2006.04.17
        if (pLevl =="2") {
            sTempArr = model.getValue("/root/MainData/mrrfflshh["+pRow+"]/seq").split("|");
            sTempstr1 = sTempArr[2]; //내원일자  예) <seq>1|520050601|20050601|ZZZZZZZZZZ|99999999|29991231|</seq>
            sTempstr2 = sTempArr[5]; //퇴원일자
        } else if (pLevl =="3") {  //levl 이 3일 경우 한날짜만 Display되기 때문에 시작일 종료일을 동일하게 한다.
            for (var j = parseInt(pRow)+1 ; j < iGridCnt+1; j++) {
                sTemplevl = model.getValue("/root/MainData/mrrfflshh["+j+"]/levl");
                //시작일, 종료일이 바뀌는것을 주의한다.
                if (sTemplevl == "4") {
                    if (j == parseInt(pRow)+1) {
                        sTempstr2 = model.getValue("/root/MainData/mrrfflshh["+j+"]/chrtnm").lvReplaceWord("/","");
                        sTempstr1 = sTempstr2;
                    } else {
                        sTempstr1 = model.getValue("/root/MainData/mrrfflshh["+j+"]/chrtnm").lvReplaceWord("/","");
                    }
                } else {//sTemplevl == "4"
                    break;
                }
            } //for
        } else if (pLevl =="4") {
            sTempstr1 = model.getValue("/root/MainData/mrrfflshh["+pRow+"]/chrtnm").lvReplaceWord("/","");
            sTempstr2 = sTempstr1;
        } else {
            return; // levl이 1일 경우 return
        }
    }
    return sTempstr1.substr(0 ,8) +"▦"+sTempstr2.substr(0,8) ;  //frdd▦todd
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : OCR조회
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCall_OCR(pRow, pNode, pOnlyOcr) {
    var sLevl = model.getValue(pNode + pRow + "]/levl");
    var sChrtnm = model.getValue(pNode + pRow + "]/chrtnm");
    var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
    var sParm = sChrtkind.lvReplaceWord("|", "▦");
    var sEmr = TFGetMatrixData(sParm, 0, 0); //EMR, OCR 구분
    var sDpfg = TFGetMatrixData(sParm, 0, 1); //진료기록,간호기록 구분
    var sIdno = TFGetMatrixData(sParm, 0, 2);
    var sPtdt = TFGetMatrixData(sParm, 0, 3);
    var sIofg = TFGetMatrixData(sParm, 0, 4);
    var sChrt = TFGetMatrixData(sParm, 0, 5);
    var sDate = TFGetMatrixData(sParm, 0, 6);
    var sDept = TFGetMatrixData(sParm, 0, 7);
    var sDept2 = sDept;
    var sDoct = TFGetMatrixData(sParm, 0, 8);
    var sOcrKind = model.getValue("/root/HideData/condition/cond1");

    //***************************        아주 중요!!!!   *************************
    // 2007.10.16일 : BIT가 관리하는 chartpaget 테이블의 진료과 정보는 전과할 경우
    //              입원과로 들어가있다. 현재 기록검색에서 좌측 리스트는 재원과로
    //              수정하였으므로 BIT쪽 테이블 데이터는 update하지 않고 조회 시
    //              진료과 정보를 뺀다.(2007년 10월4일 저녁부터는 전과시 재원과로 BIT에서
    //              insert 하도록 소스 수정하고 감. 5일부터 Scan한 내역에대하여는 정상 작동)
    //              추후, BIT 데이터 정보를
    //****************************************************************************
    sDept = ""; //조치된 사항.
    //****************************************************************************

    //외래/입원구분, 등록번호, 진료과코드, 조회시작일자(진료일자 기준), 조회종료일자(진료일자 기준), 서식지코드, 서식지종류
    if (sIofg == "O" || sIofg == "E") {
        if (sLevl == "3") {
            if (sChrtnm.substr(0,1) == "2") { //일자별조회인 경우
                if (sOcrKind == "nurse") { //간호기록
                    var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept + ";dtpube=" + sDate + ";dtpuen=" + sDate + ";ctkd= '05'"; //3단계틀릭시 시행일자를 argument로 한다.
                } else if (sOcrKind == "rslt") { //검사기록
                    var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept + ";dtpube=" + sDate + ";dtpuen=" + sDate + ";ctkd= '03'"; //3단계틀릭시 시행일자를 argument로 한다.
                } else if (sOcrKind == "ocr") { //기타
                    var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept + ";dtpube=" + sDate + ";dtpuen=" + sDate + ";ctkd= '07'"; //3단계틀릭시 시행일자를 argument로 한다.
                } else { //진료기록
                    var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept + ";dtpube=" + sDate + ";dtpuen=" + sDate + ";ctkd= '01','02','04','06','99'"; //3단계틀릭시 시행일자를 argument로 한다.
                }
            } else { //서식지별조회인 경우
                var sFrdt = model.getValue("/root/MainData/condition/frdt");
                var sTodt = model.getValue("/root/MainData/condition/todt");
                var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept + ";dtbe=" + sFrdt + ";dten=" + sTodt + ";fmcd=" + sChrt; //3단계틀릭시 입원일자,서식지코드를 argument로 한다.
            }
        } else {
            var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept2 + ";dtpube=" + sDate + ";dtpuen=" + sDate + ";fmcd=" + sChrt; //4단계틀릭시 시행일자,서식지코드를 argument로 한다.
        }
    } else if (sIofg == "I") {
        if (sLevl == "2") {
            if (sOcrKind == "nurse") { //간호기록
                var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";dtbe=" + sPtdt + ";dten=" + sPtdt + ";ctkd= '05'"; //2단계틀릭시 입원일자를 argument로 한다.
            } else if (sOcrKind == "rslt") { //검사기록
                var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";dtbe=" + sPtdt + ";dten=" + sPtdt + ";ctkd= '03'"; //2단계틀릭시 입원일자를 argument로 한다.
            } else if (sOcrKind == "ocr") { //기타
                var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";dtbe=" + sPtdt + ";dten=" + sPtdt + ";ctkd= '07'"; //2단계틀릭시 입원일자를 argument로 한다.
            } else { //진료기록
                var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";dtbe=" + sPtdt + ";dten=" + sPtdt + ";ctkd= '01','02','04','06','99'"; //2단계틀릭시 입원일자를 argument로 한다.
            }
        } else if (sLevl == "3") {
            var sNewDateBeg = "";
            var sNewDateEnd = "";
            var sTempSeq    = "";
            var sTempLevl = "";

            var sFlag = model.getValue("/root/HideData/condition/cond1");

            if (sFlag == "date") {
                sTempSeq = model.getValue(pNode + pRow + "]/seq").split("|");
                sNewDateBeg = sTempSeq[3];
                sNewDateEnd = sTempSeq[3]+"235959";
            } else if (sFlag == "form") {
                for (var k = pRow -1 ; k > 0 ; k--) {
                    sTempLevl = model.getValue(pNode + k + "]/levl");
                    if (sTempLevl == "2") {
                        sTempSeq = model.getValue(pNode + k + "]/seq").split("|");
                        sNewDateBeg = sTempSeq[2];
                        sNewDateEnd = sTempSeq[5]+"235959";
                        break;
                    }
                }
            } else {
                sNewDateBeg = sDate;
                sNewDateEnd = sDate;
            }
            //======================= 추가사항 End ===========================
            if (sChrtnm.substr(0,1) == "2") {//일자별조회인 경우
                if (sOcrKind == "nurse") {//간호기록
                    var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept + ";dtpube=" + sNewDateBeg + ";dtpuen=" + sNewDateEnd + ";ctkd= '05'"; //3단계틀릭시 시행일자를 argument로 한다.
                } else if (sOcrKind == "rslt") {//검사기록
                    var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept + ";dtpube=" + sNewDateBeg + ";dtpuen=" + sNewDateEnd + ";ctkd= '03'"; //3단계틀릭시 시행일자를 argument로 한다.
                } else if (sOcrKind == "ocr") {//기타
                    var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept + ";dtpube=" + sNewDateBeg + ";dtpuen=" + sNewDateEnd + ";ctkd= '07'"; //3단계틀릭시 시행일자를 argument로 한다.
                } else {//진료기록
                    var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept + ";dtpube=" + sNewDateBeg + ";dtpuen=" + sNewDateEnd + ";ctkd= '01','02','04','06','99'"; //3단계틀릭시 시행일자를 argument로 한다.
                }
            } else { //서식지별조회인 경우
                var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept + ";dtbe=" + sNewDateBeg + ";dten=" + sNewDateEnd + ";fmcd=" + sChrt; //3단계틀릭시 입원일자,서식지코드를 argument로 한다.
            }
        } else {
            var sData = "trty=" + sIofg + ";ptno=" + sIdno + ";puclcd=" + sDept + ";dtpube=" + sDate + ";dtpuen=" + sDate + ";fmcd=" + sChrt; //4단계틀릭시 시행일자,서식지코드를 argument로 한다.
        }
    }

    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_OCR");
    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sData);
    model.setAttribute("MRR_GRGeomSaek_Ocr.pOnlyOcr", pOnlyOcr);
    
    var emr_Right = null;
    if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") {
        model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
    } else {
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            emr_Right.javascript.searchChart(sData);    // CMR_Viewer_S.js
            if (pOnlyOcr == "Y") { //이미지만 있는경우 display
                fWaitBegin("이미지 Loading중...");
                emr_Right.javascript.downloadChart();
                emr_Right.javascript.setupChart("ChartView");//연속보기로 설정
                fWaitEnd();
            } else {//이미지와 TextEMR이 같이 존재하는경우 마지막에 display
            }

            model.setAttribute("MRR_GRGeomSaek_Ocr.function", "");
            model.setAttribute("MRR_GRGeomSaek_Ocr.sData", "");
            model.setAttribute("MRR_GRGeomSaek_Ocr.pOnlyOcr", "");
        } else {
            model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
            return;
        }
    }
    var sBitprtauth = TFGetSessionInfo("bitprtauth"); //출력권한

    if (sBitprtauth == "1" || model.getValue("/root/MainData/limit/ercheck") == "Y" || gGiRokSinCheong_print == "Y") { //2007-09-19 4:54오후 김태범 권한신청 관련 출력권한 수정
        model.enable("button_print", "true"); //출력
    }
    //2006-11-23 6:34오후 김태범 조회중 리스트를 클릭 전역변수 세팅
    sTempSearchFlag = "N"
    model.setAttribute("girokgeomsaek_searchflag_param","N");
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Order조회(OEM_JRJBJoHoi.js 참조) 처방조회
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCall_OCS(pRow, pNode) {
    var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
    var sParm = sChrtkind.lvReplaceWord("|", "▦");
    var sEmr = TFGetMatrixData(sParm, 0, 0);
    var sIdno = TFGetMatrixData(sParm, 0, 1);
    var sDate = TFGetMatrixData(sParm, 0, 2);
    var sDept = TFGetMatrixData(sParm, 0, 3);
    var sKind = TFGetMatrixData(sParm, 0, 4);
    var sGubn = TFGetMatrixData(sParm, 0, 5);
    var sDoct = TFGetMatrixData(sParm, 0, 6);
    var sIndd = TFGetMatrixData(sParm, 0, 7); //외래(진료일자) 입원(입원일자)
    var sOudd = TFGetMatrixData(sParm, 0, 8); //외래(진료일자) 입원(퇴원일자)
    var sOutordkind = TFGetMatrixData(sParm, 0, 9); //외래,입원,응급
		
    if (sGubn == "I1") { //입원버튼(2단계)을 클릭했을경우
        if (sOudd == "29991231") { //재원중인 경우
            var sTodt = new Date();
            var sFrdt = new Date(sTodt - (1000 * 60 * 60 * 24 * 7)); //today의 7일전
            sTodt = sTodt.lvFormat();
            sFrdt = sFrdt.lvFormat();
        } else {
            var sTodt = sOudd.lvToDate("YYYYMMDD");
            var sFrdt = new Date(sTodt - (1000 * 60 * 60 * 24 * 7)); //퇴원일자의 7일전
            sTodt = sTodt.lvFormat();
            sFrdt = sFrdt.lvFormat();
        }

        var sChrtkind2 = model.getValue(pNode + (parseInt(pRow) + 1) + "]/dept");
        var sParm2 = sChrtkind2.lvReplaceWord("|", "▦");
        var sDeptName = TFGetMatrixData(sParm2, 0, 1);

    } else {
        var sFrdt = sDate;
        var sTodt = sDate;

        var sChrtkind2 = model.getValue(pNode + pRow + "]/dept");
        var sParm2 = sChrtkind2.lvReplaceWord("|", "▦");
        var sDeptName = TFGetMatrixData(sParm2, 0, 1);

    }
	
    model.setAttribute("sMRR_GRGeomSaek_idno", sIdno);
    model.setAttribute("sMRR_GRGeomSaek_gubn", sGubn);
    model.setAttribute("sMRR_GRGeomSaek_frdt", sFrdt);
    model.setAttribute("sMRR_GRGeomSaek_todt", sTodt);
    model.setAttribute("sMRR_GRGeomSaek_dept", sDept);
    model.setAttribute("sMRR_GRGeomSaek_kind", sKind);
    model.setAttribute("sMRR_GRGeomSaek_doct", sDoct);
    model.setAttribute("sMRR_GRGeomSaek_indd", sIndd);
    model.setAttribute("sMRR_GRGeomSaek_oudd", sOudd);
    model.setAttribute("sMRR_GRGeomSaek_outordkind", sOutordkind);
    model.setAttribute("sMRR_GRGeomSaek_name", model.getValue("/root/MainData/opmcptbsm/name"));
    model.setAttribute("sMRR_GRGeomSaek_age_sex", model.getValue("/root/MainData/opmcptbsm/age_sex"));
    model.setAttribute("sMRR_GRGeomSaek_deptname", sDeptName);

    if (fGetIP_xfm() != "MRR_GRGeomSaek_Order") {	
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Order.xfm","replace");
		model.setAttribute("MRR_GRGeomSaek_PSYChk", model.getValue("/root/MainData/limit/grcheck"));	
    } else {	
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            emr_Right.javascript.fOrder_Open();

            emr_Right.resetInstanceNode("/root/SendData");
            emr_Right.setValue("/root/SendData/Mode", "MRR_reqGetOrder"); // Action Method
            emr_Right.setValue("/root/SendData/Data1", sIdno);
            emr_Right.setValue("/root/SendData/Data2", sFrdt); //시작일자
            emr_Right.setValue("/root/SendData/Data3", sTodt); //종료일자
            emr_Right.setValue("/root/SendData/Data4", sDept);
            emr_Right.setValue("/root/SendData/Data5", sKind);
            emr_Right.setValue("/root/SendData/Data6", sGubn);
            emr_Right.setValue("/root/SendData/Data7", sDoct);
            emr_Right.setValue("/root/SendData/Data8", "-"); //처방분류
            emr_Right.setValue("/root/SendData/Data9", sOutordkind); //외래,입원,응급 구분
            emr_Right.setValue("/root/SendData/psychk", model.getValue("/root/MainData/limit/grcheck"));
            emr_Right.setValue("/root/SendData/anecheck", model.getValue("/root/MainData/limit/anecheck"));
            
            if (emr_Right.submitInstance("MRR_reqGetOpmcptbsm")) {
                TFSetMessage("/root/MainData/order");
                fSellMerge("ocs", "grid_order");
                
                // 2019.04.29 추가 (분류 -> 지시), (상태 -> 처방)인 경우 실시내역을 표시하지 않기 위함.
                var rGrid = emr_Right.vsGrid("grid_order");
                for(var i = 1; i < rGrid.Rows; i++){
                	if(emr_Right.getValue("/root/MainData/order["+i+"]/temp_gubn") == "09"){
						if(emr_Right.getValue("/root/MainData/order["+i+"]/opod_ordstat") == "A"){
							emr_Right.setValue("/root/MainData/order["+i+"]/temp_col03", "");
						}
					}
                }
                
            } else {
                emr_Right.removeNodeset("/root/MainData/order");
            }
            emr_Right.gridRefresh("grid_order");
        } else {
            return;
        }	
    }
    return;
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Order상세조회(삭제함)
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCall_OCS_dept(pNode) {
    model.setAttribute("OER_CBJJHwakIn_parm", "");
    model.setAttribute("OER_CBJJHwakIn_rtnflag", "N");
    model.setAttribute("OER_CBJJHwakIn_rtnparm", "");

    var order_exit = "0";
    var iRow = TFGetGridCount("grid_main");
    if (iRow > 0) {
        var fg = model.vsGrid("grid_main");
        for (var i = fg.FixedRows; i < fg.Rows; i++) {
            var sLevl = model.getValue(pNode + i + "]/levl");
            if (sLevl == "2") {
                order_exit = "1";
                var sCherkind2 = model.getValue(pNode + i + "]/chrtkind2");
                var sParm = sCherkind2.lvReplaceWord("|", "▦");
                model.setAttribute("OEM_JRJBJoHoiHelp_parm", sParm);
                TFshowModal(null, null, "1", "/ocs/cheobangweb/xfm/OEM_JRJBJoHoiHelp.xfm", "-", 1035, 650, 0, 0, false, false);
                return;
            }
        } // for문
    }
    if (order_exit == "0") {
        TFGetMsgBox(-1, "Order를 확인하십시요.", "확인", "I", "OK");
    }
    TFSetMessage("");
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Nurse상세조회(간호기록 정보조사지)
 * @param  :
 * @return :
 * @이  력 :
 * @        김태범 2007-01-25 2:46오후 간호 EMR기록 3레벨 조회기능 추가
 * @---------------------------------------------------
 */
function fCall_NUR1(pRow, pNode) {
    var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
    var sParm = sChrtkind.lvReplaceWord("|", "▦");
    var sNur = TFGetMatrixData(sParm, 0, 0); //Nurse구분
    var sIofg = TFGetMatrixData(sParm, 0, 1);
    var sPtdt = TFGetMatrixData(sParm, 0, 2);
    var sChnm = TFGetMatrixData(sParm, 0, 3);
    var sChrt = TFGetMatrixData(sParm, 0, 4);
    var sDate = TFGetMatrixData(sParm, 0, 5);
    var sTime = TFGetMatrixData(sParm, 0, 6);
    var sChrtNm = TFGetMatrixData(sParm, 0, 7);
    var sPkiDateTime = sDate + sTime + "%";

    model.removeNodeset("/root/HideData/savedata_nurse");
    TFclearNodeValue("/root/SendData"); //이전의 서버 호출에 사용된 정보 지움
    model.setValue("/root/SendData/Mode", "MRR_reqGetMrnipkidh"); //Action Method
    model.setValue("/root/SendData/Data1", "NUR1"); //간호조회 구분
    model.setValue("/root/SendData/Data2", model.getValue("/root/MainData/condition/cond1")); //등록번호

    var sLevl = model.getValue(pNode + pRow + "]/levl");
    var sTempLevl;
    var sTempPkiDateTime = "";
    if (sLevl == "3") {
		sPkiDateTime = "";

		for (var i = pRow+1; i <= TFGetGridCount("grid_main"); i++) {

		//---- 2010.12 PSY 기록검색권한 제한처리
		var psychk = model.getValue("/root/MainData/mrrfflshh[" + i + "]/chrtkind");
		var arr_psychk = psychk.split("|");
		sTempLevl = model.getValue(pNode + i + "]/levl");
		if (sTempLevl == "4") {
			if (sMRR_GRGeomSaek_ChrtPSY == "Y" || (sMRR_GRGeomSaek_ChrtPSY != "Y" && arr_psychk[8] != "N")) {
				if(sMRR_GRGeomSaek_ChrtPSY == "S" && arr_psychk[8] == "S"){
					sTempPkiDateTime = "";
				} else {
					sParm = model.getValue(pNode + i + "]/chrtkind").lvReplaceWord("|", "▦");
					sDate = TFGetMatrixData(sParm, 0, 5);
					sTime = TFGetMatrixData(sParm, 0, 6);
					sPkiDateTime = sDate + sTime + "%";
					if (sTempPkiDateTime != "" && sTempPkiDateTime != null) {
						sTempPkiDateTime = sTempPkiDateTime +","+ sPkiDateTime
					} else {
						sTempPkiDateTime = sPkiDateTime
					}
				}
			}
		} else {
			break;
		}
	}
        model.setValue("/root/SendData/Data3", sTempPkiDateTime); //인증 data 찾아오기 위한 argument //추가(20050725)
        model.setValue("/root/SendData/Data6", "levl3"); //인증 data 찾아오기 위한 argument //추가(20050725)
	} else {
        model.setValue("/root/SendData/Data3", sPkiDateTime); //인증 data 찾아오기 위한 argument //추가(20050725)
    }

    if (model.submitInstance("MRR_reqGetMrnipkidh")) {
        TFSetMessage("root/HideData/savedata_nurse");
        //인증데이터 환자정보의 주치의, 세션정보의 id, 부서 정보 제외. - 2007-03-20 1:24오후 김태범
        // 인증데이터 로그인 시간 제외 추가 - 2007-05-08 4:31오후 김태범
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/PatientInfo/MainDoctor");
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo/userid");
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo/empldeptgmnm");
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo/logondatetime");

        /*
            2007-12-13 5:04오후 : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
            중환자실 요구사항, 중환자실 간호기록(INS0000009) 의 경우 세션정보 안나오도록 처리함
            사유 : 이 기록은 근무 별로(D, E, N) 작성을 하는데 세션정보에는 한분만 보인다고
                   나중에 법정에 이 기록을 출력을하여 갔을 때 오해의 소지존재
        */
        if (sChrt == "INS0000009") {
            model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo");
        }
    } else {
        return false;
    }

    var xPath = "/root/HideData/savedata_nurse";

    TFRemoveNoChild(findNode(xPath));
    //var sText = fCommon_getInstanceStringSignValue(xPath); //현재는 한건만 가져오도록 함
    var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");

    var iNodeCnt = getNodesetCnt(xPath);
    var iCurPos = TFGetGridPos("grid_main");

    if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") { //이미지 조회인경우
        //2007-03-07 1:30오후 간호기록 출력 print와 같은 패턴으로 변경함.
        fDeleteFile("NURSE_");
        sMRR_GRGeomSaek_FileCnt = 0;
        model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_NUR1");
        model.setAttribute("MRR_GRGeomSaek_Ocr.sMRR_GRGeomSaek_FileCnt", 1);
        if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") {
            //2007-03-07 1:30오후 김태범 간호기록 출력 print와 같은 패턴으로 변경함. MRR_GRGeomSaek_Ocr 화면에서 콜하도록 수정함.
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param1", xPath);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param2", sMRR_GRGeomSaek_ImageView);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param3", sTextImage);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param4", iNodeCnt);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param5", iCurPos);

            model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_NUR1_NEW");
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
        } else {
            //2007-03-07 1:30오후 간호기록 출력 print와 같은 패턴으로 변경함.
            fMakeNUR1(xPath, sMRR_GRGeomSaek_ImageView, sTextImage, iNodeCnt, iCurPos);

            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                sTotalKind = sParm + "▩";
                /*
                  == 간호기록 관련 == 2007-09-11 6:16오후 김태범 - 이태진님 요청사항
                  * 해당 서식지일 경우 발생일자 대신 기록지별 특정일자를 보여준다.
                  * 해당 서식지 정보
                  - 수술전처치상태확인 기록지   INC0000001
                  - 수술간호기록                CNS0000001
                  - 회복실 간호기록             CNC0000002
                  - 안정실 기록                 CNS0000003
                 == == == == == == ==
                */
                var sTempArrInfo = sTotalKind.split("▩");
                var sTempArrInfoDetail;
                var sInsertTempData = "";
                var sTempTotalkind = "";

                for (var itempArrSeq = 0; itempArrSeq < sTempArrInfo.length ; itempArrSeq++) {
                    sTempArrInfoDetail = sTempArrInfo[itempArrSeq].split("▦");
                    var sTempChrtcdInfo = sTempArrInfoDetail[4];
                    if (sTempChrtcdInfo == "INC0000001" || sTempChrtcdInfo == "CNS0000001" || sTempChrtcdInfo == "CNC0000002" || sTempChrtcdInfo == "CNS0000003") {
                        sInsertTempData = sTempArrInfoDetail[0] + "▦" +        //해당 선택된 grid 간호 기록의 chrtkind
                                          sTempArrInfoDetail[1] + "▦" +
                                          model.getValue(pNode + pRow + "]/chrtnm").lvReplaceWord("/","").substr(0,8) + "▦" +      // title date (OCR기록 상단날짜 정보)
                                          sTempArrInfoDetail[3] + "▦" +      //
                                          sTempArrInfoDetail[4] + "▦" +
                                          sTempArrInfoDetail[5] + "▦" +      //

                                          sTempArrInfoDetail[6] + "▦" +
                                          sTempArrInfoDetail[7] + "▦" + "▩";
                        sTempTotalkind = sTempTotalkind+sInsertTempData;
                    } else {
                        if (sTempChrtcdInfo != null && sTempChrtcdInfo != "") {
                            sInsertTempData = sTempArrInfoDetail[0] + "▦" +        //해당 선택된 grid 간호 기록의 chrtkind
                                              sTempArrInfoDetail[1] + "▦" +
                                              sTempArrInfoDetail[2] + "▦" +
                                              sTempArrInfoDetail[3] + "▦" +
                                              sTempArrInfoDetail[4] + "▦" +
                                              sTempArrInfoDetail[5] + "▦" +
                                              sTempArrInfoDetail[6] + "▦" +
                                              sTempArrInfoDetail[7] + "▦" + "▩";
                            sTempTotalkind = sTempTotalkind+sInsertTempData;
                        }
                    }
                }
                sTotalKind = sTempTotalkind;
                emr_Right.javascript.fViewerSetting(sTotalKind, "NURSE_");
                fWaitBegin("이미지 Loading중...");
                emr_Right.javascript.downloadChart(); //
                fWaitEnd();
                emr_Right.javascript.setupChart("ChartView"); //연속보기로 설정
            } else {
                return;
            }
        }
    } else if (sTextImage == "P") {
        //XFR로 출력할 경우..
        //fMakeImage_XFR_Print("EMR_GanHo"); 시점차이로 인한 문제 해결을 위해 주석처리함.
        model.setAttribute("MRR_GRGeomSaek_ReportView.function", "fMakeImage_XFR_Print");
        model.setAttribute("MRR_GRGeomSaek_ReportView.function_param", "EMR_GanHo");
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
    } else {
         if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") {
            model.setAttribute("MRR_GRGeomSaek_Grid.function" , "fCall_NUR1_NEW");
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param1", xPath);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param2", sMRR_GRGeomSaek_ImageView);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param3", sTextImage);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param4", iNodeCnt);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param5", iCurPos);

            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
         } else {
            if (emr_Right) {
                fMakeNUR1(xPath, sMRR_GRGeomSaek_ImageView, sTextImage, iNodeCnt, iCurPos);
            } else {
                return;
            }
        }
    }
}

 /**
 * @group  :
 * @ver    : 2007.03.07
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 간호기록 Text로 저장
 * @param  : xPath - signData의 xPath
 * @param  : sMRR_GRGeomSaek_ImageView - Y
 * @param  : sTextImage - T/I/P
 * @return :
 * @---------------------------------------------------
 */
function fMakeNUR1(xPath, sMRR_GRGeomSaek_ImageView, sTextImage, iNodeCnt, iCurPos) {
    var pFileWriteFlag = "";
    var MyFile;
    var fso;
    var sFilePath;
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    var emr_Right_fg;
    var sBeforeData;
    var sPtchchrtnm;
    var tempArr;
    var sPtchchrtcd;
    var iGanHoImgSeqNo = 1;
    var sTempString = "";

    var sPidInfo    = model.getValue("/root/MainData/condition/cond1");
    model.setAttribute("sMRR_GiRokGeomSaek_Param.fMakeNUR1.pid", sPidInfo);
    var sKornmInfo  = model.getValue("/root/MainData/opmcptbsm/name");
    var sSexAgeInfo = model.getValue("/root/MainData/opmcptbsm/age_sex");

    var ctrlID = 1;

    if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") { //이미지 조회인경우
        pFileWriteFlag = "Y";
        fso = new ActiveXObject("Scripting.FileSystemObject");
        sFilePath = g_EMRImageTempPath+"\\";
    }
    if (sTextImage == "T") {
        emr_Right.removeNodeset("/root/SubData/viewinfo");
        emr_Right.gridRebuild("grid_view");
        emr_Right_fg = emr_Right.vsGrid("grid_view");
        //첫번째 Data display 세팅.
        //emr_Right.javascript.fInsertNurseData("Data","","", 0, null);
    }

    //조회된 간호 인증 정보를 불러와 grid일 경우 insert 한다.
    for (var i = 1; i <= iNodeCnt; i++) {
        for (var k = iCurPos; k > 0; k--) {
            if (model.getValue("/root/MainData/mrrfflshh["+k+"]/levl")== "3") {
                sPtchchrtnm = model.getValue("/root/MainData/mrrfflshh["+k+"]/chrtnm");
                //수술기록 관련된 차트코드를 확인하기 위한 차트코드 정보
                tempArr     = model.getValue("/root/MainData/mrrfflshh["+k+"]/seq").split("|");
                sPtchchrtcd = tempArr[3].replace("a_","");
                break;
            }
        }
        if (pFileWriteFlag == "Y") {
            MyFile = fso.CreateTextFile(sFilePath+"NURSE_"+i+".txt",true);
            MyFile.WriteLine("[<Font Size=20, decorate=underline, ForeColor=000000, weight=bold>]");

            MyFile.Write(sPtchchrtnm);
            MyFile.WriteLine();
            MyFile.WriteLine("[<Font Size=10, decorate=, ForeColor=000000, weight=bold>]"); //[<Paragraph LineSpace=130>] 제외함.

            MyFile.WriteLine();
            MyFile.WriteLine();
        }

        /*
          == 간호기록 관련 == 2007-09-11 6:16오후 김태범 - 이태진님 요청사항
          * 환자기본 정보가 존재하지 않을 경우 만들어 준다. *
          * 해당 서식지 정보
          - 수술전처치상태확인 기록지   INC0000001
          - 수술간호기록                CNS0000001
          - 회복실 간호기록             CNC0000002
          - 안정실 기록                 CNS0000003
         == == == == == == ==
        */
        var sPatientInfoXpath = "SignData/PatientInfo";
        var sPatientInfo = model.getValue(xPath+"["+i+"]/"+sPatientInfoXpath+"/HJID");

        if (sPtchchrtcd == "INC0000001" || sPtchchrtcd == "CNS0000001" || sPtchchrtcd == "CNC0000002" || sPtchchrtcd == "CNS0000003") {
            if (sPatientInfo == "") {
                //환자정보 없음. 생성로직 진행
                if (pFileWriteFlag == "Y") {
                    MyFile.Write("["+""+"환자정보"+""+"]");
                    MyFile.WriteLine();
                    MyFile.Write("  "+""+"환자아이디"+""+"> :"+ sPidInfo );
                    MyFile.WriteLine();
                    MyFile.Write("  "+""+"환자이름"+""+">: "  + sKornmInfo);
                    MyFile.WriteLine();
                    MyFile.Write("  "+""+"환자성별"+""+">: "  + sSexAgeInfo);
                    MyFile.WriteLine();
                } else {
                    emr_Right.javascript.fInsertNurseData("["+""+"환자정보"+""+"]"               , 1, "T" , emr_Right_fg.Rows, null);
                    emr_Right.javascript.fInsertNurseData("  "+""+"환자아이디"+""+"> :"+sPidInfo , 2, "T" , emr_Right_fg.Rows, null);
                    emr_Right.javascript.fInsertNurseData("  "+""+"환자이름"+""+">: "+sKornmInfo , 2, "T" , emr_Right_fg.Rows, null);
                    emr_Right.javascript.fInsertNurseData("  "+""+"환자성별"+""+">: "+sSexAgeInfo, 2, "T" , emr_Right_fg.Rows, null);
                }
            }
        }

        var strXML = model.Instance;
        var Node = strXML.selectSingleNode(xPath+"["+i+"]");
        var rootTemp = Node;
        var temp     = Node.firstChild;
        var sTempDepth;
        var sBeFlag = "";
        var sAfFlag = "";
        var iInsertCnt = 1;

        while ((temp != rootTemp)) {
            sBeFlag = "";//초기화
            sAfFlag = "";
            if (temp.nodeName == "#text") {
                if (temp == null) {
                    break;
                }
                var iDepth = fReturnSignNodeLevl(temp,"ganho");
                if (iDepth == 1) {
                    sBeFlag = " ";
                    sAfFlag = "";
                } else if (iDepth == 2) {
                    sBeFlag = "  ";
                    sAfFlag = "";
                } else if (iDepth == 3) {
                    sBeFlag = ":";
                    sAfFlag = "";
                } else if (iDepth == 4) {
                    sBeFlag = ":";
                    sAfFlag = "";
                } else {
                    sBeFlag = ":";
                    sAfFlag = "";
                }

                var iTempDepth = fReturnSignNodeLevl(temp.parentNode,"ganho");
                if (pFileWriteFlag == "Y") {
                    var sTempSpace = "";
                    if (iTempDepth == 1) {
                        sTempSpace = " ";
                    } else if (iTempDepth == 2) {
                        sTempSpace = "  ";
                    } else if (iTempDepth == 3) {
                        sTempSpace = "   ";
                    } else if (iTempDepth == 4) {
                        sTempSpace = "    ";
                    } else {
                        sTempSpace = "     ";
                    }
                    //textdata 중 \n이 있을 경우 \n + tempSpace로 바꿔준다.
                    if (temp.text != null && temp.text != "") {
                        var sTextData = temp.text.lvReplaceWord("\n","\n"+sTempSpace);
                        MyFile.Write(sBeFlag+sTextData);
                        MyFile.WriteLine();
                    }
                } else {
                    //node를 만들어서 setValue 하면됨.
                    if (sTextImage == "T") {
                        sBeforeData = emr_Right_fg.textMatrix(emr_Right_fg.Rows-1, 0);
                        emr_Right.javascript.fInsertNurseData(sBeforeData + sBeFlag+temp.text, iTempDepth, "T", emr_Right_fg.Rows , emr_Right_fg.Rows);
                        sBeforeData = "";
                        iInsertCnt++;
                    }
                }
            } else {
                //2007-10-05 9:04오전 김태범 간호 기록용 이미지 관련 추가
                if (temp.nodeName == "#cdata-section") {

                } else if (temp.nodeName.substring(0, 5) == "image" && temp.nodeName != "images") {//image tag를 만나면.. 다른 루틴을 탄다..
                    var imgChList = temp.childNodes;
                    var orgImg = "";
                    var xvgData = "";
                    var m_fhspace     = "   ";
                    for (var tmpIdx = 0; tmpIdx < imgChList.length; tmpIdx++) {
                        if (imgChList.item(tmpIdx).nodeName == "path") {
                            orgImg = imgChList.item(tmpIdx).nodeTypedValue;
                        } else if (imgChList.item(tmpIdx).nodeName == "exvg") {
                            xvgData = imgChList.item(tmpIdx).nodeTypedValue;
                        }
                    }

                    sTempString ="";
                    sTempString ="<object id=\"imgCtrl" + ctrlID +"\" classid=\"CLSID:72e5d9ed-0d6a-46e8-aead-23144bfef878\" width=\"240px\" height=\"150px\">\n"+
                                 "<PARAM name=\"Location\" value=\"" + model.getURI() + "\"/>"+
                                 "<PARAM name=\"ImgPath\" value=\"" + orgImg + "\"/>"+
                                 "<PARAM name=\"ShapeData\" value=\"" + xvgData + "\"/>"+
                                 "</object>";
                    //fSetGridValue_girokView(iRowCount, 2, sTempString, "I", orgImg, xvgData, pGrid, pTextImgFlag, "Y");
                    if (pFileWriteFlag == "Y") {
                        //이미지일 경우 로컬에서 File을 Make한다.
                        fMakeNUR1_OCRView_Image("EMR", orgImg, xvgData, "EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg");
                        MyFile.WriteLine();
                        //MyFile.Write(sTempString);
                        MyFile.Write("[<image path="+g_EMRImageTempPath+"\\EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg, size=200>]");
                        MyFile.WriteLine();
                        iGanHoImgSeqNo++;
                    } else {
                        if (sTextImage == "T") {
                            emr_Right.javascript.fInsertNurseData("", 2, "I", emr_Right_fg.Rows , emr_Right_fg.Rows, orgImg, xvgData);
                        }
                    }
                    sTempString ="";
                } else if (temp.nodeName.substring(0, 2) == "FH") {
                    if (temp.nodeName == "FHimages") {
                        //필요없는 등록일시 정보 삭제함.
                        model.removeNodeSet("/root/HideData/savedata_nurse/SignData/SignInfo/famyillnhist/FHimages/FHRegdt");
                        var m_FHImgList   = temp.childNodes;
                        var m_XMDData     = "";
                        var m_Info        = "";
                        var m_CommentData = "";
                        var m_fhspace     =   "　"; //2007-02-05 5:40오후 공백대신 특수문자 추가. 김태범

                        for (var tmpIdx = 0; tmpIdx < m_FHImgList.length; tmpIdx++) {
                            if (m_FHImgList.item(tmpIdx).nodeName == "FHimage") {
                                m_XMDData = m_FHImgList.item(tmpIdx).nodeTypedValue;
                            }
                            if (m_FHImgList.item(tmpIdx).nodeName == "FHInfo") {
                                m_Info    = m_FHImgList.item(tmpIdx).nodeTypedValue;
                            }
                            if (m_FHImgList.item(tmpIdx).nodeName == "FHComment") {
                                m_CommentData = m_FHImgList.item(tmpIdx).nodeTypedValue;
                            }
                        }

                        if (pFileWriteFlag == "Y") {
                            MyFile.WriteLine();
                            MyFile.Write("     ※ 가계도 이미지 정보 ※     ");
                            MyFile.WriteLine();
                            fMakeNUR1_OCRView_Image("FamilyImage", orgImg, m_XMDData, "EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg");
                            MyFile.Write("[<image path="+g_EMRImageTempPath+"\\EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg, size=200>]");
                            MyFile.WriteLine();
                            iGanHoImgSeqNo++;
                        } else {
                            if (sTextImage == "T") {
                                emr_Right.javascript.fInsertNurseData(m_fhspace+"※ 가계도", 1, "T", emr_Right_fg.Rows);
                                //sTempString = "familyHistroyMaker"; // 가계도 공백 그리드
                                emr_Right.javascript.fInsertNurseData("", 3, "F", emr_Right_fg.Rows , null , null , m_XMDData);
                                emr_Right.javascript.fInsertNurseData("", 3, "T", emr_Right_fg.Rows);
                            }
                        }

                        sTempString = "";
                        if (m_Info != "") {
                            if (pFileWriteFlag == "Y") {
                                MyFile.Write(m_fhspace+"※ 가계도 부연정보");
                                MyFile.WriteLine();
                                MyFile.Write(m_fhspace + m_fhspace+m_Info);
                                MyFile.WriteLine();
                            } else {
                                if (sTextImage == "T") {
                                    emr_Right.javascript.fInsertNurseData(m_fhspace+"※ 가계도 부연정보", 1, "T", emr_Right_fg.Rows);
                                    emr_Right.javascript.fInsertNurseData(m_fhspace + m_fhspace+m_Info, 1, "T", emr_Right_fg.Rows);
                                }
                            }
                        }

                        //가계도 comment
                        if (m_CommentData != "") {
                            if (pFileWriteFlag == "Y") {
                                MyFile.Write(m_fhspace+"※ 가계도 Comment");
                                MyFile.WriteLine();
                                MyFile.Write(m_CommentData);
                                MyFile.WriteLine();
                            } else {
                                if (sTextImage == "T") {
                                    emr_Right.javascript.fInsertNurseData("※ 가계도 Comment", 1, "T", emr_Right_fg.Rows);
                                    emr_Right.javascript.fInsertNurseData(m_CommentData, 1, "T", emr_Right_fg.Rows);
                                }
                            }
                        }
                    }
                //가계도 관련 끝...
                } else {
                    var sAttributeNode = temp.getAttributeNode("name");
                    var iDepth = fReturnSignNodeLevl(temp, "ganho");
                    if (iDepth == 1) {
                        sBeFlag = "[";
                        sAfFlag = "]";
                    } else if (iDepth == 2) {
                        sBeFlag = "  ";
                        sAfFlag = ">";
                    } else if (iDepth == 3) {
                        sBeFlag = "   ";
                        sAfFlag = " ";
                    } else if (iDepth == 4) {
                        sBeFlag = "    ";
                        sAfFlag = " ";
                    } else {
                        sBeFlag = "     ";
                        sAfFlag = " ";
                    }

                    //name이 존재 할 경우
                    if (sAttributeNode != null) {
                        if (pFileWriteFlag == "Y") {
                            MyFile.Write(sBeFlag+""+temp.getAttributeNode("name").value+""+sAfFlag);
                            if (iDepth == 1 || iDepth == 2) {
                                //해당 노드의 부모의 부모 노드가 환자정보일 경우에는 붙여 쓴다.
                                if (temp.parentNode.nodeName != "PatientInfo" && temp.parentNode.nodeName != "SessionInfo") {
                                    MyFile.WriteLine();
                                }
                            }
                        } else {
                            if (sTextImage == "T") {
                                emr_Right.javascript.fInsertNurseData(sBeFlag+""+temp.getAttributeNode("name").value+""+sAfFlag, iDepth, "T" , emr_Right_fg.Rows, null);
                            }
                            //node를 만들어서 setValue 하면됨.
                        }
                    }
                }
            }
            temp = fGetNode(temp, rootTemp);
            if (temp == rootTemp) {}
            if (temp == null) {
                break;
            }
        }
        if (pFileWriteFlag == "Y") {
            MyFile.WriteLine();
            MyFile.Close();
        }

        if (sTextImage == "T") {
            emr_Right.javascript.fInsertAfterNurseData();
        }
    }
}

/*
 * 간호기록 OCR Image 생성용
 * @pImageGbn : EMR : EMR이미지, FHimages : 가계도 이미지
 * @orgImg    : imagePath
 * @xvgData   : Data설정 값
 * @fileName : img 파일 경로
 */
function fMakeNUR1_OCRView_Image(pImageGbn, orgImg, xvgData, fileName) {
    if (pImageGbn == "EMR") {
        if (orgImg == "") {
            orgImg = "/emr/girokweb/GetIMJ.live?Mode=reqGetIMJ&imgecode=0000000000&imgname=Blank_Image.jpg";
        }
        var obj = model.getObject ("object1");
        obj.location = TFGetXFRIP();
        obj.SetViewMode(1); //0:원본, 1:스크린사이즈맞춤, 2:세로사이즈맞춤, 3:가로사이즈맞춤
        var ctrl = model.control("img2");
        obj.LoadImage(orgImg.lvTrim().lvReplaceString("\r",""));
        obj.DeleteAllShape();
        obj.SetShapeString(xvgData.lvTrim().lvReplaceString("\r",""));
        obj.SaveImageEx(0, 2, g_EMRImageTempPath+"\\"+fileName);
        model.toggle("case_1");
    } else if (pImageGbn == "FamilyImage") {
        var oFHMaker = model.getObject("FDiagram");
        var ctrl = model.control("img2");
        oFHMaker.LoadMetaData(xvgData);
        //로딩된 image를 임시로 로컬에 저장한다.
        oFHMaker.SaveJpg(g_EMRImageTempPath+"\\"+fileName, 90);
        oFHMaker.RemoveAll();
        model.refreshImage("img2");
    }
}

/**
 * @ver    : 2007.01.19
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : 인증정보의 레벨을 구하여 리턴하는 함수
 * @access : public
 * @desc   :
 * @param  : pTempNode 현재 노드
 * @param  : pGiRokFlag - 기록지 종류
 * @return : depth
 * @---------------------------------------------------
 */
function fReturnSignNodeLevl(pTempNode,pGiRokFlag) {
    var sTempDepth;
    if (pGiRokFlag == "ganho") { // 간호기록일 경우
        if (pTempNode.nodeName == "PatientInfo") {
            sTempDepth = 1;
            return sTempDepth;
        } else if (pTempNode.nodeName == "SignInfo") {
            sTempDepth = 1;
            return sTempDepth;
        } else if (pTempNode.nodeName == "SessionInfo") {
            sTempDepth = 1;
            return sTempDepth;
        }

        var m_TempNodeNM = pTempNode.parentNode.nodeName;

        if (m_TempNodeNM == "PatientInfo") {
            sTempDepth = 2;
            return sTempDepth;
        } else if (m_TempNodeNM == "SignInfo") {
            sTempDepth = 2;
            return sTempDepth;
        } else if (m_TempNodeNM == "SessionInfo") {
            sTempDepth = 2;
            return sTempDepth;
        }

        var m_TempNodeNMM = pTempNode.parentNode.parentNode.nodeName;

        if (m_TempNodeNMM == "PatientInfo") {
            sTempDepth = 3;
            return sTempDepth;
        } else if (m_TempNodeNM == "SignInfo") {
            m_TempNodeNMM = 3;
            return sTempDepth;
        } else if (m_TempNodeNM == "SessionInfo") {
            m_TempNodeNMM = 3;
            return sTempDepth;
        }

        var m_TempNodeNMMM = pTempNode.parentNode.parentNode.parentNode.nodeName;
        if (m_TempNodeNMMM == "PatientInfo") {
            sTempDepth = 4;
            return sTempDepth;
        } else if (m_TempNodeNMMM == "SignInfo") {
            sTempDepth = 4;
            return sTempDepth;
        } else if (m_TempNodeNMMM == "SessionInfo") {
            sTempDepth = 4;
            return sTempDepth;
        }
        return 5;   //4레벨까지 체크하고 나머지는 5레벨로 간주한다.
    }
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 간호기록 Text로 저장
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fWriteFileNurse(sChrt, sChrtNm, sDate, sPtdt, sText) {
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    sMRR_GRGeomSaek_FileCnt = sMRR_GRGeomSaek_FileCnt + 1;
    var MyFile = fso.CreateTextFile(g_EMRImageTempPath+"\\NURSE_"+ sMRR_GRGeomSaek_FileCnt +".txt", true);
    MyFile.Write("[<Font Size=20, decorate=underline, ForeColor=000000, weight=bold>]");
    MyFile.WriteLine(sChrtNm);
    var sIdno = model.getValue("/root/MainData/condition/cond1");
    var sName = model.getValue("/root/MainData/opmcptbsm/name");
    var regno = model.getValue("/root/MainData/opmcptbsm/regno1")+""+model.getValue("/root/MainData/opmcptbsm/regno2");
    var sAge = lvGetAge(regno, sDate.lvToDate("YYYYMMDD"));
    var sSex = lvGetGender(regno);
    var sAgeSex = sAge+"/"+sSex;
    var sRgno = model.getValue("/root/MainData/opmcptbsm/regno") + "******";
    var sDeptnm = "";
    var sPtdt = "";
    var sDate = "";
    MyFile.Write("[<Font Size=12, decorate=, ForeColor=000000, weight=bold>][<Paragraph LineSpace=130>]");
    MyFile.WriteLine();
    MyFile.WriteLine();
    MyFile.WriteLine(f_NoDisplayPT("< 등록번호 : " + sIdno + "     성명 : " + sName + "     나이/성별 : " + sAgeSex + " >"));
    MyFile.WriteLine();

    for (var j=0; j<sText.length; j++) {
        if (sText.charAt(j) == '\n') {
            MyFile.WriteLine();
        } else {
            MyFile.Write(sText.charAt(j));
        }
    }
    MyFile.WriteLine();

    MyFile.Close();

    model.setValue("/root/MainData/ChartInfo/fileTotal", 1);
    model.setValue("/root/MainData/ChartInfo/pageTotal", 1);
    model.setValue("/root/MainData/ChartInfo/pageCurrent", 1);
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Nuese상세조회(간호기록 정보조사지)(MRD_GRJoHoi_nurse.js 참조)
 *           마취기록 OCR조회에서만 사용함.
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCommon_getInstanceStringSignValue(xPath) {
    tt = "";
    var node = findNode(xPath);
    var tempPath = "/root";

    tempPath = xPath;
    var temp = node.firstChild;

    if (temp == null) return;

    var txt = "";
    while ((temp != node)) {
        if (temp.nodeName != "#text") {
            if (temp.selectSingleNode("./@name") != null) {
                txt = txt + "\n" + tt + temp.getAttribute("name");
            }
        } else {
            txt = txt +  ": " + temp.nodeValue;
        }

        temp = fCommon_Get_NodeNameString_Value_girokgeomsaek(temp, node);
        if (temp == null)

        break;
    }

    return txt;
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Nuese상세조회(간호기록 정보조사지)
 * @param  :
 * @return :
 * @수정이력: 2006-12-20 5:15오후
 *           function fCommon_Get_NodeNameString_Value(ins, rootTemp) 명칭을
 *           기록검색용 function으로 변경함. grid에서 공백을 맨앞에 넣을 경우 화면에서 공백이 안보이는 case.
 *            2007-03-22 9:35오전
 *           마취기록 출력시에 사용함.
 * @---------------------------------------------------
 */
function fCommon_Get_NodeNameString_Value_girokgeomsaek(ins, rootTemp) {
    var tempPath = "/root";
    var refPath  = tempPath + "/" + ins.nodeName;

    var child = ins.firstChild;
    if (child != null) {
        tempPath = refPath;
        tt = tt + "　";
        return child;
    }

    var sibling = ins.nextSibling;
    if (sibling != null) {
        tempPath = refPath;
        return sibling;
    } else {
        var nodePT = ins;
        while(1) {
            nodePT = nodePT.parentNode;
            tt = tt.substring(0, tt.length-2);
            if (nodePT ==null) return null;
            if (nodePT == rootTemp) return nodePT;
            if (nodePT.nextSibling == null)
                continue;
            else
                return nodePT.nextSibling;
        }
    }
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Nuese상세조회(Focus)
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCall_NUR2(pRow, pNode) {
    var sIdno = model.getValue("/root/MainData/condition/cond1");
    var sLevl = model.getValue(pNode + pRow + "]/levl");

    var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
    //var sSeq = model.getValue(pNode + pRow + "]/seq");
    var sParm = sChrtkind.lvReplaceWord("|", "▦");
    var sEmr = TFGetMatrixData(sParm, 0, 0); //간호기록,FOCUS,TPR 구분
    var sKind = TFGetMatrixData(sParm, 0, 1); //구분 3:외래, 응급:2, 입원:1
    var sFrDate = TFGetMatrixData(sParm, 0, 2);   //  2006.02.23  added by taja78
    var sToDate = TFGetMatrixData(sParm, 0, 2);  //  2006.02.23  added by taja78

    if (sKind == "3") {
        var sGubn = "O";
    } else if (sKind == "1") {
        var sGubn = "I";
    } else {
        var sGubn = "E";
    }

    model.setAttribute("sMRR_GRGeomSaek_sLevl", sLevl);
    model.setAttribute("sMRR_GRGeomSaek_idno", sIdno);
    model.setAttribute("sMRR_GRGeomSaek_gubn", sGubn);

    if (sLevl != "4") {
        var sDateArr = fGet_NUR2_Date(pRow).split("▦");
        sFrDate = sDateArr[1];
        sToDate = sDateArr[0];
        model.setAttribute("sMRR_GRGeomSaek_frdate", sFrDate);
        model.setAttribute("sMRR_GRGeomSaek_todate", sToDate);
    } else {
        model.setAttribute("sMRR_GRGeomSaek_frdate", sFrDate);
        model.setAttribute("sMRR_GRGeomSaek_todate", sToDate);
    }
    model.setAttribute("sMRR_GRGeomSaek_name", model.getValue("/root/MainData/opmcptbsm/name"));
    model.setAttribute("sMRR_GRGeomSaek_age_sex", model.getValue("/root/MainData/opmcptbsm/age_sex"));

    if (fGetIP_xfm() != "MRR_GRGeomSaek_NurseFocus") {
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_NurseFocus.xfm","replace");
        model.setAttribute("MRR_GRGeomSaek_PSYChk", model.getValue("/root/MainData/limit/grcheck"));
    } else {
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            // 2007.12.31 - 이태진 - 선택한 곳의 입원외래 구분을 알기위해 값 지정 start
            emr_Right.setValue("/root/MainData/condition/gubn", sGubn);
            // 2007.12.31 - 이태진 - 선택한 곳의 입원외래 구분을 알기위해 값 지정 end

            emr_Right.resetInstanceNode("/root/SendData");
            emr_Right.setValue("/root/SendData/Mode", "MRR_reqGetMrnfghgrh"); // Action Method
            emr_Right.setValue("/root/SendData/Data1", sIdno);
            emr_Right.setValue("/root/SendData/Data2", sGubn);

			//이준환
			emr_Right.setValue("/root/SendData/psychk", model.getValue("/root/MainData/limit/grcheck"));
            emr_Right.setValue("/root/SendData/anecheck", model.getValue("/root/MainData/limit/anecheck"));

            if (sLevl == "2") {
                emr_Right.setValue("/root/SendData/Data3", "-");
                emr_Right.setValue("/root/SendData/Data4", "-");
                emr_Right.setValue("/root/SendData/Data5", "-");
                emr_Right.setValue("/root/SendData/Data6", "modal_1"); //modal, nurse 호출 구분(1레벨을 클릭했을경우)
            } else if (sLevl == "3") {
                emr_Right.setValue("/root/SendData/Data3", sFrDate);
                emr_Right.setValue("/root/SendData/Data4", sToDate);
                emr_Right.setValue("/root/SendData/Data5", "-");
                emr_Right.setValue("/root/SendData/Data6", "modal_1"); //modal, nurse 호출 구분(2레벨을 클릭했을경우)

                // 2007.12.27 - 이태진 - 출력시 이력을 쌓도록 하기위해 화면 조회조건의 시작일자, 종료일자를 조회조건으로 변경함  start
                emr_Right.setValue("/root/MainData/condition/frdt", sFrDate);
                emr_Right.setValue("/root/MainData/condition/todt", sToDate);
                // 2007.12.27 - 이태진 - 출력시 이력을 쌓도록 하기위해 화면 조회조건의 시작일자, 종료일자를 조회조건으로 변경함  end
            } else {
                emr_Right.setValue("/root/SendData/Data3", sFrDate);
                emr_Right.setValue("/root/SendData/Data4", sToDate);
                emr_Right.setValue("/root/SendData/Data5", "-");
                emr_Right.setValue("/root/SendData/Data6", "modal_2"); //modal, nurse 호출 구분(3레벨을 클릭했을경우)

                // 2007.12.27 - 이태진 - 출력시 이력을 쌓도록 하기위해 화면 조회조건의 시작일자, 종료일자를 조회조건으로 변경함  start
                emr_Right.setValue("/root/MainData/condition/frdt", sFrDate);
                emr_Right.setValue("/root/MainData/condition/todt", sToDate);
                // 2007.12.27 - 이태진 - 출력시 이력을 쌓도록 하기위해 화면 조회조건의 시작일자, 종료일자를 조회조건으로 변경함  end
            }

            if (emr_Right.submitInstance("MRR_reqGetOpmcptbsm")) {
            } else {
                model.setAttribute("sMRR_GRGeomSaek_idno", "");
                model.setAttribute("sMRR_GRGeomSaek_gubn", "");
                model.setAttribute("sMRR_GRGeomSaek_date", "");
                //model.vbscript.FreeObj(emr_Right); //20060202 추가
                return false;
            }
            model.setAttribute("sMRR_GRGeomSaek_idno", "");
            model.setAttribute("sMRR_GRGeomSaek_gubn", "");
            model.setAttribute("sMRR_GRGeomSaek_date", "");

            emr_Right.gridrebuild("grid_focus");
            var fg = emr_Right.vsGrid("grid_focus");
            for (var i = fg.FixedRows; i < fg.Rows; i++) {
                var sDarm = emr_Right.getValue("/root/MainData/mrnfghgrh[" + i + "]/darm");
                if (sDarm == "D") {
                    emr_Right.setGridTooltip("grid_focus", i, 3, "Data");
                } else if (sDarm == "A") {
                    emr_Right.setGridTooltip("grid_focus", i, 3, "Action");
                } else if (sDarm == "R") {
                    emr_Right.setGridTooltip("grid_focus", i, 3, "Response");
                } else {
                    emr_Right.setGridTooltip("grid_focus", i, 3, "기타");
                }
                // 2007.12.27 - 이태진 - 삭제된 기록은 취소선 보이도록 함 start
                var sCncl = emr_Right.getValue("/root/MainData/mrnfghgrh[" + i + "]/cncl");
                if (sCncl == "C") {
                    //수정된 기록의 경우 취소선 보이도록 함
                    fg.select(i, 4, i, 4);
                    fg.CellFontStrikethru = true;
                }
                // 2007.12.27 - 이태진 - 삭제된 기록은 취소선 보이도록 함 end
            }
            fSellMerge("nur", "grid_focus");
        } else {
            return false;
        }
    }
    return true;
}

/**
 * @group  :
 * @ver    : 2006.02.23
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Focus 간호기록 3레벨 클릭시 해당 작성시작일, 종료일
 * @param  : pRow : current row
 * @param  :
 * @return : 시작, 종료일
 * @---------------------------------------------------
 */
function fGet_NUR2_Date(pRow) {
    //기존에 현재일자를 frdt, todt에 넣는것을  해당 focus 입/퇴원일로 변경 해야한다.. wited by taja78 2006.02.23
    var iGridCnt = TFGetGridCount("grid_main");
    var sTempstr1;
    var sTempstr2;
    var sTemplevl ="";
    var iCnt = 0;
    for (var i = parseInt(pRow)+1; i < iGridCnt+1; i++) {
    //            alert("sLevl 4 Opening... ...");
        sTemplevl = model.getValue("/root/MainData/mrrfflshh["+i+"]/levl");

        if (sTemplevl == "4") {
            //focus를 작성한 마지막일자
            if (iCnt == 0) {
                sTempstr1 = model.getValue("/root/MainData/mrrfflshh["+i+"]/chrtnm").lvReplaceWord("/","");
                sTempstr2 = sTempstr1;
            } else {
                sTempstr2 = model.getValue("/root/MainData/mrrfflshh["+i+"]/chrtnm").lvReplaceWord("/","");
            }
            iCnt++;
        } else {
            break;
        }
    }
    return sTempstr1+"▦"+sTempstr2;
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Nurse상세조회(마취기록-출력)
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCall_NUR3(pRow, pNode) {
    var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
    var sParm = sChrtkind.lvReplaceWord("|", "▦");
    var sIndd = TFGetMatrixData(sParm, 0, 2);
    var sFromdd = TFGetMatrixData(sParm, 0, 5).substr(0,8);
    var sTodd = TFGetMatrixData(sParm, 0, 3);
    var sPkidatetimes = TFGetMatrixData(sParm, 0, 8);

    model.removeNodeset("/root/HideData/savedata_nurse");
    TFclearNodeValue("/root/SendData"); //이전의 서버 호출에 사용된 정보 지움
    model.setValue("/root/SendData/Mode", "MRR_reqGetMrnipkidh"); //Action Method
    model.setValue("/root/SendData/Data1", "NUR3"); //간호조회 구분
    model.setValue("/root/SendData/Data2", model.getValue("/root/MainData/condition/cond1")); //등록번호
    //model.setValue("/root/SendData/Data3", sIndd);
    //2007-08-01 4:44오후 입원일자 대신 인증일시를 넣도록 수정함.
    model.setValue("/root/SendData/Data3", sPkidatetimes);
    model.setValue("/root/SendData/Data4", sFromdd);
    model.setValue("/root/SendData/Data5", sTodd);
    if (model.submitInstance("MRR_reqGetMrnipkidh")) {
        TFSetMessage("root/HideData/savedata_nurse");
    } else {
        return false;
    }
    var xPath = "/root/HideData/savedata_nurse";
    TFRemoveNoChild(findNode(xPath));
    var sText = fCommon_getInstanceStringSignValue(xPath); //현재는 한건만 가져오도록 함
    var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

    if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") { //이미지 조회인경우
        fDeleteFile("NURSE_");
        sMRR_GRGeomSaek_FileCnt = 0;
        fWriteFileNurse("마취기록", "마취기록", sIndd, sIndd, sText); //추가(20050725)
        model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_NUR1");
        model.setAttribute("MRR_GRGeomSaek_Ocr.sMRR_GRGeomSaek_FileCnt", 1);

        if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") {
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
        } else {
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                sTotalKind = sParm + "▩";
                emr_Right.javascript.fViewerSetting(sTotalKind, "NURSE_");
                fWaitBegin("이미지 Loading중...");
                emr_Right.javascript.downloadChart(); //
                fWaitEnd();
                emr_Right.javascript.setupChart("ChartView"); //연속보기로 설정
            } else {
                return;
            }
        }
    } else if (sTextImage == "P") { //XFR 출력일 경우..
        if (fGetIP_xfm() != "MRR_GRGeomSaek_ReportView") {
            model.setAttribute("MRR_GRGeomSaek_ReportView.function", "fMakeImage_XFR_Print");
            model.setAttribute("MRR_GRGeomSaek_ReportView.function_param", "AnstGirok");
            model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm", "replace");
        } else {
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            model.setAttribute("MRR_GRGeomSaek_ReportView.function", "fMakeImage_XFR_Print");
            model.setAttribute("MRR_GRGeomSaek_ReportView.function_param", "AnstGirok");
            emr_Right.javascript.fInitializeReportView();
        }
    } else {
         if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") {
            model.setAttribute("MRR_GRGeomSaek_Grid.function", "fCall_NUR1");
            model.setAttribute("MRR_GRGeomSaek_Grid.sText", sText);
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
         } else {
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                emr_Right.javascript.fMakeGridNurse(sText, 0); //MRR_GRGeomSaek_Grid에 있는 function호출
                emr_Right.refresh();
            } else {
                return;
            }
        }
    }
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 누적조회
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCall_ACC(pRow, pNode) {
    var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
    TFSetGlobalAttribute("MRR_GRGeomSaek_Acc_sChrtkind", sChrtkind);

    if (fGetIP_xfm() != "MRR_GRGeomSaek_Acc") {
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Acc.xfm","replace");
    } else {
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            emr_Right.javascript.fMakeAccMain(sChrtkind);
            emr_Right.refresh();
        } else {
            //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
            return;
        }
    }

    return;
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 제증명조회
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCall_PM(pRow, pNode) {
    var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
    var sParm = sChrtkind.lvReplaceWord("|", "▦");

    var sCode = TFGetMatrixData(sParm, 0, 3);
    var sXfrPath = TFGetMatrixData(sParm, 0, 1);

    fXfrPrintObjectInit("object100",sXfrPath,pNode, pRow);
    return;

}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 제증명조회
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fXfrPrintObjectInit(ObjectNm, srcTar, pNode, pRow) {
    // xfr object 경로 저장
    TFSetGlobalAttribute("fXfrPrintObject", ObjectNm + "▦" + srcTar);

    if (fGetIP_xfm() != "MRR_XFR_VIEW") {
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_XFR_VIEW.xfm","replace");
    } else {
        var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
        var sParm = sChrtkind.lvReplaceWord("|", "▦");
        var sEmr = TFGetMatrixData(sParm, 0, 0); //제증명구분
        var sXfm = TFGetMatrixData(sParm, 0, 1);
        var sIdno = TFGetMatrixData(sParm, 0, 2);
        var sCode = TFGetMatrixData(sParm, 0, 3);
        var sYear = TFGetMatrixData(sParm, 0, 4);
        var sYeearno = TFGetMatrixData(sParm, 0, 5);
        var sSeqno = TFGetMatrixData(sParm, 0, 6);
        var sCdnm = TFGetMatrixData(sParm, 0, 7);
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            var obj100 = emr_Right.getObject(ObjectNm);
            obj100.location = emr_Right.getURI();
            obj100.src = srcTar;

            emr_Right.javascript.fSetValue(sIdno, sCode, sYear, sYeearno, sSeqno, sCdnm);
            emr_Right.javascript.fXFR_View_initData();
            emr_Right.refresh();
        } else {
            //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
            return;
        }
    }
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 투약정보조회
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCall_DRUG(pRow, pNode) {
    var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
    var sParm = sChrtkind.lvReplaceWord("|", "▦");
    var sEmr = TFGetMatrixData(sParm, 0, 0);
    var sIdno = TFGetMatrixData(sParm, 0, 1);
    var sDate = TFGetMatrixData(sParm, 0, 2);
    var sDept = TFGetMatrixData(sParm, 0, 3);
    var sKind = TFGetMatrixData(sParm, 0, 4);
    var sGubn = TFGetMatrixData(sParm, 0, 5);
    var sDoct = TFGetMatrixData(sParm, 0, 6);
    var sIndd = TFGetMatrixData(sParm, 0, 7); //외래(진료일자) 입원(입원일자)
    var sOudd = TFGetMatrixData(sParm, 0, 8); //외래(진료일자) 입원(퇴원일자)

    if (sGubn == "I1") { //입원버튼(2단계)을 클릭했을경우
        if (sOudd == "29991231") { //재원중인 경우
            var sTodt = new Date();
            var sFrdt = new Date(sTodt - (1000 * 60 * 60 * 24 * 7)); //today의 7일전
            sTodt = sTodt.lvFormat();
            sFrdt = sFrdt.lvFormat();
        } else {
            var sTodt = sOudd.lvToDate("YYYYMMDD");
            var sFrdt = new Date(sTodt - (1000 * 60 * 60 * 24 * 7)); //퇴원일자의 7일전
            sTodt = sTodt.lvFormat();
            sFrdt = sFrdt.lvFormat();
        }
    } else {
        var sFrdt = sDate;
        var sTodt = sDate;
    }

    model.setAttribute("sMRR_GRGeomSaek_idno", sIdno);
    model.setAttribute("sMRR_GRGeomSaek_gubn", sGubn);
    model.setAttribute("sMRR_GRGeomSaek_frdt", sFrdt);
    model.setAttribute("sMRR_GRGeomSaek_todt", sTodt);
    model.setAttribute("sMRR_GRGeomSaek_dept", sDept);
    model.setAttribute("sMRR_GRGeomSaek_kind", sKind);
    model.setAttribute("sMRR_GRGeomSaek_doct", sDoct);
    model.setAttribute("sMRR_GRGeomSaek_indd", sIndd);
    model.setAttribute("sMRR_GRGeomSaek_oudd", sOudd);

    if (fGetIP_xfm() != "MRR_GRGeomSaek_Drug") {
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Drug.xfm","replace");
    } else {
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            emr_Right.javascript.fDrug_Open();
            emr_Right.resetInstanceNode("/root/SendData");
            emr_Right.setValue("/root/SendData/Mode", "MRR_reqGetDrug"); // Action Method
            emr_Right.setValue("/root/SendData/Data1", sIdno);
            emr_Right.setValue("/root/SendData/Data2", sFrdt); //시작일자
            emr_Right.setValue("/root/SendData/Data3", sTodt); //종료일자
            emr_Right.setValue("/root/SendData/Data4", sDept);
            emr_Right.setValue("/root/SendData/Data5", sKind);
            emr_Right.setValue("/root/SendData/Data6", sGubn);
            emr_Right.setValue("/root/SendData/Data7", sDoct);
            emr_Right.setValue("/root/SendData/Data8", "-"); //처방분류
			emr_Right.setValue("/root/SendData/psychk", model.getValue("/root/MainData/limit/grcheck")); //신경전신과 권한
            emr_Right.setValue("/root/SendData/anecheck", model.getValue("/root/MainData/limit/anecheck"));

            if (emr_Right.submitInstance("MRR_reqGetOpmcptbsm")) {
                TFSetMessage("/root/MainData/drug");
                fSellMerge("drug", "grid_drug");
            } else {
                emr_Right.removeNodeset("/root/MainData/drug");
            }
            model.setAttribute("sMRR_GRGeomSaek_idno", "");
            model.setAttribute("sMRR_GRGeomSaek_gubn", "");
            model.setAttribute("sMRR_GRGeomSaek_frdt", "");
            model.setAttribute("sMRR_GRGeomSaek_todt", "");
            model.setAttribute("sMRR_GRGeomSaek_dept", "");
            model.setAttribute("sMRR_GRGeomSaek_kind", "");
            model.setAttribute("sMRR_GRGeomSaek_doct", "");
            model.setAttribute("sMRR_GRGeomSaek_indd", "");
            model.setAttribute("sMRR_GRGeomSaek_oudd", "");

            emr_Right.gridRefresh("grid_drug");
        } else {
            return;
        }
    }
    return;
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 챠트출력 기록
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fChartPrint(pPrint) {
    var sUserid = TFGetSessionInfo("userid"); //사용자
    var sUserdeptcd = TFGetSessionInfo("userdeptcd"); //부서코드
    var sDate = new Date();
    sDate = sDate.lvFormat();
    var sTime = new Date();
    sTime = sTime.lvFormat("hhmmss");

    var sPrint = pPrint.lvReplaceWord(",", "▩");
    sPrint = sPrint + "▩";
    var sParm = sPrint.lvReplaceWord("|", "▦");
    var sParmCnt = sParm.split("▩");
    model.removeNodeset("/root/MainData/ChartPrint");

    //마취전 환자방문기록, 마취기록일 경우 서식지정보 세팅.
    var sCurrentCur = TFGetGridPos("grid_main");
    var sSearchFlag = model.getValue("/root/HideData/condition/cond1");
    var sLevl = model.getValue("/root/MainData/mrrfflshh["+sCurrentCur+"]/levl");
    var sTempArr = model.getValue("/root/MainData/mrrfflshh["+sCurrentCur+"]/chrtkind").split("|");
    var sTempArr2 = model.getValue("/root/MainData/mrrfflshh["+sCurrentCur+"]/seq").split("|");
    var sPid = model.getValue("/root/MainData/condition/cond1");

    if (sParmCnt.length > 0) {
        var sTempUserdeptcd = "-";
        var sTempUserid = "-";
        var sTempDate = "-";
        var sTempTime = "-";
        var sTempPtdt = "-";
        var sTempClcd = "-";
        var sTempUrcd = "-";
        var sTempFmcd = "-";
        var sTempPgno = "-";
        var cnt = 0;
        for (var idx = 0; idx < sParmCnt.length - 1; idx++) {
            var sCtpg = TFGetMatrixData(sParm, idx, 0);
            var sTrno = TFGetMatrixData(sParm, idx, 1);
            var sPgno = TFGetMatrixData(sParm, idx, 2);
            var sFmcd = "";
            var sClcd = TFGetMatrixData(sParm, idx, 4);
            if (sClcd == "") sClcd = "-";
            var sUrcd = TFGetMatrixData(sParm, idx, 6);
            if (sUrcd == "") sUrcd = "-";
            var sPtdt = TFGetMatrixData(sParm, idx, 5);

            if (sSearchFlag == "date") {
                if ((sTempArr2[3] == "ANT" && sLevl =="4") || (sTempArr2[3] == "ANTBEFORE" && sLevl =="4") || (sTempArr2[3] == "LIT" && sLevl =="4")) {
                    if (sTempArr2[3] == "ANT") {
                        sFmcd = "INS0000012";
                        sClcd = "-";
                        sUrcd = "-";
                        sPtdt  = sTempArr[2].substr(0,8);
                    } else if (sTempArr2[3] == "LIT") { // 20181128  doublej 외래마취기록
                    	sFmcd = "ONC0000005";
                        sClcd = "-";
                        sUrcd = "-";
                        sPtdt  = sTempArr[2].substr(0,8);
                	} else if (sTempArr2[3] == "ANTBEFORE") {
                        sFmcd = "CDT0000001";
                        sClcd = sTempArr[9];
                        sUrcd = sTempArr[10];
                        sPtdt  = sTempArr[8].substr(0,8);
                    } else {
                        sFmcd = TFGetMatrixData(sParm, idx, 3);
                    }
                } else {
                    sFmcd = TFGetMatrixData(sParm, idx, 3);
                }
            } else if (sSearchFlag == "form") {
                //마취전 환자방문기록, 마취기록은 hard coding...
                if ((sTempArr2[3] == "ANT" || sTempArr2[3] == "ANTBEFORE" || sTempArr2[3] == "LIT" ) && sLevl =="4") {
                    if (sTempArr2[3] == "ANT") {
                        sFmcd = "INS0000012";
                        sClcd = "-";
                        sUrcd = "-";
                        sPtdt  = sTempArr[2].substr(0,8);
                    } else if (sTempArr2[3] == "LIT") { // 20181128  doublej 외래마취기록
                    	sFmcd = "ONC0000005";
                        sClcd = "-";
                        sUrcd = "-";
                        sPtdt  = sTempArr[2].substr(0,8);
                    } else if (sTempArr2[3] == "ANTBEFORE") {
                        sFmcd = "CDT0000001";
                        sClcd = sTempArr[9];
                        sUrcd = sTempArr[10];
                        sPtdt = sTempArr[8].substr(0,8);
                    } else {
                        sFmcd = TFGetMatrixData(sParm, idx, 3);
                    }
                } else {
                    sFmcd = TFGetMatrixData(sParm, idx, 3);
                }
            } else {
                sFmcd = TFGetMatrixData(sParm, idx, 3);
            }
            // ======================================================
            var sIndt = TFGetMatrixData(sParm, idx, 7);
            var sOudt = TFGetMatrixData(sParm, idx, 8);
            var sRmad = TFGetMatrixData(sParm, idx, 9);
            var sRmpt = TFGetMatrixData(sParm, idx, 10);

            //key값이 같을경우 skip한다.
            if (sTempUserdeptcd == sUserdeptcd && sTempUserid == sUserid && sTempDate == sDate && sTempTime == sTime && sTempPtdt == sPtdt && sTempClcd == sClcd && sTempUrcd == sUrcd && sTempFmcd == sFmcd && sTempPgno == sPgno) {
                continue;
            } else {
                sTempUserdeptcd = sUserdeptcd;
                sTempUserid = sUserid;
                sTempDate = sDate;
                sTempTime = sTime;
                sTempPtdt = sPtdt;
                sTempClcd = sClcd;
                sTempUrcd = sUrcd;
                sTempFmcd = sFmcd;
                sTempPgno = sPgno;
                cnt = cnt + 1;
            }

            if (findNode("/root/MainData/ChartPrint["+parseInt(cnt)+"]") == null) {
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprdept");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flpridno");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprprdd");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprprtm");

                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprctpg");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprtrno");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprpgno");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprfmcd");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprclcd");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprptdt");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprurcd");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprindt");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flproudt");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprrmad");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprrmpt");
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprptno");    //등록번호 추가 taja78
                model.makeNode("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/tempflag");
            }

            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprdept", sUserdeptcd);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flpridno", sUserid);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprprdd", sDate);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprprtm", sTime);

            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprctpg", sCtpg);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprtrno", sTrno);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprpgno", sPgno);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprfmcd", sFmcd);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprclcd", sClcd);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprptdt", sPtdt);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprurcd", sUrcd);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprindt", sIndt);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flproudt", sOudt);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprrmad", sRmad);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprrmpt", sRmpt);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/flprptno", sPid);
            model.setValue("/root/MainData/ChartPrint[" + parseInt(cnt) + "]/tempflag", "I");
        } //for

        model.resetInstanceNode("/root/SendData");
        model.setValue("/root/SendData/Mode", "MRR_reqSetChartPrint"); // Action Method
        model.setValue("/root/SendData/Data1", TFNodeSendDataMake2("/root/MainData/ChartPrint", "tempflag"));
        if (model.submitInstance("MRR_reqSetChartPrint")) {
            TFSetMessage("/root/MainData/iudmsg");
        } else {
        }
        model.refresh();
        return true;
    } else {
        return false;
    }
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 성별/나이 계산
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSexAge() {
    var sex = model.getValue("/root/MainData/opmcptbsm/regno2");
    model.setValue("/root/MainData/opmcptbsm/sex",lvGetGender(sex));

    var age = (model.getValue("/root/MainData/opmcptbsm/regno1")).concat(model.getValue("/root/MainData/opmcptbsm/regno2"));
    model.setValue("/root/MainData/opmcptbsm/age",lvGetAge(age));

    model.setValue("/root/MainData/opmcptbsm/regno", model.getValue("/root/MainData/opmcptbsm/regno1")+"-"+model.getValue("/root/MainData/opmcptbsm/regno2").substr(0,1));
    //model.setValue("/root/MainData/opmcptbsm/age_sex", lvGetAge(age)+"/"+lvGetGender(sex));
	model.setValue("/root/MainData/opmcptbsm/age_sex", model.getValue("/root/MainData/opmcptbsm/age_sex")+"/"+lvGetGender(sex));
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 입력한 Date 날짜를 String YYYYMMDD 로 반환한다.
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function convDate2String(pValue) {
    var year = pValue.getYear();
    var month = pValue.getMonth() + 1;
    var day = pValue.getDate();

    return "" + year +
            ((("" + month).length == 1) ? "0" + month : month) +
            ((("" + day).length == 1) ? "0" + day : day);
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Cell Merge 기능
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSellMerge(pGubn, pGridID) {
    //2006-08-16 1:51오후 get Model 관련 수정 김태범
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        var fg = emr_Right.vsGrid(pGridID);

        // 1. Cell Merge 표현
        fg.MergeCells  = 4; //flexMergeRestrictAll(1:free, 2:above 3:left 4:all)
        if (pGubn == "ocs") {
            fg.MergeCells  = 1;
            fg.MergeCol(0) = false;
            fg.MergeCol(1) = false;
            fg.MergeCol(2) = false;
            fg.MergeCol(3) = false;
            fg.MergeCol(4) = false;
            fg.MergeCol(5) = false;
            fg.MergeCol(6) = false;
            fg.MergeCol(7) = false;
            fg.MergeCol(8) = false;
            fg.MergeCol(9) = false;
            fg.MergeCol(10) = true;
            fg.MergeCol(11) = true; //default가 true이므로 merge안할 col만 false주면 됨
            fg.MergeCol(12) = true;
            fg.MergeCol(13) = false;
            fg.MergeCol(14) = false;
            fg.MergeCol(15) = false;
            fg.MergeCol(16) = false;
            fg.MergeCol(17) = false;
            fg.MergeCol(18) = false;
            fg.MergeCol(19) = false;
            fg.MergeCol(20) = false;
            fg.MergeCol(21) = false;
            fg.MergeCol(22) = false;
            fg.MergeCol(23) = false;
            fg.MergeCol(24) = false;
            fg.MergeCol(25) = false;
            fg.MergeCol(26) = false;
            fg.MergeCol(27) = false;
            fg.MergeCol(28) = false;
            fg.MergeCol(29) = false;
            fg.MergeCol(30) = false;
        } else if (pGubn == "drug") {
            fg.MergeCol(0) = true;
            fg.MergeCol(1) = true;
            fg.MergeCol(2) = true;
            fg.MergeCol(3) = false;
            fg.MergeCol(4) = false;
            fg.MergeCol(5) = false;
            fg.MergeCol(6) = false;
        } else if (pGubn == "nur") {
            fg.MergeCol(0) = true;
            fg.MergeCol(1) = true;
            fg.MergeCol(2) = true;
            fg.MergeCol(3) = true;
            fg.MergeCol(4) = false;
            fg.MergeCol(5) = false;
        } else if (pGubn == "twgirok") {              //2005-09-15 added by taja78
            fg.MergeCol(0) = true;
            fg.MergeCol(1) = false;
            fg.MergeCol(2) = false;
            fg.MergeCol(3) = false;
            fg.MergeCol(4) = false;
            fg.MergeCol(5) = false;
            fg.MergeCol(6) = false;
            fg.MergeCol(7) = false;
            fg.MergeCol(8) = false;
        } else if (pGubn == "twgirok_jinryo") {
            var twfg = model.vsGrid(pGridID);
            fg.MergeCol(0) = true;
            fg.MergeCol(1) = false;
            fg.MergeCol(2) = false;
            fg.MergeCol(3) = false;
            return;
        }

        // 2. WordWrap 표현
        fg.WordWrap = true;

        fg.AutoSizeMode  = 1;
        fg.RowHeightMin  = 350;

        fg.AutoSize(0, fg.Cols -1);
        return;
    } else {
        return;
    }
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 화면 왼쪽 Cell Merge 기능
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSellMergeReft(pGubn, pGridID) {
    // 1. Cell Merge 표현
    var fg = model.vsGrid(pGridID);
    fg.MergeCells  = 4; //flexMergeRestrictAll(1:free, 2:above 3:left 4:all)
    if (pGubn == "twgirok_jinryo") {
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = false;
        fg.MergeCol(2) = false;
        fg.MergeCol(3) = false;
        return;
    }

    // 2. WordWrap 표현
    fg.WordWrap = true;

    fg.AutoSizeMode  = 1;
    fg.RowHeightMin  = 350;

    fg.AutoSize(0, fg.Cols -1);
    return;
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 한줄삭제
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fGridDelRow() {
    /*
    var fg = model.vsGrid("grid_copylist");

    var iFindRow = fg.FindRow("false", fg.fixedRows, 0);
    if (iFindRow < 1) iFindRow = fg.FindRow(0, fg.fixedRows, 0);

    if (iFindRow > 0) {
        model.removeNodeset("/root/MainData/copylist[" + iFindRow + "]");
        model.gridRebuild("grid_copylist");
    }
    */
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 높이 조절
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fWordWrap(pGrid) {
    var fg = model.vsGrid(pGrid);
    fg.WordWrap = true;
    fg.AutoSizeMode = 1;
    fg.RowHeightMin = 400;
    fg.AutoSize(0, fg.Cols - 1);
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : get IP
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fGetIP() {
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        var uri = emr_Right.getURI ();

        var pos = uri.indexOf("http://");
        var pos2 = uri.indexOf("/", pos + 1 + 7);
        var ip = uri.substring(pos, pos2);

        return ip;
    } else {
        return;
    }
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : get xfm명칭
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fGetIP_xfm() {
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        var uri = emr_Right.getURI ();
        var pos = uri.indexOf("/xfm/") + 5;
        var pos2 = uri.indexOf(".xfm", pos);
        var ip = uri.substring(pos, pos2);
        return ip;
    } else {
        return;
    }
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과조회(service 호출)
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fRequestAction(pSubmitInfo, pAction, pRef, pResultRef) {
    var sb = model.submitInfo(pSubmitInfo);
    sb.action = pAction;
    sb.ref = pRef;
    sb.resultRef =  pResultRef;

    return model.submitInstance(pSubmitInfo);
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과조회
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSetFunction(mode, bizname, jspname) {
    model.setValue("/root/SendData/Mode", mode);
    model.setValue("/root/SendData/BizName", bizname);
    model.setValue("/root/SendData/JspName", jspname);
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과조회
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
//gubn : 1:collapse, 2:expand
function fMakeTree(grid, gubn) {
    var fg = model.vsGrid(grid);
    var row = 0;

    for (var i = fg.FixedRows; i < fg.Rows; i++) {
        row++;
        var level = model.getValue("/root/MainData/hist/list[" + row + "]/level");
        level = level.lvStripWhite();

        fg.IsSubtotal(i) = true;
        fg.RowOutlineLevel(i) = fg.TextMatrix(i, fg.cols-1); //마지막 column에 level

        //fg.Cell(5,i, 1) = 2;
        if (level == "1") {
            fg.Cell(7, i, 0) = model.rgb(51,0,187);
            fg.Cell(13,i, 0) = true;
        } else if (level == "2") {
            fg.Cell(7, i, 0) = model.rgb(51,17,187);
        }
    }

    fg.OutlineBar = 4;//+ , - , 사각형 안보이게 하려면 0 으로 세팅
    fg.OutlineCol = 0;

    if (gubn == 1) {
        for (var i = fg.FixedRows; i < fg.Rows; i++) fg.IsCollapsed(i) = 2;  //collapse
    } else {
        for (var i = fg.FixedRows; i < fg.Rows; i++) fg.IsCollapsed(i) = 0;  //expand
    }
    model.gridrefresh(grid);
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Text file 삭제
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fDeleteFile(pGubn) {
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    //기존에 존재하는 파일들을 삭제한다.
    var folder = fso.GetFolder(g_EMRImageTempPath);
    var fc = new Enumerator(folder.files);
    for (; !fc.atEnd(); fc.moveNext()) {
         var sItem = fc.item();
         if (InStr(sItem, pGubn)) {
             fso.DeleteFile(sItem);
         } else {
         }
    }
}

/**
 * @group  :
 * @ver    : 2007.10.08
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과 세부내역 조회 이미지 조회시 현재 선택된 레벨별 전역변수 세팅
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fSetGeomSaGyeolGwaParam() {
    var iCurrentPos = TFGetGridPos("grid_list");
    var iGSTotalCnt = TFGetGridCount("grid_list")
    var sLevl = model.getValue("/root/MainData/hist/list["+iCurrentPos+"]/level");
    var sTempLevl = "";
    var sGeomSaGyeolGwaParam = "";

    //OBH 20080326
    //TFGetMsgBox(-1, "iCurrentPos"+iCurrentPos, "확인", "I", "OK");
    //TFGetMsgBox(-1, "iGSTotalCnt"+iGSTotalCnt, "확인", "I", "OK");
    //TFGetMsgBox(-1, "sLevl"+sLevl, "확인", "I", "OK");
    //TFGetMsgBox(-1, "sTempLevl"+sTempLevl, "확인", "I", "OK");
    //TFGetMsgBox(-1, "sGeomSaGyeolGwaParam"+sGeomSaGyeolGwaParam, "확인", "I", "OK");

    if (sLevl == "1") {
        for (var iGSPos = iCurrentPos+1; iGSPos <= iGSTotalCnt; iGSPos++) {
            sTempLevl = model.getValue("/root/MainData/hist/list["+iGSPos+"]/level");
            if (sTempLevl == "3") {
                var sTempPrintParam = model.getValue("/root/MainData/hist/list["+iGSPos+"]/printparam");
                if (sTempPrintParam != "" && sTempPrintParam != null) {
                    if (sGeomSaGyeolGwaParam == "") {
                        sGeomSaGyeolGwaParam = sTempPrintParam;
                    } else {
                        sGeomSaGyeolGwaParam = sGeomSaGyeolGwaParam+"▦"+sTempPrintParam;
                    }
                }
                //sGeomSaGyeolGwaParam = sGeomSaGyeolGwaParam+"▦"+model.getValue("/root/MainData/hist/list["+iGSPos+"]/printparam");
            } else if (sTempLevl == "1") {
                model.setAttribute("GiRokGeomSaek_sGeomSaGyeolGwaParam", sGeomSaGyeolGwaParam);
                break;
            }
        }
        model.setAttribute("GiRokGeomSaek_sGeomSaGyeolGwaParam", sGeomSaGyeolGwaParam);
    } else if (sLevl == "2") {
        for (var iGSPos = iCurrentPos+1; iGSPos <= iGSTotalCnt; iGSPos++) {
            sTempLevl = model.getValue("/root/MainData/hist/list["+iGSPos+"]/level");
            if (sTempLevl == "3") {
                var sTempPrintParam = model.getValue("/root/MainData/hist/list["+iGSPos+"]/printparam");
                if (sTempPrintParam != "" && sTempPrintParam != null) {
                    if (sGeomSaGyeolGwaParam == "") {
                        sGeomSaGyeolGwaParam = sTempPrintParam;
                    } else {
                        sGeomSaGyeolGwaParam = sGeomSaGyeolGwaParam+"▦"+sTempPrintParam;
                    }
                }
            } else {
                model.setAttribute("GiRokGeomSaek_sGeomSaGyeolGwaParam", sGeomSaGyeolGwaParam);
                break;
            }
        }
        model.setAttribute("GiRokGeomSaek_sGeomSaGyeolGwaParam", sGeomSaGyeolGwaParam);
        //alert("sGeomSaGyeolGwaParam:" + sGeomSaGyeolGwaParam);
    } else if (sLevl == "3") {
        sGeomSaGyeolGwaParam = model.getValue("/root/MainData/hist/list["+iCurrentPos+"]/printparam");
        model.setAttribute("GiRokGeomSaek_sGeomSaGyeolGwaParam", sGeomSaGyeolGwaParam);
    }
    //alert("check");
	
	model.trace("[KIS] fSetGeomSaGyeolGwaParam 1");
    fClicked("grid_list");
	model.trace("[KIS] fSetGeomSaGyeolGwaParam 2");
}

/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과 Text로 저장(page보기)
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
 /*
function fWriteGyeolGwa() {
    var iEnd = 0;

    var sPageSkip = 0;
    var sFontSize14 = 0;
    var sFontSize12 = 0;
    var sFontSize10 = 0;
    var sDecorate = 0;

    var sChrt = "";
    var sChrtnm = "";
    var sDept = "";
    var sDeptnm = "";
    var sDate = "";
    var sCode = "";

    var fso = new ActiveXObject("Scripting.FileSystemObject");
    sMRR_GRGeomSaek_FileCnt = sMRR_GRGeomSaek_FileCnt + 1;
    var MyFile = fso.CreateTextFile("C:\\Program Files\\his\\temp\\RESULT_"+ sMRR_GRGeomSaek_FileCnt +".txt", true);
    var fg ;
    //2006-08-16 1:51오후 get Model 관련 수정 김태범
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        fg = emr_Right.vsgrid("grid_result");
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }

    for (var i = 1; i <= fg.Rows - fg.FixedRows; i++) {
        var sData = emr_Right.getValue("/root/MainData/result/list[" + i + "]/data");
        var sPath = emr_Right.getValue("/root/MainData/result/list[" + i + "]/level");

        if (sFontSize12 == 0 && sPath == "1") {
            if (sPageSkip != 0) { //첫장이 아닌경우
                if (i != fg.Rows - fg.FixedRows) { //끝이 아니면 파일을 새로 연다.
                    MyFile.Close();
                    sMRR_GRGeomSaek_FileCnt = sMRR_GRGeomSaek_FileCnt + 1;
                    var MyFile = fso.CreateTextFile("C:\\Program Files\\his\\temp\\RESULT_"+ sMRR_GRGeomSaek_FileCnt +".txt", true);
                } else { //마지막문장이므로 파일을 close할 필요가 없으므로 iEnd를 '1'로 setting한다.
                    iEnd = 1;
                }
            } else {
                sPageSkip = 1;
            }

            sChrt = emr_Right.getValue("/root/MainData/result/list[" + i + "]/rsltno"); //추가(20050725)
            sChrtnm = emr_Right.getValue("/root/MainData/result/list[" + i + "]/data"); //추가(20050725)
            sDept = emr_Right.getValue("/root/MainData/result/list[" + i + "]/suppdept"); //추가(20050725)
            sDeptnm = emr_Right.getValue("/root/MainData/result/list[" + i + "]/suppdept"); //추가(20050725)
            sDate = emr_Right.getValue("/root/MainData/result/list[" + i + "]/rsltdd"); //추가(20050725)
            sCode = emr_Right.getValue("/root/MainData/result/list[" + i + "]/tclscd"); //추가(20050725)
            sCode = sCode.substr(0,2);
            if (i == 1) {
                sTotalKind = sChrt + "▦" + sChrtnm + "▦" + sDept + "▦" + sDeptnm + "▦" + sDate + "▦";
            } else {
                sTotalKind = sTotalKind  + "▩" + sChrt + "▦" + sChrtnm + "▦" + sDept + "▦" + sDeptnm + "▦" + sDate + "▦";
            }
            //20060926 taja78   0000ff에서 000000으로 수정함 [메모리 누적 문제]
            MyFile.Write("[<Font Size=12, decorate=underline, ForeColor=000000, weight=bold>]");
            sFontSize14 = 0;
            sFontSize12 = 1;
            sFontSize10 = 0;
            sDecorate = 1;
        } else if (sFontSize10 == 0 && sPath != "1") {
            if (sDecorate == 1) {
                var sIdno = model.getValue("/root/MainData/condition/cond1");
                var sName = model.getValue("/root/MainData/opmcptbsm/name");
                var regno = model.getValue("/root/MainData/opmcptbsm/regno1")+""+model.getValue("/root/MainData/opmcptbsm/regno2");
                var sAge = lvGetAge(regno, sDate.lvToDate("YYYYMMDD"));
                var sSex = lvGetGender(regno);
                var sAgeSex = sAge+"/"+sSex;
                var sRgno = model.getValue("/root/MainData/opmcptbsm/regno") + "******";
                var sPtdt = "";
                sDeptnm = "";
                sDate = "";
                if (sDept == "LIS") {
                    MyFile.Write("[<Font Size=8, decorate=, ForeColor=000000, weight=normal>][<Paragraph LineSpace=130>]");
                } else {
                    MyFile.Write("[<Font Size=10, decorate=, ForeColor=000000, weight=normal>][<Paragraph LineSpace=130>]");
                }
                MyFile.WriteLine();
                MyFile.WriteLine();
                MyFile.WriteLine(f_NoDisplayPT("< 등록번호 : " + sIdno + "     성명 : " + sName + "     나이/성별 : " + sAgeSex + " >"));
                MyFile.WriteLine();
            } else {
                if (sDept == "LIS") {
                    MyFile.Write("[<Font Size=8>]");
                } else {
                    MyFile.Write("[<Font Size=10>]");
                }
            }
            sFontSize14 = 0;
            sFontSize12 = 0;
            sFontSize10 = 1;
            sDecorate = 0;
        }

        for (var j=0; j<sData.length; j++) {
            if (sData.charAt(j) == '\n') {
                MyFile.WriteLine();
            } else {
                MyFile.Write(sData.charAt(j));
            }
        }
        MyFile.WriteLine();
    } // for문
    if (iEnd == 0) MyFile.Close();

    model.setAttribute("MRR_GRGeomSaek_Ocr.sMRR_GRGeomSaek_FileCnt", sMRR_GRGeomSaek_FileCnt);
}
*/
/**
 * @group  :
 * @ver    : 2005.02.15
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과 Text로 저장(연속보기)
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
 /*
 function fWriteGyeolGwa2() {
    var iEnd = 0;

    var sPageSkip = 0;
    var sFontSize14 = 0;
    var sFontSize12 = 0;
    var sFontSize10 = 0;
    var sDecorate = 0;

    var sChrt = "";
    var sChrtnm = "";
    var sDept = "";
    var sDeptnm = "";
    var sDate = "";
    var sCode = "";

    var fso = new ActiveXObject("Scripting.FileSystemObject");
    sMRR_GRGeomSaek_FileCnt = sMRR_GRGeomSaek_FileCnt + 1;
    var MyFile = fso.CreateTextFile("C:\\Program Files\\his\\temp\\RESULT_"+ sMRR_GRGeomSaek_FileCnt +".txt", true);

    var fg ;
    //2006-08-16 1:51오후 get Model 관련 수정 김태범
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        fg = emr_Right.vsgrid("grid_result");
    } else {
        return;
    }

    for (var i = 1; i <= fg.Rows - fg.FixedRows; i++) {
        var sData = emr_Right.getValue("/root/MainData/result/list[" + i + "]/data");
        var sPath = emr_Right.getValue("/root/MainData/result/list[" + i + "]/level");

        if (sFontSize12 == 0 && sPath == "1") {
            if (sPageSkip != 0) { //첫장이 아닌경우
                if (i != fg.Rows - fg.FixedRows) { //끝이 아니면 파일을 새로 연다.
                    MyFile.WriteLine();
                    MyFile.WriteLine();
                    MyFile.WriteLine();
                    MyFile.WriteLine();
                    MyFile.WriteLine();
                } else { //마지막문장이므로 파일을 close할 필요가 없으므로 iEnd를 '1'로 setting한다.
                    iEnd = 1;
                }
            } else {
                sPageSkip = 1;
            }

            sChrt = emr_Right.getValue("/root/MainData/result/list[" + i + "]/rsltno"); //추가(20050725)
            sChrtnm = emr_Right.getValue("/root/MainData/result/list[" + i + "]/data"); //추가(20050725)
            sDept = emr_Right.getValue("/root/MainData/result/list[" + i + "]/suppdept"); //추가(20050725)
            sDeptnm = emr_Right.getValue("/root/MainData/result/list[" + i + "]/suppdept"); //추가(20050725)
            sDate = emr_Right.getValue("/root/MainData/result/list[" + i + "]/rsltdd"); //추가(20050725)
            sCode = emr_Right.getValue("/root/MainData/result/list[" + i + "]/tclscd"); //추가(20050725)
            sCode = sCode.substr(0,2);
            if (i == 1) {
                sTotalKind = sChrt + "▦" + sChrtnm + "▦" + sDept + "▦" + sDeptnm + "▦" + sDate + "▦";
            } else {
                sTotalKind = sTotalKind  + "▩" + sChrt + "▦" + sChrtnm + "▦" + sDept + "▦" + sDeptnm + "▦" + sDate + "▦";
            }
//20060926 taja78   0000ff에서 000000으로 수정함 [메모리 누적 문제]
            MyFile.Write("[<Font Size=12, decorate=underline, ForeColor=000000, weight=bold>]");
            //MyFile.WriteLine();
            sFontSize14 = 0;
            sFontSize12 = 1;
            sFontSize10 = 0;
            sDecorate = 1;
        } else if (sFontSize10 == 0 && sPath != "1") {
            if (sDecorate == 1) {
                var sIdno = model.getValue("/root/MainData/condition/cond1");
                var sName = model.getValue("/root/MainData/opmcptbsm/name");
                var regno = model.getValue("/root/MainData/opmcptbsm/regno1")+""+model.getValue("/root/MainData/opmcptbsm/regno2");
                var sAge = lvGetAge(regno, sDate.lvToDate("YYYYMMDD"));
                var sSex = lvGetGender(regno);
                var sAgeSex = sAge+"/"+sSex;
                var sRgno = model.getValue("/root/MainData/opmcptbsm/regno") + "******";
                var sPtdt = "";
                sDeptnm = "";
                sDate = "";
                if (sDept == "LIS") {
                    MyFile.Write("[<Font Size=8, decorate=, ForeColor=000000, weight=normal>][<Paragraph LineSpace=130>]");
                } else {
                    MyFile.Write("[<Font Size=10, decorate=, ForeColor=000000, weight=normal>][<Paragraph LineSpace=130>]");
                }
                MyFile.WriteLine();
                MyFile.WriteLine();
                MyFile.WriteLine("< 등록번호 : " + sIdno + "     성명 : " + sName + "     나이/성별 : " + sAgeSex + " >");
                MyFile.WriteLine();
            } else {
                if (sDept == "LIS") {
                    MyFile.Write("[<Font Size=8>]");
                } else {
                    MyFile.Write("[<Font Size=10>]");
                }
            }
            sFontSize14 = 0;
            sFontSize12 = 0;
            sFontSize10 = 1;
            sDecorate = 0;
        }

        for (var j=0; j<sData.length; j++) {
            if (sData.charAt(j) == '\n') {
                MyFile.WriteLine();
            } else {
                MyFile.Write(sData.charAt(j));
            }
        }
        MyFile.WriteLine();
    } // for문
    if (iEnd == 0) MyFile.Close();
    model.setAttribute("MRR_GRGeomSaek_Ocr.sMRR_GRGeomSaek_FileCnt", sMRR_GRGeomSaek_FileCnt);
}
*/

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : OCR조회
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fViewerSetting(sChrt, sChrtNm, sDept, sDeptNm, sDate, sDoct, sDoctNm, sPtdt, sFileCnt, sGubn) {
    if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") {
        //model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
    } else {
        //2006-08-16 1:51오후 get Model 관련 수정 김태범
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            emr_Right.setValue("/root/MainData/ChartInfo/fileTotal", sMRR_GRGeomSaek_FileCnt);
            emr_Right.setValue("/root/MainData/ChartInfo/pageTotal", 1);
            emr_Right.setValue("/root/MainData/ChartInfo/pageCurrent", 1);

            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/ctpg");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/trno");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/pgno");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/fmcd");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/fmnm");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/clcd");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/clnm");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/ptdt");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/urcd");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/urnm");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/indt");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/oudt");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/seen");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/fext");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmad");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmpo");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmur");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmpa");
            emr_Right.makeNode("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmpt");

            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/ctpg", "1");
            //emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/trno", "01285232I0003742005060120040036M");
            //emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/pgno", "323");
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/trno", "");
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/pgno", "");
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/fmcd", sChrt);
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/fmnm", sChrtNm);
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/clcd", sDept);
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/clnm", sDeptNm);
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/ptdt", sDate.substr(0,8));
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/urcd", sDoct);
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/urnm", sDoctNm);
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/indt", sPtdt);
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/oudt", "29991231");
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/seen", "0");
            //emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/fext", "tif");
            //emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmad", "10.20.210.1");
            //emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmpo", "2002");
            //emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmur", "bit");
            //emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmpa", "bit");
            //emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmpt", "/app/sft/chartimg");
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/fext", ""); //temp
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmad", ""); //temp
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmpo", ""); //temp
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmur", ""); //temp
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmpa", ""); //temp
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmpt", "/app/sft/chartimg"); //temp
            emr_Right.setValue("/root/MainData/Charts/Chart[" + sFileCnt + "]/rmpt", g_EMRImageTempPath+"\\" + sGubn + sFileCnt + ".txt");

            emr_Right.refresh();
        } else {
            //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
            return;
        }
    }

    if (sGubn == "EMR_") {
        model.removeNodeset("/root/MainData/Charts/Chart");
    } else if (sGubn == "NURSE_") {
        model.removeNodeset("/root/MainData/Charts/Chart");
    } else if (sGubn == "RESULT_" && sFileCnt == 1) {
        model.removeNodeset("/root/MainData/Charts/Chart");
    }

    var sBitprtauth = TFGetSessionInfo("bitprtauth"); //출력권한

    if (sBitprtauth == "1" || model.getValue("/root/MainData/limit/ercheck") == "Y" || gGiRokSinCheong_print == "Y") { //2007-09-19 4:57오후 김태범 권한 신청 관련 수정
        model.enable("button_print", "true"); //출력
    }
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : EMR, OCR image조회
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fEmrOcrMerge() {

}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : wait begin
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fWaitBegin(sMessage) {
    model.setAttribute("sMRR_GRGeomSaek_Message", sMessage);
    TFshowModal(null, "WaitPopup", "2", "/emr/girokweb/xfm/MRR_Wait.xfm", "-",498, 80, 0, 0, false, false);
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : wait end
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fWaitEnd() {
    //2006-08-16 1:51오후 get Model 관련 수정 김태범
    var emr_Popup = TFGetModel("WaitPopup","Y");
    if (emr_Popup) {
        emr_Popup.closeBrowser();
        emr_Popup.doAction();
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }
}

/**
 *
 *  @group  :
 *  @ver    : 2006.02.14
 *  @by     : 김태범
 *  @---------------------------------------------------
 *  @type   : function
 *  @access : public
 *  @desc   : 코드 또는 명칭의 Validation Check
 *  @param pGubn   : 약품
 *  @param pStatId : Statement ID
 *  @param pJobGbn : Validation Check할 항목
 *  @param pCond   : 조회시 필요한 추가 정보
 */
function fValidate_drug(pGubn, pStatId, pJobGbn, pCond) {
    var sCodePath = "";  //code XPath
    var sNamePath = "";  //name XPath
    var sName = "";      //입력 항목 Name
    var sControlId = ""; //입력 Control의 ID
    var sSrhTxt = "";    //Validation Check하고자 하는 정보

    sControlId = "input2";
    sSrhTxt = model.getValue("/root/HideData/condition/cond11");

    if (sSrhTxt == "") {
        return;
    }

    var sParam = pJobGbn + "▦1▦" + sSrhTxt;
    var iCnt = 0; //해당되는 자료의 건수

    if (model.getValue("/root/HideData/condition/cond8") == "20") pStatId = "girok_twoyak_suhyulhelp"; //수혈일경우

    if (model.getValue("/root/HideData/condition/cond8") == "20") pStatId = "girok_twoyak_suhyulhelp"; //수혈일경우

    if (EuiMuGiRok_FValidate(pStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            //한건일 경우... 그리드에 insert한다...
            var iCur = EuiMuGiRok_FGridRowAdd("grid1", "/root/HideData/druggirok_durg", false);
            model.setValue("/root/HideData/druggirok_durg["+iCur+"]/drugcd",model.getValue("/root/HideData/codeName/code"));
            model.setValue("/root/HideData/druggirok_durg["+iCur+"]/drugnm",model.getValue("/root/HideData/codeName/name1"));
            model.gridRefresh("grid1");
            fSelectDrug2("BB");
        } else if (iCnt > 1) {
            fClicked("button_drug_help1");
        } else {
            TFGetMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 코드 입니다.", "확인!", "E", "OK");
            model.setValue(sCodePath, "");
            model.refresh();
            model.setFocus(sControlId);
        }
    } else {
        TFGetMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 코드 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        model.refresh();
        model.setFocus(sControlId);
    }
}

/**
 *
 *  @group  :
 *  @ver    : 2013.05.15
 *  @by     : 백종하
 *  @---------------------------------------------------
 *  @type   : function
 *  @access : public
 *  @desc   : 코드 또는 명칭의 Validation Check
 *  @param pGubn   : 약품
 *  @param pStatId : Statement ID
 *  @param pJobGbn : Validation Check할 항목
 *  @param pCond   : 조회시 필요한 추가 정보
 */
function fValidate_drug1(pGubn, pStatId, pJobGbn, pCond) {
    var sCodePath = "";  //code XPath
    var sNamePath = "";  //name XPath
    var sName = "";      //입력 항목 Name
    var sControlId = ""; //입력 Control의 ID
    var sSrhTxt = "";    //Validation Check하고자 하는 정보

    sControlId = "input_drugcode";
    sSrhTxt = model.getValue("/root/HideData/condition/cond4");

    if (sSrhTxt == "") {
        return;
    }

    var sParam = pJobGbn + "▦1▦" + sSrhTxt;
    var iCnt = 0; //해당되는 자료의 건수

    if (model.getValue("/root/HideData/condition/cond8") == "20") pStatId = "girok_twoyak_suhyulhelp"; //수혈일경우

    if (model.getValue("/root/HideData/condition/cond8") == "20") pStatId = "girok_twoyak_suhyulhelp"; //수혈일경우

    if (EuiMuGiRok_FValidate(pStatId, sParam)) {
        iCnt = parseInt(model.getValue("/root/HideData/codeName/count"));
        if (iCnt == 1) {
            //한건일 경우... 그리드에 insert한다...
            var iCur = EuiMuGiRok_FGridRowAdd("grid5", "/root/HideData/druggirok_durg1", false);
            model.setValue("/root/HideData/druggirok_durg1["+iCur+"]/drugcd",model.getValue("/root/HideData/codeName/code"));
            model.setValue("/root/HideData/druggirok_durg1["+iCur+"]/drugnm",model.getValue("/root/HideData/codeName/name1"));
            model.gridRefresh("grid5");
            fSelectDrug("BB");
        } else if (iCnt > 1) {
            fClicked("button_drug_help1");
        } else {
            TFGetMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 코드 입니다.", "확인!", "E", "OK");
            model.setValue(sCodePath, "");
            model.refresh();
            model.setFocus(sControlId);
        }
    } else {
        TFGetMsgBox(-1, "'" + sSrhTxt + "'는 잘못된 코드 입니다.", "확인!", "E", "OK");
        model.setValue(sCodePath, "");
        model.refresh();
        model.setFocus(sControlId);
    }
}

/**
 *
 *  @group  :
 *  @ver    : 2006.03.14
 *  @by     : 김태범
 *  @---------------------------------------------------
 *  @type   : function
 *  @access : public
 *  @desc   : 부서코드 리스트를 조회하는 함수
 *  @param :
 */
function fGetDeptCode() {
    TFclearNodeValue("/root/SendData"); //이전의 서버 호출에 사용된 정보 지움
    model.setValue("/root/SendData/Mode","reqGetDeptCodeList");
    model.submitInstance("reqGetDeptCodeList");
}

/**
 *
 *  @group  :
 *  @ver    : 2006.05.03
 *  @by     : 김태범
 *  @---------------------------------------------------
 *  @type   : function
 *  @access : public
 *  @desc   : 수술탭을 눌렀을 경우
 *  @param :
 */
function fOnClickSuSulTab() {
    model.toggle("case_6");

    fGetSuSulList();

    model.setAttribute("MRR_GRGeomSaek_SuSul.pid",   model.getValue("/root/MainData/condition/cond1"));
    model.setAttribute("MRR_GRGeomSaek_SuSul.frdd",  model.getValue("/root/MainData/condition/frdt"));
    model.setAttribute("MRR_GRGeomSaek_SuSul.todd",  model.getValue("/root/MainData/condition/todt"));
    model.setAttribute("MRR_GRGeomSaek_SuSul.kornm", model.getValue("/root/MainData/opmcptbsm/name"));
    model.setAttribute("MRR_GRGeomSaek_SuSul.regno", model.getValue("/root/MainData/opmcptbsm/regno"));
    model.setAttribute("MRR_GRGeomSaek_SuSul.regno1",model.getValue("/root/MainData/opmcptbsm/regno1"));
    model.setAttribute("MRR_GRGeomSaek_SuSul.regno2",model.getValue("/root/MainData/opmcptbsm/regno2"));
    model.setAttribute("MRR_GRGeomSaek_SuSul.gndr",  model.getValue("/root/MainData/opmcptbsm/sex"));

    //시점문제로 앞으로 가져옴
    if (fGetIP_xfm() != "MRR_GRGeomSaek_SuSul" && fGetIP_xfm() != "MRR_GRGeomSaek_SuSul_OnlySize") {
        // 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
        var m_xfm = "MRR_GRGeomSaek_SuSul.xfm";

        if (g_GRSearchVersion != "") {
            m_xfm = "MRR_GRGeomSaek_SuSul_OnlySize.xfm"; // 기능은 같으나 Size가 조정된 버전
        }

        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/" + m_xfm, "replace");
    }

    /*
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        alert("error.");
        emr_Right.setValue("/root/HideData/condition/pid"  ,model.getValue("/root/MainData/condition/cond1"));
        emr_Right.setValue("/root/HideData/condition/kornm",model.getValue("/root/MainData/opmcptbsm/name"));
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }
    */
}

/**
 *
 *  @group  :
 *  @ver    : 2006.05.03
 *  @by     : 김태범
 *  @---------------------------------------------------
 *  @type   : function
 *  @access : public
 *  @desc   : 수술정보 조회
 *  @param :
 */
function fGetSuSulList() {
    TFclearNodeValue("/root/SendData");
    model.setValue("/root/SendData/Mode","reqGetSuSulDateList");
    model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1"));
    model.setValue("/root/SendData/Data2", model.getValue("/root/MainData/condition/frdt"));
    model.setValue("/root/SendData/Data3", model.getValue("/root/MainData/condition/todt"));
    if (model.submitInstance("reqGetSuSulDateList")) {
        TFSetMessage("/root/MainData/susullist");

        if (TFGetGridCount("grid3") > 0) {
            var sfg = model.vsGrid("grid3");
            for (var i = sfg.FixedRows; i < sfg.Rows; i++) {
                var sLevel = model.getValue("root/MainData/susullist["+i+"]/levl");
                sfg.IsSubtotal(i) = true;
                sfg.RowOutlineLevel(i) = sLevel; //Returns or sets the outline level for a subtotal row.

                //color setting
                if (sLevel != "2" && sLevel != "3") {
                    //TFGridRowColor("grid3", i, 1, i, 1, 1, 230, 230, 230);
                } else {
                    TFGridFontColor("grid3", i, 0, i, 1, 1, 0, 0, 255);
                }
            }

            // 펼칩니당~
            for (var i = sfg.FixedRows; i < sfg.Rows; i++) sfg.IsCollapsed(i) = 1;

            sfg.PicturesOver = false; //글자와 이미지를 겹치지 않게.
            sfg.OutlineBar = 4; //+ , - , 사각형 안보이게 하려면 0 으로 세팅
            sfg.OutlineCol = 0; //returns or sets the column used to display the outline tree
            sfg.Row = 0;
        }
    }
}

/**
 *
 *  @group  :
 *  @ver    : 2006.05.03
 *  @by     : 김태범
 *  @---------------------------------------------------
 *  @type   : function
 *  @access : public
 *  @desc   : 수술일자별 리스트 더블클릭시 이벤트
 *  @param :
 */
function fOnClickSuSulList() {
    var sFrdd_susul = "";
    var sTodd_susul = "";
    var sPid_susul  = model.getValue("/root/MainData/condition/cond1");
    if  (sPid_susul == "") {
        model.alert("환자정보가 존재하지 않습니다. 등록번호를 입력하시기 바랍니다...");
        return;
    }

    var iCurrentRow = TFGetGridPos("grid3");
    var sOifg = model.getValue("/root/MainData/susullist["+iCurrentRow+"]/oifg");
    var sLevel = model.getValue("/root/MainData/susullist["+iCurrentRow+"]/levl");

    if (sLevel == "1") {
        if (sOifg == "O") {
            var arrODate = model.getValue("/root/MainData/susullist["+iCurrentRow+"]/optodd").split("/");
            sFrdd_susul = arrODate[1];
            sTodd_susul = arrODate[2];
            if (sFrdd_susul !="" && sFrdd_susul != null && sTodd_susul != "" && sTodd_susul != null) {
                 if (fGetIP_xfm() != "MRR_GRGeomSaek_SuSul" && fGetIP_xfm() != "MRR_GRGeomSaek_SuSul_OnlySize") {
                    var m_xfm = "MRR_GRGeomSaek_SuSul.xfm";
                    if (g_GRSearchVersion != "") {
                        m_xfm = "MRR_GRGeomSaek_SuSul_OnlySize.xfm"; // 기능은 같으나 Size가 조정된 버전
                    }

                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/" + m_xfm,"replace");
                } else {
                    fMakeSuSulListCond(sPid_susul, sFrdd_susul, sTodd_susul, "A", "A", "Y", sOifg);
                }
            }
        } else {
           return;
        }
    } else {
        if (sOifg == "O") {
            if (sLevel == "2") {
                return;
            } else {
                //해당 진료일자 파라미터 던진후 조회!!
                sFrdd_susul = model.getValue("/root/MainData/susullist["+iCurrentRow+"]/opfromdd");
                sTodd_susul = sFrdd_susul;

                if (fGetIP_xfm() != "MRR_GRGeomSaek_SuSul" && fGetIP_xfm() != "MRR_GRGeomSaek_SuSul_OnlySize") {
                    var m_xfm = "MRR_GRGeomSaek_SuSul.xfm";
                    if (g_GRSearchVersion != "") {
                        m_xfm = "MRR_GRGeomSaek_SuSul_OnlySize.xfm"; // 기능은 같으나 Size가 조정된 버전
                    }

                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/" + m_xfm,"replace");
                } else {
                    fMakeSuSulListCond(sPid_susul, sFrdd_susul, sTodd_susul, "A", "A", "Y", sOifg);
                }
            }
        } else if (sOifg == "I") {
            if (sLevel == "2") {
                if (fGetIP_xfm() != "MRR_GRGeomSaek_SuSul" && fGetIP_xfm() != "MRR_GRGeomSaek_SuSul_OnlySize") {
                    var m_xfm = "MRR_GRGeomSaek_SuSul.xfm";
                    if (g_GRSearchVersion != "") {
                        m_xfm = "MRR_GRGeomSaek_SuSul_OnlySize.xfm"; // 기능은 같으나 Size가 조정된 버전
                    }

                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/" + m_xfm,"replace");
                } else {
                    //해당  입/퇴원일자 파라미터 던진후 조회!!
                    var sTempdt = model.getValue("/root/MainData/susullist["+iCurrentRow+"]/opfromdd").split("|");
                    var sTempdt2 = model.getValue("/root/MainData/susullist["+iCurrentRow+"]/optodd");
                    sFrdd_susul = sTempdt[0];
                    sTodd_susul = sTempdt2;
                    fMakeSuSulListCond(sPid_susul, sFrdd_susul, sTodd_susul, "A", "A", "Y", sOifg);
                }
            } else if (sLevel == "3") {
                var tempArr;
                tempArr = model.getValue("/root/MainData/susullist["+iCurrentRow+"]/opfromdd").split("|");
                sFrdd_susul = tempArr[0];
                sTodd_susul = sFrdd_susul;

                if (fGetIP_xfm() != "MRR_GRGeomSaek_SuSul" && fGetIP_xfm() != "MRR_GRGeomSaek_SuSul_OnlySize") {
                    var m_xfm = "MRR_GRGeomSaek_SuSul.xfm";
                    if (g_GRSearchVersion != "") {
                        m_xfm = "MRR_GRGeomSaek_SuSul_OnlySize.xfm"; // 기능은 같으나 Size가 조정된 버전
                    }

                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/" + m_xfm,"replace");
                } else {
                    fMakeSuSulListCond(sPid_susul, sFrdd_susul, sTodd_susul, "A", "A", "Y", sOifg);
                }
            } else {
                return;
            }
        } else {
            //외래입원 구분자 에러...
        }
    }

}

/**
 *
 *  @group  :
 *  @ver    : 2006.05.03
 *  @by     : 김태범
 *  @---------------------------------------------------
 *  @type   : function
 *  @access : public
 *  @desc   : 수술정보 내용 더블클릭
 *  @param
 */
function fOnDoubleClickSuSulList() {
    var fg = model.vsGrid("grid3");
    var iRow = TFGetGridPos("grid3");
    var iCol = TFGetGridCol("grid3");
    if (iRow > 0) {
        if (fg.IsCollapsed(iRow) == 0) {
            fg.IsCollapsed(iRow) = 2;
        } else if (fg.IsCollapsed(iRow) == 2) {
            fg.IsCollapsed(iRow) = 1;
        }
    } //(iRow > 0)
}

/**
 *
 *  @group  :
 *  @ver    : 2006.05.03
 *  @by     : 김태범
 *  @---------------------------------------------------
 *  @type   : function
 *  @access : public
 *  @desc   : 수술정보 조회 파라미터 세팅
 *  @param sPid        : 등록번호
 *  @param pFrdd       : 시작일자
 *  @param pTodd       : 종료일자
 *  @param pOrddeptcd  : 진료과
 *  @param pOpstat     : 의뢰상태
 *  @param pSearchFlag : 조회구분자("Y": 조회)
 */
function fMakeSuSulListCond(pPid, pFrdd, pTodd, pOrddeptcd, pOpstat, pSearchFlag, PIoFlag) {

	model.setAttribute("MRR_GRGeomSaek_PSYChk", model.getValue("/root/MainData/limit/grcheck"));

    if (fGetIP_xfm() == "MRR_GRGeomSaek_SuSul" || fGetIP_xfm() == "MRR_GRGeomSaek_SuSul_OnlySize") {
        //2006-08-16 1:51오후 get Model 관련 수정 김태범
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            //전체 조회 일자 조건
            emr_Right.setValue("/root/HideData/dtcondition/frdd",pFrdd);
            emr_Right.setValue("/root/HideData/dtcondition/todd",pTodd);

            emr_Right.setValue("/root/HideData/condition/pid",pPid);
            emr_Right.setValue("/root/HideData/condition/frdd",pFrdd);
            emr_Right.setValue("/root/HideData/condition/todd",pTodd);
            emr_Right.setValue("/root/HideData/condition/orddeptcd","A");
            emr_Right.setValue("/root/HideData/condition/opstat","A");

            //진단정보 세팅
            emr_Right.setValue("/root/HideData/condition1/frdd",pFrdd);
            emr_Right.setValue("/root/HideData/condition1/todd",pTodd);
            emr_Right.setValue("/root/HideData/condition1/orddeptcd","A");
            emr_Right.setValue("/root/HideData/condition1/ioflag","A");
            emr_Right.setValue("/root/HideData/condition1/ioflag",PIoFlag);
            emr_Right.setValue("/root/HideData/condition1/jindanflag","A");

            //영양정보
            emr_Right.setValue("/root/HideData/condition2/frdd" ,pFrdd);
            emr_Right.setValue("/root/HideData/condition2/todd" ,pTodd);
            emr_Right.setValue("/root/HideData/condition2/stat","A");

            //감염/알러지정보
            emr_Right.setValue("/root/HideData/condition3/frdd" ,pFrdd);
            emr_Right.setValue("/root/HideData/condition3/todd" ,pTodd);
            emr_Right.setValue("/root/HideData/condition3/gb","A");

            //약 처방 정보
            emr_Right.setValue("/root/HideData/condition4/frdd" ,pFrdd);
            emr_Right.setValue("/root/HideData/condition4/todd" ,pTodd);
            emr_Right.setValue("/root/HideData/condition4/ordergb","A");
            emr_Right.setValue("/root/HideData/condition4/detail","%");
            emr_Right.visible("cbgubn", false);
            emr_Right.refresh();

            if (pSearchFlag == "Y") {
               emr_Right.javascript.fOnClickSearchBtn();// 수술정보 조회에서 전체 조회로 변경함. 2007-07-02 3:09오후 김태범
            }
        } else {
            //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
            return;
        }
    }
}

/**
 *
 *  @group  :
 *  @ver    : 2006.05.03
 *  @by     : 김태범
 *  @---------------------------------------------------
 *  @type   : function
 *  @access : public
 *  @desc   : 마취전 환자 방문기록지 조회
 *  @param
 */
function fGetAnstPatientVist() {
    if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") {
        model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm", "replace");
    }
    var sTempArr = model.getValue("/root/MainData/mrrfflshh["+TFGetGridPos("grid_main")+"]/chrtkind").split("!");
    var sTemp = sTempArr[0].split("|");
    var sPid  = sTemp[1];   //등록번호
    var sPkix = sTemp[2];   //인증키

    // temp, appended by OBH on 2008.02.27
    // TFGetMsgBox(-1, "sTempArr = " + sTempArr + " : " + "sTemp = " + sTemp + " : " + "Mode = reqGetAnstBeforVistFrom : " + "sPid = " + sPid + " : " + "sPkix = " + sPkix, "확인", "I", "OK");

    if (sPid != "" && sPkix != "") {
        TFclearNodeValue("/root/SendData");
        model.setValue("/root/SendData/Mode","reqGetAnstBeforVistFrom")
        model.setValue("/root/SendData/Data1", sPid);
        model.setValue("/root/SendData/Data2", sPkix);

        if (model.submitInstance("reqGetAnstBeforVistFrom")) {
            TFSetMessage("/root/MainData/anstvisitform");
            fMakeAnstVisitForm();
        }
    }
}

/**
 *
 *  @group  :
 *  @ver    : 2006.05.03
 *  @by     : 김태범
 *  @---------------------------------------------------
 *  @type   : function
 *  @access : public
 *  @desc   : 마취전 환자 방문기록 Display 로직
 *  @param
 *  @수정이력 : 상단정보의 잘못된 데이터가 보이는 PatientInfo는 보여지지 않도록 수정
 */
function fMakeAnstVisitForm() {
    var iRowCnt = 2;
    var sTempDepth = 0;

    //환자기본정보는 제거.
    //model.removeNodeSet("/root/MainData/anstvisitform/data/SignData/SignInfo/HwaInfo");
    //환자정보 제거 2007-02-22 11:12오전
    model.removeNodeSet("/root/MainData/anstvisitform/data/SignData/PatientInfo");
    //방문일자 제거 2007-10-23 2:08오후 - 윤정웅 과장님 요청사항
    model.removeNodeSet("/root/MainData/anstvisitform/data/SignData/SignInfo/HwaInfo/vstrecdt");

    //appended by OBH on 2008.09.28 , 인증정보의 환자기본정보 제거, 이정진선생 요청사항
    model.removeNodeSet("/root/MainData/anstvisitform/data/SignData/SignInfo/HwaInfo");

    //2006-08-16 1:51오후 get Model 관련 수정 김태범
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    var sTempPid = model.getValue("/root/MainData/condition/cond1");
    var sTextImage = model.getValue("/root/HideData/condition/cond6");
    var sTempFg;
    if (emr_Right) {
        emr_Right.removeNodeSet("/root/SubData/viewinfo");
        emr_Right.gridRebuild("grid_view");
        sTempFg = emr_Right.vsGrid("grid_view");
        var sTitle = model.getValue("/root/MainData/mrrfflshh["+TFGetGridPos("grid_main")+"]/dept").split("|");

        var strXML = model.Instance;
        var Node = strXML.selectSingleNode("/root/MainData/anstvisitform/data/SignData");

        var rootTemp = Node;
        var temp     = Node.firstChild;

        var sFileChangeFlag = "Y";

        var sDate = sTitle[1];
        var sIndt = sTitle[2];
        var sDept = sTitle[3];
        var sDoct = sTitle[4];
        var sChrt = sTitle[5];
        var sOudt = sTitle[6];
        var sIofg = sTitle[7];

        model.setAttribute("MRR_GRGeomSaek_EMR.date",sDate);
        model.setAttribute("MRR_GRGeomSaek_EMR.indt",sIndt);
        model.setAttribute("MRR_GRGeomSaek_EMR.dept",sDept);
        model.setAttribute("MRR_GRGeomSaek_EMR.doct",sDoct);
        model.setAttribute("MRR_GRGeomSaek_EMR.chrt",sChrt);

        model.setAttribute("MRR_GRGeomSaek_EMR.oudt",sOudt);
        model.setAttribute("MRR_GRGeomSaek_EMR.iofg",sIofg);

        if (sTextImage == "P") {
            fWriteFile_AnstVist(0, "", "T", sFileChangeFlag);
        } else {
            fWriteFile_AnstVist(0, "마취전환자방문기록", "T", sFileChangeFlag);
            emr_Right.javascript.fMakeSetAnstNode(1, sTitle[0], 0,0,"", sTempFg);
        }

        var sBeFlag = "";
        var sAfFlag = "";
        var iPatientInfoCnt = 0;
        var sPatientInfoFlag = "N";
        while ((temp != rootTemp)) {
            if (temp.nodeName == "#text") {
                //인증정보에 환자기본정보 제외
                var stempNode_NM = temp.parentNode.parentNode.nodeName;

                //if (stempNode_NM == "HwaInfo") {
                if (stempNode_NM == "PatientInfo") {
                    temp = fGetNode(temp, rootTemp);
                    if (temp == null) {
                        break;
                    }
                    continue;
                } else {
                    var sTempParentNode = temp.parentNode.parentNode.nodeName;
                    var sTempParent2Node = temp.parentNode.parentNode.parentNode.nodeName;
                    var sTempFirstParentNode = temp.parentNode.nodeName;
                    if (sTempParentNode == "PatientInfo" || sTempParent2Node == "PatientInfo") {
                        sPatientInfoFlag = "PatientInfo";
                        iPatientInfoCnt++;
                    } else if (sTempParentNode == "SurgInfo" || sTempParent2Node == "SurgInfo") {
                        sPatientInfoFlag = "SignInfo";
                    } else if (sTempParentNode == "TestInfolist" || sTempParent2Node == "TestInfolist") {
                        sPatientInfoFlag = "SignInfo";
                    } else if (sTempParentNode == "ShinCheInfo" || sTempParent2Node == "ShinCheInfo") {
                        sPatientInfoFlag = "SignInfo";
                    } else if (sTempParentNode == "historyInfo" || sTempParent2Node == "historyInfo") {
                        sPatientInfoFlag = "SignInfo";
                    } else if (sTempParentNode == "anestmethInfo" || sTempParent2Node == "anestmethInfo") {
                        sPatientInfoFlag = "SignInfo";
                    } else if (sTempParentNode == "authorInfo" || sTempParent2Node == "authorInfo") {
                        sPatientInfoFlag = "SignInfo";
                    }

                    //들여쓰기...
                    if (temp.text != null && temp.text != "") {
                        var sTextData = temp.text.lvReplaceWord("\n","\n"+"        ");
                    }

                    /*
                       2007-02-22 11:35오전
                       인증정보의 공통 모듈인 PatientInfo 데이터가 잘못된 케이스가 있으므로 보여주지 않으면서
                       상위 노드가 HwaInfo 이면서 노드명이 optrustorddept일 경우 부서코드를 부서명으로 수정하여 처리
                    */
                    if (sTempFirstParentNode=="optrustorddept" && sTempParentNode == "HwaInfo" && sTempParent2Node == "SignInfo") {
                        var sDeptcd = temp.text;

                        // 2007-06-30 10:00오전 : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
                        if (sTempParentNode == "HwaInfo") {
                            if (fEMR_getGeneMode() != true) {
                               if (sDeptcd != "" && sDeptcd != null) {
                                    var sDeptnm = model.getValue("root/HideData/deptList[cd ='"+sDeptcd+"']/nm");
                                    emr_Right.javascript.fMakeSetAnstNode(iRowCnt,sDeptnm, 5, iPatientInfoCnt, sPatientInfoFlag , sTempFg);
                                } else {
                                    Emr_Right.javascript.fMakeSetAnstNode(iRowCnt,sTextData, 5, iPatientInfoCnt, sPatientInfoFlag , sTempFg);
                                }
                            }
                        }
                    } else {
                        // 2007-06-30 10:00오전 : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
                    	if (sTempParentNode == "HwaInfo") {
                            if (fEMR_getGeneMode() != true) {
                                emr_Right.javascript.fMakeSetAnstNode(iRowCnt,sTextData, 5, iPatientInfoCnt, sPatientInfoFlag , sTempFg);
                            }
                        } else {
							// 2019.06 마취전환자방문기록 마취준비사항 Text grid row 처리 (긴 문구로 인해 화면 넘어가는 현상발생)
							if ( sTempFirstParentNode == "anestbefsetup" ) {
								var sTmptext = sTextData.split("\n");
								for (var i = 0; i< sTmptext.length; i++) {
									var sTmptextArry = sTmptext[i].split("\n");
									emr_Right.javascript.fMakeSetAnstNode(iRowCnt,sTmptextArry, 5, iPatientInfoCnt, sTempFirstParentNode , sTempFg);
									iRowCnt++;
								}
							} else {
                            emr_Right.javascript.fMakeSetAnstNode(iRowCnt,sTextData, 5, iPatientInfoCnt, sPatientInfoFlag , sTempFg);
							}
                        }
                    }

                    // file에 직접 로드해서 쓸경우에 들여쓰기 포함함...
                    sBeFlag = "  :    ";
                    sAfFlag = "";
                    iRowCnt++;
                }
            } else {
                var sAttributeNode = temp.getAttributeNode("name");
                if (sAttributeNode != null) {
                    //name이 존재 할 경우
                    if (temp.nodeName == "SessionInfo") {
                        sTempDepth = 1;
                        sPatientInfoFlag = "";
                        break;
                    } else if (temp.nodeName == "SignInfo") {
                        sTempDepth = 1;
                        sPatientInfoFlag = "";
                    } else if (temp.nodeName == "PatientInfo") {
                        sPatientInfoFlag = "PatientInfo";
                        iPatientInfoCnt++;
                        sTempDepth = 1;
                    } else if (temp.nodeName == "HwaInfo") {
                        sTempDepth = 2;
                        sPatientInfoFlag = "HwaInfo";
                    } else if (temp.nodeName == "SurgInfo") {
                        sTempDepth = 2;
                        sPatientInfoFlag = "SignInfo";
                    } else if (temp.nodeName == "TestInfolist") {
                        sTempDepth = 2;
                        sPatientInfoFlag = "SignInfo";
                    } else if (temp.nodeName == "ShinCheInfo") {
                        sTempDepth = 2;
                        sPatientInfoFlag = "SignInfo";
                    } else if (temp.nodeName == "historyInfo") {
                        sTempDepth = 2;
                        sPatientInfoFlag = "SignInfo";
                    } else if (temp.nodeName == "anestmethInfo") {
                        sTempDepth = 2;
                        sPatientInfoFlag = "SignInfo";
                    } else if (temp.nodeName == "authorInfo") {
                        sTempDepth = 2;
                        sPatientInfoFlag = "SignInfo";
                    }

                    //환자기본정보는 쓰지 않는다.
                    //if (temp.nodeName == "HwaInfo") {
                    if (temp.nodeName == "PatientInfo") {
                        temp = fGetNode(temp, rootTemp);

                        if (temp == null) {
                            break;
                        }
                        continue;
                    }

                    var nameAttNode = temp.getAttributeNode("name").value;
                    var m_TempNodeNM = temp.parentNode.nodeName;

                    if (m_TempNodeNM == "PatientInfo") {
                        sPatientInfoFlag = "PatientInfo";
                        iPatientInfoCnt++;
                        sTempDepth = 4;
                    } else if (m_TempNodeNM == "SurgInfo") {
                        sPatientInfoFlag = "SignInfo";
                        sTempDepth = 4;
                    } else if (m_TempNodeNM == "TestInfolist") {
                        sPatientInfoFlag = "SignInfo";
                        sTempDepth = 4;
                    } else if (m_TempNodeNM == "ShinCheInfo") {
                        sPatientInfoFlag = "SignInfo";
                        sTempDepth = 4;
                    } else if (m_TempNodeNM == "historyInfo") {
                        sPatientInfoFlag = "SignInfo";
                        sTempDepth = 4;
                    } else if (m_TempNodeNM == "anestmethInfo") {
                        sPatientInfoFlag = "SignInfo";
                        sTempDepth = 4;
                    } else if (m_TempNodeNM == "authorInfo") {
                        sPatientInfoFlag = "SignInfo";
                        sTempDepth = 4;
                        //2007-02-22 11:23오전 추가
                    } else if (m_TempNodeNM == "HwaInfo") {
                        sPatientInfoFlag = "SignInfo";
                        sTempDepth = 4;
                    //} else if (m_TempNodeNM == "HwaInfo") {
                    } else if (m_TempNodeNM == "PatientInfo") {
                        temp = fGetNode(temp, rootTemp);

                        if (temp == null) {
                            break;
                        }
                        continue;
                    }

                    if (sTempDepth == 0) {
                        sBeFlag = "";
                        sAfFlag = "";
                    } else if (sTempDepth == 1) {
                        sBeFlag = "　　[ ";

                        sAfFlag = " ] ";
                    } else if (sTempDepth == 2) {
                        sBeFlag = "　　　　";
                        sAfFlag = ">";
                    } else if (sTempDepth == 3) {
                        sBeFlag = "　 ";
                        sAfFlag = "　　";
                    } else if (sTempDepth == 4) {
                        sBeFlag = "　　";
                        sAfFlag = "";
                    } else {
                        sBeFlag = "";
                        sAfFlag = "";
                    }

                    // 2007-06-30 10:31오전 : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
                    if (temp.parentNode.nodeName == "HwaInfo" || temp.nodeName == "HwaInfo") {
                        if (fEMR_getGeneMode() != true) {
                            emr_Right.javascript.fMakeSetAnstNode(iRowCnt, nameAttNode, sTempDepth, iPatientInfoCnt, sPatientInfoFlag, sTempFg);
                            iRowCnt++;
                        }
                    } else {
                        emr_Right.javascript.fMakeSetAnstNode(iRowCnt, nameAttNode, sTempDepth, iPatientInfoCnt, sPatientInfoFlag, sTempFg);
                        iRowCnt++;
                    }
                } else {
                    //name이 존재하지 않을 경우
                }
            }

            temp = fGetNode(temp, rootTemp);

            if (temp == null) {
                break;
            }
        }

        sTempFg.WordWrap = true;
        sTempFg.AutoSizeMode  = 1;
        sTempFg.AutoSize(0, sTempFg.Cols -1);

        if ((sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T")|| sTextImage == "P") {
            //왼쪽 기록검색 그리드의 완성된 내용의 정보들을 추출하여 가져온후 화일에 기록한다.
            emr_Right.javascript.fMakeFileWiteAnstInfo();

            //결과를 얻어옴.
            var sResultData = model.getAttribute("fMakeFileWiteAnstInfo.resultData");
            //초기화
            model.setAttribute("fMakeFileWiteAnstInfo.resultData","");
            if (sResultData != null && sResultData != "") {
                var arrayRowData = sResultData.split("▦");
                var sRow_Path = "";
                var sRow_Data = "";
                //제목을 제외한 기록을 전부 화일에 쓴다.
                for (var p = 1; p < arrayRowData.length - 1 ; p++) {
                    var sRowData2 = arrayRowData[p].split("▩");
                    sRow_Path = sRowData2[0];
                    sRow_Data = sRowData2[1];
                    //화일에 기록한다.
                    fWriteFile_AnstVist(sRow_Path, sRow_Data, "T",  "N");
                    //초기화
                    sRow_Path = "";
                    sRow_Data = "";
                }
            }
        }
        var sTempArr = model.getValue("/root/MainData/mrrfflshh["+TFGetGridPos("grid_main")+"]/chrtkind").split("!");
        var sTotalKinds = sTempArr[1].lvReplaceWord("|", "▦") + "▩";

        if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I") { //kkk
            model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_EMR_text");
            model.setAttribute("MRR_GRGeomSaek_Ocr.sMRR_GRGeomSaek_FileCnt", 1);
            model.setAttribute("MRR_GRGeomSaek_ocr_write_param",sTempPid);

            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") {
                model.setAttribute("MRR_GRGeomSaek_Ocr.function","Visit");
                model.setAttribute("MRR_GRGeomSaek_Ocr.visit_sData", sTotalKinds);
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
            }
         } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "C") { //kkk
            model.setAttribute("MRR_GRGeomSaek_ocr_write_param",sTempPid);
            model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_EMR_text");
            model.setAttribute("MRR_GRGeomSaek_Ocr.sMRR_GRGeomSaek_FileCnt", 1);

            if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") {
                model.setAttribute("MRR_GRGeomSaek_Ocr.function","Visit");
                model.setAttribute("MRR_GRGeomSaek_Ocr.visit_sData", sTotalKinds);
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
            }
        } else if (sTextImage == "P") {
             if (fGetIP_xfm() != "MRR_GRGeomSaek_ReportView") {
                model.setAttribute("MRR_GRGeomSaek_ReportView.function", "fMakeImage_XFR_Print");
                model.setAttribute("MRR_GRGeomSaek_ReportView.function_param", "AnstBeForVisGirok");
                model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm", "replace");
            }
        }
    } else {
        //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
        return;
    }
}

/**
* 파일에 write(page)
*/
var sFontSize14 = 0;
var sFontSize12 = 0;
var sFontSize10 = 0;
var sDecorate = 0;
function fWriteFile_AnstVist(pPath, pData, pType, pFileChange) {
   var fso = new ActiveXObject("Scripting.FileSystemObject");
   var sFilePid = model.getValue("/root/MainData/condition/cond1");
   var sTextImage = model.getValue("/root/HideData/condition/cond6");

   if (sFilePid =="") {
       return;
   }

   if (pFileChange == "Y" && sTextImage !="P") {
       var MyFile = fso.CreateTextFile(g_EMRImageTempPath+"\\EMR_1.txt", true);
       sMRR_GRGeomSaek_FileChange = "N";
   } else if (pFileChange == "Y" && sTextImage =="P") {
       var MyFile = fso.CreateTextFile(g_EMRImageTempPath+"\\EMR_1.txt", true);
       MyFile.WriteLine("[<Page MarginLeft=7, MarginTop=0, MarginRight=7, MarginBottom=0, Width=288, Height=310>]");
       sMRR_GRGeomSaek_FileChange = "N";
   } else {
       var ForAppending = 8; //Open a file and write to the end of the file
       var MyFile = fso.OpenTextFile(g_EMRImageTempPath+"\\EMR_1.txt", ForAppending, true);
   }
   if (pType == "I") {
       MyFile.WriteLine();
       MyFile.Write(pData);
       MyFile.WriteLine();
       sFontSize14 = 0;
       sFontSize12 = 1;
       sFontSize10 = 1;
       sDecorate = 0;
   } else {
       if (sFontSize14 == 0 && pPath == 0) {
           if (iRowCount == 1) {
           } else {
               var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분
               if (sTextImage == "C") {
                   MyFile.WriteLine();
                   MyFile.WriteLine();
                   MyFile.WriteLine();
                   MyFile.WriteLine();
                   MyFile.WriteLine();
               }
           }

           if (sTextImage =="P") {
               MyFile.WriteLine("[<Font FaceName=굴림체, Size=4, weight=normal, BackColor=FFFFFF>]");
           } else {
               MyFile.Write("[<Font Size=20, decorate=underline, ForeColor=000000, weight=bold>]");
           }

           sFontSize14 = 1;
           sFontSize12 = 0;
           sFontSize10 = 0;
           sDecorate = 1;
       } else if (sFontSize12 == 0 && pPath == 1) {
           if (sDecorate == 1) {
               var sIdno = model.getValue("/root/MainData/condition/cond1");
               var sName = model.getValue("/root/MainData/opmcptbsm/name");
               var sDate = model.getAttribute("MRR_GRGeomSaek_EMR.date");
               var sIndt = model.getAttribute("MRR_GRGeomSaek_EMR.indt");
               var sAgeSex = "";
               if (sIofg == "O") {
                   var regno = model.getValue("/root/MainData/opmcptbsm/regno1")+""+model.getValue("/root/MainData/opmcptbsm/regno2");
                   var sAge = lvGetAge(regno, sIndt.lvToDate("YYYYMMDD"));
                   var sSex = lvGetGender(regno);
                   sAgeSex = sAge+"/"+sSex; //model.getValue("/root/MainData/opmcptbsm/age_sex"); // 2006.03.24 taja78
               } else {
                  var regno = model.getValue("/root/MainData/opmcptbsm/regno1")+""+model.getValue("/root/MainData/opmcptbsm/regno2");
                  var sAge = lvGetAge(regno, sDate.lvToDate("YYYYMMDD"));
                  var sSex = lvGetGender(regno);
                  sAgeSex = sAge+"/"+sSex; //model.getValue("/root/MainData/opmcptbsm/age_sex"); // 2006.03.24 taja78
               }

               sDate = sDate.substr(0,4) + "/" + sDate.substr(4,2) + "/" + sDate.substr(6,2);
               var sDept = model.getAttribute("MRR_GRGeomSaek_EMR.dept");
               var sDoct = model.getAttribute("MRR_GRGeomSaek_EMR.doct");
               var sChrt = model.getAttribute("MRR_GRGeomSaek_EMR.chrt");

               sIndt = sIndt.substr(0,4) + "/" + sIndt.substr(4,2) + "/" + sIndt.substr(6,2);
               var sOudt = model.getAttribute("MRR_GRGeomSaek_EMR.oudt");
               var sIofg = model.getAttribute("MRR_GRGeomSaek_EMR.iofg");
               if (sOudt == "29991231") {
                   sOudt = "(재원중)";
               } else {
                   sOudt = sOudt.substr(0,4) + "/" + sOudt.substr(4,2) + "/" + sOudt.substr(6,2);
               }

               if (sTextImage =="P") {
                   MyFile.WriteLine("[<Font FaceName=굴림체, Size=4, weight=normal, BackColor=FFFFFF>]");
               } else {
                    MyFile.Write("[<Font Size=12, decorate=, ForeColor=000000, weight=normal>]");
                    MyFile.WriteLine();
                    MyFile.WriteLine();

                   MyFile.WriteLine(f_NoDisplayPT("< 등록번호:" + sIdno + "  성명:" + sName + "  나이/성별:" + sAgeSex + " >"));
                   if (sIofg == "O") {
                        MyFile.WriteLine("< 진료일자:" + sIndt + "  진료과:" + sDept + "  진료의사:" + sDoct + " >");
                   } else {
                        MyFile.WriteLine("< 기록일자:" + sDate + "  진료과:" + sDept + "  진료의사:" + sDoct + " >");
                   }

                   if (sIofg == "I") { //20060207 수정
                       MyFile.WriteLine("< 입원일자:" + sIndt + "  퇴원일자:" + sOudt + " >");
                   }
                   MyFile.WriteLine();
                   MyFile.Write("[<Font Size=10, decorate=, ForeColor=000000, weight=bold>][<Paragraph LineSpace=130>]");
               }
           } else {
               if (sTextImage =="P") {
                   MyFile.WriteLine("[<Font FaceName=굴림체, Size=4, weight=normal, BackColor=FFFFFF>]");
               } else {
                   MyFile.Write("[<Font Size=10>]");
               }
           }
           sFontSize14 = 0;
           sFontSize12 = 1;
           sFontSize10 = 0;
           sDecorate = 0;
       } else if (sFontSize10 == 0 && (pPath != 0 && pPath != 1)) {
           if (sDecorate == 1) {
               var sIdno = model.getValue("/root/MainData/condition/cond1");
               var sName = model.getValue("/root/MainData/opmcptbsm/name");
               var sIndt = model.getAttribute("MRR_GRGeomSaek_EMR.indt");
               var sDate = model.getAttribute("MRR_GRGeomSaek_EMR.date");
               var sAgeSex =  "";

               if (sIofg == "O") {
                    var regno = model.getValue("/root/MainData/opmcptbsm/regno1")+""+model.getValue("/root/MainData/opmcptbsm/regno2");
                    var sAge = lvGetAge(regno, sIndt.lvToDate("YYYYMMDD"));
                    var sSex = lvGetGender(regno);
                    sAgeSex = sAge+"/"+sSex; //model.getValue("/root/MainData/opmcptbsm/age_sex"); // 2006.03.24 taja78
               } else {
                    var regno = model.getValue("/root/MainData/opmcptbsm/regno1")+""+model.getValue("/root/MainData/opmcptbsm/regno2");
                    var sAge = lvGetAge(regno, sDate.lvToDate("YYYYMMDD"));
                    var sSex = lvGetGender(regno);
                    sAgeSex = sAge+"/"+sSex; //model.getValue("/root/MainData/opmcptbsm/age_sex"); // 2006.03.24 taja78
               }

               sDate = sDate.substr(0,4) + "/" + sDate.substr(4,2) + "/" + sDate.substr(6,2);
               var sDept = model.getAttribute("MRR_GRGeomSaek_EMR.dept");
               var sDoct = model.getAttribute("MRR_GRGeomSaek_EMR.doct");
               var sChrt = model.getAttribute("MRR_GRGeomSaek_EMR.chrt");

               sIndt = sIndt.substr(0,4) + "/" + sIndt.substr(4,2) + "/" + sIndt.substr(6,2);
               var sOudt = model.getAttribute("MRR_GRGeomSaek_EMR.oudt");
               var sIofg = model.getAttribute("MRR_GRGeomSaek_EMR.iofg");
               if (sOudt == "29991231") {
                   sOudt = "(재원중)";
               } else {
                   sOudt = sOudt.substr(0,4) + "/" + sOudt.substr(4,2) + "/" + sOudt.substr(6,2);
               }

               if (sTextImage =="P") {
                   //MyFile.WriteLine("[<Font FaceName=굴림체, Size=4, weight=normal, BackColor=FFFFFF>]");
               } else {
                   MyFile.Write("[<Font Size=10, decorate=, ForeColor=000000, weight=normal>]");
                   MyFile.WriteLine();
                   MyFile.WriteLine(f_NoDisplayPT("< 등록번호:" + sIdno + "  성명:" + sName + "  나이/성별:" + sAgeSex + " >"));
                   if (sIofg == "O") {
                        MyFile.WriteLine("< 진료일자:" + sIndt + "  진료과:" + sDept + "  진료의사:" + sDoct + " >");
                   } else {
                        MyFile.WriteLine("< 기록일자:" + sDate + "  진료과:" + sDept + "  진료의사:" + sDoct + " >");
                   }
                   if (sIofg == "I") { //20060207 수정
                       MyFile.WriteLine("< 입원일자:" + sIndt + "  퇴원일자:" + sOudt + " >");
                   }
                   MyFile.WriteLine();
                   MyFile.WriteLine();
               }
           } else {
               if (sTextImage =="P") {
                   MyFile.WriteLine("[<Font FaceName=굴림체, Size=4, weight=normal, BackColor=FFFFFF>]");
               } else {
                   MyFile.Write("[<Font Size=10>]");
               }
           }
           sFontSize14 = 0;
           sFontSize12 = 0;
           sFontSize10 = 1;
           sDecorate = 0;
       }

       for (var j=0; j<pData.length; j++) {
           if (pData.charAt(j) == '\n') {
               MyFile.WriteLine();
           } else {
               MyFile.Write(pData.charAt(j));
           }
       }
   }
   MyFile.WriteLine();
   MyFile.Close();
}

/**
 * @ver    : 2004.11.28
 * @by     : 김효중
 * @ver    : 2004.12.01
 * @by     : 박진희
 * @---------------------------------------------------
 * @type   : function(fEMR_MakeMRBogiString 함수 연결)
 * @access : public
 * @desc   : 인스턴스 구조형태로 순환하여 node 반환
 * @param  : ins           - node 명
 * @param  : rootTemp - start node Path
 * @return :
 * @---------------------------------------------------
 */
function fGetNode(ins, rootTemp) {
    if (ins.nodeName != "image") {
        var child = ins.firstChild;
        if (child != null) {
            tt = tt+"  ";
            nodeCheck = nodeCheck + 1;
            return child;
        }
    }

    var sibling = ins.nextSibling;

    if (sibling != null) {
        return sibling;
    } else {
        var nodePT = ins;
        while(1) {
            nodePT = nodePT.parentNode;
            nodeCheck = nodeCheck - 1;
            tt = tt.substring(0, tt.length-2);

            if (nodePT == null) return null;
            if (nodePT == rootTemp)
                return nodePT;
            if (nodePT.nextSibling == null)
                continue;
            else
                return nodePT.nextSibling;
        }
    }
}

/**
 * @ver    : 2006.10.17
 * @by     :
 * @---------------------------------------------------
 * @type   : 임상관찰 출력 클릭시
 * @access : public
 * @desc   :
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fOnClickImSangPrintBtn() {
    var sPid = model.getValue("/root/MainData/condition/cond1");

    if (sPid !="") {
        model.setAttribute("MRR_GRGeomSaek_GISGCGiRok_parm", sPid);
    }

    var tempEmr_Popup = TFGetModel("MRR_GRGeomSaek_GISGCGiRok","N");
    if (tempEmr_Popup == null) {
        model.showModeless("MRR_GRGeomSaek_GISGCGiRok", "/emr/girokweb/xfm/MRR_GRGeomSaek_GISGCGiRok.xfm", 50, 50, 1220, 800, false, false);
    } else {
        tempEmr_Popup.javascript.fOpen("popup");
    }
}

/**
 * @ver    : 2006.10.17
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : 경장비 리스트 조회
 * @access : public
 * @desc   :
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fGetEquipmentData() {
    var sPid = model.getValue("/root/MainData/condition/cond1");
    var sFrdt = model.getValue("/root/MainData/condition/frdt");
    var sTodt = model.getValue("/root/MainData/condition/todt");

    if (sPid == "" || sFrdt == "" || sTodt == "") {
        return;
    }
    //초기화
    model.removeNodeSet("/root/MainData/equipment/list");
    model.gridRebuild("grid4");

    TFclearNodeValue("/root/SendData");
    model.setValue("root/SendData/Mode","reqGetLightEquipMentList");
    model.setValue("root/SendData/Data1",sPid);
    model.setValue("root/SendData/Data2",sFrdt);
    model.setValue("root/SendData/Data3",sTodt);

    if (model.submitInstance("reqGetLightEquipMentList")) {
        if (TFGetGridCount("grid4") > 0) {
            var sfg = model.vsGrid("grid4");
            for (var i = sfg.FixedRows; i < sfg.Rows; i++) {
                var sLevel = model.getValue("root/MainData/equipment/list["+i+"]/levl");
                sfg.IsSubtotal(i) = true;
                sfg.RowOutlineLevel(i) = sLevel; //Returns or sets the outline level for a subtotal row.

                //color setting
                if (sLevel != "2" && sLevel != "3") {
                    //TFGridRowColor("grid3", i, 1, i, 1, 1, 230, 230, 230);
                } else {
                    TFGridFontColor("grid4", i, 0, i, 1, 1, 0, 0, 255);
                }
            }
            //tree를 접는다.
            for (var i = sfg.FixedRows; i < sfg.Rows; i++) sfg.IsCollapsed(i) = 2;

            sfg.PicturesOver = false; //글자와 이미지를 겹치지 않게.
            sfg.OutlineBar = 4; //+ , - , 사각형 안보이게 하려면 0 으로 세팅
            sfg.OutlineCol = 0; //returns or sets the column used to display the outline tree
            sfg.Row = 0;
        } else {
            model.alert("조회된 데이터가 없습니다...");
        }
    } else {
        model.alert("경장비 리스트 조회시 서버오류가 발생하였습니다. 담당자에게 문의하세요.");
        return;
    }
}

// 2007-07-03 9:44오전, 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
var g_ReqPname        = null;
var g_ReqPid          = null;
var g_ReqRegno1       = null;
var g_ReqRegno2       = null;
var g_GeneUserSetting = "N";

// desc : 개인정보 보호모듈 (유전자은행연구)
function f_setGeneClearMode() {
    if (fEMR_getGeneMode() == true) {
        var m_pname   = model.getAttribute("Gene-Research-Clear-Pname");
        var m_pid     = model.getAttribute("Gene-Research-Clear-Pid");
        var m_regno1  = model.getAttribute("Gene-Research-Clear-Regno1");
        var m_regno2  = model.getAttribute("Gene-Research-Clear-Regno2");
        var m_userset = model.getAttribute("Gene-Research-Clear-UserSetting");

        if (m_pname  != "")  g_ReqPname    = new RegExp(m_pname,  ["gi"]);
        if (m_pid    != "")  g_ReqPid      = new RegExp(m_pid,    ["gi"]);
        if (m_regno1 != "")  g_ReqRegno1   = new RegExp(m_regno1, ["gi"]);
        if (m_regno2 != "")  g_ReqRegno2   = new RegExp(m_regno2, ["gi"]);

        g_GeneUserSetting = "N";
        if (m_userset == "Y") {
            g_GeneUserSetting = "Y"; // 유전자 정보제공자의 개인정보 보호 요청
        }
    }
}

/**
 * @group  :
 * @ver    : 2007-07-12 1:22오후
 * @by     : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   :
 * @return :
 * @---------------------------------------------------
 */
function f_SearchChartName(pMsg) {
    var re = /동의서/i;
    var r = pMsg.search(re);

    return(r);
}

// desc : 유전자은행과 연동시, 해당 환자의 개인정보를 조회하지 않도록 수정처리
function f_NoDisplayPT(pString) {
    var m_Temp = pString;
    if (fEMR_getGeneMode() == true) {
        // 개인정보보호를 신청했을 때만 적용
        if (g_GeneUserSetting == "Y") {
            try {
                if (g_ReqPname != null) {
                    m_Temp = m_Temp.replace(g_ReqPname, "○○○");
                }

                if (g_ReqPid != null) {
                    m_Temp = m_Temp.replace(g_ReqPid,   "*******");
                }

                if (g_ReqRegno1 != null) {
                    m_Temp = m_Temp.replace(g_ReqRegno1,   "******");
                }

                if (g_ReqRegno2 != null) {
                    m_Temp = m_Temp.replace(g_ReqRegno2,   "*******");
                }
                //alert("f_NoDisplayPT:" + pString + "\n##" + g_ReqPname + ":" + g_ReqPid + ":" + g_ReqRegno1 + ":" + g_ReqRegno2 + "\n" + m_Temp);
            } catch(E) {}
        }
    }

    return m_Temp;
}

/**
 * @ver    : 2006.10.17
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : 경장비 CatchView 팝업 호출
 * @access : public
 * @desc   :
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fClickEquipmentCatchView() {
    var row = TFGetGridPos("grid4");
    if (row <= 0) {
        model.alert("선택된 내역이 없습니다.");
        return;
    }
    var sLevl = model.getValue("/root/MainData/equipment/list[" + row + "]/levl");
    if (sLevl != 3) {
        return;
    }
    var gPid = model.getValue("/root/MainData/condition/cond1");
    var pacsno = model.getValue("/root/MainData/equipment/list[" + row + "]/accessionnumber");

    fCommonShowCATH_infinitt(pacsno);
    //2006.07.10 새로 추가.

    //model.setAttribute("MRR_GRGeomSaek_CatchViewModal_param",gPid+"▦"+pacsno+"▦"+"MRR_GRGeomSaek.js");
    //TFshowModal(null, "WaitPopup", "2", "/emr/girokweb/xfm/MRR_GRGeomSaek_CatchViewModal.xfm", "-",1080, 830, 0, 0, false, false, false);

	//var arg_str = " -O" + pacsno + " -AWorkServer -Wnexus -Dnexus";
	//var arg_str = " -O" + pacsno + " -ANEXUSSVR3 -Wnexus -Dnexus";
	//model.executeProgramEx("", "C:\\Program Files\\his\\NexusX\\NexusX.exe", arg_str);
	//var m_ver = model.getBranchName();
    /** 2014.06.24 이제 사용안함
    if (m_ver == "3.0_KunDae") {
    	model.executeProgramEx("", "C:\\Program Files\\his\\NexusX\\NexusX.exe", arg_str);
	} else {
		model.executeProgramEx("C:\\Program Files\\his\\NexusX\\NexusX.exe", arg_str);
	}*/
}

/**
 * @ver    : 2006.10.17
 * @by     : 김태범
 * @---------------------------------------------------
 * @type   : 기록신청 - 최초시작일, 종료일 정보 관련 호출
 * @access : public
 * @desc   :
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fCallGiRokSinCheongOpenData() {
    var pam_Popup = TFGetModel("OPR_GiRokJoHoiSingCheong","N");
    if (pam_Popup == null) {
        return;
    } else {
        pam_Popup.javascript.fInSertGiRokGeomSaekOpenData("close");
        //pam_Popup.closeBrowser();
        //pam_Popup.doAction();
        //model.closeBrowser();
    }
}

/**
 * @ver    : 2007-11-16 5:56오후
 * @by     : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   :
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function f_SetNavigatorPathInfo(pName, pSubNam1, pURLCheck) {
    var m_text = pName;
    if (pSubNam1 != null) {
        m_text = pName + " ▶ " + pSubNam1;
    }

    model.makenode("/root/HideData/navigator/path");
    model.setValue("/root/HideData/navigator/path", m_text);

    if (pURLCheck != "pass") {
        // 2008-07-10 1:31오후 : 제관 (U-Healthcare 처리로 인해 변경되는 브라우져 회복기능
        var m_url  = model.getURI().split("/");
        var m_send = m_url[0] + "//" + m_url[2] + "/emr/girokweb/jsp/MRR_GRGeomSaek_body.jsp";

        if (g_browser.src != m_send) {
            g_browser.src = m_send;
        }
    }

    model.refresh();
}

/*
 * XFR화면 refresh
 *
 */
 /*
function fXfrRefresh() {
    var emr_Right = TFGetModel("body_girokgeomsaek_Right", "Y");
    //alert("xfr refresh");
    var objReport = emr_Right.getObject("object2");
    var v1 = objReport.document;
    v1.instance = emr_Right.instance;

    objReport.window.showTB(false);
    objReport.window.showRuler(false);
    // copy node
    v1.refresh();

    //var obj1 = emr_Right.getObject("object2");
    //var v1 = obj1.document;
    //v1.instance = emr_Right.instance;
    //obj1.refresh();
}
*/

/**
 * @group  :
 * @ver    : 2008.03.06
 * @by     : 오봉훈(OBH)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 1. 특정과의경우, 초기화면이 검사별,일자별,이미지조회로 설정
 *         : 2. 적진요구사항
 *         : 3. 강정규선생의뢰
 * @param  : userdeptcd -> 로그인한 사용자가 속한 부서코드
 * @param  : gOption -> "2"(넘겨 받은 상수 값)
 * @return :
 * @---------------------------------------------------
 */
function f_initViewImage(userdeptcd,gOption) {
    //TFGetMsgBox(-1, "userdeptcd = " + userdeptcd, "확인", "I", "OK");
    model.toggle(model.getwindowText("button_rslt"));
    model.toggle("case_2");
    //임시로 아래와 같이 변경하여 테스트 해보자
    //var vCtrl = model.control("button_date");
    //vCtrl.selected = true;
    var vCtrl = model.control("button_rslt");
    vCtrl.selected = true;

    //f_SetNavigatorPathInfo("검사");
    //fClicked("button_rslt");
    //model.setFocus("button_rslt");
    model.setValue("/root/HideData/condition/cond6","I");
    fOnClickPrintFlag();

    model.visible("button_rslt_result", "false"); //검사별
    model.toggle("result_case1");
    model.removenodeset("/root/MainData/hist");   model.gridrebuild("grid_list");

	var sLanguage = model.getValue("/root/HideData/condition/cond15"); //한글 혹은 영어로 조회

    var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분
    if (sTextImage == "T") { //
        if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") { //시점문제로 앞으로 가져옴
            model.setAttribute("MRR_GRGeomSaek_Result.kind", "clear");
			model.trace("[KIS] body_girokgeomsaek_Right loadURI 1");
            model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm", "replace");
        }
    } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") {
        if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") { //시점문제로 앞으로 가져옴
            model.setAttribute("MRR_GRGeomSaek_Ocr.function", "clear");
            model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm", "replace");
        }
    } else if (sTextImage == "P") {
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
    }

    if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") { //이미지 조회인 경우
        fDeleteFile("RESULT_");
        sMRR_GRGeomSaek_FileCnt = 0;
    }

    //2007-07-04 1:23오후 해당 기간안의 모든 검사를 조회하는 부분 선택 처리함(기간이 길어질 수록 오래 걸리는 문제 해결) - 김태범
    if (!fGetGyeolGwaList(gOption, "1")) {//조회 기간이 2년을 넘을 경우 자동 return
        return;
    }

    var sTempGSGridCnt = TFGetGridCount("grid_list");
    if (sTempGSGridCnt > 10) {
        //2007-07-04 1:23오후 해당 기간안의 모든 검사를 조회하는 부분 선택 처리함(기간이 길어질 수록 오래 걸리는 문제 해결)
        if (model.messageBox ("모든 일자별-세부내역을 조회 하려면 '예'를 ... 단, 검사내역이 많을 경우 조회시간이 길어질 수 있습니다.", "선택", 1) != 1) {                             //재확인
            return;
        }
    } else if (sTempGSGridCnt == 0) {
        return;
    }

    if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") {
        model.setAttribute("MRR_GRGeomSaek_Result.kind", "date"); //일자별 검사결과조회
		model.trace("[KIS] body_girokgeomsaek_Right loadURI 2");
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm","replace");
    } else {
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            emr_Right.removenodeset("/root/MainData/result"); emr_Right.gridrebuild("grid_result");
            var gPid = model.getValue("/root/MainData/condition/cond1");
            var gUserId = TFGetSessionInfo("userid");
            var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
            var sGsTodd   = model.getValue("/root/MainData/condition/todt");

            // 의사확인 출력여부 설정
            var sPrint = "0";
            //if (sTextImage != "T") sPrint = "0";

            fWaitBegin("검사결과 조회중...*******");
            emr_Right.javascript.fGetResult(1, 0, "1", gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd, sLanguage, sPrint);
            fWaitEnd();

            if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I") { //이미지 조회인 경우
                fWriteGyeolGwa();
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                sTotalKind = sTotalKind + "▩";
                model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
            } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "C") { //이미지 조회인 경우
                fWriteGyeolGwa2();
                model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                sTotalKind = sTotalKind + "▩";
                model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
            }
        } else {
            //model.alert("조회에 실패하였습니다. 다시한번 선택해 주시기 바랍니다...");
            return;
        }
    }
}

/**
 * @group  :
 * @ver    : 2008.03.06
 * @by     : 오봉훈(OBH)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과 세부내역 조회 이미지 조회시 현재 선택된 레벨별 전역변수 세팅
 * @param  :
 * @param  :
 * @return :
 * Comment : 2008.03.27현재 미사용 함수임.
 * @---------------------------------------------------
 */
function fInit_fSetGeomSaGyeolGwaParam_OBH(gOption) {
    fSetGeomSaGyeolGwaParam_OBH(gOption);
    fClicked_gridList_OBH(gOption);
}

/**
 * @group  :
 * @ver    : 2008.03.06
 * @by     : 오봉훈(OBH)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 검사결과 세부내역 조회 이미지 조회시 현재 선택된 레벨별 전역변수 세팅
 * @param  :
 * @param  :
 * @return :
 * Comment : 2008.03.27현재 미사용 함수임.
 * @---------------------------------------------------
 */
function fSetGeomSaGyeolGwaParam_OBH(gOption) {
    var iCurrentPos = TFGetGridPos("grid_list");
    var iGSTotalCnt = TFGetGridCount("grid_list")
    var sLevl = model.getValue("/root/MainData/hist/list["+iCurrentPos+"]/level");
    var sTempLevl = "";
    var sGeomSaGyeolGwaParam = "";

    //OBH 20080326
    //TFGetMsgBox(-1, "여기를 타는것인가?????????????????????", "확인", "I", "OK");
    //TFGetMsgBox(-1, "iCurrentPos := "+iCurrentPos, "확인", "I", "OK");
    //TFGetMsgBox(-1, "iGSTotalCnt := "+iGSTotalCnt, "확인", "I", "OK");
    //TFGetMsgBox(-1, "sLevl := "+sLevl, "확인", "I", "OK");
    //TFGetMsgBox(-1, "sTempLevl := "+sTempLevl, "확인", "I", "OK");
    //TFGetMsgBox(-1, "sGeomSaGyeolGwaParam := "+sGeomSaGyeolGwaParam, "확인", "I", "OK");
    //iCurrentPos = "1";
    //iGSTotalCnt =
    //sLevl

    if (sLevl == "1") {
        for (var iGSPos = iCurrentPos+1; iGSPos <= iGSTotalCnt; iGSPos++) {
            sTempLevl = model.getValue("/root/MainData/hist/list["+iGSPos+"]/level");
            if (sTempLevl == "3") {
                var sTempPrintParam = model.getValue("/root/MainData/hist/list["+iGSPos+"]/printparam");
                if (sTempPrintParam != "" && sTempPrintParam != null) {
                    if (sGeomSaGyeolGwaParam == "") {
                        sGeomSaGyeolGwaParam = sTempPrintParam;
                    } else {
                        sGeomSaGyeolGwaParam = sGeomSaGyeolGwaParam+"▦"+sTempPrintParam;
                    }
                }
                //sGeomSaGyeolGwaParam = sGeomSaGyeolGwaParam+"▦"+model.getValue("/root/MainData/hist/list["+iGSPos+"]/printparam");
            } else if (sTempLevl == "1") {
                model.setAttribute("GiRokGeomSaek_sGeomSaGyeolGwaParam", sGeomSaGyeolGwaParam);
                break;
            }
        }
        model.setAttribute("GiRokGeomSaek_sGeomSaGyeolGwaParam", sGeomSaGyeolGwaParam);
    } else if (sLevl == "2") {
        for (var iGSPos = iCurrentPos+1; iGSPos <= iGSTotalCnt; iGSPos++) {
            sTempLevl = model.getValue("/root/MainData/hist/list["+iGSPos+"]/level");
            if (sTempLevl == "3") {
                var sTempPrintParam = model.getValue("/root/MainData/hist/list["+iGSPos+"]/printparam");
                if (sTempPrintParam != "" && sTempPrintParam != null) {
                    if (sGeomSaGyeolGwaParam == "") {
                        sGeomSaGyeolGwaParam = sTempPrintParam;
                    } else {
                        sGeomSaGyeolGwaParam = sGeomSaGyeolGwaParam+"▦"+sTempPrintParam;
                    }
                }
            } else {
                model.setAttribute("GiRokGeomSaek_sGeomSaGyeolGwaParam", sGeomSaGyeolGwaParam);
                break;
            }
        }
        model.setAttribute("GiRokGeomSaek_sGeomSaGyeolGwaParam", sGeomSaGyeolGwaParam);
        //alert("sGeomSaGyeolGwaParam:" + sGeomSaGyeolGwaParam);
    } else if (sLevl == "3") {
        sGeomSaGyeolGwaParam = model.getValue("/root/MainData/hist/list["+iCurrentPos+"]/printparam");
        model.setAttribute("GiRokGeomSaek_sGeomSaGyeolGwaParam", sGeomSaGyeolGwaParam);
    }
}

/**
 * @group  :
 * @ver    : 2008.03.06
 * @by     : 오봉훈(OBH)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 1. 특정과의경우, 초기화면이 검사별,일자별,이미지조회로 설정
 *         : 2. 적진요구사항
 *         : 3. 강정규선생의뢰
 * @param  : userdeptcd -> 로그인한 사용자가 속한 부서코드
 * @param  : gOption -> "2"(넘겨 받은 상수 값)
 * @return :
 * Comment : 2008.03.27현재 미사용 함수임.
 *
 *
 * @---------------------------------------------------
 */
function fClicked_gridList_OBH(gOption) {
    g_TotalTestResultTab = 3;

    var iRow_f = TFGetGridPos("grid_list");
    var iRow_s = iRow_f;
    var iTotalCount = TFGetGridCount("grid_list");
    var fg = model.vsGrid("grid_list");

	var sLanguage = model.getValue("/root/HideData/condition/cond15"); //한글 혹은 영어로 조회

    if (iRow_s > 0) {
        var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

        if (sTextImage == "P") {
            model.removeNodeset("/root/MainData/result_print/result_print_interface");
            var sLevel = model.getValue("/root/MainData/hist/list["+iRow_s+"]/level");

            var strDest = "/root/MainData/result_print";
            var strAppendTmp = "/root/MainData/result_print_tmp";

            model.makenode(strAppendTmp+"/result_print_interface");
            model.makenode(strAppendTmp+"/result_print_interface/data");
            model.makenode(strAppendTmp+"/result_print_interface/ordstatnm");
            model.makenode(strAppendTmp+"/result_print_interface/execdd");
            model.makenode(strAppendTmp+"/result_print_interface/suppdept");
            model.makenode(strAppendTmp+"/result_print_interface/rsltdd");
            model.makenode(strAppendTmp+"/result_print_interface/rsltno");
            model.makenode(strAppendTmp+"/result_print_interface/printparam");
            model.makenode(strAppendTmp+"/result_print_interface/ordcd");
            model.makenode(strAppendTmp+"/result_print_interface/ordcdnm");
            model.makenode(strAppendTmp+"/result_print_interface/level");

            var m_node = findNode("/root/MainData/result_print_tmp");

            // sLevel이 1인경우. 다음 row가 1일때까지 roof
            // sLevel이 2인경우. 다음 row가 1이나 2일때까지 roof
            // sLevel이 3인경우. 현재 row만
            if (sLevel=="1") {
                for (var i = iRow_s; i < iTotalCount; i++) {
                    iRow_s++;
                    var sLevel = model.getValue("/root/MainData/hist/list["+iRow_s+"]/level");

                    if (sLevel=="" || sLevel=="1") {
                        break;
                    } else if (sLevel=="2") {
                        continue;
                    }

                    var sTempOrdcdnm = model.getValue("/root/MainData/hist/list["+iRow_s+"]/data");
                    var sOrdcdnm = sTempOrdcdnm.split("【");
                    var sSuppDept= model.getValue("/root/MainData/hist/list["+iRow_s+"]/suppdept");
                    var sSuppDeptnm = model.getValue("/root/HideData/deptList[cd = '"+sSuppDept+"']/nm");
                    if (sSuppDeptnm != "") {
                        sSuppDept = sSuppDeptnm;
                    }

                    model.setValue(strAppendTmp+"/result_print_interface/data",       sOrdcdnm[0]);
                    model.setValue(strAppendTmp+"/result_print_interface/ordstatnm",  model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordstatnm"));
                    model.setValue(strAppendTmp+"/result_print_interface/execdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/execdd"));
                    model.setValue(strAppendTmp+"/result_print_interface/suppdept",   model.getValue("/root/MainData/hist/list["+iRow_s+"]/suppdept"));
                    model.setValue(strAppendTmp+"/result_print_interface/rsltdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltdd"));
                    model.setValue(strAppendTmp+"/result_print_interface/rsltno",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltno"));
                    model.setValue(strAppendTmp+"/result_print_interface/printparam", model.getValue("/root/MainData/hist/list["+iRow_s+"]/printparam"));
                    model.setValue(strAppendTmp+"/result_print_interface/ordcd",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcd"));
                    model.setValue(strAppendTmp+"/result_print_interface/ordcdnm",    model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcdnm"));
                    model.setValue(strAppendTmp+"/result_print_interface/level",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/level"));

                    var m_saveLists = m_node.childNodes;
                    var m_SaveJob    = findNode(strDest);

                    for (var jCnt = 0; jCnt < m_saveLists.length; jCnt++) {
                        var m_SaveChild = m_saveLists.item(jCnt);
                        m_SaveJob.appendChild(m_SaveChild.cloneNode(true));
                    }
                }
            } else if (sLevel=="2") {
                for (var i = iRow_s; i < iTotalCount; i++) {
                    iRow_s++;
                    var sLevel = model.getValue("/root/MainData/hist/list["+iRow_s+"]/level");

                    if (sLevel=="") {
                        break;
                    } else if (sLevel=="2" || sLevel=="1") {
                        break;
                    }

                    var sTempOrdcdnm = model.getValue("/root/MainData/hist/list["+iRow_s+"]/data");
                    var sOrdcdnm = sTempOrdcdnm.split("【");
                    var sSuppDept= model.getValue("/root/MainData/hist/list["+iRow_s+"]/suppdept");
                    var sSuppDeptnm = model.getValue("/root/HideData/deptList[cd = '"+sSuppDept+"']/nm");
                    if (sSuppDeptnm != "") {
                        sSuppDept = sSuppDeptnm;
                    }

                    model.setValue(strAppendTmp+"/result_print_interface/data",       sOrdcdnm[0]);
                    model.setValue(strAppendTmp+"/result_print_interface/ordstatnm",  model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordstatnm"));
                    model.setValue(strAppendTmp+"/result_print_interface/execdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/execdd"));
                    model.setValue(strAppendTmp+"/result_print_interface/suppdept",   model.getValue("/root/MainData/hist/list["+iRow_s+"]/suppdept"));
                    model.setValue(strAppendTmp+"/result_print_interface/rsltdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltdd"));
                    model.setValue(strAppendTmp+"/result_print_interface/rsltno",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltno"));
                    model.setValue(strAppendTmp+"/result_print_interface/printparam", model.getValue("/root/MainData/hist/list["+iRow_s+"]/printparam"));
                    model.setValue(strAppendTmp+"/result_print_interface/ordcd",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcd"));
                    model.setValue(strAppendTmp+"/result_print_interface/ordcdnm",    model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcdnm"));
                    model.setValue(strAppendTmp+"/result_print_interface/level",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/level"));

                    var m_saveLists = m_node.childNodes;
                    var m_SaveJob    = findNode(strDest);

                    for (var jCnt = 0; jCnt < m_saveLists.length; jCnt++) {
                        var m_SaveChild = m_saveLists.item(jCnt);
                        m_SaveJob.appendChild(m_SaveChild.cloneNode(true));
                    }
                }
            } else if (sLevel=="3") {
                var sTempOrdcdnm = model.getValue("/root/MainData/hist/list["+iRow_s+"]/data");
                var sOrdcdnm = sTempOrdcdnm.split("【");
                var sSuppDept= model.getValue("/root/MainData/hist/list["+iRow_s+"]/suppdept");
                var sSuppDeptnm = model.getValue("/root/HideData/deptList[cd = '"+sSuppDept+"']/nm");
                if (sSuppDeptnm != "") {
                    sSuppDept = sSuppDeptnm;
                }

                model.setValue(strAppendTmp+"/result_print_interface/data",       sOrdcdnm[0]);
                model.setValue(strAppendTmp+"/result_print_interface/ordstatnm",  model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordstatnm"));
                model.setValue(strAppendTmp+"/result_print_interface/execdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/execdd"));
                model.setValue(strAppendTmp+"/result_print_interface/suppdept",   sSuppDept);
                model.setValue(strAppendTmp+"/result_print_interface/rsltdd",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltdd"));
                model.setValue(strAppendTmp+"/result_print_interface/rsltno",     model.getValue("/root/MainData/hist/list["+iRow_s+"]/rsltno"));
                model.setValue(strAppendTmp+"/result_print_interface/printparam", model.getValue("/root/MainData/hist/list["+iRow_s+"]/printparam"));
                model.setValue(strAppendTmp+"/result_print_interface/ordcd",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcd"));
                model.setValue(strAppendTmp+"/result_print_interface/ordcdnm",    model.getValue("/root/MainData/hist/list["+iRow_s+"]/ordcdnm"));
                model.setValue(strAppendTmp+"/result_print_interface/level",      model.getValue("/root/MainData/hist/list["+iRow_s+"]/level"));

                var m_saveLists = m_node.childNodes;
                var m_SaveJob    = findNode(strDest);

                for (var jCnt = 0; jCnt < m_saveLists.length; jCnt++) {
                    var m_SaveChild = m_saveLists.item(jCnt);
                    m_SaveJob.appendChild(m_SaveChild.cloneNode(true));
                }
            }

        }

        if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T") { //이미지 조회인 경우
            fDeleteFile("RESULT_");
            sMRR_GRGeomSaek_FileCnt = 0;
        }
        if (fGetIP_xfm() != "MRR_GRGeomSaek_Result") {
            model.setAttribute("MRR_GRGeomSaek_Result.kind", iRow_f);
			model.trace("[KIS] body_girokgeomsaek_Right loadURI 9");			
            model.loadUrI("body_girokgeomsaek_Right", "", "", "/emr/girokweb/xfm/MRR_GRGeomSaek_Result.xfm", "replace");
        } else {
            //2006-08-16 1:51오후 get Model 관련 수정 김태범
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                emr_Right.removenodeset("/root/MainData/result"); emr_Right.gridrebuild("grid_result");
                var gPid = model.getValue("/root/MainData/condition/cond1");
                var gUserId = TFGetSessionInfo("userid");
                //조회 기간이 2년을 넘길 경우 메세지 호출 2007-07-04 1:56오후 - 김태범
                var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
                var sGsTodd   = model.getValue("/root/MainData/condition/todt");
                var sDayCount = lvGetDateTerm(sGsFromdd,sGsTodd);

                //조회 시작일 종료일 세팅
                model.setValue("/root/HideData/fromdd", sGsFromdd);
                model.setValue("/root/HideData/todd"  , sGsTodd);

                fWaitBegin("검사결과 조회중...********");

                // 2008-01-04 11:28오전 : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
                var sTempFlag = "1";

                switch (g_TotalTestResultTab) {
                    case "1" :
                        sTempFlag = g_TotalTestResultTab;
                        break;
                    case "2" :
                        sTempFlag = g_TotalTestResultTab;
                        break;
                    case "3" :
                        sTempFlag = g_TotalTestResultTab;
                        break;
                    default :
                        sTempFlag = "2";
                }

                // 의사확인 출력여부 설정
                var sPrint = "0";
                //if (sTextImage != "T") sPrint = "0";

                //alert("g_TotalTestResultTab:3 / sTempFlag:3 / iRow_f:1");
                //alert("g_TotalTestResultTab:" + g_TotalTestResultTab + " sTempFlag:" + sTempFlag);
                emr_Right.javascript.fGetResult(0, 1, 3, gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd, sLanguage, sPrint);
                //emr_Right.javascript.fGetResult(0, iRow_f, sTempFlag, gPid, "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "reqGetGyeolGwaJoHoi", gUserId, gOption, sGsFromdd, sGsTodd);
                fWaitEnd();

                if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "I") {  //이미지 조회인 경우
                    fWriteGyeolGwa();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                } else if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage == "C") {  //이미지 조회인 경우
                    fWriteGyeolGwa2();
                    model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fClicked"); //검사인경우
                    sTotalKind = sTotalKind + "▩";
                    model.setAttribute("MRR_GRGeomSaek_Ocr.sData", sTotalKind); //검사인경우
                    TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
                }
            } else {
                return;
            }
        }
    }
    model.setFocus("grid_list");
}

/**
 * @group  :
 * @ver    : V 0.1
 * @by     : 오봉훈(OBH)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 결과조회 호출후 다음 환자 선택시 열려 있는 결과 종료 함수
 * @param  :
 * @param  :
 * @return :
 * Comment : 2008.05.08 ,적정진료 담당자 요청사항(김부식)
 * @---------------------------------------------------
 */
function Close() {
    //model.CloseBrowser();
    TFCloseBrowser()
    model.doAction();
}

/**
 * @group  :
 * @ver    : V 0.1
 * @by     : 오봉훈(OBH)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 기록검색 화면 오픈시 조회사유를 체크한다.
 * @param  :
 * @param  :
 * @return :
 * Comment : 2008.05.08
 * @---------------------------------------------------
 */
function fPermissionCheck(g_Pid) {
    var sUsyn_OBH = model.getValue("/root/MainData/opmcptbsm/usyn"); //권한체크
    var occukind_OBH = model.getValue("/root/MainData/opmcptbsm/occukind")
    var priv_OBH = model.getValue("/root/MainData/opmcptbsm/priv")
    var donate_OBH = model.getValue("/root/MainData/opmcptbsm/donate")
    var dup_OBH = model.getValue("/root/MainData/opmcptbsm/dup")
    var userdeptcd_OBH = TFGetSessionInfo("userdeptcd");

    g_DupChk_Pid_Array[g_ArrCnt] = g_Pid;

    // Display on the computer, If you want confirm the data.
    /*
    for (i=0; i<=g_DupChk_Pid_Array.length-1 ; i++)
    {
        model.alert("i := " + i);
        model.alert("g_DupChk_Pid_Array" + i + g_DupChk_Pid_Array[i].toString());
    }
    */

    if (g_ArrCnt > 0) {
        for (i=0; i<=g_DupChk_Pid_Array.length-1 ; i++) {
            if (g_Pid == g_DupChk_Pid_Array[i]) {
                ++g_DupChk_Cnt;
                if (g_DupChk_Cnt > 1) {
                    g_DupChk_Permission = "Y";
                    g_DupChk_Cnt = 0;
                    break;
                }
            } else {
                g_DupChk_Permission = "N";
            }
        }
    }

    ++g_ArrCnt;

    //model.alert("여기를 타야 되는뎅 22222222.............");
    //model.alert("sUsyn_OBH := " + sUsyn_OBH);
    //model.alert("g_Pid := " + g_Pid);
    //model.alert("g_DupChk_Pid := " + g_DupChk_Pid);
    //model.alert("g_DupChk_Permission := " + g_DupChk_Permission);
    //model.alert("occukind_OBH := " + occukind_OBH);
    //model.alert("priv_OBH := " + priv_OBH);
    //model.alert("donate_OBH := " + donate_OBH);
    //model.alert("dup_OBH := " + dup_OBH);
    //model.alert("userdeptcd_OBH := " + userdeptcd_OBH);

    if (sUsyn_OBH == "Y" && g_DupChk_Permission == "Y") {
        // fPermissionCheck_Reset();
        return false;
    }

    // userdeptcd_OBH -> 000178 : 의무기록팀, 000010 : 의료정보팀, occukind_OBH -> 10 : 의사직
    if (sUsyn_OBH == "Y" && (userdeptcd_OBH == "000178" || userdeptcd_OBH == "000010" ||occukind_OBH == "10")) {
        model.enable("button_date", "true");
        model.enable("button_dept", "true");
        model.enable("button_form", "true");
        model.enable("button_nurse", "true");
        model.enable("button_order1", "true");
        model.enable("button_order2", "true");
        model.enable("button_rslt", "true");
        model.enable("button_pm", "true");
        model.enable("button_acc", "true");
        model.enable("button_ocr", "true");
        model.enable("button_drug", "true");
        model.enable("button_copy", "true");
        model.enable("button_drug2", "true");
        model.enable("button6", "true");
        model.enable("button_date_lt", "true");
        model.enable("button8", "true");
        return true;
    } else {
        var MsgRtn = TFGetMsgBox(-1, "조회권한이 없습니다.\n사유입력후 조회가 가능합니다.\n사유를 입력하시겠읍니까?", "확인", "Q", "YN"); //6:예 7:아니오
        // model.alert("MsgRtn := " + MsgRtn);
        if (MsgRtn == '6') {
            model.setAttribute("sMRR_GRGeomSaek_usyn", "N");
            model.setAttribute("sMRR_GRGeomSaek_parm", model.getValue("/root/MainData/condition/cond1"));
            model.setAttribute("sMRR_GRGeomSaek_Reason_param",gGiRokSinCheong_use);
			model.setAttribute("sMRR_GRGeomSaek_psycheck", model.getValue("/root/MainData/limit/grcheck"));

            TFshowModal(null, null, "1", "/emr/girokweb/xfm/MRR_GRGeomSaek_Reason.xfm", "-", 538, 210, 0, 0, false, false);
            var sUsyn_OBH = model.getAttribute("sMRR_GRGeomSaek_usyn");
            model.setAttribute("sMRR_GRGeomSaek_parm", "");
            model.setAttribute("sMRR_GRGeomSaek_usyn", "N");

            if (sUsyn_OBH == "N") {
                model.enable("button_date", "false");
                model.enable("button_dept", "false");
                model.enable("button_form", "false");
                model.enable("button_nurse", "false");
                model.enable("button_order1", "false");
                model.enable("button_order2", "false");
                model.enable("button_rslt", "false");
                model.enable("button_pm", "false");
                model.enable("button_acc", "false");
                model.enable("button_ocr", "false");
                model.enable("button_drug", "false");
                model.enable("button_copy", "false");
                model.enable("button_drug2", "false");
                model.enable("button6", "false");
                model.enable("button_date_lt", "false");
                model.enable("button8", "false");
                fPermissionCheck_Reset();
                return false;
            } else {
                model.enable("button_date", "true");
                model.enable("button_dept", "true");
                model.enable("button_form", "true");
                model.enable("button_nurse", "true");
                model.enable("button_order1", "true");
                model.enable("button_order2", "true");
                model.enable("button_rslt", "true");
                model.enable("button_pm", "true");
                model.enable("button_acc", "true");
                model.enable("button_ocr", "true");
                model.enable("button_drug", "true");
                model.enable("button_copy", "true");
                model.enable("button_drug2", "true");
                model.enable("button6", "true");
                model.enable("button_date_lt", "true");
                model.enable("button8", "true");
                //사유 입력을 제대로 했을 경우
                //현재 전역변수의 등록번호와 조회할 등록번호가 같을 경우 다시 조회 하지 않는다.
                // sPid = model.getValue("/root/MainData/condition/cond1");
                //g_Pid = sPid;
                //g_Pid = model.getValue("/root/MainData/condition/cond1");
                //model.alert("g_Pid := " + g_Pid);
                //model.alert("g_DupChk_Pid := " + g_DupChk_Pid);
                /*
                g_DupChk_Pid = g_Pid;
                if (g_Pid == g_DupChk_Pid) {
                    g_DupChk_Permission = "Y";

                    var sTempUsyn = model.getValue("/root/MainData/opmcptbsm/usyn");
                    if (sTempUsyn == "N") {
                        model.setValue("/root/MainData/opmcptbsm/usyn","Y");
                    }
                }
                else {
                    g_DupChk_Permission = "N";
                }
                */
                return true;
            }
        } else { //if (MsgRtn == '6')
            model.enable("button_date", "false");
            model.enable("button_dept", "false");
            model.enable("button_form", "false");
            model.enable("button_nurse", "false");
            model.enable("button_order1", "false");
            model.enable("button_order2", "false");
            model.enable("button_rslt", "false");
            model.enable("button_pm", "false");
            model.enable("button_acc", "false");
            model.enable("button_ocr", "false");
            model.enable("button_drug", "false");
            model.enable("button_copy", "false");
            model.enable("button_drug2", "false");
            model.enable("button6", "false");
            model.enable("button_date_lt", "false");
            model.enable("button8", "false");
            fPermissionCheck_Reset();
            return false;
        }
    }
}

/**
 * @group  :
 * @ver    : V 0.1
 * @by     : 오봉훈(OBH)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 조회사유 입력 "아니오"의 경우, 왼쪽 그리드를 리셋
 * @param  :
 * @param  :
 * @return :
 * Comment : 2008.05.08
 * @---------------------------------------------------
 */
function fPermissionCheck_Reset() {
    model.removeNodeset("/root/MainData/mrrfflshh");
    model.removeNodeset("/root/MainData/mrrfflshh_copy");
    model.removeNodeset("/root/MainData/copylist");

    model.gridRebuild("grid_main");
    model.gridRebuild("grid_copy");
    model.gridRebuild("grid_list");
}

/**
 * 2008-07-10 1:36오후, USN 버튼활성화 Test.... (송명근 OR 의료정보팀 OR 상단아이콘)
 * u-Healthcare System 연동데모 함수구현 : 이제관
 */
function f_viewBtnUSN() {
    var gUserId    = TFGetSessionInfo("userid");
    var userdeptcd = TFGetSessionInfo("userdeptcd");

    // 송명근 교수님 or 의료정보팀 or 가정의학과전공의(가상:테스트) or 이인식교수
    if (gUserId == "20070372" || userdeptcd == "000010" || gUserId == "29990030" || gUserId == "20040036") {
        model.visible("btnUSN", true);
    }
}

/**
 * 2008-07-10 2:07오후, 송명근교수 USN Test....
 * u-Healthcare System 연동데모 함수구현 : 이제관
 */
function f_viewUHealthcare() {
    var fg = model.vsGrid("grid_UHealthcare");
    if (fg.Row < 1) return;

    var m_levl = fg.TextMatrix(fg.Row, 0);
    // 2레벨 검사항목만 실행
    if (m_levl == "2") {
        f_SetNavigatorPathInfo("U-Healthcare", fg.TextMatrix(fg.Row, 1), "pass");

        var m_code = fg.TextMatrix(fg.Row, 3);
        if (m_code != "Active") {
            // 주민번호 임시 KEY : 사후에는 주민번호가 아닌 KEY로 매칭해야할 것, 보안문제
            var gRegNo1 = model.getValue("/root/MainData/opmcptbsm/regno1");
            var gRegNo2 = model.getValue("/root/MainData/opmcptbsm/regno2");
            var gRegNo = gRegNo1 + gRegNo2;

            //alert(gRegNo);
            var bsr = model.control("browser1");
            // URL은 테스트로 네이버, 향후에 USN에서 URL 및 GET파라메터를 주민번호로 맞춰야 함
            //http://211.41.100.11/doctor.ECGChart.cmd?Pincode=7911302047611
            //http://211.41.100.11:9081/doctor.ECGChart.cmd?Pincode= // 포트가 변경됨 9081 --> 80
            //
            bsr.src = "http://211.41.100.11/doctor.ECGChart.cmd?Pincode=" + gRegNo;
        } else {
            alert(fg.TextMatrix(fg.Row, 1) + "가 Active 상태가 아닙니다");
        }
    }
}

/**
 * 2008-08-29 1:15오후, 적정진료팀 예진기록 조회 요청
 */
function f_YJEMRView() {
    var tmphsize = 800;
    var m_pid = model.getValue("/root/MainData/condition/cond1");
    model.setAttribute("MRD_YEJINGiRokTPL_importxfm_param", "");
    TFSetGlobalAttribute("MRD_YEJINGiRokTPL_parm", m_pid + "▦" + "1200" + "▦" + tmphsize);
    TFSetGlobalAttribute("MRD_YEJINGiRokTPL_ALLVIEW", "Y");  // 예진정보 모두 보이기

    TFshowModal(null, null, "1", "/emr/girokweb/xfm/MRD_YEJINGiRokTPL.xfm", "-",  1200 + 2, tmphsize, 0, 25, false, false);
    TFSetGlobalAttribute("MRD_YEJINGiRokTPL_parm", "");

    TFSetGlobalAttribute("MRD_YEJINGiRokTPL_ALLVIEW", "");
    model.setAttribute("MRD_YEJINGiRokTPL_EMRCopy_param", "");
}

/**
 * @group  :
 * @ver    : 2008.12.11
 * @by     : 김형조
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 진행내역에서 판독결과 조회
 * @param  :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fGetJinHaengNaeYuk() {
    var iRow_f = TFGetGridPos("grid_jinhaeng");
    var iRow_s = iRow_f;
    var iTotalCount = TFGetGridCount("grid_jinhaeng");
    var fg = model.vsGrid("grid_jinhaeng");

	var sLanguage = model.getValue("/root/HideData/condition/cond15"); //한글 혹은 영어로 조회
	var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분

    var suppdept = "";

    if (iRow_s > 0) {

        model.removeNodeset("/root/MainData/result_print/result_print_interface");
        var sLevel = "2";

        var strDest = "/root/MainData/result_print";
        var strAppendTmp = "/root/MainData/result_print_tmp";

        //model.makenode(strAppendTmp+"/result_print_interface");
        //model.makenode(strAppendTmp+"/result_print_interface/data");
        //model.makenode(strAppendTmp+"/result_print_interface/printparam");
        model.makenode(strAppendTmp+"/result_print_interface/ordstatnm");
        model.makenode(strAppendTmp+"/result_print_interface/orddd");
        model.makenode(strAppendTmp+"/result_print_interface/execdd");
        model.makenode(strAppendTmp+"/result_print_interface/suppdept");
        model.makenode(strAppendTmp+"/result_print_interface/rsltdd");
        model.makenode(strAppendTmp+"/result_print_interface/rsltno");
        model.makenode(strAppendTmp+"/result_print_interface/ordcd");
        model.makenode(strAppendTmp+"/result_print_interface/ordcdnm");
        model.makenode(strAppendTmp+"/result_print_interface/bcno");
        model.makenode(strAppendTmp+"/result_print_interface/level");

        var m_node = findNode("/root/MainData/result_print_tmp");

        // sLevel이 1인경우. 다음 row가 1일때까지 roof
        // sLevel이 2인경우. 다음 row가 1이나 2일때까지 roof
        // sLevel이 3인경우. 현재 row만

        // 검사코드에 따라서 진검,핵의학,병리로 나눈다.(없으면 검사실의 판독)
        var ordcd = model.getValue("/root/MainData/jinhaeng/list[" + iRow_s + "]/ordcd");
        var orddd = model.getValue("/root/MainData/jinhaeng/list[" + iRow_s + "]/orddd");

        if (ordcd.substring(0,1) == "L") {
            suppdept = "LIS";
        } else if (ordcd.substring(0,1) == "N") {
            suppdept = "NIS";
        } else if (ordcd.substring(0,1) == "P") {
            suppdept = "PIS";
        } else {
            suppdept = "";
        }

        if (suppdept == "") {
            sLevel = "3"; // 검사실의 경우
        } else {
            sTempFlag = "1"; // 진검,핵의학,병리일 경우
            sLevel = "2";

            orddd = "-"; // 검체/병리 번호로 조회하기 위한 처방일자 제외
            /*
            if (suppdept == "LIS" || suppdept == "NIS") { // LIS검사일 경우에는 orddd값을 리셋시켜주어야 한다.
                //level = "3"; // 해당일의 모든검사 레벨
                orddd = "-"; // 검체번호로 조회하기 위한 처방일자
            }
            */
        }
        //if (sLevel=="3") {

            //var sTempOrdcdnm = model.getValue("/root/MainData/hist/list["+iRow_s+"]/data");
            //var sOrdcdnm = sTempOrdcdnm.split("【");
            //var sSuppDept= model.getValue("/root/MainData/hist/list["+iRow_s+"]/suppdept");
            //var sSuppDeptnm = model.getValue("/root/HideData/deptList[cd = '"+sSuppDept+"']/nm");
            //if (sSuppDeptnm != "") {
            //    sSuppDept = sSuppDeptnm;
            //}

            //model.setValue(strAppendTmp+"/result_print_interface/data",       sOrdcdnm[0]);
            //model.setValue(strAppendTmp+"/result_print_interface/ordstatnm",  model.getValue("/root/MainData/jinhaeng/list["+iRow_s+"]/ordstatnm"));
            //model.setValue(strAppendTmp+"/result_print_interface/printparam", model.getValue("/root/MainData/jinhaeng/list["+iRow_s+"]/printparam"));
            model.setValue(strAppendTmp+"/result_print_interface/execdd",     model.getValue("/root/MainData/jinhaeng/list["+iRow_s+"]/execdd"));
            model.setValue(strAppendTmp+"/result_print_interface/suppdept",   suppdept);
            model.setValue(strAppendTmp+"/result_print_interface/rsltdd",     model.getValue("/root/MainData/jinhaeng/list["+iRow_s+"]/rsltdd"));
            model.setValue(strAppendTmp+"/result_print_interface/rsltno",     model.getValue("/root/MainData/jinhaeng/list["+iRow_s+"]/rsltno"));
            model.setValue(strAppendTmp+"/result_print_interface/ordcd",      "");
            model.setValue(strAppendTmp+"/result_print_interface/orddd",      orddd);
            //model.setValue(strAppendTmp+"/result_print_interface/ordcdnm",    model.getValue("/root/MainData/jinhaeng/list["+iRow_s+"]/ordcdnm"));
            model.setValue(strAppendTmp+"/result_print_interface/bcno",    model.getValue("/root/MainData/jinhaeng/list["+iRow_s+"]/bcno"));
            model.setValue(strAppendTmp+"/result_print_interface/level",      sLevel);

            var m_saveLists = m_node.childNodes;
            var m_SaveJob    = findNode(strDest);

            for (var jCnt = 0; jCnt < m_saveLists.length; jCnt++) {
                var m_SaveChild = m_saveLists.item(jCnt);
                m_SaveJob.appendChild(m_SaveChild.cloneNode(true));
            }
        //}

        //2006-08-16 1:51오후 get Model 관련 수정 김태범
        var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
        if (emr_Right) {
            emr_Right.removenodeset("/root/MainData/result"); emr_Right.gridrebuild("grid_result");
            var gPid = model.getValue("/root/MainData/condition/cond1");
            var gUserId = TFGetSessionInfo("userid");
            //조회 기간이 2년을 넘길 경우 메세지 호출 2007-07-04 1:56오후 - 김태범
            var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
            var sGsTodd   = model.getValue("/root/MainData/condition/todt");
            var sDayCount = lvGetDateTerm(sGsFromdd,sGsTodd);

            //조회 시작일 종료일 세팅
            model.setValue("/root/HideData/fromdd", sGsFromdd);
            model.setValue("/root/HideData/todd"  , sGsTodd);

            fWaitBegin("검사결과 조회중...*********");

            // 2008-01-04 11:28오전 : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
            var sTempFlag = "1";

            switch (g_TotalTestResultTab) {
                case "1" :
                    sTempFlag = g_TotalTestResultTab;
                    break;
                case "2" :
                    sTempFlag = g_TotalTestResultTab;
                    break;
                case "3" :
                    sTempFlag = g_TotalTestResultTab;
                    break;
                default :
                    sTempFlag = "2";
            }

            // 의사확인 출력여부 설정
            var sPrint = "0";
            //if (sTextImage != "T") sPrint = "0";

            emr_Right.javascript.fGetResults(0, iRow_f, sTempFlag, gPid, "/ast/geomsachiryoweb/JinHaengGyeolGwaJoHoi.live", "reqJinHaengGyeolGwaJoHoi", gUserId, "1", sGsFromdd, sGsTodd, sLanguage, sPrint);
            //emr_Right.javascript.fGetResult(0, iRow_f, sTempFlag, gPid, "/ast/geomsachiryoweb/JinHaengGyeolGwaJoHoi.live", "reqJinHaengGyeolGwaJoHoi", gUserId, "1", sGsFromdd, sGsTodd);
            fWaitEnd();

        } else {
            return;
        }

    }
    model.setFocus("grid_jinhaeng");
}

/**
 * @group  :
 * @ver    : 2009.01.21
 * @by     : 오봉훈
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   :
 * @param  :
 * @return :
 * @---------------------------------------------------
 */

function fOCS_GetGridPos(pGridID) {
    var fg = model.vsgrid(pGridID);

    for (var i = fg.FixedRows; i < fg.Rows; i++) if (fg.IsSelected(i) == true) return (i - fg.FixedRows + 1);

    return 0;
}

/**
 * @group  : KUH Project 종속적인 함수
 * @ver    : 2004.11.26 (KUHDEV-0001)
 * @by     : 이동근
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 구분자로 분리된 특정자료를 Reading
 * @param  : pData
 * @param  : pRow(▩)
 * @param  : pCol(▦)
 * @return : String
 * @---------------------------------------------------
 */

function fOCS_GetMatrixData(pData, pRow, pCol) {
    var rowSep = "▩";
    var colSep = "▦";
    var arrRow = pData.split(rowSep);

    if (arrRow.length <= pRow) return "";

    var arrCol = arrRow[pRow].split(colSep);
    if (arrCol.length <= pCol) return "";

    return arrCol[pCol];
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Nurse상세조회(간호기록 정보조사지)
 * @param  :
 * @param  :
 * @return :
 * @이  력 :
 * @        김태범 2007-01-25 2:46오후 간호 EMR기록 3레벨 조회기능 추가
 * @---------------------------------------------------
 */
function fCall_NUR4(pRow, pNode) {
    var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
    var sParm = sChrtkind.lvReplaceWord("|", "▦");
    var sNur = TFGetMatrixData(sParm, 0, 0); //Nurse구분
    var sIofg = TFGetMatrixData(sParm, 0, 1);
    var sPtdt = TFGetMatrixData(sParm, 0, 2);
    var sChnm = TFGetMatrixData(sParm, 0, 3);
    var sChrt = TFGetMatrixData(sParm, 0, 4);
    var sDate = TFGetMatrixData(sParm, 0, 5);
    var sTime = TFGetMatrixData(sParm, 0, 6);
    var sChrtNm = TFGetMatrixData(sParm, 0, 7);
    var sPkiDateTime = sDate + sTime + "%";

    model.removeNodeset("/root/HideData/savedata_nurse");
    TFclearNodeValue("/root/SendData"); //이전의 서버 호출에 사용된 정보 지움
    model.setValue("/root/SendData/Mode", "MRR_reqGetMrnipkidh"); //Action Method
    model.setValue("/root/SendData/Data1", "NUR1"); //간호조회 구분
    model.setValue("/root/SendData/Data2", model.getValue("/root/MainData/condition/cond1")); //등록번호

    var sLevl = model.getValue(pNode + pRow + "]/levl");
    var sTempLevl;
    var sTempPkiDateTime = "";

    if (sLevl == "3") {
        sPkiDateTime = "";
        for (var i = pRow+1; i <= TFGetGridCount("grid_main"); i++) {
            sTempLevl = model.getValue(pNode + i + "]/levl");
            if (sTempLevl == "4") {
                sParm = model.getValue(pNode + i + "]/chrtkind").lvReplaceWord("|", "▦");
                sDate = TFGetMatrixData(sParm, 0, 5);
                sTime = TFGetMatrixData(sParm, 0, 6);
                sPkiDateTime = sDate + sTime + "%";
                if (sTempPkiDateTime != "" && sTempPkiDateTime != null) {
                    sTempPkiDateTime = sTempPkiDateTime +","+ sPkiDateTime
                } else {
                    sTempPkiDateTime = sPkiDateTime
                }
            } else {
                break;
            }
        }
        model.setValue("/root/SendData/Data3", sTempPkiDateTime); //인증 data 찾아오기 위한 argument //추가(20050725)
        model.setValue("/root/SendData/Data6", "levl3"); //인증 data 찾아오기 위한 argument //추가(20050725)
    } else {
        model.setValue("/root/SendData/Data3", sPkiDateTime); //인증 data 찾아오기 위한 argument //추가(20050725)
    }

    if (model.submitInstance("MRR_reqGetMrnipkidh")) {
        TFSetMessage("root/HideData/savedata_nurse");
        //인증데이터 환자정보의 주치의, 세션정보의 id, 부서 정보 제외. - 2007-03-20 1:24오후 김태범
        // 인증데이터 로그인 시간 제외 추가 - 2007-05-08 4:31오후 김태범
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/PatientInfo/MainDoctor");
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo/userid");
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo/empldeptgmnm");
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo/logondatetime");

        /*
            2007-12-13 5:04오후 : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
            중환자실 요구사항, 중환자실 간호기록(INS0000009) 의 경우 세션정보 안나오도록 처리함
            사유 : 이 기록은 근무 별로(D, E, N) 작성을 하는데 세션정보에는 한분만 보인다고
                   나중에 법정에 이 기록을 출력을하여 갔을 때 오해의 소지존재
        */
        if (sChrt == "INS0000009") {
            model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo");
        }
    } else {
        return false;
    }

    var xPath = "/root/HideData/savedata_nurse";

    TFRemoveNoChild(findNode(xPath));
    //var sText = fCommon_getInstanceStringSignValue(xPath); //현재는 한건만 가져오도록 함
    var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");

    var iNodeCnt = getNodesetCnt(xPath);
    var iCurPos = TFGetGridPos("grid_main");

    if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") { //이미지 조회인경우
        //2007-03-07 1:30오후 간호기록 출력 print와 같은 패턴으로 변경함.
        fDeleteFile("NURSE_");
        sMRR_GRGeomSaek_FileCnt = 0;
        model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_NUR1");
        model.setAttribute("MRR_GRGeomSaek_Ocr.sMRR_GRGeomSaek_FileCnt", 1);
        if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") {
            //2007-03-07 1:30오후 김태범 간호기록 출력 print와 같은 패턴으로 변경함. MRR_GRGeomSaek_Ocr 화면에서 콜하도록 수정함.
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param1", xPath);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param2", sMRR_GRGeomSaek_ImageView);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param3", sTextImage);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param4", iNodeCnt);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param5", iCurPos);
            model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_NUR1_NEW");

            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
        } else {
            //2007-03-07 1:30오후 간호기록 출력 print와 같은 패턴으로 변경함.
            fMakeNUR4(xPath, sMRR_GRGeomSaek_ImageView, sTextImage, iNodeCnt, iCurPos);

            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                sTotalKind = sParm + "▩";
                /*
                  == 간호기록 관련 == 2007-09-11 6:16오후 김태범 - 이태진님 요청사항
                  * 해당 서식지일 경우 발생일자 대신 기록지별 특정일자를 보여준다.
                  * 해당 서식지 정보
                  - 수술전처치상태확인 기록지   INC0000001
                  - 수술간호기록                CNS0000001
                  - 회복실 간호기록             CNC0000002
                  - 안정실 기록                 CNS0000003
                 == == == == == == ==
                */
                var sTempArrInfo = sTotalKind.split("▩");
                var sTempArrInfoDetail;
                var sInsertTempData = "";
                var sTempTotalkind = "";

                for (var itempArrSeq = 0; itempArrSeq < sTempArrInfo.length ; itempArrSeq++) {
                    sTempArrInfoDetail = sTempArrInfo[itempArrSeq].split("▦");
                    var sTempChrtcdInfo = sTempArrInfoDetail[4];
                    if (sTempChrtcdInfo == "INC0000001" || sTempChrtcdInfo == "CNS0000001" || sTempChrtcdInfo == "CNC0000002" || sTempChrtcdInfo == "CNS0000003") {
                        sInsertTempData = sTempArrInfoDetail[0] + "▦" +        //해당 선택된 grid 간호 기록의 chrtkind
                                          sTempArrInfoDetail[1] + "▦" +
                                          model.getValue(pNode + pRow + "]/chrtnm").lvReplaceWord("/","").substr(0,8) + "▦" +      // title date (OCR기록 상단날짜 정보)
                                          sTempArrInfoDetail[3] + "▦" +      //
                                          sTempArrInfoDetail[4] + "▦" +
                                          sTempArrInfoDetail[5] + "▦" +      //

                                          sTempArrInfoDetail[6] + "▦" +
                                          sTempArrInfoDetail[7] + "▦" + "▩";
                        sTempTotalkind = sTempTotalkind+sInsertTempData;
                    } else {
                        if (sTempChrtcdInfo != null && sTempChrtcdInfo != "") {
                            sInsertTempData = sTempArrInfoDetail[0] + "▦" +        //해당 선택된 grid 간호 기록의 chrtkind
                                              sTempArrInfoDetail[1] + "▦" +
                                              sTempArrInfoDetail[2] + "▦" +
                                              sTempArrInfoDetail[3] + "▦" +
                                              sTempArrInfoDetail[4] + "▦" +
                                              sTempArrInfoDetail[5] + "▦" +
                                              sTempArrInfoDetail[6] + "▦" +
                                              sTempArrInfoDetail[7] + "▦" + "▩";
                            sTempTotalkind = sTempTotalkind+sInsertTempData;
                        }
                    }
                }
                sTotalKind = sTempTotalkind;
                emr_Right.javascript.fViewerSetting(sTotalKind, "NURSE_");
                fWaitBegin("이미지 Loading중...");
                emr_Right.javascript.downloadChart(); //
                fWaitEnd();
                emr_Right.javascript.setupChart("ChartView"); //연속보기로 설정
            } else {
                return;
            }
        }
    } else if (sTextImage == "P") {
        //XFR로 출력할 경우..
        //fMakeImage_XFR_Print("EMR_GanHo"); 시점차이로 인한 문제 해결을 위해 주석처리함.
        model.setAttribute("MRR_GRGeomSaek_ReportView.function", "fMakeImage_XFR_Print");
        model.setAttribute("MRR_GRGeomSaek_ReportView.function_param", "EMR_GanHo");
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
    } else {
         if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") {
            model.setAttribute("MRR_GRGeomSaek_Grid.function" , "fCall_NUR1_NEW");
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param1", xPath);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param2", sMRR_GRGeomSaek_ImageView);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param3", sTextImage);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param4", iNodeCnt);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param5", iCurPos);

            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
         } else {
            if (emr_Right) {
                fMakeNUR4(xPath, sMRR_GRGeomSaek_ImageView, sTextImage, iNodeCnt, iCurPos);
            } else {
                return;
            }
        }
    }
}

 /**
 * @group  :
 * @ver    : 2009.08.31
 * @by     : 오봉훈
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 중환자실 간호기록 Text로 저장
 * @param  : xPath - signData의 xPath
 * @param  : sMRR_GRGeomSaek_ImageView - Y
 * @param  : sTextImage - T/I/P
 * @return :
 * @---------------------------------------------------
 */
function fMakeNUR4(xPath, sMRR_GRGeomSaek_ImageView, sTextImage, iNodeCnt, iCurPos) {
    var pFileWriteFlag = "";
    var MyFile;
    var fso;
    var sFilePath;
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    var emr_Right_fg;
    var sBeforeData;
    var sPtchchrtnm;
    var tempArr;
    var sPtchchrtcd;
    var iGanHoImgSeqNo = 1;
    var sTempString = "";

    var sPidInfo    = model.getValue("/root/MainData/condition/cond1");
    model.setAttribute("sMRR_GiRokGeomSaek_Param.fMakeNUR1.pid", sPidInfo);
    var sKornmInfo  = model.getValue("/root/MainData/opmcptbsm/name");
    var sSexAgeInfo = model.getValue("/root/MainData/opmcptbsm/age_sex");

    var ctrlID = 1;

    if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") { //이미지 조회인경우
        pFileWriteFlag = "Y";
        fso = new ActiveXObject("Scripting.FileSystemObject");
        sFilePath = g_EMRImageTempPath+"\\";
    }
    if (sTextImage == "T") {
        emr_Right.removeNodeset("/root/SubData/viewinfo");
        emr_Right.gridRebuild("grid_view");
        emr_Right_fg = emr_Right.vsGrid("grid_view");
        //첫번째 Data display 세팅.
        //emr_Right.javascript.fInsertNurseData("Data","","", 0, null);
    }

    //조회된 간호 인증 정보를 불러와 grid일 경우 insert 한다.
    for (var i = 1; i <= iNodeCnt; i++) {
        for (var k = iCurPos; k > 0; k--) {
            if (model.getValue("/root/MainData/mrrfflshh["+k+"]/levl")== "3") {
                sPtchchrtnm = model.getValue("/root/MainData/mrrfflshh["+k+"]/chrtnm");
                //수술기록 관련된 차트코드를 확인하기 위한 차트코드 정보
                tempArr     = model.getValue("/root/MainData/mrrfflshh["+k+"]/seq").split("|");
                sPtchchrtcd = tempArr[3].replace("a_","");
                break;
            }
        }
        if (pFileWriteFlag == "Y") {
            MyFile = fso.CreateTextFile(sFilePath+"NURSE_"+i+".txt",true);
            MyFile.WriteLine("[<Font Size=20, decorate=underline, ForeColor=000000, weight=bold>]");

            MyFile.Write(sPtchchrtnm);
            MyFile.WriteLine();
            MyFile.WriteLine("[<Font Size=10, decorate=, ForeColor=000000, weight=bold>]"); //[<Paragraph LineSpace=130>] 제외함.

            MyFile.WriteLine();
            MyFile.WriteLine();
        }

        /*
          == 간호기록 관련 == 2007-09-11 6:16오후 김태범 - 이태진님 요청사항
          * 환자기본 정보가 존재하지 않을 경우 만들어 준다. *
          * 해당 서식지 정보
          - 수술전처치상태확인 기록지   INC0000001
          - 수술간호기록                CNS0000001
          - 회복실 간호기록             CNC0000002
          - 안정실 기록                 CNS0000003
         == == == == == == ==
        */
        var sPatientInfoXpath = "SignData/PatientInfo";
        var sPatientInfo = model.getValue(xPath+"["+i+"]/"+sPatientInfoXpath+"/HJID");

        if (sPtchchrtcd == "INC0000001" || sPtchchrtcd == "CNS0000001" || sPtchchrtcd == "CNC0000002" || sPtchchrtcd == "CNS0000003") {
            if (sPatientInfo == "") {
                //환자정보 없음. 생성로직 진행
                if (pFileWriteFlag == "Y") {
                    MyFile.Write("["+""+"환자정보"+""+"]");
                    MyFile.WriteLine();
                    MyFile.Write("  "+""+"환자아이디"+""+"> :"+ sPidInfo  );
                    MyFile.WriteLine();
                    MyFile.Write("  "+""+"환자이름"+""+">: "  + sKornmInfo);
                    MyFile.WriteLine();
                    MyFile.Write("  "+""+"환자성별"+""+">: "  + sSexAgeInfo);
                    MyFile.WriteLine();
                } else {
                    emr_Right.javascript.fInsertNurseData("["+""+"환자정보"+""+"]"               , 1, "T" , emr_Right_fg.Rows, null);
                    emr_Right.javascript.fInsertNurseData("  "+""+"환자아이디"+""+"> :"+sPidInfo , 2, "T" , emr_Right_fg.Rows, null);
                    emr_Right.javascript.fInsertNurseData("  "+""+"환자이름"+""+">: "+sKornmInfo , 2, "T" , emr_Right_fg.Rows, null);
                    emr_Right.javascript.fInsertNurseData("  "+""+"환자성별"+""+">: "+sSexAgeInfo, 2, "T" , emr_Right_fg.Rows, null);
                }
            }
        }

        var strXML = model.Instance;
        var Node = strXML.selectSingleNode(xPath+"["+i+"]");
        var rootTemp = Node;
        var temp     = Node.firstChild;
        var sTempDepth;
        var sBeFlag = "";
        var sAfFlag = "";
        var iInsertCnt = 1;

        //temp, 2009.08.18, OBH
        if (sTextImage == "T")
        //if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr")
        {
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            var pGrid = emr_Right.vsGrid("grid_view");
        }
        //var sTempString = "";
        //sTempString = temp.text;

        //model.alert("i := " + i);
        //model.alert("k := " + k);
        //model.alert("iRowCount := " + iRowCount);
        //model.alert("sTempString := " + sTempString);

        //fSetGridValue_T(iRowCount, 2, sTempString, "T", "", "", pGrid, null, null, null);

        //iRowCount = iRowCount +1;

        while ((temp != rootTemp)) {

            sBeFlag = "";//초기화
            sAfFlag = "";
            if (temp.nodeName == "#text") {
                if (temp == null) {
                    break;
                }
                var iDepth = fReturnSignNodeLevl(temp,"ganho");
                if (iDepth == 1) {
                    sBeFlag = " ";
                    sAfFlag = "";
                } else if (iDepth == 2) {
                    sBeFlag = "  ";
                    sAfFlag = "";
                } else if (iDepth == 3) {
                    sBeFlag = ":";
                    sAfFlag = "";
                } else if (iDepth == 4) {
                    sBeFlag = ":";
                    sAfFlag = "";
                } else {
                    sBeFlag = ":";
                    sAfFlag = "";
                }

                var iTempDepth = fReturnSignNodeLevl(temp.parentNode,"ganho");
                if (pFileWriteFlag == "Y") {
                    var sTempSpace = "";
                    if (iTempDepth == 1) {
                        sTempSpace = " ";
                    } else if (iTempDepth == 2) {
                        sTempSpace = "  ";
                    } else if (iTempDepth == 3) {
                        sTempSpace = "   ";
                    } else if (iTempDepth == 4) {
                        sTempSpace = "    ";
                    } else {
                        sTempSpace = "     ";
                    }
                    //textdata 중 \n이 있을 경우 \n + tempSpace로 바꿔준다.
                    if (temp.text != null && temp.text != "") {
                        var sTextData = temp.text.lvReplaceWord("\n","\n"+sTempSpace);
                        MyFile.Write(sBeFlag+sTextData);
                        MyFile.WriteLine();
                    }
                } else {
                    //node를 만들어서 setValue 하면됨.
                    if (sTextImage == "T") {
                        sBeforeData = emr_Right_fg.textMatrix(emr_Right_fg.Rows-1, 0);
                        emr_Right.javascript.fInsertNurseData(sBeforeData + sBeFlag+temp.text, iTempDepth, "T", emr_Right_fg.Rows , emr_Right_fg.Rows);
                        sBeforeData = "";
                        iInsertCnt++;
                    }
                }

            } else {
                //2007-10-05 9:04오전 김태범 간호 기록용 이미지 관련 추가
                if (temp.nodeName == "content") {
                    //MyFile.Write(sBeFlag+""+temp.getAttributeNode("content").value+""+sAfFlag);
                    //model.alert(temp.text);
                    //model.alert("FFFFFFFFFFFFFFFFFFFFFFFFFFFF");

                    if (sTextImage == "T") {
                        fSetGridValue_T(iRowCount, 2, temp.text, "T", "", "", pGrid, null, null, null);
                        //temp = fGetNode(temp, rootTemp);
                        //if (pFileWriteFlag == "Y") {
                        //    MyFile.WriteLine();
                        //    MyFile.Close();
                        //}
                        //emr_Right.javascript.fInsertAfterNurseData();
                        break;
                    }
                } else if (temp.nodeName == "#cdata-section") {
                } else if (temp.nodeName.substring(0, 5) == "image" && temp.nodeName != "images") {//image tag를 만나면.. 다른 루틴을 탄다..
                    var imgChList = temp.childNodes;
                    var orgImg = "";
                    var xvgData = "";
                    var m_fhspace     = "   ";
                    for (var tmpIdx = 0; tmpIdx < imgChList.length; tmpIdx++) {
                        if (imgChList.item(tmpIdx).nodeName == "path") {
                            orgImg = imgChList.item(tmpIdx).nodeTypedValue;
                        } else if (imgChList.item(tmpIdx).nodeName == "exvg") {
                            xvgData = imgChList.item(tmpIdx).nodeTypedValue;
                        }
                    }

                    sTempString ="";
                    sTempString ="<object id=\"imgCtrl" + ctrlID +"\" classid=\"CLSID:72e5d9ed-0d6a-46e8-aead-23144bfef878\" width=\"240px\" height=\"150px\">\n"+
                                 "<PARAM name=\"Location\" value=\"" + model.getURI() + "\"/>"+
                                 "<PARAM name=\"ImgPath\" value=\"" + orgImg + "\"/>"+
                                 "<PARAM name=\"ShapeData\" value=\"" + xvgData + "\"/>"+
                                 "</object>";

                    if (pFileWriteFlag == "Y") {
                        //이미지일 경우 로컬에서 File을 Make한다.
                        fMakeNUR1_OCRView_Image("EMR", orgImg, xvgData, "EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg");
                        MyFile.WriteLine();
                        //MyFile.Write(sTempString);
                        MyFile.Write("[<image path="+g_EMRImageTempPath+"\\EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg, size=200>]");
                        MyFile.WriteLine();
                        iGanHoImgSeqNo++;
                    } else {
                        if (sTextImage == "T") {
                            emr_Right.javascript.fInsertNurseData("", 2, "I", emr_Right_fg.Rows , emr_Right_fg.Rows, orgImg, xvgData);
                        }
                    }
                    sTempString ="";
                } else if (temp.nodeName.substring(0, 2) == "FH") {
                    if (temp.nodeName == "FHimages") {
                        //필요없는 등록일시 정보 삭제함.
                        model.removeNodeSet("/root/HideData/savedata_nurse/SignData/SignInfo/famyillnhist/FHimages/FHRegdt");
                        var m_FHImgList   = temp.childNodes;
                        var m_XMDData     = "";
                        var m_Info        = "";
                        var m_CommentData = "";
                        var m_fhspace     =   "　"; //2007-02-05 5:40오후 공백대신 특수문자 추가. 김태범

                        for (var tmpIdx = 0; tmpIdx < m_FHImgList.length; tmpIdx++) {
                            if (m_FHImgList.item(tmpIdx).nodeName == "FHimage") {
                                m_XMDData = m_FHImgList.item(tmpIdx).nodeTypedValue;
                            }
                            if (m_FHImgList.item(tmpIdx).nodeName == "FHInfo") {
                                m_Info    = m_FHImgList.item(tmpIdx).nodeTypedValue;
                            }
                            if (m_FHImgList.item(tmpIdx).nodeName == "FHComment") {
                                m_CommentData = m_FHImgList.item(tmpIdx).nodeTypedValue;
                            }
                        }

                        if (pFileWriteFlag == "Y") {
                            MyFile.WriteLine();
                            MyFile.Write("     ※ 가계도 이미지 정보 ※     ");
                            MyFile.WriteLine();
                            fMakeNUR1_OCRView_Image("FamilyImage", orgImg, m_XMDData, "EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg");
                            MyFile.Write("[<image path="+g_EMRImageTempPath+"\\EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg, size=200>]");
                            MyFile.WriteLine();
                            iGanHoImgSeqNo++;
                        } else {
                            if (sTextImage == "T") {
                                emr_Right.javascript.fInsertNurseData(m_fhspace+"※ 가계도", 1, "T", emr_Right_fg.Rows);
                                //sTempString = "familyHistroyMaker"; // 가계도 공백 그리드
                                emr_Right.javascript.fInsertNurseData("", 3, "F", emr_Right_fg.Rows , null , null , m_XMDData);
                                emr_Right.javascript.fInsertNurseData("", 3, "T", emr_Right_fg.Rows);
                            }
                        }

                        sTempString = "";
                        if (m_Info != "") {
                            if (pFileWriteFlag == "Y") {
                                MyFile.Write(m_fhspace+"※ 가계도 부연정보");
                                MyFile.WriteLine();
                                MyFile.Write(m_fhspace + m_fhspace+m_Info);
                                MyFile.WriteLine();
                            } else {
                                if (sTextImage == "T") {
                                    emr_Right.javascript.fInsertNurseData(m_fhspace+"※ 가계도 부연정보", 1, "T", emr_Right_fg.Rows);
                                    emr_Right.javascript.fInsertNurseData(m_fhspace + m_fhspace+m_Info, 1, "T", emr_Right_fg.Rows);
                                }
                            }
                        }

                        //가계도 comment
                        if (m_CommentData != "") {
                            if (pFileWriteFlag == "Y") {
                                MyFile.Write(m_fhspace+"※ 가계도 Comment");
                                MyFile.WriteLine();
                                MyFile.Write(m_CommentData);
                                MyFile.WriteLine();
                            } else {
                                if (sTextImage == "T") {
                                    emr_Right.javascript.fInsertNurseData("※ 가계도 Comment", 1, "T", emr_Right_fg.Rows);
                                    emr_Right.javascript.fInsertNurseData(m_CommentData, 1, "T", emr_Right_fg.Rows);
                                }
                            }
                        }
                    }
                //가계도 관련 끝...
                } else {
                    var sAttributeNode = temp.getAttributeNode("name");
                    var iDepth = fReturnSignNodeLevl(temp, "ganho");
                    if (iDepth == 1) {
                        sBeFlag = "[";
                        sAfFlag = "]";
                    } else if (iDepth == 2) {
                        sBeFlag = "  ";
                        sAfFlag = ">";
                    } else if (iDepth == 3) {
                        sBeFlag = "   ";
                        sAfFlag = " ";
                    } else if (iDepth == 4) {
                        sBeFlag = "    ";
                        sAfFlag = " ";
                    } else {
                        sBeFlag = "     ";
                        sAfFlag = " ";
                    }

                    //name이 존재 할 경우
                    if (sAttributeNode != null) {
                        if (pFileWriteFlag == "Y") {
                            MyFile.Write(sBeFlag+""+temp.getAttributeNode("name").value+""+sAfFlag);
                            if (iDepth == 1 || iDepth == 2) {
                                //해당 노드의 부모의 부모 노드가 환자정보일 경우에는 붙여 쓴다.
                                if (temp.parentNode.nodeName != "PatientInfo" && temp.parentNode.nodeName != "SessionInfo") {
                                    MyFile.WriteLine();
                                }
                            }
                        } else {
                            if (sTextImage == "T") {
                                emr_Right.javascript.fInsertNurseData(sBeFlag+""+temp.getAttributeNode("name").value+""+sAfFlag, iDepth, "T" , emr_Right_fg.Rows, null);
                            }
                            //node를 만들어서 setValue 하면됨.
                        }
                    }
                }
            }
            temp = fGetNode(temp, rootTemp);
            if (temp == rootTemp) {}
            if (temp == null) {
                break;
            }
        }
        if (pFileWriteFlag == "Y") {
            MyFile.WriteLine();
            MyFile.Close();
        }

        if (sTextImage == "T") {
            emr_Right.javascript.fInsertAfterNurseData();
        }
    }
}

/**
 * @group  :
 * @ver    : 2008-04-22 3:06오후
 * @by     : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : XML구조를 Grid형태로 변환
 * @param  : 01. pNo - 추가 행번호
 * @param  : 02. pPath - 레벨Path
 * @param  : 03. pData - 데이터
 * @param  : 04. pType - 이미지/텍스트
 * @param  : 05. pImgPath  - 이미지경로
 * @param  : 06. pImgExvg  - 이비지편집
 * @param  : 07. pGrid     - 그리드 아이디
 * @param  : 08. pNextMode - 아래로 보내기 구현(그리드 다음줄 판단)
 * @param  : 09. pSpaceTab - 처음 문구를 출력할때 SpaceTab (없으면 미적용)
 * @param  : 10. pCodeInfo - 항목정보코드를 표현함
 * @return :
 * @---------------------------------------------------
 */
var g_OldMode_T = "";
var g_GIRE       = new RegExp("[▩]",["gi"])
function fSetGridValue_T(pNo, pPath, pData, pType, pImgPath, pImgExvg, pGrid, pNextMode, pSpaceTab, pCodeInfo) {
    if (pData == ""||pData == "G999") {
        return;
    }

    if (pType == "T") {
        if (pData.indexOf("exvg") != -1 || pData.indexOf("Xvg") != -1) {
            return;
        }
    }

    if (pPath == 1) {
        pData = pData + ">";
    }

    var m_TabSpace = "";
    if (pSpaceTab != null) {
        m_TabSpace = pSpaceTab;
    }

    var m_CodeInfo = "";
    if (pCodeInfo != null) {F
        m_CodeInfo = pCodeInfo;
    }

    //alert("pData + " + pData);
    //var re = new RegExp("[▩]",["gi"])
    pData  = pData.replace(g_GIRE, "\n   ");
    //alert("FpData-e + " + pData);

    if (pType == "T") {
        var m_TempData = pData.split("\n");
        var m_Temp = "";
        //model.alert(m_TempData);
        /**
        -- Line 갯수 변화 : LinePrintln : Cell에 라인갯수를 조정하여 표현하고자 할때 사용로직

        var m_LineCnt = 1; // Line을 몇개로 표현할 지를 결정
        for (var iCnt = 1; iCnt <= m_TempData.length; iCnt++) {
            if ((iCnt % m_LineCnt) != 0) {
                if (iCnt != m_TempData.length) {
                    m_Temp += m_TempData[iCnt-1] + "\n";
                } else {
                    m_Temp += m_TempData[iCnt-1];
                }
            } else {
                m_Temp += m_TempData[iCnt-1];
                pGrid.AddItem(m_Temp + g_Separators + pPath + g_Separators + g_Separators + g_Separators + pType);
                m_Temp = "";
            }
        }

        // m_Temp 변수 데이터에 값이 쌓여 있으면 찍어라...
        if (m_Temp != "") {
            pGrid.AddItem(m_Temp + g_Separators + pPath + g_Separators + g_Separators + g_Separators + pType);
            m_Temp = "";
        }
        */

        if (g_OldMode_T == "") {
            g_OldMode_T = pNextMode;
        }

        // Line을 연이어 표현함
        //alert("fSetGridValue : " + pGrid.Rows + " || " + g_OldMode + " || " + pNextMode + " || " + m_TempData);
        if (g_OldMode_T == "N") {
            for (var iCnt = 0; iCnt < m_TempData.length; iCnt++) {
                var m_Temp = pGrid.TextMatrix(pGrid.Rows - pGrid.FixedRows, 0);
                pGrid.TextMatrix(pGrid.Rows - pGrid.FixedRows, 0) = m_Temp + m_TempData[iCnt];
            }
        } else {
            // 단순히 한줄로만 표현하고자 한다면, 위로직을 쓰지 않고 아래처럼 간단히 구현함
            for (var iCnt = 0; iCnt < m_TempData.length; iCnt++) {
                /*
                model.alert("m_TabSpace " + m_TabSpace);
                model.alert("m_TempData[iCnt] " + m_TempData[iCnt]);
                model.alert("g_Separators " + g_Separators);
                model.alert("pPath " + pPath);
                model.alert("g_Separators " + g_Separators);
                model.alert("g_Separators " + g_Separators);
                model.alert("g_Separators " + g_Separators);
                model.alert("pType " + pType);
                model.alert("g_Separators " + g_Separators);
                model.alert("g_Separators " + g_Separators);
                model.alert("m_CodeInfo " + m_CodeInfo);
                */
                //pGrid.AddItem(m_TabSpace + m_TempData[iCnt] + g_Separators + pPath + g_Separators + g_Separators + g_Separators + pType + g_Separators + g_Separators + m_CodeInfo);
                pGrid.AddItem(m_TabSpace + m_TempData[iCnt] + m_CodeInfo);
            }
        }
    } else if (pType == "I") {
        pGrid.AddItem(pData + g_Separators + pPath + g_Separators + pImgPath + g_Separators + pImgExvg  + g_Separators + pType + g_Separators + g_Separators + m_CodeInfo);
    } else if (pType == "F") {
        pGrid.AddItem(pData + g_Separators + pPath + g_Separators + pImgPath + g_Separators + pImgExvg  + g_Separators + pType + g_Separators + g_Separators + m_CodeInfo);
    }

    iRowCount = iRowCount + 1;

    // Grobal Value Setting
    g_OldMode_T = pNextMode;
}

/**
 * @group  :
 * @ver    : 2005.01.17
 * @by     : 김건국
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : Nurse상세조회(간호기록 정보조사지)
 * @param  :
 * @param  :
 * @return :
 * @이  력 :
 * @        김태범 2007-01-25 2:46오후 간호 EMR기록 3레벨 조회기능 추가
 * @---------------------------------------------------
 */
function fCall_NUR5(pRow, pNode) {
    var sChrtkind = model.getValue(pNode + pRow + "]/chrtkind");
    var sParm = sChrtkind.lvReplaceWord("|", "▦");
    var sNur = TFGetMatrixData(sParm, 0, 0); //Nurse구분
    var sIofg = TFGetMatrixData(sParm, 0, 1);
    var sPtdt = TFGetMatrixData(sParm, 0, 2);
    var sChnm = TFGetMatrixData(sParm, 0, 3);
    var sChrt = TFGetMatrixData(sParm, 0, 4);
    var sDate = TFGetMatrixData(sParm, 0, 5);
    var sTime = TFGetMatrixData(sParm, 0, 6);
    var sChrtNm = TFGetMatrixData(sParm, 0, 7);
    var sPkiDateTime = sDate + sTime + "%";

    model.removeNodeset("/root/HideData/savedata_nurse");
    TFclearNodeValue("/root/SendData"); //이전의 서버 호출에 사용된 정보 지움
    model.setValue("/root/SendData/Mode", "MRR_reqGetMrnipkidh"); //Action Method
    model.setValue("/root/SendData/Data1", "NUR1"); //간호조회 구분
    model.setValue("/root/SendData/Data2", model.getValue("/root/MainData/condition/cond1")); //등록번호

    var sLevl = model.getValue(pNode + pRow + "]/levl");
    var sTempLevl;
    var sTempPkiDateTime = "";
    if (sLevl == "3") {

        sPkiDateTime = "";
        for (var i = pRow+1; i <= TFGetGridCount("grid_main"); i++) {
			      //---- 2010.12 PSY 기록검색권한 제한처리
            var psychk = model.getValue("/root/MainData/mrrfflshh[" + i + "]/chrtkind");
			var arr_psychk = psychk.split("|");
            sTempLevl = model.getValue(pNode + i + "]/levl");
            if (sTempLevl == "4") {
				if(sMRR_GRGeomSaek_ChrtPSY == "Y" || (sMRR_GRGeomSaek_ChrtPSY != "Y" && arr_psychk[8] != "N")){
					if(sMRR_GRGeomSaek_ChrtPSY == "S" && arr_psychk[8] == "S"){
						sTempPkiDateTime = "";
					} else {
					    sParm = model.getValue(pNode + i + "]/chrtkind").lvReplaceWord("|", "▦");
					    sDate = TFGetMatrixData(sParm, 0, 5);
					    sTime = TFGetMatrixData(sParm, 0, 6);
					    sPkiDateTime = sDate + sTime + "%";
					    if (sTempPkiDateTime != "" && sTempPkiDateTime != null) {
							sTempPkiDateTime = sTempPkiDateTime +","+ sPkiDateTime
						} else {
							sTempPkiDateTime = sPkiDateTime
						}
					}
				}
            } else {
                break;
            }
        }
        model.setValue("/root/SendData/Data3", sTempPkiDateTime); //인증 data 찾아오기 위한 argument //추가(20050725)
        model.setValue("/root/SendData/Data6", "levl3"); //인증 data 찾아오기 위한 argument //추가(20050725)
    } else {
        model.setValue("/root/SendData/Data3", sPkiDateTime); //인증 data 찾아오기 위한 argument //추가(20050725)
    }

    if (model.submitInstance("MRR_reqGetMrnipkidh")) {
        TFSetMessage("root/HideData/savedata_nurse");
        //인증데이터 환자정보의 주치의, 세션정보의 id, 부서 정보 제외. - 2007-03-20 1:24오후 김태범
        // 인증데이터 로그인 시간 제외 추가 - 2007-05-08 4:31오후 김태범
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/PatientInfo/MainDoctor");
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo/userid");
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo/empldeptgmnm");
        model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo/logondatetime");

        /*
            2007-12-13 5:04오후 : 현대정보기술 의료기술팀, 꿈꾸는 프로그래머 이제관 (je2kwan2@naver.com)
            중환자실 요구사항, 중환자실 간호기록(INS0000009) 의 경우 세션정보 안나오도록 처리함
            사유 : 이 기록은 근무 별로(D, E, N) 작성을 하는데 세션정보에는 한분만 보인다고
                   나중에 법정에 이 기록을 출력을하여 갔을 때 오해의 소지존재
        */
        if (sChrt == "INS0000009") {
            model.removeNodeset("/root/HideData/savedata_nurse/SignData/SessionInfo");
        }
    } else {
        return false;
    }

    var xPath = "/root/HideData/savedata_nurse";

    TFRemoveNoChild(findNode(xPath));
    //var sText = fCommon_getInstanceStringSignValue(xPath); //현재는 한건만 가져오도록 함
    var sTextImage = model.getValue("/root/HideData/condition/cond6"); //Text, Image 조회구분
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");

    var iNodeCnt = getNodesetCnt(xPath);
    var iCurPos = TFGetGridPos("grid_main");

    if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") { //이미지 조회인경우
        //2007-03-07 1:30오후 간호기록 출력 print와 같은 패턴으로 변경함.
        fDeleteFile("NURSE_");
        sMRR_GRGeomSaek_FileCnt = 0;
        model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_NUR1");
        model.setAttribute("MRR_GRGeomSaek_Ocr.sMRR_GRGeomSaek_FileCnt", 1);
        if (fGetIP_xfm() != "MRR_GRGeomSaek_Ocr") {
            //2007-03-07 1:30오후 김태범 간호기록 출력 print와 같은 패턴으로 변경함. MRR_GRGeomSaek_Ocr 화면에서 콜하도록 수정함.
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param1", xPath);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param2", sMRR_GRGeomSaek_ImageView);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param3", sTextImage);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param4", iNodeCnt);
            model.setAttribute("MRR_GRGeomSaek_Ocr.NUR1_NEW_param5", iCurPos);

            model.setAttribute("MRR_GRGeomSaek_Ocr.function", "fCall_NUR1_NEW");
            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Ocr.xfm","replace");
        } else {
            //2007-03-07 1:30오후 간호기록 출력 print와 같은 패턴으로 변경함.
            fMakeNUR4(xPath, sMRR_GRGeomSaek_ImageView, sTextImage, iNodeCnt, iCurPos);

            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            if (emr_Right) {
                sTotalKind = sParm + "▩";
                /*
                  == 간호기록 관련 == 2007-09-11 6:16오후 김태범 - 이태진님 요청사항
                  * 해당 서식지일 경우 발생일자 대신 기록지별 특정일자를 보여준다.
                  * 해당 서식지 정보
                  - 수술전처치상태확인 기록지   INC0000001
                  - 수술간호기록                CNS0000001
                  - 회복실 간호기록             CNC0000002
                  - 안정실 기록                 CNS0000003
                 == == == == == == ==
                */
                var sTempArrInfo = sTotalKind.split("▩");
                var sTempArrInfoDetail;
                var sInsertTempData = "";
                var sTempTotalkind = "";

                for (var itempArrSeq = 0; itempArrSeq < sTempArrInfo.length ; itempArrSeq++) {
                    sTempArrInfoDetail = sTempArrInfo[itempArrSeq].split("▦");
                    var sTempChrtcdInfo = sTempArrInfoDetail[4];
                    if (sTempChrtcdInfo == "INC0000001" || sTempChrtcdInfo == "CNS0000001" || sTempChrtcdInfo == "CNC0000002" || sTempChrtcdInfo == "CNS0000003") {
                        sInsertTempData = sTempArrInfoDetail[0] + "▦" +        //해당 선택된 grid 간호 기록의 chrtkind
                                          sTempArrInfoDetail[1] + "▦" +
                                          model.getValue(pNode + pRow + "]/chrtnm").lvReplaceWord("/","").substr(0,8) + "▦" +      // title date (OCR기록 상단날짜 정보)
                                          sTempArrInfoDetail[3] + "▦" +      //
                                          sTempArrInfoDetail[4] + "▦" +
                                          sTempArrInfoDetail[5] + "▦" +      //

                                          sTempArrInfoDetail[6] + "▦" +
                                          sTempArrInfoDetail[7] + "▦" + "▩";
                        sTempTotalkind = sTempTotalkind+sInsertTempData;
                    } else {
                        if (sTempChrtcdInfo != null && sTempChrtcdInfo != "") {
                            sInsertTempData = sTempArrInfoDetail[0] + "▦" +        //해당 선택된 grid 간호 기록의 chrtkind
                                              sTempArrInfoDetail[1] + "▦" +
                                              sTempArrInfoDetail[2] + "▦" +
                                              sTempArrInfoDetail[3] + "▦" +
                                              sTempArrInfoDetail[4] + "▦" +
                                              sTempArrInfoDetail[5] + "▦" +
                                              sTempArrInfoDetail[6] + "▦" +
                                              sTempArrInfoDetail[7] + "▦" + "▩";
                            sTempTotalkind = sTempTotalkind+sInsertTempData;
                        }
                    }
                }
                sTotalKind = sTempTotalkind;
                emr_Right.javascript.fViewerSetting(sTotalKind, "NURSE_");
                fWaitBegin("이미지 Loading중...");
                emr_Right.javascript.downloadChart(); //
                fWaitEnd();
                emr_Right.javascript.setupChart("ChartView"); //연속보기로 설정
            } else {
                return;
            }
        }
    } else if (sTextImage == "P") {
        //XFR로 출력할 경우..
        //fMakeImage_XFR_Print("EMR_GanHo"); 시점차이로 인한 문제 해결을 위해 주석처리함.
        model.setAttribute("MRR_GRGeomSaek_ReportView.function", "fMakeImage_XFR_Print");
        model.setAttribute("MRR_GRGeomSaek_ReportView.function_param", "EMR_GanHo");
        TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_ReportView.xfm","replace");
    } else {
         if (fGetIP_xfm() != "MRR_GRGeomSaek_Grid") {
            model.setAttribute("MRR_GRGeomSaek_Grid.function" , "fCall_NUR1_NEW");
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param1", xPath);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param2", sMRR_GRGeomSaek_ImageView);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param3", sTextImage);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param4", iNodeCnt);
            model.setAttribute("MRR_GRGeomSaek_Grid.NUR1_NEW_param5", iCurPos);

            TF_LoadURI("body_girokgeomsaek_Right","/emr/girokweb/xfm/MRR_GRGeomSaek_Grid.xfm","replace");
         } else {
            if (emr_Right) {
                fMakeNUR5(xPath, sMRR_GRGeomSaek_ImageView, sTextImage, iNodeCnt, iCurPos);
            } else {
                return;
            }
        }
    }
}

/**
 * @group  :
 * @ver    : 2009.08.31
 * @by     : 오봉훈
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 중환자실 간호기록 Text로 저장
 * @param  : xPath - signData의 xPath
 * @param  : sMRR_GRGeomSaek_ImageView - Y
 * @param  : sTextImage - T/I/P
 * @return :
 * @---------------------------------------------------
 */
function fMakeNUR5(xPath, sMRR_GRGeomSaek_ImageView, sTextImage, iNodeCnt, iCurPos) {
    var pFileWriteFlag = "";
    var MyFile;
    var fso;
    var sFilePath;
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    var emr_Right_fg;
    var sBeforeData;
    var sPtchchrtnm;
    var tempArr;
    var sPtchchrtcd;
    var iGanHoImgSeqNo = 1;
    var sTempString = "";

    var sPidInfo    = model.getValue("/root/MainData/condition/cond1");
    model.setAttribute("sMRR_GiRokGeomSaek_Param.fMakeNUR1.pid", sPidInfo);
    var sKornmInfo  = model.getValue("/root/MainData/opmcptbsm/name");
    var sSexAgeInfo = model.getValue("/root/MainData/opmcptbsm/age_sex");

    var ctrlID = 1;

    if (sMRR_GRGeomSaek_ImageView == "Y" && sTextImage != "T" && sTextImage != "P") { //이미지 조회인경우
        pFileWriteFlag = "Y";
        fso = new ActiveXObject("Scripting.FileSystemObject");
        sFilePath = g_EMRImageTempPath+"\\";
    }
    if (sTextImage == "T") {
        emr_Right.removeNodeset("/root/SubData/viewinfo");
        emr_Right.gridRebuild("grid_view");
        emr_Right_fg = emr_Right.vsGrid("grid_view");
        //첫번째 Data display 세팅.
        //emr_Right.javascript.fInsertNurseData("Data","","", 0, null);
    }

    //조회된 간호 인증 정보를 불러와 grid일 경우 insert 한다.
    for (var i = 1; i <= iNodeCnt; i++) {
        for (var k = iCurPos; k > 0; k--) {
            if (model.getValue("/root/MainData/mrrfflshh["+k+"]/levl")== "3") {
                sPtchchrtnm = model.getValue("/root/MainData/mrrfflshh["+k+"]/chrtnm");
                //수술기록 관련된 차트코드를 확인하기 위한 차트코드 정보
                tempArr     = model.getValue("/root/MainData/mrrfflshh["+k+"]/seq").split("|");
                sPtchchrtcd = tempArr[3].replace("a_","");
                break;
            }
        }
        if (pFileWriteFlag == "Y") {
            MyFile = fso.CreateTextFile(sFilePath+"NURSE_"+i+".txt",true);
            MyFile.WriteLine("[<Font Size=20, decorate=underline, ForeColor=000000, weight=bold>]");

            MyFile.Write(sPtchchrtnm);
            MyFile.WriteLine();
            MyFile.WriteLine("[<Font Size=10, decorate=, ForeColor=000000, weight=bold>]"); //[<Paragraph LineSpace=130>] 제외함.

            MyFile.WriteLine();
            MyFile.WriteLine();
        }

        /*
          == 간호기록 관련 == 2007-09-11 6:16오후 김태범 - 이태진님 요청사항
          * 환자기본 정보가 존재하지 않을 경우 만들어 준다. *
          * 해당 서식지 정보
          - 수술전처치상태확인 기록지   INC0000001
          - 수술간호기록                CNS0000001
          - 회복실 간호기록             CNC0000002
          - 안정실 기록                 CNS0000003
         == == == == == == ==
        */
        var sPatientInfoXpath = "SignData/PatientInfo";
        var sPatientInfo = model.getValue(xPath+"["+i+"]/"+sPatientInfoXpath+"/HJID");

        if (sPtchchrtcd == "INC0000001" || sPtchchrtcd == "CNS0000001" || sPtchchrtcd == "CNC0000002" || sPtchchrtcd == "CNS0000003") {
            if (sPatientInfo == "") {
                //환자정보 없음. 생성로직 진행
                if (pFileWriteFlag == "Y") {
                    MyFile.Write("["+""+"환자정보"+""+"]");
                    MyFile.WriteLine();
                    MyFile.Write("  "+""+"환자아이디"+""+"> :"+ sPidInfo  );
                    MyFile.WriteLine();
                    MyFile.Write("  "+""+"환자이름"+""+">: "  + sKornmInfo);
                    MyFile.WriteLine();
                    MyFile.Write("  "+""+"환자성별"+""+">: "  + sSexAgeInfo);
                    MyFile.WriteLine();
                } else {
                    emr_Right.javascript.fInsertNurseData("["+""+"환자정보"+""+"]"               , 1, "T" , emr_Right_fg.Rows, null);
                    emr_Right.javascript.fInsertNurseData("  "+""+"환자아이디"+""+"> :"+sPidInfo , 2, "T" , emr_Right_fg.Rows, null);
                    emr_Right.javascript.fInsertNurseData("  "+""+"환자이름"+""+">: "+sKornmInfo , 2, "T" , emr_Right_fg.Rows, null);
                    emr_Right.javascript.fInsertNurseData("  "+""+"환자성별"+""+">: "+sSexAgeInfo, 2, "T" , emr_Right_fg.Rows, null);
                }
            }
        }

        var strXML = model.Instance;
        var Node = strXML.selectSingleNode(xPath+"["+i+"]");
        var rootTemp = Node;
        var temp     = Node.firstChild;
        var sTempDepth;
        var sBeFlag = "";
        var sAfFlag = "";
        var iInsertCnt = 1;

        //temp, 2009.08.18, OBH
        if (sTextImage == "T") {
            var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
            var pGrid = emr_Right.vsGrid("grid_view");
            //var sCurData = "";
        }
        var textData = "";

        while ((temp != rootTemp)) {
            //sCurData = emr_Right_fg.textMatrix(emr_Right_fg.Rows-1, 0);
            sBeFlag = "";//초기화
            sAfFlag = "";
            if (temp.nodeName == "content") {
                if (sTextImage == "T") {
                    fSetGridValue_T(iRowCount, 2, temp.text, "T", "", "", pGrid, null, null, null);
                }
            } else if (temp.nodeName == "#text") {
                if (temp == null) {
                    break;
                }

                var iDepth = fReturnSignNodeLevl(temp,"ganho");
                if (iDepth == 1) {
                    sBeFlag = " ";
                    sAfFlag = "";
                } else if (iDepth == 2) {
                    sBeFlag = "  ";
                    sAfFlag = "";
                } else if (iDepth == 3) {
                    sBeFlag = ":";
                    sAfFlag = "";
                } else if (iDepth == 4) {
                    sBeFlag = ":";
                    sAfFlag = "";
                } else {
                    sBeFlag = ":";
                    sAfFlag = "";
                }

                var iTempDepth = fReturnSignNodeLevl(temp.parentNode,"ganho");
                if (pFileWriteFlag == "Y") {

                    var sTempSpace = "";
                    if (iTempDepth == 1) {
                        sTempSpace = " ";
                    } else if (iTempDepth == 2) {
                        sTempSpace = "  ";
                    } else if (iTempDepth == 3) {
                        sTempSpace = "   ";
                    } else if (iTempDepth == 4) {
                        sTempSpace = "    ";
                    } else {
                        sTempSpace = "     ";
                    }
                    //textdata 중 \n이 있을 경우 \n + tempSpace로 바꿔준다.
                    if (temp.text != null && temp.text != "") {
                        var sTextData = temp.text.lvReplaceWord("\n","\n"+sTempSpace);
                        MyFile.Write(sBeFlag+sTextData);
                        MyFile.WriteLine();
                    }
                } else {
                    //node를 만들어서 setValue 하면됨.
                    if (sTextImage == "T") {
                        sBeforeData = emr_Right_fg.textMatrix(emr_Right_fg.Rows-1, 0);
                        //emr_Right.javascript.fInsertNurseData(sBeforeData + sBeFlag+temp.text, iTempDepth, "T", emr_Right_fg.Rows , emr_Right_fg.Rows);
                        //model.alert("sCurData :=" + sCurData);
                        //model.alert("sCurData :=" + sCurData.substring(5,11));
                        //model.alert("temp.text0 :=" + textData);
                        //model.alert("temp.text1 :=" + textData.substring(2,3));
                        textData = temp.text;
                        if (sBeforeData.substring(5,11)=="수행일자 :") {
                        } else if (textData.substring(2,3)==":" || textData.substring(2,3)==";") {  // temp, 2009.09.09
                        } else {
                            emr_Right.javascript.fInsertNurseData(sBeforeData + sBeFlag+temp.text, iTempDepth, "T", emr_Right_fg.Rows , emr_Right_fg.Rows);
                        }
                        sBeforeData = "";
                        iInsertCnt++;
                    }
                }
            } else {
                if (temp.nodeName == "#cdata-section") {
                } else if (temp.nodeName.substring(0, 5) == "image" && temp.nodeName != "images") {//image tag를 만나면.. 다른 루틴을 탄다..
                    var imgChList = temp.childNodes;
                    var orgImg = "";
                    var xvgData = "";
                    var m_fhspace     = "   ";
                    for (var tmpIdx = 0; tmpIdx < imgChList.length; tmpIdx++) {
                        if (imgChList.item(tmpIdx).nodeName == "path") {
                            orgImg = imgChList.item(tmpIdx).nodeTypedValue;
                        } else if (imgChList.item(tmpIdx).nodeName == "exvg") {
                            xvgData = imgChList.item(tmpIdx).nodeTypedValue;
                        }
                    }

                    sTempString ="<object id=\"imgCtrl" + ctrlID +"\" classid=\"CLSID:72e5d9ed-0d6a-46e8-aead-23144bfef878\" width=\"240px\" height=\"150px\">\n"+
                                 "<PARAM name=\"Location\" value=\"" + model.getURI() + "\"/>"+
                                 "<PARAM name=\"ImgPath\" value=\"" + orgImg + "\"/>"+
                                 "<PARAM name=\"ShapeData\" value=\"" + xvgData + "\"/>"+
                                 "</object>";

                    if (pFileWriteFlag == "Y") {
                        //이미지일 경우 로컬에서 File을 Make한다.
                        fMakeNUR1_OCRView_Image("EMR", orgImg, xvgData, "EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg");
                        MyFile.WriteLine();
                        //MyFile.Write(sTempString);
                        MyFile.Write("[<image path="+g_EMRImageTempPath+"\\EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg, size=200>]");
                        MyFile.WriteLine();
                        iGanHoImgSeqNo++;
                    } else {
                        if (sTextImage == "T") {
                            emr_Right.javascript.fInsertNurseData("", 2, "I", emr_Right_fg.Rows , emr_Right_fg.Rows, orgImg, xvgData);
                        }
                    }
                    sTempString ="";
                } else if (temp.nodeName.substring(0, 2) == "FH") {
                    if (temp.nodeName == "FHimages") {
                        //필요없는 등록일시 정보 삭제함.
                        model.removeNodeSet("/root/HideData/savedata_nurse/SignData/SignInfo/famyillnhist/FHimages/FHRegdt");
                        var m_FHImgList   = temp.childNodes;
                        var m_XMDData     = "";
                        var m_Info        = "";
                        var m_CommentData = "";
                        var m_fhspace     =   "　"; //2007-02-05 5:40오후 공백대신 특수문자 추가. 김태범

                        for (var tmpIdx = 0; tmpIdx < m_FHImgList.length; tmpIdx++) {
                            if (m_FHImgList.item(tmpIdx).nodeName == "FHimage") {
                                m_XMDData = m_FHImgList.item(tmpIdx).nodeTypedValue;
                            }
                            if (m_FHImgList.item(tmpIdx).nodeName == "FHInfo") {
                                m_Info    = m_FHImgList.item(tmpIdx).nodeTypedValue;
                            }
                            if (m_FHImgList.item(tmpIdx).nodeName == "FHComment") {
                                m_CommentData = m_FHImgList.item(tmpIdx).nodeTypedValue;
                            }
                        }

                        if (pFileWriteFlag == "Y") {
                            MyFile.WriteLine();
                            MyFile.Write("     ※ 가계도 이미지 정보 ※     ");
                            MyFile.WriteLine();
                            fMakeNUR1_OCRView_Image("FamilyImage", orgImg, m_XMDData, "EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg");
                            MyFile.Write("[<image path="+g_EMRImageTempPath+"\\EMR_Nurse_OCR_"+sPidInfo+"_"+iGanHoImgSeqNo+".jpg, size=200>]");
                            MyFile.WriteLine();
                            iGanHoImgSeqNo++;
                        } else {
                            if (sTextImage == "T") {
                                emr_Right.javascript.fInsertNurseData(m_fhspace+"※ 가계도", 1, "T", emr_Right_fg.Rows);
                                //sTempString = "familyHistroyMaker"; // 가계도 공백 그리드
                                emr_Right.javascript.fInsertNurseData("", 3, "F", emr_Right_fg.Rows , null , null , m_XMDData);
                                emr_Right.javascript.fInsertNurseData("", 3, "T", emr_Right_fg.Rows);
                            }
                        }

                        sTempString = "";
                        if (m_Info != "") {
                            if (pFileWriteFlag == "Y") {
                                MyFile.Write(m_fhspace+"※ 가계도 부연정보");
                                MyFile.WriteLine();
                                MyFile.Write(m_fhspace + m_fhspace+m_Info);
                                MyFile.WriteLine();
                            } else {
                                if (sTextImage == "T") {
                                    emr_Right.javascript.fInsertNurseData(m_fhspace+"※ 가계도 부연정보", 1, "T", emr_Right_fg.Rows);
                                    emr_Right.javascript.fInsertNurseData(m_fhspace + m_fhspace+m_Info, 1, "T", emr_Right_fg.Rows);
                                }
                            }
                        }

                        //가계도 comment
                        if (m_CommentData != "") {
                            if (pFileWriteFlag == "Y") {
                                MyFile.Write(m_fhspace+"※ 가계도 Comment");
                                MyFile.WriteLine();
                                MyFile.Write(m_CommentData);
                                MyFile.WriteLine();
                            } else {
                                if (sTextImage == "T") {
                                    emr_Right.javascript.fInsertNurseData("※ 가계도 Comment", 1, "T", emr_Right_fg.Rows);
                                    emr_Right.javascript.fInsertNurseData(m_CommentData, 1, "T", emr_Right_fg.Rows);
                                }
                            }
                        }
                    }
                //가계도 관련 끝...
                } else {
                    var sAttributeNode = temp.getAttributeNode("name");
                    var iDepth = fReturnSignNodeLevl(temp, "ganho");
                    if (iDepth == 1) {
                        sBeFlag = "[";
                        sAfFlag = "]";
                    } else if (iDepth == 2) {
                        sBeFlag = "  ";
                        sAfFlag = ">";
                    } else if (iDepth == 3) {
                        sBeFlag = "   ";
                        sAfFlag = " ";
                    } else if (iDepth == 4) {
                        sBeFlag = "    ";
                        sAfFlag = " ";
                    } else {
                        sBeFlag = "     ";
                        sAfFlag = " ";
                    }

                    //name이 존재 할 경우
                    if (sAttributeNode != null) {
                        if (pFileWriteFlag == "Y") {
                            MyFile.Write(sBeFlag+""+temp.getAttributeNode("name").value+""+sAfFlag);
                            if (iDepth == 1 || iDepth == 2) {
                                //해당 노드의 부모의 부모 노드가 환자정보일 경우에는 붙여 쓴다.
                                if (temp.parentNode.nodeName != "PatientInfo" && temp.parentNode.nodeName != "SessionInfo") {
                                    MyFile.WriteLine();
                                }
                            }
                        } else {
                            if (sTextImage == "T") {
                                //model.alert(temp.getAttributeNode("name").value);
                                emr_Right.javascript.fInsertNurseData(sBeFlag+""+temp.getAttributeNode("name").value+""+sAfFlag, iDepth, "T" , emr_Right_fg.Rows, null);
                            }
                            //node를 만들어서 setValue 하면됨.
                        }
                    }
                }
            }
            temp = fGetNode(temp, rootTemp);
            if (temp == rootTemp) {}
            if (temp == null) {
                break;
            }
        }
        if (pFileWriteFlag == "Y") {
            MyFile.WriteLine();
            MyFile.Close();
        }

        if (sTextImage == "T") {
            emr_Right.javascript.fInsertAfterNurseData();
        }
    }
}

/**
 * @group  :
 * @ver    : 2009-10-16 오전 11:39
 * @by     : 김상준 (한양정보기술)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 기록이력 조회
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function f_GRHistoryView() {
    var m_xpath = "/root/HideData/savedatas/savedata/mrdrptchh";

    model.setAttribute("MRD_GRHistory.parents", "MRD_GRJoHoi");
    model.setAttribute("MRD_GRHistory.ptchptno", model.getValue(m_xpath + "/ptchptno"));
    model.setAttribute("MRD_GRHistory.ptchptdt", model.getValue(m_xpath + "/ptchptdt").lvStripWhite());
    model.setAttribute("MRD_GRHistory.ptchiofg", model.getValue(m_xpath + "/ptchiofg"));
    model.setAttribute("MRD_GRHistory.ptchchrt", model.getValue(m_xpath + "/ptchchrt"));
    model.setAttribute("MRD_GRHistory.ptchdate", model.getValue(m_xpath + "/ptchdate").lvStripWhite());
    model.setAttribute("MRD_GRHistory.ptchdept", model.getValue(m_xpath + "/ptchdept"));
    model.setAttribute("MRD_GRHistory.ptchdoct", model.getValue(m_xpath + "/ptchdoct"));

    var m_xfm = "/emr/girokweb/xfm/MRD_GRHistory.xfm";
    TFshowModal(null, null, "1", m_xfm, "-", 1024+5, 700+15, 0, 0, false, false);
}

/**
 * @group  :
 * @ver    : 2009-10-16 오후 04:51
 * @by     : 김상준 (한양정보기술)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 슈퍼 유저 체크용
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function f_checkSuperUser() {
    var sUserid     = TFGetSessionInfo("userid");
    var sUserdeptcd = TFGetSessionInfo("userdeptcd");

    // 지정된 사용자이거나 의료정보팀 소속일때만
    if (sUserid == "20040036" || sUserid == "29990030" || sUserdeptcd == "000010") {
        model.visible("button_history","true");
    }
}

/**
 * @group  :
 * @ver    : 2010-01-06 오후 05:25
 * @by     : 김상준 (한양정보기술)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 영양 정보 조회
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function f_SelectNutri(pSortkind) {
    var m_level = "";

    if (!fSelect()) return false;  // 등록번호 Validation 체크

    model.resetInstanceNode("/root/SendData");  //인자로 들어간 인스턴스 path의 첫번째 depth의 자식 노드들까지의 값을 지우고 두번째 depth의 자식 노드들은 아예 노드 자체를 삭제
    model.setValue("/root/SendData/Mode",  "MRR_reqGetOancnutrm");                              // Action Method
    model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1"));   // 등록번호
    model.setValue("/root/SendData/Data2", model.getValue("/root/MainData/condition/frdt"));    // 시작일자
    model.setValue("/root/SendData/Data3", model.getValue("/root/MainData/condition/todt"));    // 종료일자
    model.setValue("/root/SendData/Data4", model.getValue("/root/HideData/condition/showNST")); // NST 기록보기
    model.setValue("/root/SendData/Data5", pSortkind);                                          // 일자별, 그룹별...

    if (model.submitInstance("MRR_reqGetOancnutrm")) {                  // 영양정보검색조회
        if (model.getvalue("/root/MainData/oancnutrm/levl") == "") {    // 조회실패
            fReset(); //화면정리
            return false;
        } else {
            TFSetMessage("/root/MainData/oancnutrm");
        }
    } else {
        fReset(); //화면정리
        return false;
    }

    model.gridrebuild("grid_nutri");

    var iRow = TFGetGridCount("grid_nutri");

    if (iRow > 0) {
        var sTree = 0;
        var fg = model.vsGrid("grid_nutri");
        for (var i = fg.FixedRows; i < fg.Rows; i++) {
            m_level = model.getValue("/root/MainData/oancnutrm[" + i + "]/levl");

            if (m_level == "2") {
                var sPtdt = model.getValue("/root/MainData/oancnutrm[" + i + "]/ptdt");

                if (sPtdt.substr(8, 8) == "29991231") { // 재원 중일 경우
                    TFGridFontColor("grid_nutri", i, 0, i, 1, 1, 255, 0, 255);
                } else {
                    TFGridFontColor("grid_nutri", i, 0, i, 1, 1, 0, 0, 255);
                }
            } else if (m_level == "3" || m_level == "4") {
                if (model.getValue("/root/MainData/oancnutrm[" + i + "]/giroktype") == "EMR") {
                    TFGridFontColor("grid_nutri", i, 0, i, 1, 1, 0, 128, 128);
                } else {
                    TFGridFontColor("grid_nutri", i, 0, i, 1, 1, 0, 0, 255);
                }
            }

            fg.IsSubtotal(i) = true;
            fg.RowOutlineLevel(i) = m_level;
        }

        // 그리드의 트리 레벨을 다 지정하기 전에 접으면 에러남. 레벨 지정 완료된 후 한번에 접기
        for (var i = fg.FixedRows; i < fg.Rows; i++) {
            m_level = model.getValue("/root/MainData/oancnutrm[" + i + "]/levl");

            if (m_level == "1" || m_level == "2") {
                fg.IsCollapsed(i) = 1;
            } else {
                fg.IsCollapsed(i) = 2;
            }
        }

        fg.PicturesOver = false;    //글자와 이미지를 겹치지 않게.
        fg.OutlineBar = 4;          //+ , - , 사각형 안보이게 하려면 0 으로 세팅
        fg.OutlineCol = 0;
        fg.Row = 0;
    }

    fWordWrap("grid_nutri");
    return true;
}

/**
 * @group  :
 * @ver    : 2010-01-07 오후 04:25
 * @by     : 김상준
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 영양정보내용조회
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fNutriSelect_getLoadURL() {
    var iRow = TFGetGridPos("grid_nutri");

    if (iRow < 1) {
        return;
    }

    if (!fSelect()) return false;  // 등록번호 Validation 체크

    var pNode = "/root/MainData/oancnutrm[" + iRow + "]/" ;
    var m_sortkind = "";

    if (model.getGroupBtnStatus("button_nutri_date") == 1) {
        m_sortkind = "D";
    } else if (model.getGroupBtnStatus("button_nutri_group") == 1) {
        m_sortkind = "G";
    } else if (model.getGroupBtnStatus("button_nutri_sujin") == 1) {
        m_sortkind = "S";
    } else {
        return;
    }

    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");
    if (emr_Right) {
        emr_Right.removeNodeSet("/root/SubData/viewinfo");
        model.removeNodeset("/root/SubData");
        emr_Right.gridRebuild("grid_view");
    } else {
        return;
    }

    model.resetInstanceNode("/root/SendData");  // 이전의 서버 호출에 사용된 정보 지움
    model.removeNodeset("/root/HideData/savedatas");

    model.setValue("/root/SendData/Mode",  "reqGetNutriView");                                  // Action Method
    model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1"));   // 등록번호
    model.setValue("/root/SendData/Data2", model.getValue("/root/MainData/condition/frdt"));    // 시작일자
    model.setValue("/root/SendData/Data3", model.getValue("/root/MainData/condition/todt"));    // 종료일자

    model.setValue("/root/SendData/Data4", model.getValue(pNode + "giroktype"));    // 기록의 종류 (영양+EMR, 영양 only, EMR only)
    model.setValue("/root/SendData/Data5", model.getValue(pNode + "kind"));         // 영양정보 종류
    model.setValue("/root/SendData/Data6", model.getValue(pNode + "ptdt"));         // 내원일자
    model.setValue("/root/SendData/Data7", model.getValue(pNode + "datekey"));      // 기록일자
    model.setValue("/root/SendData/Data8", model.getValue(pNode + "ordcd"));        // 처방코드

    model.setValue("/root/SendData/Data9",  model.getValue("/root/HideData/condition/showNST"));    // NST 기록보기
    model.setValue("/root/SendData/Data10", m_sortkind);                                            // 정렬방식

    if (model.submitInstance("reqGetNutriView")) {      // 영양정보 내용 조회
        fMakeGridForNutri(iRow);
    } else {
        return false;
    }
}

/**
 * @group  :
 * @ver    : 2010-01-07 오후 05:38
 * @by     : 김상준
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 영양정보내용 그리드에 표시
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fMakeGridForNutri(pRow) {
    var emr_Right = TFGetModel("body_girokgeomsaek_Right","Y");

    if (!emr_Right) {
        return;
    }

    var m_gPath = "/root/MainData/oancnutrm" ;                  // 그리드 노드
    var m_nPath = "/root/HideData/savedatas/nutri_savedata";    // 영양기록 노드
    var m_ePath = "/root/HideData/savedatas/emr_savedata";      // EMR(타과회신) 노드
    var m_tPath = "/root/HideData/savedata";                    // EMR 데이터 한줄 복사용

    var m_gCnt  = getNodesetCnt(m_gPath);
    var m_nCnt  = getNodesetCnt(m_nPath);
    var m_eCnt  = getNodesetCnt(m_ePath);

    var fg = emr_Right.vsGrid("grid_view");

    var sTempTab = "　"; //*** 특수문자 ㄱ 1번의 공백으로 사용하였음...

    var m_lv1;
    var m_lv2;
    var m_gtype;

    var m_nCus = 0;
    var m_eCus = 0;

    var m_title;
    var m_kind;
    var m_updtnm;
    var m_updtdt;
    var m_soap;
    var i;
    var j;

    if (m_nCnt < 1 && m_eCnt < 1) {
        model.alert("조회된 데이터가 없습니다...");
        return;
    }

    iRowCount = 1;      // 출력 위치 초기화
    m_lv1 = model.getValue(m_gPath + "[" + pRow + "]/levl");

    for (i = pRow; i <= m_gCnt; i++) {
        m_lv2 = model.getValue(m_gPath + "[" + i + "]/levl");

        if (i > pRow && m_lv1 >= m_lv2) {
            break;
        }

        if (m_lv2 == "4") {     // 말단 노드일 때만
            m_gtype = model.getValue(m_gPath + "[" + i + "]/giroktype");

            if (m_gtype == "NUTRI") {   // 영양기록일 때
                m_nCus++;
                m_kind   = model.getValue(m_nPath + "[" + m_nCus + "]/kindnm");
                m_execdd = model.getValue(m_nPath + "[" + m_nCus + "]/execdd");
                m_updtnm = model.getValue(m_nPath + "[" + m_nCus + "]/lastupdtnm");
                m_updtdt = model.getValue(m_nPath + "[" + m_nCus + "]/lastupdtdt");
                m_soap   = model.getValue(m_nPath + "[" + m_nCus + "]/soap");

                m_title = m_kind + "  / " + m_updtnm + "  " + m_execdd.substr(0, 4) + "-" + m_execdd.substr(4, 2) + "-" + m_execdd.substr(6, 2)
                       + "  수정>" + m_updtdt.substr(0, 4) + "-" + m_updtdt.substr(4, 2) + "-" + m_updtdt.substr(6, 2) + " " + m_updtdt.substr(8, 2) + ":" +  m_updtdt.substr(10, 2);

                emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]");
                emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/data");
                emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/path");
                emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/imgpath");
                emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/imgexvg");
                emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/type");
                emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/size");
                emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/InfoCode");

                emr_Right.setValue("/root/SubData/viewinfo[" + iRowCount + "]/data", m_title);
                emr_Right.setValue("/root/SubData/viewinfo[" + iRowCount + "]/path", 0);

                iRowCount++;

                var m_soapArray = m_soap.split("\n");

                for (j = 0; j < m_soapArray.length; j++) {
                    emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]");
                    emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/data");
                    emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/path");
                    emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/imgpath");
                    emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/imgexvg");
                    emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/type");
                    emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/size");
                    emr_Right.makenode("/root/SubData/viewinfo[" + iRowCount + "]/InfoCode");

                    emr_Right.setValue("/root/SubData/viewinfo[" + iRowCount + "]/data", sTempTab + m_soapArray[j]);
                    emr_Right.setValue("/root/SubData/viewinfo[" + iRowCount + "]/path", 2);
                    emr_Right.setValue("/root/SubData/viewinfo[" + iRowCount + "]/type", "T");

                    iRowCount++;
                }
            } else if (m_gtype == "EMR") {   // 타과회신 기록 차트일 때
                m_eCus++;

                model.removeNodeset(m_tPath);
                model.makenode(m_tPath);

                copyNode(m_ePath + "[" + m_eCus + "]", m_tPath);
                f_ViewingDetailProcess("CDC0000009", fg, "T");
            } else {
                alert(i + "번째 줄 - 알 수 없는 형식의 기록입니다");
                return;
            }
        }
    }

    if (m_nCnt != m_nCus || m_eCnt != m_eCus) {
        alert("출력되지 않은 데이터가 남아 있습니다\n\n의료정보팀에 환자번호와 함께 문의하세요");
    }

    emr_Right.gridRebuild("grid_view");
    fMakeImage_girokView(fg, "T");

    fg.WordWrap = true;
    fg.AutoSizeMode = 1;
    fg.AutoSize(0, fg.Cols -1);

    fg.Row = 0;
}

/**
 * @group  :
 * @ver    : 2010-01-11 오후 05:09
 * @by     : 김상준
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : NST 기록보기 체크박스 변경시
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function fOnChangeShowNST() {
    if (model.getGroupBtnStatus("button_nutri_date") == 1) {
        f_SetNavigatorPathInfo("영양", "일자별");
        f_SelectNutri("D");
    } else if (model.getGroupBtnStatus("button_nutri_group") == 1) {
        f_SetNavigatorPathInfo("영양", "그룹별");
        f_SelectNutri("G");
    } else if (model.getGroupBtnStatus("button_nutri_sujin") == 1) {
        f_SetNavigatorPathInfo("영양", "수진별");
        f_SelectNutri("S");
    } else {
        return;
    }
}

/**
 * @group  :
 * @ver    : 2010-01-27 오후 04:21
 * @by     : 김상준 (한양정보기술)
 * @---------------------------------------------------
 * @type   : function
 * @access : public
 * @desc   : 영양기록 버튼 보기 (나중에 삭제)
 * @param  :
 * @return :
 * @---------------------------------------------------
 */
function f_viewBtnNutri() {
    var sUserid     = TFGetSessionInfo("userid");
    var sUserdeptcd = TFGetSessionInfo("userdeptcd");

    // 지정된 사용자이거나 의료정보팀 소속일때만
    if (sUserid == "20040036" || sUserid == "29990030" || sUserdeptcd == "000010" || sUserdeptcd == "000156") {
        model.visible("button_nutri","true");
    }
}

// 특정인 진료정보보호 권한 점검(1) 2012.09 sopi
// 신경정신과기록 검색권한 조회 (2)
function fGetPSYGRCheck()
{
	model.trace("[KIS] fGetPSYGRCheck 1");
    // 특정인 진료정보보호 권한 점검
    model.resetInstanceNode("/root/SendData");
    model.makeValue("/root/SendData/Mode", "reqChkPSYJoHoi");
    model.makeValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1")); // PID
	model.trace("[KIS] fGetPSYGRCheck 2");
	  if(TFGetSessionInfo("userid") != ""){
     		model.makeValue("/root/SendData/Data2", TFGetSessionInfo("userid"));
	  } else {
    		model.makeValue("/root/SendData/Data2", model.getAttribute("sMRR_GRGeomSaek_userid"));
	  }
	  model.makeValue("/root/SendData/Data3", "S"); // 점검구분

	model.trace("[KIS] fGetPSYGRCheck 3");
    if (model.submitInstance("ChkTJIJoHoi")) {
    	var sPsycheck = model.getValue("root/MainData/limit/tjicheck");
    	if ( sPsycheck == "SY" ) {
    		TFGetMsgBox(-1, "[진료정보보호환자] 조회시 각별한 주의를 바랍니다!", "확인", "I", "OK");
      } else if ( sPsycheck == "SN" ) {
      	TFGetMsgBox(-1, "[진료정보보호환자] 조회 권한이 없습니다. 권한을 확인해 주시길 바랍니다!"
      	                + "\r\n\r\n문의 : 환자 주치의 OR 의무기록팀(Tel:5973)"
						+ "\r\n\r\n☞ 주치의 등록방법 : 진료대상리스트에서 해당환자 우측클릭 -> 주치의 등록", "확인", "I", "OK");

		model.enable("button_date", "false");
		model.enable("button_dept", "false");
		model.enable("button_form", "false");
		model.enable("button_nurse", "false");
		model.enable("button_order1", "false");
		model.enable("button_order2", "false");
		model.enable("button_rslt", "false");
		model.enable("button_pm", "false");
		model.enable("button_acc", "false");
		model.enable("button_ocr", "false");
		model.enable("button_drug", "false");
		model.enable("button_copy", "false");
		model.enable("button_drug2", "false");
		model.enable("button6", "false");
		model.enable("button_date_lt", "false");
		model.enable("button8", "false");
		model.enable("button_nutri", "false");

      	return "N";
      }
    }
	
	model.trace("[KIS] fGetPSYGRCheck 4");
    // 신경정신과기록 검색권한 점검
    model.makeValue("/root/SendData/Data3", "-"); // 점검구분

    if (model.submitInstance("ChkPSYJoHoi")) {}
    model.refresh();
	model.trace("[KIS] fGetPSYGRCheck 5");	
    return "Y"
}

// 공통팀 개발지원 BY NEWHOPE, 2011.04.22
function fOCS_TotalEMRView2(pPID){
    model.setAttribute("sMRR_GRGeomSaek_parm", "");
    model.setAttribute("sMRR_GRGeomSaek_parm", pPID);

	pPID = "GRGeomSaek▦" + pPID;
	model.setAttribute("MRR_GRGeomSaek_param", pPID);

    var m_ver = model.getBranchName();
    if(m_ver == "3.0_KunDae"){
        var sModel = TFGetModel("OPM_JeJeungMyeongGwanRi", "N");
    } else {
    	  var sModel = TFGetModel("MainView!OPM_JeJeungMyeongGwanRi", "N");
    }

    if (sModel != null) {
		sModel.javascript.fInitJejeungMyoung();
    } else {
        model.uiActivate();
        var pModel = model.getModel("MainView");
        pModel.showModeless("OPM_JeJeungMyeongGwanRi", "/pam/weonmuweb/xfm/OPM_JeJeungMyeongGwanRi.xfm", 50, 50, 1220, 800, false, false);
    }
}

/* 타병원 진료가 존재하는지 조회 */
function fGetOtherHospCheck(){
	model.resetInstanceNode("/root/SendData");

    model.setValue("/root/SendData/Mode", "reqOtherHospCheck");
    model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1")); // PID
    //model.setValue("/root/SendData/Data2", model.getValue("/root/HideData/condition/ptchptdt")); // 진료일

    if (model.submitInstance("ChkOtherHospJoHoi")) {}


    model.refresh();
}

/* 타병원 조회 버튼 visible과 존재할 경우 붉은색 버튼 처리 */
function f_AlertOtherHospBtn() {
	var userdeptcd = TFGetSessionInfo("userdeptcd");
	var userid = TFGetSessionInfo("userid");
	var uri = model.getURI();

	var pos = uri.indexOf("/xfm/") + 5;
	var pos2 = uri.indexOf(".xfm", pos);
	var ip = uri.substring(pos, pos2);

	//if(userdeptcd == "000109"){ //적정진료지원팀일 경우에만 타병원 진료조회 버튼이 visible
	//if(userid == "29990030" || userdeptcd == "000109"){
	if (ip != "SVC_PACS_GRGeomSaek") {
		model.visible("button_th", "true");
		model.clearInterval(920);
		var m_obj = model.control("button_th");
		var m_OtherHospCheck = model.getValue("/root/MainData/limit/ohcheck");
		//alert(g_CheckChangeBtn);

		if (m_OtherHospCheck != "N") {
			//m_obj.img = "../../../com/commonweb/images/geomsaek2_o.gif";
			m_obj.style.backgroundColor = "#ffcccc";
			m_obj.hint = "타병원 진료 기록이 존재합니다";
			m_obj.refresh();

			//model.setinterval(920, "f_AlertFreeNoteBtn()", 1000);
		} else {
			model.clearInterval(920);
			//m_obj.img = "../../../com/commonweb/images/geomsaek2.gif";
			m_obj.style.backgroundColor = "#dddddd";
			m_obj.hint = "타병원 진료 기록 조회";
			m_obj.refresh();

			//g_CheckChangeBtn = 0;
		}
	}
	//}
}

/* 타병원 진료검색 화면 호출 */
function fEMR_goTHJR() {
	//model.uiActivate();
	//var deptcd = TFGetSessionInfo("userdeptcd");
	//TFSetGlobalAttribute("Ganho_OCR", deptcd);
	model.setAttribute("sOtherHosp_search", model.getValue("/root/MainData/condition/cond1"));
	var pModel = model.getModel("MainView");
    pModel.showModeless("MRR_TBJRGeomSaek", "/emr/girokweb/xfm/MRR_TBJRGeomSaek.xfm", 10, 10, 1220, 920, false, false);

	//model.showModeless("MRR_TGJRGeomSaek", "/emr/girokweb/xfm/MRR_TBJRGeomSaek.xfm", 10, 10, 1220, 950, false, false);
}

// 검사결과 의사확인 (조회포함)
function fDoctConfirm(gOption)
{
    // 내과만 적용해제 : 2012-01-10
    var usrdtcd = TFGetSessionInfo("userdeptcd");
    var occukind  = TFGetSessionInfo("occukind");

    //if (fAuthCheck(usrdtcd) == "false") return;
    if (occukind != '10') return; // 직종이 의사인 경우만 가능

	var iRow = TFGetGridPos("grid_list");
	if (iRow <= 0) return;

	var level = model.getValue("/root/MainData/hist/list[" + iRow + "]/level");
	if (level == "1") return; // 레벨 1에서는 의사확인 불가능

	var fg = model.vsGrid("grid_list");
	var header = "m▦pid▦ordstat▦suppdept▦rsltdd▦rsltno▦lastupdtid▩";
	var iOption = model.getValue("/root/HideData/option");
	var senddata = "";

	var ordstat = model.getValue("/root/MainData/hist/list[" + iRow + "]/ordstat");
	var suppdept = model.getValue("/root/MainData/hist/list[" + iRow + "]/suppdept");
	var rsltdd = "";
	if (iOption == "1") rsltdd = model.getValue("/root/MainData/hist/list[" + iRow + "]/rsltdd");
	else rsltdd = model.getValue("/root/MainData/hist/list[" + iRow + "]/execdd");
	var orsltdd = model.getValue("/root/MainData/hist/list[" + iRow + "]/orsltdd");
	var rsltno = model.getValue("/root/MainData/hist/list[" + iRow + "]/rsltno");
	var ordcdnm = model.getValue("/root/MainData/hist/list[" + iRow + "]/ordcdnm");
    var lastupdtid = model.getValue("/root/MainData/hist/list[" + iRow + "]/cnfmflag");
	//var lastupdtid = "29990030"; // 테스트

    var vCtrl1 = model.control("button_rslt_date");
    var vCtrl2 = model.control("button_rslt_dept");
    var vCtrl3 = model.control("button_rslt_silp");
	var vCtrl4 = model.control("button_rslt_date_2nd");
    var gSelectedTab = "";

    if (vCtrl1.selected == true) gSelectedTab = "1";
    if (vCtrl2.selected == true) gSelectedTab = "2";
    if (vCtrl3.selected == true) gSelectedTab = "3";
	if (vCtrl4.selected == true) gSelectedTab = "6";

	// H(중간,예비보고), I(최종보고), J(수정보고)인 경우만 의사확인 가능
	// 모든 의사가 검사결과 확인가능(사용자 선택사항)
	var tmp_ordstat = "";
	var tmp_suppdept = "";
	var tmp_rsltdd = "";
	var tmp_orsltdd = "";
	var tmp_rsltno = "";
	var tmp_lastupdtid = "";

	if (level == "2") {
		if (gSelectedTab == "1" || gSelectedTab == "2" || gSelectedTab == "6") {
			for (var i = 1; i < fg.rows; i++) {
				if (model.getValue("/root/MainData/hist/list[" + i + "]/level") != "3") continue;

				tmp_ordstat = model.getValue("/root/MainData/hist/list[" + i + "]/ordstat");
				tmp_suppdept = model.getValue("/root/MainData/hist/list[" + i + "]/suppdept");
				if (iOption == "1") tmp_rsltdd = model.getValue("/root/MainData/hist/list[" + i + "]/rsltdd");
				else tmp_rsltdd = model.getValue("/root/MainData/hist/list[" + i + "]/execdd");
                tmp_orsltdd = model.getValue("/root/MainData/hist/list[" + i + "]/orsltdd");
				tmp_rsltno = model.getValue("/root/MainData/hist/list[" + i + "]/rsltno");
				tmp_lastupdtid = model.getValue("/root/MainData/hist/list[" + i + "]/cnfmflag");

				if (tmp_lastupdtid == "-") continue;

				if (tmp_suppdept == suppdept && tmp_rsltdd == rsltdd) {
					senddata = senddata + "i▦" + g_Pid + "▦" + tmp_ordstat + "▦" + tmp_suppdept + "▦" + tmp_orsltdd + "▦" + tmp_rsltno + "▦" + tmp_lastupdtid + "▩";
				}
			}

		} else {
			for (var i = 1; i < fg.rows; i++) {
				if (model.getValue("/root/MainData/hist/list[" + i + "]/level") != "3") continue;

				tmp_ordstat = model.getValue("/root/MainData/hist/list[" + i + "]/ordstat");
				tmp_suppdept = model.getValue("/root/MainData/hist/list[" + i + "]/suppdept");
				if (iOption == "1") tmp_rsltdd= model.getValue("/root/MainData/hist/list[" + i + "]/rsltdd");
				else tmp_rsltdd = model.getValue("/root/MainData/hist/list[" + i + "]/execdd");
                tmp_orsltdd = model.getValue("/root/MainData/hist/list[" + i + "]/orsltdd");
				tmp_rsltno = model.getValue("/root/MainData/hist/list[" + i + "]/rsltno");
				tmp_lastupdtid = model.getValue("/root/MainData/hist/list[" + i + "]/cnfmflag");
				tmp_ordcdnm = model.getValue("/root/MainData/hist/list[" + i + "]/ordcdnm");

				if (tmp_lastupdtid == "-") continue;

				if (tmp_suppdept == suppdept && tmp_rsltdd == ordcdnm) {
					senddata = senddata + "i▦" + g_Pid + "▦" + tmp_ordstat + "▦" + tmp_suppdept + "▦" + tmp_orsltdd + "▦" + tmp_rsltno + "▦" + tmp_lastupdtid + "▩";
				}
			}
		}

	} else {
		if (lastupdtid != "-") {
			senddata = "i▦" + g_Pid + "▦" + ordstat + "▦" + suppdept + "▦" + orsltdd + "▦" + rsltno + "▦" + lastupdtid + "▩";
		}
	}

	if (senddata == "") return;

	// 의사 확인창 추가
	/*
	var cfmessage = "선택하신 검사결과를 확인하시겠습니까? (하위포함)\n\n'예(Y)'를 클릭하시면 검사결과 확인이력이 발생됩니다.";
	if (TFGetMsgBox(-1, cfmessage, "확인", "Q", "yn") == "7") { // yes = 6, no = 7
		return;
    }
    */

	// 의사확인 저장
	senddata = header + senddata;
	model.setValue("/root/SendData/Data1", senddata);
    //model.setValue("/root/HideData/option", gOption);
    //model.setValue("/root/HideData/gubn", gSelectedTab);
	if (gSelectedTab == "1" || gSelectedTab == "2"){
		model.setValue("/root/HideData/option", gOption);
		model.setValue("/root/HideData/gubn", gSelectedTab);
	} else {
		model.setValue("/root/HideData/option", gOption);
		model.setValue("/root/HideData/gubn", "1");
	}

    var gPid = model.getValue("/root/MainData/condition/cond1");
    model.setValue("/root/HideData/pid", gPid);

    //조회 기간이 2년을 넘길 경우 메세지 호출 2007-07-04 1:56오후 - 김태범
    var sGsFromdd = model.getValue("/root/MainData/condition/frdt");
    var sGsTodd   = model.getValue("/root/MainData/condition/todt");
    var sDayCount = lvGetDateTerm(sGsFromdd,sGsTodd);

    if (sDayCount > 735) {
        model.alert("조회 기간은 2년을 넘길 수 없습니다. 조회기간을 변경 하시기 바랍니다.");
        model.setFocus("input_frdt");
        return false;
    } else {
        //조회 시작일 종료일 세팅
        model.setValue("/root/HideData/fromdd", sGsFromdd);
        model.setValue("/root/HideData/todd"  , sGsTodd);
    }

    var sGlucose = model.getValue("/root/HideData/glucoseyn");
    if (sGlucose != "Y") model.setValue("/root/HideData/glucoseyn", "N");
    copyNode("/root/HideData", "/root/SendData/Data");
	fSetFunction("reqSetDoctConfirm", "setDoctConfirm", "OAC_GyeolGwaJoHoi_View1");

    fRequestAction("reqAction", "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "/root/SendData", "/root/MainData/hist");
    model.resetInstanceNode("/root/SendData");
    TFSetMessage("/root/MainData");

    var row = TFGetGridCount("grid_list");

    if (row > 0) {
        fMakeTree("grid_list", 2);

        // 내과만 적용해제 : 2012-01-10
        //if (fAuthCheck(usrdtcd) == "true") {
    		// 의사확인 색상변경 - 붉은색
    		var fg = model.vsGrid("grid_list");
    		for (var i = 1; i < fg.rows;i++) {
				if (parseInt(fg.TextMatrix(i, 5)) > 20140831) {//20120111 -> 20140831 인증관련 김윤숙 샘 요청.
                   if ((fg.TextMatrix(i, 10) != "-") || (fg.TextMatrix(i, 2) == "-")) { //
                        TFGridFontColor("grid_list", i, 0, i, 0, 1, 255, 0, 0);
					}
				}
    		}
    	//}
    }
}

// 권한 임시
function fAuthCheck(g_userdeptcd)
{
    var tDeptcd = new Array("000187", "000192", "000204", "000224", "000235", "000240", "000245", "000250", "000255", "000814");
    var sFlag = "false";

    for (var i = 0 ; i < tDeptcd.length ; i++) {
        if (g_userdeptcd == tDeptcd[i]) {
        	sFlag = "true";
          break;
        }
    }
    return sFlag;
}

//연구기록/검색 화면 바로 이동
function fEMR_goYGGR(){
	var temp_pid = model.getValue("/root/MainData/condition/cond1");
	model.setAttribute("temp_pid", temp_pid);

	model.uiActivate();
	//var pModel = model.getModel("MainView");
    model.showModeless("MRD_YGGRJoHoi_Modal", "/emr/girokweb/xfm/MRD_YGGRJoHoi_Modal.xfm", 150, 50, 1220, 798, false, false);

}

//심평원 화면 링크
function fEMR_goSPW(){
	model.showModal("text/html", "http://biz.hira.or.kr/index.jsp?SSOServiceType=C&LinkMenuId=MP00000141&pgmid=HIRAA030129000000", 50, 200, 1220, 790 , false, false);

}


//2014.01.16 jung12 검사텝 응급판독로그 생성
function fGetReadLogCheck(pPid){
	model.resetInstanceNode("/root/MainData/readlog");

	model.setValue("/root/HideData/pid", pPid);
	copyNode("/root/HideData", "/root/SendData/Data");

	fSetFunction("reqGetReadLogCheck", "getReadLogCheck", "OAC_GyeolGwaJoHoi_View8"); 
	fRequestAction("reqAction", "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "/root/SendData", "/root/MainData/readlog"); 

	return getNodesetCnt("root/MainData/readlog/list"); 
}



//2014.01.16 jung12 검사텝 응급판독로그 기록
function fSetReadLog(){	
	var emr_Left = fEMR_getGRGeomSaekModel();	
	
	if (TFGetSessionInfo("occukind") != '10') return;	//의사직종만
	
	var gPid = emr_Left.getValue("/root/MainData/condition/cond1");// 환자 번호
	var gUserId = TFGetSessionInfo("userid");
	
	var fg = model.vsGrid("grid_result");
	
	if (fg.Rows <= 1) return;	//데이터가 있을때만
	
	var oldRsltdd = "";
	var rsltdd = "";
	
	var oldRsltno = "";
	var rsltno = "";
	
	var oldSuppdept = "";
	var suppdept = "";
	
	var pacsno = "";
	
	var senddata = "";
	var find ;
	
	var header = "m▦pid▦suppdept▦rsltdd▦rsltno▦readid▦pacsno▩";

	for(var i = fg.FixedRows ; i <  fg.rows; i++) {					
		suppdept = model.getValue("/root/MainData/result/list["+ i + "]/suppdept");
		rsltdd = model.getValue("/root/MainData/result/list["+ i + "]/rsltdd");
		rsltno = model.getValue("/root/MainData/result/list["+ i + "]/rsltno");
		pacsno = model.getValue("/root/MainData/result/list["+ i + "]/pacsno");
		
		if ((oldSuppdept != suppdept) || (oldRsltdd != rsltdd) || (oldRsltno != rsltno) ) {
		
			find = false;
			for (var j = 1; j <= emr_Left.javascript.getNodesetCnt("/root/MainData/readlog/list"); j++){
			
				if (	suppdept == emr_Left.getValue("/root/MainData/readlog/list[" + j + "]/suppdept") && 
					rsltdd == emr_Left.getValue("/root/MainData/readlog/list[" + j + "]/rsltdd") && 
					rsltno == emr_Left.getValue("/root/MainData/readlog/list[" + j + "]/rsltno") 
					){
					find = true;
					continue;
				}
			} 
			if (find){ 
				
				senddata = senddata + "i▦" + gPid + "▦" + suppdept + "▦" + rsltdd +  "▦" + rsltno + "▦" + gUserId + "▦" + pacsno + "▩"; 
			}
		}
		oldSuppdept = suppdept;
		oldRsltdd = rsltdd;
		oldRsltno = rsltno;
		
		
	}
	
	if (senddata != ""){
		senddata = header + senddata;
		
		emr_Left.setValue("/root/SendData/Data1", senddata);
		
		emr_Left.javascript.fSetFunction("reqSetReadLog", "setReadLog", "OAC_GyeolGwaJoHoi_View8");
		emr_Left.javascript.fRequestAction("reqAction", "/ast/geomsachiryoweb/GyeolGwaJoHoi.live", "/root/SendData", "/root/MainData/readlog");
				
		emr_Left.javascript.TFclearNodeValue("/root/SendData");
		emr_Left.javascript.TFSetMessage("/root/MainData");

	
		gReadLogCheckCnt = emr_Left.javascript.getNodesetCnt("root/MainData/readlog/list");
		model.setAttribute("MRR_GRGeomSaek_Modal_gReadLogCheckCnt", gReadLogCheckCnt); // 검사텝 클릭시 조회값 setAttribute
	}
}

// 응급실 호출의 출력권한 조회
function fChkCRER()
{
	model.trace("[KIS] fChkCRER process:0");
	if (fEMR_getGeneMode() == true) return;
	
	model.trace("[KIS] fChkCRER process:1");
    // 특정인 진료정보보호 권한 점검
    model.resetInstanceNode("/root/SendData");
    model.makeValue("/root/SendData/Mode", "reqChkCRER");
    model.makeValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1")); // PID
	  if(TFGetSessionInfo("userid") != ""){
     		model.makeValue("/root/SendData/Data2", TFGetSessionInfo("userid"));
	  } else {
    		model.makeValue("/root/SendData/Data2", model.getAttribute("sMRR_GRGeomSaek_userid"));
	  }

    // 응급실 호출의 출력권한 조회
	model.trace("[KIS] fChkCRER process:2");
	//alert(model.getValue("/root/SendData"));
	
    if (model.submitInstance("ChkCRER")) {}
	model.trace("[KIS] fChkCRER process:2-0");	
    model.refresh();
	model.trace("[KIS] fChkCRER process:2-1");
    model.setAttribute("gGiRokSinCheong_ER", model.getValue("/root/MainData/limit/ercheck"));
	model.trace("[KIS] fChkCRER process:2-2");
    if (model.getValue("/root/MainData/limit/ercheck") == "Y") {
		model.trace("[KIS] fChkCRER process:2-3");
        gGiRokSinCheong_print = "Y";
		model.setAttribute("gGiRokSinCheong_print", gGiRokSinCheong_print);
		model.trace("[KIS] fChkCRER process:2-4");
		model.enable("button7", "true"); //임상관찰
    }
	model.trace("[KIS] fChkCRER process:3");
}

//마취과 마취환자의 신경정신과 열람
function fChkANE()
{
    model.resetInstanceNode("/root/SendData");
    model.setValue("/root/SendData/Mode", "reqChkANE");
    model.setValue("/root/SendData/Data1", model.getValue("/root/MainData/condition/cond1")); // PID

    // 마취과 마취환자의 신경정신과 열람

    if (model.submitInstance("ChkANE")) {}
    model.refresh();    
}

//소아성장 그래프화면 popup
function fSOAGRAPH()
{
    model.setAttribute("sMRI_SCSoaGraphParm", "1");
    model.setAttribute("sMRI_SCSoaGraphParm1", model.getValue("/root/MainData/opmcptbsm/age_sex"));
    model.setAttribute("sMRI_SCSoaGraphParm2", model.getValue("/root/MainData/condition/cond1"));
    model.setAttribute("sMRI_SCSoaGraphParm3", model.getValue("/root/MainData/opmcptbsm/name"));
    model.setAttribute("sMRI_SCSoaGraphParm4", model.getValue("/root/MainData/opmcptbsm/regno1") + model.getValue("/root/MainData/opmcptbsm/regno2"));
        
    var soa_Popup = model.getmodel("MRI_SOAGRAPH");
    if (soa_Popup == null) {
        model.showModeless("MRI_SOAGRAPH", "/emr/girokweb/xfm/MRI_SOAGRAPH.xfm", 10, 10, 665, 927, false, false);
    } else {
        soa_Popup.javascript.fInitialize();
    }
    
    model.setAttribute("sMRI_SCSoaGraphParm", "");
    model.setAttribute("sMRI_SCSoaGraphParm1", "");
    model.setAttribute("sMRI_SCSoaGraphParm2", "");
    model.setAttribute("sMRI_SCSoaGraphParm3", "");
    model.setAttribute("sMRI_SCSoaGraphParm4", "");
}


/**
 * 제      목 : fClose
 * 설      명 : 진료쪽에서 다른 환자선택시에 기오픈된 전체기록검색이 있으면 자동 close 처리용
 * 작  성 자 : 
 * 작  성 일 : 2014.03.04
 */
function fClose() {
    //model.closeBrowser();
    TFCloseBrowser()
    model.doaction();
}

/**
 * 제      목 : fCallTopinfoReload
 * 설      명 : 진료쪽에서 다른 환자선택시에 기오픈된 전체기록검색 환자 Reload
 * 작  성 일 : 2017.09.06
 */
function fCallTopinfoReload() {
	model.setValue("/root/MainData/condition/cond1", TFGetTopInfo("HJID"));
    fClicked("button_query");
}

function f_dumpCopyNode(strSrc, strDest) {
    if(isHugeGrid(strSrc) == true) {
        copyNodeFromHugeGrid(strSrc, strDest);
        return;
    }

    if(findNode(strSrc) == null) return;

    if(findNode(strDest) == null)
        model.makeNode(strDest);

    var insXml = model.instance();

    insXml.setProperty("SelectionNamespaces", "xmlns:my='http://www.comsquare.co.kr/example'");
    insXml.setProperty("SelectionLanguage", "XPath");

    var destNode = insXml.selectSingleNode(strDest);
    var destList = destNode.childNodes;

    var srcNode = insXml.selectSingleNode(strSrc);
    var srcList = srcNode.childNodes;

    /*for (var iCnt = 0; iCnt < destList.length; iCnt++) {
        destNode.removeChild(destList.item(0));
    }*/

    var clone_node = null;
    for (var iCnt = 0; iCnt < srcList.length; iCnt++) {
        var node   = srcList.item(iCnt);
        clone_node = node.cloneNode(true);

        destNode.appendChild(clone_node);
    }
}