#-*- coding: utf-8 -*-
import re
import stringManager as sm

# TODO 추후에 서브미션 관련해서 필요하다면 마저 구현
# 읽어온 xfm 파일에서 submitinfo 관련 데이터 뽑아옴
# <submitinfo> 태그 찾아 각각의 속성들을 딕셔너리에 넣어준다.
# <submitInfo id="reqGetInitData" ref="root/SendData" replace="instance" mediaType="application/x-www-form-urlencoded" method="post" action="/com/commonweb/GCCodeLoader.live" resultRef="root/InitData"/>
# {
#   'reqGetInitData' : {
#       'id' : 'reqGetInitData',
#       'ref' : 'root/SendData'
#       'replace' : 'instance'
#       'mediaType' : 'application/x-www-form-urlencoded'
#       'method' : 'post'
#       'action' : '/com/commonweb/GCCodeLoader.live'
#       'resultRef' : 'root/InitData'
#   },
#   ...
# }
# def getSubmitinfoDictList(codeList):
#     submitInfoDict = {}
#     for tfCodeLine in codeList:
#         if tfCodeLine.find('<submitInfo'):
#             # 1:id, 2:ref, 3:replace, 4:mediaType, 5:method, 6:action, 7:resultRef
#             submitInfoAttrList = tfCodeLine.split(' ')

#             for submitInfoAttr in submitInfoAttrList:
#                 submitAttrName = submitInfoAttr.split("=")[0]
#                 submitAttrValue = submitInfoAttr.split("=")[1]


# 함수이름+functionNameReg => 함수이름(인자1 ...) 까지 뽑아주는 정규표현식
functionNameReg = '\\(.+?\\)'


def gridRebuild2Refresh(codeLine):
    '''    
    model.gridRebuild(gridId) -> gridId.refresh()
    '''
    parameter = sm.strInBracket(codeLine)
    if sm.isString(parameter):
        # 매개변수가 문자열인 경우
        return re.sub(f'model.grid[R|r]ebuild{functionNameReg}', f'{sm.removeQuotes(parameter.strip())}.refresh()', codeLine)
    else:
        # 매개변수가 변수인 경우
        return re.sub(f'model.grid[R|r]ebuild{functionNameReg}', f'{parameter}.refresh()', codeLine)

def vsGrid2Controls(codeLine):
    '''
    model.vsGrid(gridId) -> document.controls(gridId) gridId를 문자열로 넣어줘야함
    '''
    return codeLine.replace('model.vsGrid', 'document.controls')

def enable2Disabled(codeLine):
    '''
    model.enable(<"contrilId">, [true|false]) -> <controlId>.disabled = [true|false]
    '''
    # 인자 가져와서 첫번째 인자.disabled = 두번쨰 인자
    argumentList = sm.getArgumentList(codeLine, 'model.enable')
    controlId = argumentList[0].strip()
    controlValue = argumentList[1].strip()

    returnStr = controlId if not sm.isString(controlId) else sm.removeQuotes(controlId)

    returnStr += ".disabled = " 
    returnStr  += ("true" if controlValue.upper() == "FALSE" else "false")
    return re.sub(f'model.enable{functionNameReg}', returnStr, codeLine)

def visible2Visible(codeLine):
    '''
    model.visible(ctrlID, [true|false]) -> ctrlOb.visible = [true|false]
    '''
    parameterList = sm.getArgumentList(codeLine, 'model.visible')
    controlId = parameterList[0].strip()
    controlValue = parameterList[1].strip()

    removeModelStr = codeLine.replace('model', f'document.controls({controlId})')
    replaceVisibleStr = re.sub('.visible\\(.*', f'.visible = {controlValue};', removeModelStr)

    return replaceVisibleStr

def submitInstance2Send(codeList):
    '''
    model.submitInstance("서브밋아이디") -> model.send("서브밋아이디")
    '''
    return codeList.replace('submitInstance', 'send')

def copyNode2copyNode(codeLine):
    '''
    model.copyNode(param1, param2) -> model.copyNode(param2 -> param1)
    '''
    #파라미터의 순서 바꾼다
    parameterList = sm.strInBracket(codeLine).split(',')
    param1 = parameterList[0].strip()
    param2 = parameterList[1].strip()

    return re.sub(f'model.copyNode{functionNameReg}', f"model.copyNode({param2}, {param1})", codeLine)


def lvTrim2replace(codeLine):
    '''
    .lvTrim() -> .replace(/\s/gi, "") 로 변경
    '''
    return codeLine.replace('.lvTrim()', '.replace(/\\s/gi, "")')


def control2Controls(codeLine):
    '''
    model.control("<controlID>") -> document.contorls("<controlID>")
    '''
    return codeLine.replace('model.control', 'document.controls')


def ERP_style2AttributeWidth(codeLine):
    '''
    .style.width -> .attribute("width")
    '''
    return codeLine.replace('.style.width', '.attribute("width")')


def ERP_style2AttributeHeight(codeLine):
    '''
    .style.height -> .attribute("height")
    '''
    return codeLine.replace('.style.height', '.attribute("height")')


def ERP_style2AttributeLeft(codeLine):
    '''
    .style.left -> .attribute("left")
    '''
    return codeLine.replace('.style.left', '.attribute("left")')


def ERP_style2AttributeTop(codeLine):
    '''
    .style.top -> .attribute("top")
    '''
    return codeLine.replace('.style.top', '.attribute("top")')


def ERP_style2AttributeBackgroundColor(codeLine):
    '''
    .style.backgroundColor -> .attribute("background-color")
    '''
    return codeLine.replace('.style.backgroundColor', '.attribute("background-color")')


def date2Moment(codeLine):
    '''
    new Date() -> moment()
    '''
    return codeLine.replace('new Date(', 'moment(')

def getYear2get(codeLine):
    '''
    .getYear() -> moment().get('year')
    '''
    return codeLine.replace('.getYear()', ".get('year')")

def getMonth2get(codeLine):
    '''
    .getMonth() -> moment().get('month')
    '''
    return codeLine.replace('.getMonth()', ".get('month')")

def getDate2get(codeLine):
    '''
    .getDate() -> moment().get('date')
    '''
    return codeLine.replace('.getDate()', ".get('date')")




# LiveTF.js 함수 대체
def msgBox2TF5msgBox(codeLine):
    '''
    TFGetMsgBox() -> TF.window.getMsgBox()
    '''
    return codeLine.replace('TFGetMsgBox(', 'TF.window.getMsgBox(')
def getSessionInfo2TF5getSessionInfo(codeLine):
    '''
    TFGetSessionInfo() -> TF.com.session.getSessionInfo()
    '''
    return codeLine.replace('TFGetSessionInfo(', 'TF.com.session.getSessionInfo(')
def getGridCount2TF5DataRows(codeLine):
    '''
    TFGetGridCount() -> TF.grid.dataRows()
    인자가 문자열일경우 "" 지우고 컨트롤 ID로 직접 접근
    인자가 변수인경우 document.controls("stringId")로 컨트롤 객체 가져오는 방식으로 변환
    '''
    argumentList = sm.getArgumentList(codeLine, 'TFGetGridCount')
    ctrlId = ''
    if sm.isString(argumentList[0]):
        ctrlId = sm.removeQuotes(argumentList[0])
    else:
        ctrlId = 'document.controls(' + argumentList[0] + ')'
    return re.sub(f'TFGetGridCount{functionNameReg}', f'TF.grid.dataRows({ctrlId})', codeLine)
def setMessage2Comment(codeLine):
    '''
    TFSetMessage()함수 주석처리
    '''
    return "// " + codeLine
def getGridPos2Row(codeLine):
    '''
    TFGetGridPos(<girdID>) -> <gridID>.row
    '''
    argumentList = sm.getArgumentList(codeLine, 'TFGetGridPos')
    ctrlId = ''
    if sm.isString(argumentList[0]):
        ctrlId = sm.removeQuotes(argumentList[0])
    else:
        ctrlId = f'document.controls({argumentList[0]})'

    return re.sub(f'TFGetGridPos{functionNameReg}',f'{ctrlId}.row', codeLine)
def getGridCol2Col(codeLine):
    '''
    TFGetGridCol(<girdID>) -> <gridID>.col
    '''
    argumentList = sm.getArgumentList(codeLine, 'TFGetGridCol')
    ctrlId = ''
    if sm.isString(argumentList[0]):
        ctrlId = sm.removeQuotes(argumentList[0])
    else:
        ctrlId = f'document.controls({argumentList[0]})'

    return re.sub(f'TFGetGridCol{functionNameReg}', f'{ctrlId}.col', codeLine)


def findNode2SelectSingleNode(codeLine):
    '''
    findNode() -> TF.xml.selectSingleNode(<wid>, <xpath>)
    '''
    
    return codeLine.replace('findNode(', 'TF.xml.selectSingleNode(')

def makeNode2ModelMakeNode(codeLine):
    '''
    makeNode() -> model.makeNode()
    '''
    return codeLine.replace('makeNode(', 'model.makeNode(')


def trace2log(codeLine):
    '''
    model.trace("[KIS] body_girokgeomsaek_Right loadURI 12"); -> TF.log4js.log("DEBUG", 문자열 가져옴, "string");
    '''
    argumentList = sm.getArgumentList(codeLine, 'model.trace')
    logStr = argumentList[0]
    return re.sub(f'model.trace{functionNameReg}', f'TF.log4js.log("DEBUG", {logStr}, "string")', codeLine)
    

# datagrid 관련 함수 변환
def row2Lower(codeLine):
    '''
    <datagrid>.Row -> <datagrid>.row
    <datagrid>.Col -> <datagrid>.col
    <datagrid>.Rows -> <datagrid>.rows
    <datagrid>.Cols -> <datagrid>.cols
    <datagrid>.FixedRows -> <datagrid>.Fixedrows
    '''
    # return codeLine.replace('Row', 'Row'.lower()).replace('Col', 'Col'.lower()).replace('Rows', 'Rows'.lower()).replace('Cols', 'Cols'.lower()).replace('FixedRows', 'FixedRows'.lower())
    return codeLine.replace('.Row', '.row').replace('.Col', '.col').replace('.FixedRows', '.fixedRows')

def textMatrix2valueMatrix(codeLine):
    '''
    <gridId>.textMatrix(r,c) -> <gridId>.valueMatrix(r,c)
    '''
    return codeLine.replace('.TextMatrix(','.valueMatrix(').replace('.textMatrix(','.valueMatrix(')
